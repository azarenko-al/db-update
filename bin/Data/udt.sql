select 
object_id,
    name, 
    schema_name,

   create_date ,
   modify_date ,

--DROP TYPE dbo.TIDTable


  replace( 'if object_id('':name'') is  null  ', ':name',name_full ) + char(10) +
   'CREATE TYPE '+ name_full +' AS TABLE ( '+  [sql_columns] + ')'   [sql_create] ,
      
   'DROP TYPE '  + name_full  [sql_drop] ,

   [sql_columns] as definition


from 

(
  select 
  
   o.create_date ,
   o.modify_date ,
  
  
    type_table_object_id as object_id,
 --object_id,
  
    m.name, 
    schema_name(m.schema_id) as schema_name,
    
    schema_name(m.schema_id) +'.'+ m.name  as name_full,

      
     SUBSTRING(
          (
              SELECT ','+sql_create  AS [text()]
            --  FROM sync.view_UDT_columns 
                            
               FROM               
                  (
                   SELECT 
                      tt.name as table_name, 
                      schema_name(tt.schema_id) as schema_name,         
                      c.name + ' '+

                  case
                    when st.name='varchar' then st.name +'('+ cast(c.max_length as varchar(10)) +  ')'
                    else st.name
                  end 

                  as sql_create
                          
                    FROM sys.columns c
                      JOIN sys.table_types tt ON c.[object_id] = tt.type_table_object_id  
                      JOIN sys.types st ON st.system_type_id  = c.system_type_id
                           
                   ) S   WHERE S.table_name = M.name
                
          FOR XML PATH ('')
      ), 2, 1000) [sql_columns] 
        
 from sys.table_types M
   INNER JOIN sys.all_objects AS o ON m.type_table_object_id = o.object_id
 
 ) A
 
   where
       name not like '%111'