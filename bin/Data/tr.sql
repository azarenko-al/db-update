 select 
  
 o.object_id,


 o.parent_object_id,
  object_name (o.parent_object_id) as table_name,
  
  
  o.type,


 definition as sql_create,


  'if object_id(''' +o.NAME +''') is not null   drop Trigger '+ o.NAME as sql_drop,
    
  
  o.NAME,
  o.create_date,
  o.modify_date,
  
--  o.*,
  
  schema_name (o.schema_id) as schema_name,
  
  definition
--  definition
  
  
from sys.objects     o
join sys.sql_modules m on m.object_id = o.object_id

where o.type='tr'
      and o.NAME not like '%_audit_%'
