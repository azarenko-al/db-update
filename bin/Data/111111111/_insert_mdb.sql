insert into  _compare_v1 (group_name, xtype,  schema, Definition_local,  Definition_remote,  name_local, name_remote, SQL_create, SQL_drop)

select ':group_name', xtype, schema,  Definition_L,  Definition_R, name_local, name_remote,  SQL_create, SQL_drop
from

(
SELECT L.*, 
  l.name as name_local, 
  r.name as name_remote,

  l.SQL_create as SQL_create, 
  R.SQL_drop  as SQL_drop, 


   L.Definition AS Definition_L, 
   R.Definition AS Definition_R,
(L.Definition = R.Definition) AS is_equal
  

FROM (select * from sys_objects  where host = 'local' and xtype = ':xtype'   ) AS L     
   LEFT JOIN    (select * from sys_objects  where host = 'remote' and xtype = ':xtype'   )   AS R 
       ON (L.schema = R.schema) AND (L.name = R.name) and ( L.xtype = R.xtype)



ORDER BY l.schema, L.name
)  M

where not is_equal  or is_equal is null
 