select * from

(

SELECT L.*, 
  l.name as name_local, 
  r.name as name_remote,

  l.SQL_create as SQL_create_, 
  R.SQL_drop  as SQL_drop_, 


   L.Definition AS Definition_L, 
   R.Definition AS Definition_R,

( L.Definition = R.Definition ) AS is_equal
  

FROM (select * from sys_objects  where host = 'local' and xtype = ':xtype'   ) AS L     
   LEFT JOIN    (select * from sys_objects  where host = 'remote' and xtype = ':xtype'   )   AS R 
       ON (L.schema = R.schema) AND (L.name = R.name) and ( L.xtype = R.xtype)
                      
ORDER BY l.schema, L.name

) 
where not is_equal  or is_equal is null
 