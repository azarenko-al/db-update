select *,

schema_name+'.'+Table_Name as Table_Name_full,




replace (replace (replace (replace (
'IF IndexProperty(Object_Id('':TABLE''), '':name'', ''IndexId'') Is Null '+  char(10) +


case 

--ADD PRIMARY KEY CLUSTERED

  when type_desc='SPATIAL'	  then 'CREATE SPATIAL INDEX [:name] ON :TABLE (:columns) USING GEOMETRY_AUTO_GRID ' +
                                    ' WITH ( BOUNDING_BOX = ( XMIN = 0, YMIN = 0, XMAX = 180, YMAX = 90 ) )'
  
  when Index_Name like 'pk_%' or Index_Name like '%_pk' or
       is_primary_key=1 	  then 'ALTER TABLE :TABLE ADD PRIMARY KEY :CLUSTERED (:columns) ON [PRIMARY] '  
  when is_unique_constraint=1 then 'ALTER TABLE :TABLE ADD CONSTRAINT :name UNIQUE :CLUSTERED (:columns) ON [PRIMARY] ' 
  when is_unique=1 			  then 'CREATE UNIQUE INDEX :name ON :TABLE (:columns)  ON [PRIMARY]  '  
                              else 'CREATE INDEX :name ON :TABLE (:columns) ON [PRIMARY] '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames),
 ':CLUSTERED', type_desc)  

as [sql_create],

/*
CREATE INDEX IX_IndexName
ON dbo.TableName
WITH (DROP_EXISTING = ON);

*/
-------------------------


replace (replace (replace (
'IF IndexProperty(Object_Id('':TABLE''), '':name'', ''IndexId'') Is not Null '+  char(10) +


case 
  when is_primary_key=1 	  then 'ALTER TABLE :TABLE DROP CONSTRAINT :name'  
--  when is_unique=1 			  then ' ALTER TABLE :TABLE DROP CONSTRAINT :name' 
  
  --DROP INDEX Link_uq ON dbo.Link

  when is_unique_constraint=1 then 'ALTER TABLE :TABLE DROP CONSTRAINT :name' 
--  when is_unique=1 			  then 'DROP INDEX :name ON :TABLE' 

                              else 'DROP INDEX  :name ON :TABLE '
end ,
 ':TABLE', schema_name+'.'+Table_Name),
 ':name', Index_Name),
 ':columns', IndexColumnsNames)

as [sql_drop],


-----------------------------------------------------------------------
schema_name+'.'+Table_Name +','+ -- char(10)  +


case 
  when Index_Name like 'pk_%' or Index_Name like '%_pk' then 'is_primary_key,is_unique_constraint,'
else
  IIF(is_primary_key=1, 'is_primary_key,','') +
  IIF(is_unique=1, 'is_unique,', '') +
  IIF(is_unique_constraint=1, 'is_unique_constraint,','') 
END  + --char(10) +
 
IndexColumnsNames as [Definition]

/*
schema_name+'.'+Table_Name + char(10)  +

IIF(is_primary_key=1, 'is_primary_key,','') +
IIF(is_unique=1, 'is_unique,', '') +
IIF(is_unique_constraint=1, 'is_unique_constraint,','')  + char(10) +
 
IndexColumnsNames as [Definition]
*/  

from 

(


  SELECT 
    create_date,
    modify_date,

case
  when INX.type_desc='CLUSTERED' then 'CLUSTERED'
  when INX.type_desc='NONCLUSTERED' then 'NONCLUSTERED'
  when INX.type_desc='SPATIAL' then 'SPATIAL'

end as  type_desc,
  
      INX.object_id,
      INDEX_id,

      INX.[name] AS [Index_Name],
      TBL.[name] AS [Table_Name],
      schema_name(TBL.schema_id) as schema_name,

      INX.is_primary_key,
      INX.is_unique,
      INX.is_unique_constraint,
      
      DS1.[IndexColumnsNames]
   --   DS2.[IncludedColumnsNames]
      
      
      
FROM [sys].[indexes] INX
INNER JOIN [sys].[tables] TBL
    ON INX.[object_id] = TBL.[object_id]
    
    
CROSS APPLY 
(
    SELECT STUFF
    (
        (
            SELECT ',[' + CLS.[name] + ']'
            FROM [sys].[index_columns] INXCLS
            INNER JOIN [sys].[columns] CLS 
                ON INXCLS.[object_id] = CLS.[object_id] 
                AND INXCLS.[column_id] = CLS.[column_id]
            WHERE INX.[object_id] = INXCLS.[object_id] 
                AND INX.[index_id] = INXCLS.[index_id]
                AND INXCLS.[is_included_column] = 0
            FOR XML PATH('')
        )
        ,1
        ,1
        ,''
    ) 
) DS1 ([IndexColumnsNames])

    where INX.[name] is not null
    
) M

where 
  Table_Name not like '%[___]'  and

  Table_Name <> 'msfavorites'  and
  Table_Name not like '%111'  
  
  and schema_name not in ('test','service','sync')


--  and Table_Name='CalcMap'

