

select top 99999999


obj.object_id,
sc.object_id as column_object_id,

col.*  , 

col.table_schema +'.'+col.table_name as table_name_full,

sc.Is_Identity,
sc.Is_computed,


cc.definition as computed_definition,



'ALTER TABLE '+ col.table_schema +'.'+col.table_name + ' ADD '+

case
  when sc.Is_computed=1 then '[' + column_name + '] as ' + cc.definition
  else '[' + column_name + '] ' + data_type +
       case
          when data_type='varchar' and  col.character_maximum_length = -1 then '(MAX)'

           when IsNull(col.character_maximum_length ,0) between 1 and 10000 then  '('+  cast( col.character_maximum_length as varchar(10)) +')'
           else ''
           
       end +   
         
     --  IIF( IsNull( col.character_maximum_length ,0) between 1 and 10000, '('+  cast( col.character_maximum_length as varchar(10)) +')' , '')
       + IIF (col.IS_NULLABLE = 'NO', ' not null', '')
       
       + IIF (col.column_default is not null, ' DEFAULT '+ col.column_default, '')
       
       + IIF (sc.Is_Identity = 1, ' IDENTITY(1,1) ', '')
       
end  
as [sql_create]  ,


'ALTER TABLE '+ col.table_schema +'.'+col.table_name + ' DROP COLUMN [' + column_name + ']'
as [sql_drop]  

   


 from  INFORMATION_SCHEMA.COLUMNS  col

INNER JOIN sys.columns AS sc ON sc.object_id = object_id(table_schema + '.' + table_name) AND sc.NAME = col.COLUMN_NAME

inner join sys.objects obj on obj.object_Id=sc.object_Id 

left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME and cc.object_id = obj.object_Id

where obj.[type]='u'

  and col.COLUMN_NAME not like'[-]'
  and col.COLUMN_NAME not like'[_]%'
  and col.COLUMN_NAME not like'%[_][_]'
  and col.COLUMN_NAME not like'%[11]'
--  and col.COLUMN_NAME not like'[deleted]%'

  and col.table_schema not LIKE '[_]%'



order by 

  sc.object_id, ordinal_position  


  
--  and cc.definition is not null
