select * ,

  replace( 'if object_id('':name'') is not null   drop function :name   ', ':name', schema_name+'.'+NAME )
    + char(10) + 'GO ' + char(10) + char(10) +

IIF (schema_name ='dbo', '',
  replace( 'if schema_id('':name'') is null   execute (''create schema :name '')', ':name', schema_name )  + char(10) + 'GO' + char(10) + char(10) 
  ) 
    
--  replace( 'if schema_id('':name'') is null   execute (''create schema :name '')', ':name', schema_name (o.schema_id))  + char(10) 
 -- + 'GO' + char(10) +
  + definition as sql_create,
  
  
  replace( 'if object_id('':name'') is not null   drop function :name   ', ':name', schema_name+'.'+NAME )
    + char(10) + 'GO ' + char(10) + char(10)   as sql_drop



--   'if object_id(''' +NAME +''') is not null   drop function '+ NAME as sql_drop


from
(
  select 

      o.type,  
      
      o.NAME,
      o.create_date,
      o.modify_date,
      
o.object_id,
  
      schema_name (o.schema_id) as schema_name,
      
      definition
    
    
  from sys.objects     o
  join sys.sql_modules m on m.object_id = o.object_id

  where
     o.type  in ('fn','tf','IF')
     

     and schema_name (o.schema_id) not in ( 'service','test' )
     and schema_name (o.schema_id) not LIKE '[_]%'
     
     and o.NAME not like '%11'
     
     and o.NAME not LIKE '%[_][_]'
     and o.NAME not LIKE '[_][_]%'
   
) M
