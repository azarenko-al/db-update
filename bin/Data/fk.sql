
  SELECT 
   f.object_id,
   f.name,--   AS 'Name of Foreign Key',
  
  t.create_date,
  t.modify_date,

  
  
   delete_referential_action,
   update_referential_action,

   delete_referential_action_desc,
   update_referential_action_desc,
   
   
   schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) AS Table_name,
   
   COL_NAME(fc.parent_object_id, fc.parent_column_id) AS 'column_name',
   
   schema_name (t.schema_id) +'.'+  OBJECT_NAME(t.object_id) AS 'References_Table_name',

   
   COL_NAME(t.object_id, fc.referenced_column_id) AS 'References_column_name',

--------------------------------------
 'if object_id('''+f.name+''') is not null  '+  CHAR(10) +


replace (replace (
   'ALTER TABLE :table  DROP CONSTRAINT :name',
':table', schema_name (t.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) ),
':name', f.name ) 
as [sql_Drop],




--------------------------------------
 --  'ALTER TABLE [' + OBJECT_NAME(f.parent_object_id) + ']  DROP CONSTRAINT [' + f.name + ']' AS 'Delete foreign key',

--------------------------------------
--replace (replace (
--   'ALTER TABLE [:table]  WITH NOCHECK ADD CONSTRAINT [:name] FOREIGN KEY([:foreign_key]) REFERENCES [ + OBJECT_NAME(t.object_id) + '] ([' +
--        COL_NAME(t.object_id,fc.referenced_column_id) + '])' AS 'Create foreign key',


--WITH NOCHECK 

   'ALTER TABLE ' + schema_name (f.schema_id) +'.'+ OBJECT_NAME(f.parent_object_id) + '  ADD CONSTRAINT ' + 
        f.name + ' FOREIGN KEY([' + COL_NAME(fc.parent_object_id,fc.parent_column_id) + ']) REFERENCES ' + 
         schema_name (t.schema_id) +'.'+ OBJECT_NAME(t.object_id) + ' ([' +
        COL_NAME(t.object_id,fc.referenced_column_id) + '])' 
        + IIF(delete_referential_action_desc = 'cascade', ' on delete cascade', '')
        + IIF(update_referential_action_desc = 'cascade', ' on update cascade', '')
        
        AS [sql_create]

/*
      if FK.IsUpdateCascade1 then sON:= sON+' ON UPDATE CASCADE';
      if FK.IsDeleteCascade  then sON:= sON+' ON DELETE CASCADE';
*/
        
        
    -- , delete_referential_action_desc AS 'UsesCascadeDelete'
FROM sys.foreign_keys AS f,
     sys.foreign_key_columns AS fc,
     sys.tables t 
     
WHERE
  
    f.OBJECT_ID = fc.constraint_object_id
AND t.OBJECT_ID = fc.referenced_object_id


and schema_name (f.schema_id)<>'security'
and schema_name (f.schema_id)<>'service'

and schema_name (t.schema_id)<>'security'
and schema_name (t.schema_id)<>'service'
  
and OBJECT_NAME(f.parent_object_id) not like '%[_][_]'  
and OBJECT_NAME(t.object_id) not like '%[_][_]'

and OBJECT_NAME(f.parent_object_id) not like '%[11]'  
and OBJECT_NAME(t.object_id) not like '%[11]'

