select * from

(

SELECT L.*, 
  l.name as name_local, 
  r.name as name_remote,

  l.SQL_create as SQL_create_local, 
  r.SQL_create as SQL_create_remote,

  l.id as local_id,
  r.id as remote_id,
                                
(L.SQL_create = R.SQL_create) AS is_equal
  

FROM (select * from sys_objects_table_columns  where host = 'local'  ) AS L     
   LEFT JOIN    (select * from sys_objects_table_columns  where host = 'remote'  )   AS R 
       ON (L.TableName = R.TableName) AND (L.name = R.name) 
                      
ORDER BY l.TableName, L.name

) 
where not is_equal  or is_equal is null
 