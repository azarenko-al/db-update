object dlg_Versions: Tdlg_Versions
  Left = 624
  Top = 333
  Caption = #1042#1077#1088#1089#1080#1080
  ClientHeight = 469
  ClientWidth = 982
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -10
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 434
    Width = 982
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    object Panel2: TPanel
      Left = 887
      Top = 0
      Width = 95
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
    end
    object Button1: TButton
      Left = 5
      Top = 8
      Width = 92
      Height = 23
      Action = act_Exec_SQL
      TabOrder = 1
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 705
    Height = 434
    ActivePage = TabSheet1
    Align = alLeft
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object Splitter3: TSplitter
        Left = 313
        Top = 33
        Width = 5
        Height = 373
        ResizeStyle = rsUpdate
        ExplicitHeight = 384
      end
      object cxGrid1: TcxGrid
        Left = 0
        Top = 33
        Width = 313
        Height = 373
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView1: TcxGridDBTableView
          PopupMenu = PopupMenu
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = DataSource1
          DataController.Filter.MaxValueListCount = 1000
          DataController.KeyFieldNames = 'id'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          Filtering.ColumnPopup.MaxDropDownItemCount = 12
          OptionsBehavior.ImmediateEditor = False
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsSelection.HideFocusRectOnExit = False
          OptionsView.Indicator = True
          Preview.AutoHeight = False
          Preview.MaxLineCount = 2
          object col_id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Options.Filtering = False
            Width = 41
          end
          object cxGrid1DBTableView1DBColumn2_enabled: TcxGridDBColumn
            Caption = #1056#1072#1079#1088#1077#1096#1077#1085#1086
            DataBinding.FieldName = 'enabled'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.Alignment = taLeftJustify
            Properties.NullStyle = nssUnchecked
            Properties.ReadOnly = False
            Properties.ValueChecked = 'True'
            Properties.ValueGrayed = ''
            Properties.ValueUnchecked = 'False'
            MinWidth = 16
            Width = 68
          end
          object cxGrid1DBTableView1DBColumn6_caption: TcxGridDBColumn
            DataBinding.FieldName = 'caption'
            Width = 145
          end
          object cxGrid1DBTableView1DBColumn1_name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.MaxLength = 0
            Properties.ReadOnly = False
            Width = 67
          end
          object cxGrid1DBTableView1DBColumn1_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
            Options.Filtering = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 35
          end
          object cxGrid1DBTableView1DBColumn4_modify_date: TcxGridDBColumn
            Caption = #1044#1072#1090#1072' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
            DataBinding.FieldName = 'modify_date'
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.DateButtons = [btnClear, btnToday]
            Properties.DateOnError = deToday
            Properties.InputKind = ikStandard
            Properties.Kind = ckDateTime
            Properties.ShowTime = False
            Visible = False
            Options.Filtering = False
            Width = 92
          end
          object cxGrid1DBTableView1DBColumn7_is_run_before_all: TcxGridDBColumn
            DataBinding.FieldName = 'is_run_before_all'
            GroupIndex = 0
            SortIndex = 1
            SortOrder = soDescending
            Width = 54
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object DBMemo1: TDBMemo
        Left = 352
        Top = 33
        Width = 345
        Height = 373
        Align = alRight
        DataField = 'content'
        DataSource = DataSource1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu
        ScrollBars = ssBoth
        TabOrder = 1
        WordWrap = False
      end
      object ToolBar3: TToolBar
        Left = 0
        Top = 0
        Width = 697
        Height = 33
        BorderWidth = 1
        ButtonHeight = 25
        ButtonWidth = 58
        Caption = 'ToolBar1'
        TabOrder = 2
        object DBNavigator3: TDBNavigator
          Left = 0
          Top = 0
          Width = 135
          Height = 25
          DataSource = DataSource1
          VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel, nbRefresh]
          TabOrder = 0
        end
      end
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredProps.Strings = (
      'DBMemo1.Width')
    StoredValues = <>
    Left = 904
    Top = 58
  end
  object PopupMenu: TPopupMenu
    Left = 904
    Top = 164
    object N1: TMenuItem
      Action = act_Exec_SQL
    end
    object SQL1: TMenuItem
      Action = act_Exec_All_SQL
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object SQL4: TMenuItem
      Action = act_Exec_SQL_test
    end
    object actExecAllSQLtest1: TMenuItem
      Action = act_Exec_All_SQL_test
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object LoadFromFile3: TMenuItem
      Action = act_LoadFromFile
    end
    object SaveAs1: TMenuItem
      Action = act_FileSaveAs
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object actTestloaddbframework1: TMenuItem
      Action = act_Test_load_SQL
      Caption = 'act_Test_load_SQL'
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 904
    Top = 108
    object act_Exec_SQL: TAction
      Caption = 'Exec_SQL'
      OnExecute = act_Exec_SQLExecute
    end
    object act_Exec_SQL_test: TAction
      Caption = 'Exec_SQL_test'
      OnExecute = act_Exec_SQLExecute
    end
    object act_Exec_All_SQL: TAction
      Caption = 'Exec_All_SQL'
      OnExecute = act_Exec_SQLExecute
    end
    object act_Exec_All_SQL_test: TAction
      Caption = 'Exec_All_SQL_test'
      OnExecute = act_Exec_SQLExecute
    end
    object act_LoadFromFile: TFileOpen
      Category = 'File'
      Caption = 'Load From File'
      Dialog.DefaultExt = 'sql'
      Dialog.Filter = '*.sql|*.sql'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = act_LoadFromFileAccept
    end
    object act_FileSaveAs: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Dialog.DefaultExt = 'sql'
      Dialog.Filter = '*.sql|*.sql'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
      OnAccept = act_FileSaveAsAccept
    end
    object act_Test_load_SQL: TAction
      Caption = 'act_Test_load_db_framework'
    end
    object act_Record_Add: TAction
      Caption = 'act_Record_Add'
      OnExecute = act_Exec_SQLExecute
    end
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection1
    OnNewRecord = ADOTable1NewRecord
    TableName = 'view_version_updates1'
    Left = 792
    Top = 64
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 792
    Top = 112
  end
  object MainMenu1: TMainMenu
    Left = 904
    Top = 224
    object dfg1: TMenuItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      object SQL2: TMenuItem
        Action = act_Exec_SQL
      end
      object SQL3: TMenuItem
        Action = act_Exec_All_SQL
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object ExecSQLtest1: TMenuItem
        Action = act_Exec_SQL_test
      end
      object ExecAllSQLtest1: TMenuItem
        Action = act_Exec_All_SQL_test
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object actLoadFromFile2: TMenuItem
        Caption = 'Load From File'
        OnClick = act_Exec_SQLExecute
      end
      object SaveToFile1: TMenuItem
        Caption = 'Save To File'
        OnClick = act_Exec_SQLExecute
      end
      object LoadFromFile2: TMenuItem
        Action = act_LoadFromFile
      end
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=W:\RPLS_DB tools\_b' +
      'in\install_db.mdb;Persist Security Info=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 792
    Top = 204
  end
end
