unit dm_ExportStructure;

interface

{$DEFINE old}


uses
  SysUtils, Classes, Forms, Db, AdoDb, Variants, StrUtils, Dialogs, IniFiles, IOUtils,

  //u_export_types,

  dm_MDB,

  d_Progress,

  dm_Main_SQL,

  u_local_const,

  u_Log,

  u_func,
  u_files,
  u_db,

  d_db_ViewDataset,
//  u_const_db,

  u_sync_classes, rxStrHlder;


type
  TAddRec = record
    Object_id: integer;

    create_date: TDateTime;
    modify_date: TDateTime;

    SCHEMA: string;
    NAME: string;
    NAME_full: string;

    XTYPE: string;

    TableNAME: string;


    SQL_create : string;
    SQL_drop   : string;


    definition   : string;

    //--------------------------------
    FK: record
      PK_TABLE_NAME: string;
      PK_COLUMN_NAME: string;

      FK_TABLE_NAME: string;
      FK_COLUMN_NAME: string;

    end;

    procedure Clear;
  end;



  TdmExportStructure = class(TDataModule)
    qry_Temp: TADOQuery;
    ADOTable1: TADOTable;
    ADOQuery1: TADOQuery;
    t_Objects: TADOTable;
    t_Objects_columns: TADOTable;
    t_schemas_allowed: TADOTable;
    q_Depends: TADOQuery;
    q_Tables: TADOQuery;
    t_Depands: TADOTable;
//    ADOConnection1: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
//    procedure t_Sys_ObjectsNewRecord(DataSet: TDataSet);
// TODO: Test
//  procedure Test;
  private
//    FPrefix : string;
    FHost  : string;

//    FIsSaveToDB : Boolean;

    FTerminated : Boolean;

//    FADOConnection_ref: TADOConnection;
    FDBStructure_ref: TDBStructure;

//    procedure ExportTableFields(aDataset: TDataSet);

    procedure Export_CK;
    procedure Export_SP;

    procedure Export_Views;
    procedure Export_Triggers;
    procedure Export_Indexes;
//    procedure ExportFKToDataSet1(aDataset: TDataSet);


    procedure EmptyTable(aTableName: string);

    procedure ExecDlg_start_proc;
    procedure Export_Table1;
//    function Get_sp_HelpText1(aSchema, aName: string): string;
    procedure Log(aMsg: string);
//    function TableNameIsCorrectForExport(aName : string): Boolean;
//    DatabaseName : string;


    procedure Export_UDT;

    procedure Progress2(aValue, aMax: Integer);

    procedure Run(aHost: string);

//    procedure UpdateTable_Delete_Is_not_Exists;
//    procedure UpdateTable_Set_Is_Exists_False;

// TODO: Open_PK
//  procedure Open_PK;
//    procedure AfterRun;
//    procedure BeforeRun;
    procedure Export_FK;
    procedure Export_FN;
    procedure Export_Table_Columns;


    function AddRec(aRec: TAddRec): Integer;
    function Depends_Check(aObject_id: Integer; aObjectName: string): Integer;
    function GetScriptFileByName(aName: string): string;
    procedure OpenQuery_Tables(aQuery: tadoQuery);

  public
    Params: record
               IniFileName_: string;
               IniFileName2: string;

//               Project_ID : integer;

               is_Export_DatabaseInfo_New : boolean;
               //is_Export_DBLink_data : boolean;

             end;



    procedure ExecDlg(aHost: string; aDBStructure: TDBStructure);
    procedure Export_DatabaseInfo_New(aIniFileName,aIniFileName2: string);

//    procedure Load_DBStructure(aDBStructure:   TDBStructure; aHOST: string);

    procedure Load_DBStructure_Version(aADOConnection: TADOConnection;
        aDBStructure: TDBStructure);


    procedure Save_Service_ScriptToFile_(aADOConnection: TADOConnection; aFileName:
        string);

    class function Init: TdmExportStructure;

  end;

var
  dmExportStructure: TdmExportStructure;



//--------------------------------------------------------------------
implementation
 {$R *.DFM}


const
  TBL_sys_objects_table_columns = 'sys_objects_table_columns';
  TBL_sys_objects_depends       = 'sys_objects_depends';

  TBL_database_info = 'database_info';



class function TdmExportStructure.Init: TdmExportStructure;
begin
  if not Assigned(dmExportStructure) then
    dmExportStructure:= TdmExportStructure.Create(Application);

  Result:=dmExportStructure;
end;


// ---------------------------------------------------------------
function TdmExportStructure.AddRec(aRec: TAddRec): Integer;
// ---------------------------------------------------------------
//var
//  s: string;
begin

 // Assert (aRec.OBJECT_ID>0);

  Assert (aRec.XTYPE<>'');
  Assert (aRec.Name<>'');

  Assert(FHost<>'');


   //    s:=t_Sys_Objects.TableName;

  if not t_Objects.Locate(FLD_Host +';'+ FLD_NAME +';'+ FLD_SCHEMA +';'+  FLD_XTYPE,
                  VarArrayOf([FHost, aRec.NAme, aRec.SCHEMA, aRec.XTYPE]), []) then

//   if not t_Sys_Objects.Locate(FLD_OBJECT_ID, aRec.Object_id, []) then

       db_AddRecord_ (t_Objects,
           [
            FLD_HOST, FHost,
            FLD_OBJECT_ID,  IIF_NULL ( aRec.Object_id ),

            FLD_NAME,        aRec.Name,
            FLD_XTYPE,       aRec.XTYPE,

            FLD_SCHEMA,      IIF_NULL(aRec.SCHEMA),

            FLD_Definition,  IIF_NULL(aRec.Definition),


            FLD_TableNAME,   IIF_NULL(aRec.TableName),


            FLD_SQL_create,  aRec.SQL_create,
            FLD_SQL_drop,    aRec.SQL_drop,


            FLD_PK_TABLE_NAME,    IIF_NULL(aRec.FK.PK_TABLE_NAME),
            FLD_PK_COLUMN_NAME,   IIF_NULL(aRec.FK.PK_COLUMN_NAME),

            FLD_FK_TABLE_NAME,    IIF_NULL(aRec.FK.FK_TABLE_NAME),
            FLD_FK_COLUMN_NAME,   IIF_NULL(aRec.FK.FK_COLUMN_NAME),

            FLD_date_create,  aRec.create_date,
            FLD_date_modify,  aRec.modify_date

           ])
       else

       db_UpdateRecord__ (t_Objects,
           [

            FLD_NAME,        aRec.Name,
            FLD_XTYPE,       aRec.XTYPE,

            FLD_SCHEMA,      IIF_NULL(aRec.SCHEMA),

            FLD_Definition,  IIF_NULL(aRec.Definition),

            FLD_TableNAME,   IIF_NULL(aRec.TableName),


            FLD_SQL_create,  aRec.SQL_create,
            FLD_SQL_drop,    aRec.SQL_drop,


            FLD_PK_TABLE_NAME,    IIF_NULL(aRec.FK.PK_TABLE_NAME),
            FLD_PK_COLUMN_NAME,   IIF_NULL(aRec.FK.PK_COLUMN_NAME),

            FLD_FK_TABLE_NAME,    IIF_NULL(aRec.FK.FK_TABLE_NAME),
            FLD_FK_COLUMN_NAME,   IIF_NULL(aRec.FK.FK_COLUMN_NAME)

//            FLD_create_date,  aRec.create_date,
//            FLD_modify_date,  aRec.modify_date
           ]);



  Result:=t_Objects[FLD_ID];

end;



procedure TdmExportStructure.DataModuleCreate(Sender: TObject);
var
  sDir: string;
begin
  db_SetComponentADOConnection(Self, dmMain_SQL.ADOConnection1);

//  Params.Project_name1:='link';

//  FIsSaveToDB := True;

end;

{
procedure TdmExportStructure.UpdateTable_Set_Is_Exists_False;
var
  s: string;
begin
  s := Format('UPDATE sys_objects SET is_exists=False WHERE host=''%s''', [FHost]);
  dmMDB.ADOConnection_MDB.Execute(s);
end;


procedure TdmExportStructure.UpdateTable_Delete_Is_not_Exists;
var
  s: string;
begin
  s := Format('Delete FROM sys_objects WHERE is_exists=False and host=''%s'' ', [ FHost]);
  dmMDB.ADOConnection_MDB.Execute(s);
end;
 }


procedure TdmExportStructure.EmptyTable(aTableName: string);
begin
  dmMDB.EmptyTable(aTableName);

//  dmMDB.ADOConnection_MDB.Execute(Format('DELETE FROM %s', [aTableName]));
end;

//----------------------------------------------------------
function TdmExportStructure.GetScriptFileByName(aName: string): string;
//----------------------------------------------------------
var
  s: string;
begin
  s:=GetApplicationDir + 'data\' + aName + '.sql';
  Assert(FileExists(s), aName);

  Result:=TFile.ReadAllText(s);
end;


//----------------------------------------------------------
procedure TdmExportStructure.Export_Table1;
//----------------------------------------------------------

var
  id: Integer;
  sTableName_full: string;
//  sXTYPE: string;
  obj: TDBObject;
 // s: string;


  rec: TAddRec;

begin


  Assert(Assigned(FDBStructure_ref), 'Value not assigned');


//  s:=ReplaceStr(SQL_SELECT_SQLSERVER_USER_TABLES_new, '[%s]', DatabaseName);

//  s:=Format(SQL_SELECT_SQLSERVER_USER_TABLES_new, [DatabaseName]);
//  db_OpenQuery(qry_Temp, SQL_SELECT_SQLSERVER_USER_TABLES_new);


//  db_OpenQuery(qry_Temp, StrHolder_tables.Strings.Text);

  dmMain_SQL.OpenQuery(qry_Temp, GetScriptFileByName ('tables'));





  with qry_Temp do
    while not EOF do
  begin
    Progress2(RecNo, RecordCount);
    if FTerminated then
      Break;


//    sTableName := FieldValues[FLD_TABLE_SCHEMA];
//    sTableName := FieldValues[FLD_SCHEMA_NAME] + '.'+ FieldValues[FLD_NAME]  ;
    sTableName_full := FieldValues[FLD_NAME_FULL];


    Log('Table: '+ sTableName_full);
    Progress2(RecNo, RecordCount);

{
    if not TableNameIsCorrectForExport(sTableName) then
    begin
      Next;
      Continue;
    end;
 }


    obj := FDBStructure_ref.Tables.AddItem(otTable);

//    obj.Object_id:=FieldValues[FLD_Object_id];

    obj.SQL_create:=FieldValues[FLD_SQL_create];
    obj.SQL_drop  :=FieldValues[FLD_SQL_drop];

//    obj.DB_id:=FieldValues[FLD_id];


    obj.Name  := FieldValues[FLD_NAME];
    obj.SCHEMA:= FieldValues[FLD_schema_name];

    obj.Name_full:=FieldValues[FLD_schema_name] + '.' +FieldValues[FLD_NAME];

    // ---------------------------------------------------------------

//    FillChar (rec, SizeOf(rec),0);

    rec.Clear;
    rec.XTYPE :='U';

    rec.NAME   :=FieldValues[FLD_NAME];
    rec.SCHEMA :=FieldValues[FLD_schema_name];

//    rec.SQL_create:= obj.SQL_create;
//    rec.SQL_drop  := obj.SQL_drop;
    rec.SQL_create:=FieldValues['SQL_create'];
    rec.SQL_drop  :=FieldValues['SQL_drop'];

    rec.definition:=FieldValues['SQL_create'];


    rec.create_date:= FieldValues[FLD_create_date];
    rec.modify_date:= FieldValues[FLD_modify_date];


    obj.DB_id:= AddRec(rec);

    Next;
  end;
end;

//----------------------------------------------------------
procedure TdmExportStructure.Export_UDT;
//----------------------------------------------------------

var
  id: Integer;
  sTableName: string;
//  sXTYPE: string;
  obj: TDBObject;

  rec: TAddRec;

begin


  Assert(Assigned(FDBStructure_ref), 'Value not assigned');


//  s:=ReplaceStr(SQL_SELECT_SQLSERVER_USER_TABLES_new, '[%s]', DatabaseName);

//  s:=Format(SQL_SELECT_SQLSERVER_USER_TABLES_new, [DatabaseName]);
//  db_OpenQuery(qry_Temp, SQL_SELECT_SQLSERVER_USER_TABLES_new);


//  db_OpenQuery(qry_Temp, StrHolder_UDT.Strings.Text);

  dmMain_SQL.OpenQuery(qry_Temp, GetScriptFileByName ('UDT'));



  with qry_Temp do
    while not EOF do
  begin
    Progress2(RecNo, RecordCount);
    if FTerminated then
      Break;


//    sTableName := FieldValues[FLD_TABLE_SCHEMA];
    sTableName := FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldValues[FLD_NAME] ;


    Log('Table: '+ sTableName);
    Progress2(RecNo, RecordCount);


    {
    obj := FDBStructure_ref.UDT.AddItem(otUDT);


    obj.Name   :=FieldValues[FLD_NAME];
    obj.SCHEMA :=FieldValues[FLD_SCHEMA_NAME];

    obj.SQL_create :=FieldValues['sql_create'];
    obj.SQL_drop   :=FieldValues['SQL_drop'];
    }

//    Assert (obj.SCHEMA<>'');

   // obj.Status := FieldValues[FLD_Status];

// Assert(Params.Project_name1<>'');


    // ---------------------------------------------------------------

//    FillChar (rec, SizeOf(rec),0);
    rec.Clear;

//    rec.OBJECT_ID:=obj.Object_id;

    rec.NAME  :=sTableName;
    rec.SCHEMA:=FieldValues[FLD_SCHEMA_NAME];

    rec.NAME_full:=FieldValues[FLD_SCHEMA_NAME] +'.'+ sTableName;

    rec.XTYPE :='UDT';

    rec.Definition:=FieldValues[FLD_Definition];
//    rec.XTYPE     :='view';


    rec.SQL_create :=FieldValues['sql_create'];
    rec.SQL_drop   :=FieldValues['SQL_drop'];

    rec.create_date:= FieldValues[FLD_create_date];
    rec.modify_date:= FieldValues[FLD_modify_date];

    AddRec(rec);

    Next;
  end;
end;



//----------------------------------------------------------
procedure TdmExportStructure.Export_Table_Columns;
//----------------------------------------------------------
var
  b: Boolean;
  I: Integer;
//  iObject_id: Integer;
  sTableName_full, sName: string;
  v: variant;
  obj: TDBTableField;
  s: string;
  oTable: TDBObject;
  s1: string;
  sSchema: string;
  sTableName: string;
//  sSCHEMA: string;

begin
  Assert(Assigned(FDBStructure_ref));

  Assert (FDBStructure_ref.Tables.Count > 0);

//  if not db_OpenQuery(qry_TableFields, StrHolder1_columns.Strings.Text) then
//    Exit;

  OpenQuery_Tables(q_Tables);

//  s := Format('delete from %s where host=''%s'' ', [TBL_sys_objects_table_fields , FHost]);
//  dmMDB.ADOConnection_MDB.Execute(s);

  //
//    if not t_Objects_columns.Locate (FLD_HOST +';'+ FLD_TABLENAME+';'+FLD_NAME,  VarArrayOf([FHost, sTableName_full, sName]), []) then



  db_OpenQuery(qry_Temp, GetScriptFileByName ('table_columns'));


    // db_View(aDataset);



//  try
    with qry_Temp do
      while not EOF do
    begin
      if RecNo mod 100 = 0 then
        Progress2(RecNo, RecordCount);

      if FTerminated then
        Break;


      sName  :=FieldValues[FLD_COLUMN_NAME];
      sSchema:=FieldValues[FLD_TABLE_SCHEMA] ;
      sTableName:=FieldValues[FLD_TABLE_NAME];
      sTableName_full:=FieldValues[FLD_TABLE_SCHEMA] +'.'+ FieldValues[FLD_TABLE_NAME];


      oTable := FDBStructure_ref.Tables.FindByNameFull(sTableName_full);


  //  if Eq(sTableName,'project') then
  //    ShowMessage('project');


    if not q_Tables.Locate ( FLD_SCHEMA+';'+FLD_NAME,  VarArrayOf([ sSchema, sTableName]), []) then
      ;



    if not Assigned(oTable) then
    begin
      Next;
      Continue;
    end;


  //  iObject_id:=FieldValues[FLD_Object_id];

//    Assert (iObject_id = oTable.Object_id);



//    if Assigned(oTable) then
//    begin
      obj:=oTable.TableFields.AddItem;

      obj.Name           :=sName; //FieldValues[FLD_NAME];
      obj.TableName_Full :=sTableName_full; //FieldValues[FLD_TABLENAME];
//      obj.SCHEMA    :=FieldValues[FLD_TABLE_SCHEMA]; //FieldValues[FLD_TABLENAME];


      obj.TYPE_         := FieldByName('DATA_TYPE').AsString;
      obj.Length        := FieldByName('character_maximum_length').AsInteger;


   //   if Eq(obj.TYPE_,'nvarchar') then
     //   obj.Length:=obj.Length div 2;

//    Result:=Result + Format('(%d) COLLATE Cyrillic_General_CI_AS', [Length]);

//    Result:=Result + Format('(%d)', [Length]);




//      obj.Size          :=FieldValues[FLD_Size];
//      obj.IsVariableLen := FieldValues[FLD_VARIABLE];
      obj.IsNullable    := FieldValues['Is_Nullable'] = 'YES';
      obj.IsIdentity    := FieldByName('Is_Identity').AsBoolean; //Integer=1;
      obj.IsComputed    := FieldByName('Is_Computed').AsBoolean; //AsInteger=1;


//      obj.ordinal_position    := FieldByName('ordinal_position').AsInteger; //AsInteger=1;


//      order by table_name, ordinal_position

      obj.ComputedText  := FieldByName('Computed_definition').AsString;
      obj.DEFAULT_CONTENT := FieldByName('column_default').AsString;

      obj.sql_create := FieldValues['sql_create'];
      obj.sql_drop   := FieldValues['sql_drop'];


 //   end;


 //   if not t_Objects_columns.Locate (FLD_HOST +';'+ FLD_TABLENAME+';'+FLD_NAME,  VarArrayOf([FHost, sTableName_full, sName]), []) then

         try

          b:=db_AddRecord_ (t_Objects_columns,
            [

             FLD_HOST, FHost,

             FLD_OBJECT_ID,   q_Tables[FLD_ID], // oTable.DB_id,
             FLD_SCHEMA,      sSchema,


             FLD_SQL_CREATE,  FieldValues['sql_create'], //obj.SQL_CREATE,
             FLD_SQL_DROP,    FieldValues['sql_drop'], //obj.SQL_DROP,


//             FLD_PROJECT_NAME,    Params.Project_name1),

             FLD_TABLENAME,       FieldValues[FLD_TABLE_SCHEMA] +'.'+ FieldValues[FLD_TABLE_NAME], //obj.TableName_Full,
             FLD_NAME,            FieldValues[FLD_COLUMN_NAME], //obj.NAME,

             FLD_TYPE,            FieldByName('DATA_TYPE').AsString, //obj.Type_,
//             FLD_LENGTH,          IIF(obj.Length=0,null, obj.Length)),
             FLD_LENGTH,          FieldByName('character_maximum_length').AsInteger, //obj.Length,
//             FLD_VARIABLE,        obj.IsVariableLen),
             FLD_isnullable,      FieldValues['Is_Nullable'] = 'YES', //obj.isnullable,

             FLD_IsComputed,      FieldByName('Is_Computed').AsBoolean, //obj.IsComputed,
             FLD_Computed_Text,   FieldByName('Computed_definition').AsString, //obj.ComputedText,

             FLD_IDENTITY,        FieldByName('Is_Identity').AsBoolean, //obj.IsIdentity,

//             FLD_DEFAULT_NAME,    obj.DEFAULT_NAME),
             FLD_DEFAULT_CONTENT, FieldByName('column_default').AsString //obj.DEFAULT_CONTENT
            ]);

         except
           on E: Exception do
             ShowMessage(obj.TableName_Full + ' -- ' + obj.Name);

         end;

       //   if not b then
        //    ShowMessage(sTableName+'-'+ sName);
     //   end;


      Next;

   end;

end;


//--------------------------------------------------------------------
procedure TdmExportStructure.Export_CK;
//--------------------------------------------------------------------
var
  s: string;
  sTABLENAME: string;

  oTable, obj: TDBObject;
  rec: TAddRec;
  sNAME: string;
  //sTABLE_SCHEMA: string;
  sText: string;

//  rObj: TObjectRec;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

 // s:=ReplaceStr(SQL_SELECT_CK_new, '[%s]', DatabaseName);

  //  s:=Format(SQL_SELECT_CK_new, [DatabaseName]);

//  db_OpenQuery (qry_Temp, StrHolder_check_constr.Strings.Text);
  db_OpenQuery(qry_Temp, GetScriptFileByName ('check_const'));


//  db_OpenQuery (qry_Temp, SQL_SELECT_CK_new);


/////  db_View(qry_Temp);

//  db_OpenQuery (qry_Temp, s);

  with qry_Temp do
    while not EOF do
    begin
      sNAME         :=FieldValues[FLD_CONSTRAINT_NAME];
      sTABLENAME    :=FieldValues[FLD_TABLE_NAME] ;
//      sTABLENAME    :=FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldValues[FLD_TABLE_NAME] ;

      Log('View: '+ sName);
      Progress2(RecNo, RecordCount);



//      sText := Get_sp_HelpText(sTABLE_SCHEMA,sName);

      oTable := FDBStructure_ref.Tables.FindByName( sTableName);

      if not Assigned(oTable) then
      begin
        Next;
        Continue;
      end else

//      if Assigned(oTable) then
      begin
        obj:=oTable.CK.AddItem(otCheckConstraint);

//        obj.Object_id:=FieldValues[FLD_Object_id];

        obj.Name     :=sNAME;
        obj.TABLENAME:=sTABLENAME;

        obj.SQL_create :=FieldValues['sql_create'];
        obj.SQL_drop   :=FieldValues['SQL_drop'];

        obj.Text := trim ( FieldValues['definition'] );
      end;


//////////////////////////////////////////

//    FillChar (rec, SizeOf(rec),0);
    rec.Clear;
    rec.Object_id:= oTable.DB_id;

//    rec.OBJECT_ID :=obj.Object_id;
    rec.XTYPE     :='ck';
    rec.NAME      :=sNAME;
    rec.SCHEMA    :=FieldValues[FLD_schema_name];
    rec.TABLENAME :=sTABLENAME;
                          
    rec.SQL_create:=  obj.SQL_create;
    rec.SQL_drop  :=  obj.SQL_drop;

    rec.create_date:= FieldValues[FLD_create_date];
    rec.modify_date:= FieldValues[FLD_modify_date];
             
    AddRec(rec);     


      Next;
    end;
end;

//--------------------------------------------------------------------
procedure TdmExportStructure.Export_SP;
//--------------------------------------------------------------------

label
  while_end;

var
  iID: Integer;
  sName: string;
  obj: TDBObject;
  rec: TAddRec;
  s: string;
  sRoutine_Type: string;
  sSCHEMA: string;
  sText: string;
  sXType: string;

//  rObj: TObjectRec;

//  sSCHEMA: string;
  sType: string;


begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');


//  Assert(DatabaseName<>'', 'Value <=0');

 // s:=ReplaceStr(SQL_SELECT_ROUTINES_2000, '[%s]', DatabaseName);

 // s:=Format(SQL_SELECT_ROUTINES_2000, [DatabaseName]);

//  db_OpenQuery (qry_SP, SQL_SELECT_ROUTINES_2000);

 // db_OpenQuery (qry_SP, StrHolder_proc.Strings.Text);
  db_OpenQuery(qry_Temp, GetScriptFileByName ('proc'));



//  db_View(qry_Temp);


  with qry_Temp do
    while not EOF do
    begin
      Progress2(RecNo, RecordCount);
      if FTerminated then
        Break;

      sName   := FieldByName(FLD_name).AsString;
//      sName   := FieldValues[FLD_SCHEMA_NAME]  +'.'+  FieldByName(FLD_name).AsString;
      sSCHEMA := FieldValues[FLD_SCHEMA_NAME];

    //  ROUTINE
  //     Assert (sSCHEMA <> '');

      Log('SP: '+ sName);

{
      if (not TableNameIsCorrectForExport(sName)) then
      begin
        goto while_end;
//        Next;
  //      Continue;
      end;
 }

      sText:= trim ( FieldValues[FLD_Definition] );

      sType :=FieldByName(FLD_type).AsString;

  //      sXType :='SP';
  {
        obj:=FDBStructure_ref.StoredProcs.AddItem(otStoredPROC);

//      end;
//      obj.Object_id := FieldValues[FLD_Object_id];

      obj.Name  := FieldValues[FLD_NAME];
      obj.SCHEMA:= FieldValues[FLD_schema_name];

      obj.Text := sText;

      obj.SCHEMA := FieldValues[FLD_SCHEMA_NAME];

//      obj.SQL_drop  := FieldValues[FLD_SQL_drop];

      obj.SQL_create  := FieldValues['SQL_create'];
      obj.SQL_drop    := FieldValues['SQL_drop'];

   }

//    FillChar (rec, SizeOf(rec),0);
    rec.Clear;
    rec.XTYPE     :='sp';

    rec.NAME      :=FieldValues[FLD_NAME];
    rec.SCHEMA    :=FieldValues[FLD_SCHEMA_NAME];

    rec.NAME_full:=FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldByName(FLD_name).AsString;


//    rec.OBJECT_ID :=obj.Object_id;
    rec.SQL_create  :=FieldValues['SQL_create'];
    rec.SQL_drop    :=FieldValues['SQL_drop'];

    rec.SQL_create  := FieldValues['SQL_create'];
    rec.SQL_drop    := FieldValues['SQL_drop'];


    rec.definition:=Trim(FieldValues[FLD_Definition]);


    rec.create_date:= FieldValues[FLD_create_date];
    rec.modify_date:= FieldValues[FLD_modify_date];


    iID:=AddRec(rec);

    if FHost='local' then
      Depends_Check(iID, rec.SCHEMA + '.'+ rec.NAME);


//  while_end:

      Next;
    end;
end;


//--------------------------------------------------------------------
procedure TdmExportStructure.Export_FN;
//--------------------------------------------------------------------


//label
//  while_end;

var
  iID: integer;
  sName: string;
  obj: TDBObject;
  rec: TAddRec;
  s: string;
  sRoutine_Type: string;
  sText: string;
  sXType: string;

//  rObj: TObjectRec;

//  sSCHEMA: string;
  sType: string;

begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');


//  Assert(DatabaseName<>'', 'Value <=0');

 // s:=ReplaceStr(SQL_SELECT_ROUTINES_2000, '[%s]', DatabaseName);

 // s:=Format(SQL_SELECT_ROUTINES_2000, [DatabaseName]);

//  db_OpenQuery (qry_SP, SQL_SELECT_ROUTINES_2000);

 // db_OpenQuery (qry_SP, StrHolder_fn.Strings.Text);

  dmMain_SQL.OpenQuery(qry_Temp, GetScriptFileByName ('fn'));

 // db_View(qry_SP);


  with qry_Temp do
    while not EOF do
    begin
      Progress2(RecNo, RecordCount);
      if FTerminated then
        Break;

      sName := FieldByName(FLD_name).AsString ;

//      sName   := FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldByName(FLD_name).AsString ;
//      sSCHEMA := FieldValues[FLD_SCHEMA_NAME];

    //  ROUTINE

  //     Assert (sSCHEMA <> '');

      Log('func: '+ sName);

{
      if (not TableNameIsCorrectForExport(sName)) then
      begin
        goto while_end;
//        Next;
  //      Continue;
      end;
 }

      sText:= trim ( FieldValues[FLD_Definition]);

      sType :=FieldByName(FLD_type).AsString;

      obj:=FDBStructure_ref.StoredFuncs.AddItem(otStoredFunc);

      obj.Name        :=FieldByName(FLD_name).AsString;
      obj.SCHEMA      :=FieldValues[FLD_SCHEMA_NAME];



      obj.SQL_create  := FieldValues['SQL_create'];
      obj.SQL_drop    := FieldValues['SQL_drop'];



      obj.Text := sText;



//    FillChar (rec, SizeOf(rec),0);
    rec.Clear;

    rec.XTYPE     :='fn';

//    rec.OBJECT_ID:=obj.Object_id;

    rec.NAME      :=obj.Name;
    rec.SCHEMA    :=obj.SCHEMA;

    rec.NAME_full:=FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldByName(FLD_name).AsString;

    rec.SQL_create:=FieldValues['SQL_create'];
    rec.SQL_drop  :=FieldValues['SQL_drop'];

    rec.Definition:=sText;

    rec.create_date:= FieldValues[FLD_create_date];
    rec.modify_date:= FieldValues[FLD_modify_date];

    iID:=AddRec(rec);

    if FHost='local' then
      Depends_Check(iID, rec.NAME_full );

  //while_end:

      Next;
    end;
end;


//--------------------------------------------------------------------
procedure TdmExportStructure.Export_Views;
//--------------------------------------------------------------------


var
  i: Integer;
  iID: Integer;
  k: Integer;

  sViewName: string;

  obj: TDBObject;
  rec: TAddRec;

//  rObj: TObjectRec;


begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

//  db_OpenQuery (qry_Temp, StrHolder_view.Strings.Text);
  dmMain_SQL.OpenQuery(qry_Temp, GetScriptFileByName ('view'));


//  db_OpenQuery (qry_Temp, SQL_SELECT_VIEWS_new);


  with qry_Temp do
    while not EOF do
    begin
      Progress2(RecNo, RecordCount);
      if FTerminated then
        Break;

      sViewName:= FieldValues[FLD_schema_name] +'.'+ FieldValues[FLD_NAME] ;

      Log('View: '+ sViewName);

{
      if not TableNameIsCorrectForExport(sViewName) then
      begin
        Next;
        Continue;
      end;
 }

    //  if Eq(sName,'view_LInk_to_excel') then
     //   sText:=sText;

     {
      obj:=FDBStructure_ref.Views.AddItem(otView);

      obj.Name  := FieldValues[FLD_NAME];
      obj.SCHEMA:= FieldValues[FLD_schema_name];

      obj.SQL_create  := FieldValues['SQL_create'];
      obj.SQL_drop    := FieldValues['SQL_drop'];


      obj.Text   := trim ( FieldValues[FLD_Definition] );
      }

//    FillChar (rec, SizeOf(rec),0);
      rec.Clear;

      rec.SCHEMA    :=FieldValues[FLD_schema_name];
      rec.NAME      :=FieldValues[FLD_NAME];

      rec.NAME_full:=FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldByName(FLD_name).AsString;

      rec.XTYPE :='view';

      rec.SQL_create:=FieldValues['SQL_create'];
      rec.SQL_drop  :=FieldValues['SQL_drop'];

      rec.Definition:=trim ( FieldValues[FLD_Definition] );

      rec.create_date:= FieldValues[FLD_create_date];
      rec.modify_date:= FieldValues[FLD_modify_date];


      iID:=AddRec(rec);

      if FHost='local' then
        Depends_Check(iID, sViewName);

      Next;
    end;
end;

//--------------------------------------------------------------------
function TdmExportStructure.Depends_Check(aObject_id: Integer; aObjectName:
    string): Integer;
//--------------------------------------------------------------------
var
  s: string;
begin
  Exit;

  //////////////////////////////////

  s := Format('EXEC sp_depends @objname = N''%s''', [aObjectName]);
  try
     dmMain_SQL.OpenQuery(q_Depends, s);

  except
     ShowMessage (aObjectName);
    Exit;
  end;

  q_Depends.Filter:='type<>''user table''';
  q_Depends.Filtered:=true;

//  db_StoredProc_Open(sp_Depends, 'sp_depends', ['objname', aObjectName]);


  dmMDB.OpenTable(t_Depands, TBL_sys_objects_depends);
//  db_View (sp_Depends);

  Result:=q_Depends.RecordCount;

  with q_Depends do
    while not EOF do
  begin
    db_AddRecord_ (t_Depands,
        [
         FLD_OBJECT_ID, aObject_id,
         FLD_name,      FieldValues[FLD_NAME]
        ]);

    Next;
  end;

  s := Format('update sys_objects set depends_count = %d where id=%d ', [q_Depends.RecordCount, aObject_id]);
  dmMDB.ADOConnection_MDB.Execute(s);


//  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

  //EXEC sp_depends @objname = N'dbo.view_link' ;

//  db_OpenQuery (qry_Temp, StrHolder_tr.Strings.Text);
//  db_OpenQuery(ADOQuery2, );


//  db_View(qry_Temp);


//  db_OpenQuery (qry_Temp, SQL_SELECT_TRIGGERS_2000);


end;

//--------------------------------------------------------------------
procedure TdmExportStructure.OpenQuery_Tables(aQuery: tadoQuery);
//--------------------------------------------------------------------
var
  s: string;
begin
  s := Format('select * from sys_objects where xtype=''u'' and host=''%s'' ', [FHost]);
  dmMDB.OpenQuery(aQuery, s);

end;

//--------------------------------------------------------------------
procedure TdmExportStructure.Export_Triggers;
//--------------------------------------------------------------------

var
  k: Integer;
  sName: string;
  oTable, obj: TDBObject;
  rec: TAddRec;
  s: string;
  sTableName: string;
  sTableName_full: string;
  sText: string;

//  rObj: TObjectRec;
//  sSCHEMA: string;


begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');


//  db_OpenQuery (qry_Temp, StrHolder_tr.Strings.Text);
  dmMain_SQL.OpenQuery(qry_Temp, GetScriptFileByName ('tr'));

  OpenQuery_Tables(q_Tables);


//  db_View(qry_Temp);


//  db_OpenQuery (qry_Temp, SQL_SELECT_TRIGGERS_2000);



//  db_View(qry_Temp);


  with qry_Temp do
    while not EOF do
    begin
      Progress2(RecNo, RecordCount);
      if FTerminated then
        Break;


      sName      := FieldValues[FLD_NAME];
      sTableName_full :=FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldValues[FLD_TABLE_NAME] ;


//      sSCHEMA :=  FieldValues[FLD_SCHEMA_NAME];

    //  if sSCHEMA='' then
     //   sSCHEMA:='';

    //  Assert(sSCHEMA<>'');

{
      if Pos('_audit_', LowerCase(sName))> 0then
      begin
        Next;
        Continue;
      end;
 }

      Log('Trigger: '+ sName);
      Progress2(RecNo, RecordCount);

   //   sXTYPE:= aDataset.FieldByName(FLD_xtype).AsString;


      oTable := FDBStructure_ref.Tables.FindByNameFull (sTableName_full);
      if not Assigned(oTable) then
      begin
        Next;
        Continue;
      end;

     // oTable := FDBStructure_ref.Tables.FindByName(sTableName);

    //  k:=FDBStructure_ref.Triggers.Count;


      obj := oTable.Triggers.FindByNameFull(sName);

   //   Assert(not Assigned(obj));

//      oTable.

      if not Assigned(obj) then
      begin

        obj:=oTable.Triggers.AddItem(otTrigger);

        obj.Name      := FieldValues[FLD_NAME]; //FieldValues[FLD_NAME];

        obj.SCHEMA    := FieldValues[FLD_SCHEMA_NAME]; //FieldValues[FLD_TABLE_NAME];
        obj.TableName := FieldValues[FLD_TABLE_NAME]; //FieldValues[FLD_TABLE_NAME];

        obj.SQL_create  := FieldValues['SQL_create'];
        obj.SQL_drop    := FieldValues['SQL_drop'];

        obj.Text := trim (  FieldValues[FLD_definition] );

      end;

    // ---------------------------------------------------------------

//    FillChar (rec, SizeOf(rec),0);
    rec.Clear;

    rec.XTYPE     :='tr';
    rec.Object_id:= oTable.DB_id;

    rec.NAME      :=FieldValues[FLD_NAME];
    rec.SCHEMA    :=FieldValues[FLD_SCHEMA_NAME];

    rec.TableNAME :=obj.TableName;

  //  FieldValues[FLD_SCHEMA_NAME] +'.'+ FieldValues[FLD_NAME];

    rec.SQL_create:=FieldValues['SQL_create'];
    rec.SQL_drop  :=FieldValues['SQL_drop'];

//    rec.SQL_drop  :=obj.SQL_drop;
//    rec.SQL_create:=obj.SQL_create;

    rec.Definition:=Trim (obj.Text );

    rec.create_date:= FieldValues[FLD_create_date];
    rec.modify_date:= FieldValues[FLD_modify_date];


//    Assert  ( rec.SCHEMA<>'');

    AddRec(rec);


    Next;
  end;



end;


//--------------------------------------------------------------------
procedure TdmExportStructure.Export_DatabaseInfo_New(aIniFileName,
    aIniFileName2: string);
//--------------------------------------------------------------------
const
  SQL_SELECT  =
//    'SELECT TOP 1 * FROM '+ TBL__db_version_ +' ORDER BY  modify_date  DESC';
//    'SELECT TOP 1 * FROM '+ TBL__db_version_ +' ORDER BY version DESC';
    'SELECT TOP 1 * FROM '+ TBL__db_version_ +' ORDER BY version DESC';

var
  oIni: TIniFile;
  sProduct_name: string;
  sProduct_name_: string;
  sVersion: string;
  sVersion_: string;
begin
 // Assert(Assigned(FDBStructure_ref), 'Value not assigned');


  db_OpenQuery (qry_Temp, SQL_SELECT);

  sVersion      := qry_Temp.FieldByName(FLD_Version).AsString;
  sProduct_name := qry_Temp.FieldByName(FLD_product_name).AsString;

  db_TableOpen1 (ADOTable1, TBL_database_info);

  db_UpdateRecord__(ADOTable1,
        [
         FLD_Version,      sVersion,
         FLD_product_name,  sProduct_name
        ]);

  // -------------------------
  if aIniFileName<>'' then
  begin
    DeleteFile(aIniFileName);

    // ---------------------------------------
    oIni:=TIniFile.Create (aIniFileName);

    oIni.WriteString('main','version', sVersion);
    oIni.WriteString('main','product_name', sProduct_name);

   // oIni.UpdateFile;

    FreeAndNil(oIni);

    // ---------------------------------------
  //  DeleteFile (aIniFileName);


    oIni:=TIniFile.Create (aIniFileName);

    sVersion_:=oIni.ReadString('main','version', '');
    sProduct_name_:=oIni.ReadString('main','product_name', '');

    Assert(sVersion_=sVersion);
    Assert(sProduct_name_=sProduct_name);

   // oIni.UpdateFile;

    FreeAndNil(oIni);


    if aIniFileName2<>'' then
//       FileCopy (aIniFileName2, aIniFileName);
       TFile.Copy (aIniFileName, aIniFileName2);

  end;
end;


//--------------------------------------------------------------------
procedure TdmExportStructure.Export_Indexes;
//--------------------------------------------------------------------

    // ---------------------------------------------------------------
    procedure DoInsertIndexes(aTable: TDBObject);
    // ---------------------------------------------------------------
    var
      obj: TDBObject;
      rec: TAddRec;
      s: string;
      sIndexType: string;


    begin
    //  if Eq(aTableName,'CalcMap') then
     //   s:=s;


      with qry_Temp do
      begin

          obj:=aTable.Indexes.AddItem(otIndex);

          obj.Name     := FieldByName('index_name').AsString;;
          obj.SCHEMA   := FieldByName('SCHEMA_name').AsString;;
          Assert (obj.SCHEMA <> '');

          obj.TableName:=aTable.Name;

          obj.SQL_create:=FieldByName('SQL_create').AsString;
          obj.SQL_drop  :=FieldByName('SQL_drop').AsString;

          Assert (obj.SQL_create<>'');
          Assert (obj.SQL_drop<>'');


          obj.Definition:=FieldValues['Definition'];




//          FillChar (rec, SizeOf(rec),0);
          rec.Clear;

          rec.Object_id:= aTable.DB_id;


          rec.NAME      :=obj.Name;
          rec.SCHEMA    :=obj.SCHEMA;

          rec.TABLENAME:= aTable.Name;

          rec.Definition:=obj.Definition;

          rec.XTYPE     :='index';

          rec.SQL_create:=obj.SQL_create;
          rec.SQL_drop  :=obj.SQL_drop;

          rec.create_date:= FieldValues[FLD_create_date];
          rec.modify_date:= FieldValues[FLD_modify_date];

          AddRec(rec);

       end;

    end;    //



var
  sTableName_full: string;
  oTable: TDBObject;
 // sSchema: string;
begin
  Assert(Assigned(FDBStructure_ref), 'Value not assigned');

//  db_OpenQuery (qry_Indexes, StrHolder_indexes.Strings.Text);

  dmMain_SQL.OpenQuery(qry_Temp, GetScriptFileByName ('Index'));

  OpenQuery_Tables(q_Tables);

//  db_View(qry_Indexes);

 // sTableName:= '';

  with qry_Temp do
    while not EOF do
  begin
    Progress2(RecNo, RecordCount);

    sTableName_full:=FieldByName(FLD_schema_name).AsString +'.'+ FieldByName(FLD_TABLE_NAME).AsString;
//    sSchema   :=FieldByName(FLD_schema_name).AsString;

    //

    Log('Indexes for table: '+ sTableName_full);
    Progress2(RecNo, RecordCount);

    oTable := FDBStructure_ref.Tables.FindByNameFull(sTableName_full);

    if Assigned(oTable) then
      DoInsertIndexes (oTable);


    Next;
  end;
end;


//--------------------------------------------------------------------
procedure TdmExportStructure.Export_FK;
//--------------------------------------------------------------------
var
//  fk_Schema: string;

  pk_table_name, pk_column_name,
  fk_table_name, fk_column_name, sName: string;

  obj: TDBObject;

  oTable1,oTable2: TDBObject;

  rec: TAddRec;

begin


  //db_OpenQuery (qry_FK, StrHolder_FK.Strings.Text);
  dmMain_SQL.OpenQuery(qry_Temp, GetScriptFileByName ('FK'));


  Assert (qry_Temp.RecordCount>0);

//  db_View(qry_FK);

  with qry_Temp do
    while not EOF do
  begin

    sName := FieldByName(FLD_Name).AsString;

    Log('FK: '+ sName);
    Progress2(RecNo, RecordCount);

    pk_table_name  := FieldValues['TABLE_NAME'];
    pk_column_name := FieldByName('column_name').AsString;

    fk_table_name  := FieldByName('References_Table_name').AsString;
    fk_column_name := FieldByName('References_column_name').AsString;



    // only key for existed tables
    oTable1 := FDBStructure_ref.Tables.FindByNameFull(pk_table_name);
    oTable2 := FDBStructure_ref.Tables.FindByNameFull(fk_table_name);


    if Assigned(oTable1) or (Assigned(oTable2)) then
    begin
      obj:=FDBStructure_ref.FK.AddItem(otFK);


      obj.Name          := sName;

      obj.SQL_create    := FieldByName('SQL_create').AsString;
      obj.SQL_drop      := FieldByName('SQL_drop').AsString;


  //    obj.FK.PK_schema      := pk_Schema;
      obj.FK.PK_TABLE_NAME  := pk_table_name;
      obj.FK.PK_COLUMN_NAME := pk_column_name;

//      obj.FK.FK_schema      := fk_Schema;
      obj.FK.FK_TABLE_NAME  := fk_table_name;
      obj.FK.FK_COLUMN_NAME := fk_column_name;


    end;


    FillChar (rec, SizeOf(rec),0);

    rec.XTYPE     :='fk';

//    rec.Object_id :=obj.Object_id;

    rec.NAME      :=sNAME;
//    rec.SQL_create:=obj.SQL_create;
//    rec.SQL_drop  :=obj.SQL_drop;

    rec.SQL_create:=FieldValues['SQL_create'];
    rec.SQL_drop  :=FieldValues['SQL_drop'];


    rec.FK.PK_TABLE_NAME :=obj.FK.pk_table_name;
    rec.FK.PK_COLUMN_NAME:=obj.FK.pk_column_name;

    rec.FK.FK_TABLE_NAME :=obj.FK.fk_table_name;
    rec.FK.FK_COLUMN_NAME:=obj.FK.fk_column_name;


    rec.create_date:= FieldValues[FLD_create_date];
    rec.modify_date:= FieldValues[FLD_modify_date];

    AddRec(rec);


    Next;
  end;
end;




// ---------------------------------------------------------------
procedure TdmExportStructure.Load_DBStructure_Version(aADOConnection:  TADOConnection; aDBStructure: TDBStructure);
// ---------------------------------------------------------------

    // ---------------------------------------------------------------
    procedure Do_Database_Info(aDataset: TDataSet);
    // ---------------------------------------------------------------
    begin
      with aDataset do
         if not EOF then
       begin
         aDBStructure.DB_Version      := FieldByName(FLD_Version).AsString;
         aDBStructure.DB_Product_name := FieldByName(FLD_Product_name).AsString;
       end;
    end;
    // ---------------------------------------------------------------

begin
  ADOTable1.Connection :=aADOConnection;

  if Params.is_Export_DatabaseInfo_New then
  begin
    db_TableOpen1(ADOTable1, TBL_database_info);
    Do_Database_Info(ADOTable1);
  end;


end;



{
// ---------------------------------------------------------------
procedure TdmExportStructure.AfterRun;
// ---------------------------------------------------------------
begin
  UpdateTable_Delete_Is_not_Exists();

end;
 }


// ---------------------------------------------------------------
procedure TdmExportStructure.Run(aHost: string);
// ---------------------------------------------------------------
var
  I: Integer;
  s: string;
 // sPrefix_New: string;
 // bTerminated: Boolean;
begin
  FHost:=aHost;

 // Assert(Assigned(FADOConnection_ref), 'Value not assigned');

  FTerminated := False;


  FDBStructure_ref.Clear;


//  aPrefix :='sql_';
{
  if aHost = 'local' then
  begin
//    s := Format('SELECT * from  %s where host=''%s''', [TBL_sys_objects, aHost]);

    s := 'UPDATE sys_objects SET is_exists=False where host=''local'' ';
    dmMDB.ADOConnection_MDB.Execute(s);

//    UpdateTable_Set_Is_Exists_False(TBL_sys_objects);

  end;
 }


//    BeforeRun;


//  FIsSaveToDB := aPrefix = '';

//  FIsSaveToDB := True; // !!!!!!!!

  s := Format('DELETE FROM sys_objects where host=''%s'' ', [ aHost] );
  dmMDB.ADOConnection_MDB.Execute(s);

{
  if aHost <> 'local' then
  begin
    s := 'DELETE FROM sys_objects where host=''remote'' ' ;//, [ aHost]);
    dmMDB.ADOConnection_MDB.Execute(s);

//    EmptyTable('sys_objects');


  end;
 }

  //!!!!!!!!!
//  EmptyTable('sys_objects');


//  EmptyTable(aPrefix+'Table_Fields');


//  FADOConnection_ref.Execute(Format('DELETE FROM %s', ['Table_Fields']));


(*      FMDB.EmptyTable(['Table_Fields']);


    ExecCommand ('DELETE FROM ' + aTableName);//, [EMPTY_PARAM]);

*)


//    dmExportStructure.BeforeRun;

  ADOTable1.Connection := dmMDB.ADOConnection_MDB;
  ADOQuery1.Connection := dmMDB.ADOConnection_MDB;

{
  if aPrefix<>'' then
    sPrefix_New:='_sql'
  else
    sPrefix_New:='';
 }

//  EmptyTable(TBL_sys_objects_table_fields + sPrefix_New);

//s:=TBL_sys_objects + IIF (aPrefix='', '', '_remote') ;

//..  s := Format('SELECT sys_objects from  %s where host=''%s''', [aHost]);
//  db_OpenQuery(ADOQuery1, s);


//  EmptyTable (TBL_sys_objects              + IIF (aPrefix='', '', '_remote'));
//  EmptyTable (TBL_sys_objects_table_fields + IIF (aPrefix='', '', '_remote'));

  t_Objects.Connection         := dmMDB.ADOConnection_MDB;
  t_Objects_columns.Connection := dmMDB.ADOConnection_MDB;

  db_TableOpen1(t_Objects,          TBL_sys_objects              );
  db_TableOpen1(t_Objects_columns,  TBL_sys_objects_table_columns );


  s := Format('delete from %s where host=''%s'' ', [TBL_sys_objects_table_columns , FHost]);
  dmMDB.ADOConnection_MDB.Execute(s);

  s := Format('delete from %s ', [TBL_sys_objects_depends ]);
  dmMDB.ADOConnection_MDB.Execute(s);

//  db_TableOpen1(t_Sys_Objects,          TBL_sys_objects               + IIF (aPrefix='', '', '_remote') );
//  db_TableOpen1(t_Sys_Objects_columns,  TBL_sys_objects_table_fields  + IIF (aPrefix='', '', '_remote') );


//  db_TableOpen1(t_Sys_Objects,          TBL_sys_objects + sPrefix_New);
 // db_TableOpen1(t_Sys_Objects_columns,  TBL_sys_objects_table_fields + sPrefix_New);



(*  Progress_SetProgress1(0, 9, bTerminated);
  EmptyTable ('__sys_objects');
  db_TableOpen1(ADOTable1, '__sys_objects');
  Export_SysObjects(ADOTable1);
*)

  for I := -1 to 10 do
  begin
    Progress_SetProgress1(i, 9, FTerminated);

    if FTerminated then
      Break;


    case i of
//      -1: begin
(*
           EmptyTable ('__sys_objects');
           db_TableOpen1(ADOTable1, '__sys_objects');
           Export_SysObjects(ADOTable1);
*)
  //       end;

   (*   0: begin
           //Progress_(i, 9, FTerminated);


           db_TableReOpen(ADOTable1, aPrefix+'Indexes');
           Export_Indexes(ADOTable1) ;
         end;

*)



      0:  Export_UDT();
      1:  Export_Table1();
      2:  Export_Table_Columns();
      3:  Export_CK();
      4:  Export_FK();
      5:  Export_Indexes();

      //-------------------------------
      // include exceptions here
      //-------------------------------


      6:  Export_Views();
      7:  Export_SP();
      8:  Export_FN();
      9:  Export_Triggers();


      10:begin
       //    if aPrefix='' then
         //    Export_DatabaseInfo();
           if params.is_Export_DatabaseInfo_New then
           begin
             if aHost='' then
               Export_DatabaseInfo_New(Params.IniFileName_, Params.IniFileName2);

           end;

         end;


    end;
  end;


  if FTerminated then
    Exit;


//  if aHost = 'local' then
//    UpdateTable_Delete_Is_not_Exists();

   // UpdateTable_Delete_Is_not_Exists();

//    AfterRun;

end;


procedure TdmExportStructure.Progress2(aValue, aMax: Integer);
//var bTerminated: boolean ;
begin
  Progress_SetProgress2(aValue,aMax, FTerminated);

end;


procedure TdmExportStructure.Log(aMsg: string);
begin
  g_Log.Msg(aMsg);
end;


procedure TdmExportStructure.ExecDlg_start_proc;
begin
  Run(FHost);

end;

// ---------------------------------------------------------------
procedure TdmExportStructure.ExecDlg(aHost: string; aDBStructure:
    TDBStructure);
// ---------------------------------------------------------------
begin
  Assert (aHost<>'');

 //FADOConnection_ref  := aADOConnection;
  FHost:=aHost;

  FDBStructure_ref :=aDBStructure;


  Progress_ExecDlg_proc(ExecDlg_start_proc);

end;

// ---------------------------------------------------------------
procedure TdmExportStructure.Save_Service_ScriptToFile_(aADOConnection:
    TADOConnection; aFileName: string);
// ---------------------------------------------------------------
var
  s: string;
begin
{
  Assert (aFileName<>'');

  db_ExecStoredProc__(ADOStoredProc1, 'service.sp_Script',[], true);

  s:=ADOStoredProc1.Fields[0].Value;

  StrToTxtFile(s, aFileName);
 }
end;

procedure TAddRec.Clear;
begin
  FillChar(Self, SizeOf(Self), 0);
end;


end.




{

// ---------------------------------------------------------------
procedure TdmExportStructure.Load_DBStructure (aDBStructure: TDBStructure; aHOST: string);
// ---------------------------------------------------------------

  // ---------------------------------------------------------------
    procedure Do_CK(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
        i: Integer;
        sTableName: string;
        oTable: TDBObject;
//      sSCHEMA: string;

    begin
//      i:=aDataset.RecordCount;

Assert (aDBStructure.Tables.Count > 0);

      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          Progress2(RecNo,RecordCount);

          sTableName :=FieldValues[FLD_TABLENAME];

          oTable := aDBStructure.Tables.FindByName (sTableName);

          Assert(Assigned(oTable), 'Value not assigned - ' + sTableName);

          obj:=oTable.CK.AddItem(otCheckConstraint);
          obj.LoadFromDataset(aDataset);

          //------------------
//          obj:=aDBStructure.CK.AddItem(otCheckConstraint);
//          obj.LoadFromDataset(aDataset);



        ////////////  obj.Owner := FieldByName(FLD_Owner).AsString;

          Next;
       end;
    end;

    // ---------------------------------------------------------------
    procedure Do_FK(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
      i: Integer;
      //sOwner: string;
    begin

      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          Progress2(RecNo,RecordCount);


          obj:=aDBStructure.FK.AddItem(otFK);
          obj.LoadFromDataset(aDataset);


          Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_Views(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
    begin
      aDataset.First;

       with aDataset do
         while not EOF do
       begin
         Progress2(RecNo,RecordCount);


         obj := aDBStructure.Views.AddItem(otView);
         obj.LoadFromDataset(aDataset);

         Next;
       end;
    end;

    // ---------------------------------------------------------------
    procedure Do_StoredProcs(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var
      obj: TDBObject;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          Progress2(RecNo,RecordCount);


          obj:=aDBStructure.StoredProcs.AddItem(otStoredPROC);
          obj.LoadFromDataset(aDataset);

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_Func(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var
      obj: TDBObject;
     // sRoutine_Type: string;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          Progress2(RecNo,RecordCount);

      //    sRoutine_Type:=FieldValues[FLD_Routine_Type];

//          if Eq(sRoutine_Type, 'function') then
          obj:=aDBStructure.StoredFuncs.AddItem(otStoredFunc);
          obj.LoadFromDataset(aDataset);

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_Table_Trigger(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var
      oTable, obj: TDBObject;
      bPrimaryKey: Boolean;
      sName: string;
   //   sSCHEMA: string;
      sTableName: string;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
         Progress2(RecNo,RecordCount);


         sName :=FieldValues[FLD_NAME];
         sTableName :=FieldValues[FLD_TABLENAME];

         oTable := aDBStructure.Tables.FindByName(sTableName);
         Assert(Assigned(oTable), 'Value not assigned - '+ sTableName);


         obj:=oTable.Triggers.AddItem(otTRIGGER);
         obj.LoadFromDataset(aDataset);

         Next;
       end;
    end;

    // ---------------------------------------------------------------
    procedure Do_tables(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
      i: Integer;
    begin
      i:=aDataset.RecordCount;
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          Progress2(RecNo,RecordCount);


          obj := aDBStructure.Tables.AddItem(otTable);
          obj.LoadFromDataset(aDataset);

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_UDT(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBObject;
      i: Integer;
    begin
      i:=aDataset.RecordCount;
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
         Progress2(RecNo,RecordCount);

         obj := aDBStructure.UDT.AddItem(otUDT);
         obj.LoadFromDataset(aDataset);

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_table_indexes(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var
      obj: TDBObject;
      sTableName: string;
      oTable: TDBObject;

    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
         Progress2(RecNo,RecordCount);

         sTableName :=FieldValues[FLD_TABLENAME];

         oTable := aDBStructure.Tables.FindByName( sTableName);
         Assert(Assigned(oTable), 'Value not assigned');


         obj:=oTable.Indexes.AddItem(otIndex);
         obj.LoadFromDataset(aDataset);

         Next;
       end;
    end;


    // ---------------------------------------------------------------
    procedure Do_TableFields(aDataset: TDataSet);
    // ---------------------------------------------------------------
    var obj: TDBTableField;
       sTableName_full: string;
       oTable: TDBObject;
   //   sSCHEMA: string;
    begin
      aDataset.First;

      with aDataset do
         while not EOF do
       begin
          Progress2(RecNo,RecordCount);


          sTableName_full :=FieldValues[FLD_TABLENAME];


          oTable := aDBStructure.Tables.FindByNameFull(sTableName_full);

          if Assigned(oTable) then
          begin
            obj:=oTable.TableFields.AddItem;

            obj.Name := FieldByName(FLD_NAME).AsString;

            obj.sql_create := FieldValues[FLD_sql_create];

            obj.TableName_Full  := FieldByName(FLD_TABLENAME).AsString;

            obj.TYPE_           := FieldByName(FLD_TYPE).AsString;
            obj.Length          := FieldByName(FLD_Length).AsInteger;
///            obj.IsVariableLen   := FieldByName(FLD_VARIABLE).AsBoolean;
            obj.IsNullable      := FieldByName(FLD_IsNullable).AsBoolean;
            obj.IsIdentity      := FieldByName(FLD_IDENTITY).AsBoolean;

//            obj.DEFAULT_NAME    := FieldByName(FLD_DEFAULT_NAME).AsString;
            obj.DEFAULT_CONTENT := FieldByName(FLD_DEFAULT_CONTENT).AsString;

            obj.IsComputed      := FieldByName(FLD_IsComputed).AsBoolean;
            obj.ComputedText    := FieldByName(FLD_Computed_Text).AsString;

          end;

          Next;
       end;
    end;

    // ---------------------------------------------------------------
    procedure Do_Database_Info(aDataset: TDataSet);
    // ---------------------------------------------------------------
    begin
      with aDataset do
         if not EOF then
       begin
         aDBStructure.DB_Version      := FieldByName(FLD_Version).AsString;
         aDBStructure.DB_Product_name := FieldByName(FLD_Product_name).AsString;
       end;
    end;
    // ---------------------------------------------------------------



var
  s: string;
begin
  aDBStructure.Clear;

  ADOTable1.Connection :=dmMDB.ADOConnection_MDB;
  ADOQuery1.Connection :=dmMDB.ADOConnection_MDB;



//    s := Format('SELECT * from  %s order by name', [TBL_sys_objects]);

  s := Format('SELECT * from  %s where xtype=''UDT'' and host=''%s''  order by name', [TBL_sys_objects, aHOST]);

//  db_OpenQuery(ADOQuery1, 'select * from UDT order by name');
  db_OpenQuery(ADOQuery1, s);

//  db_TableOpen1(ADOTable1, 'tables');
  Do_UDT(ADOQuery1);


  s := Format('SELECT * from  %s where xtype=''U'' and host=''%s'' order by name', [TBL_sys_objects, aHOST]);

  db_OpenQuery(ADOQuery1, s);
  Do_tables(ADOQuery1);


  db_TableOpen1(ADOTable1, TBL_sys_objects_table_fields);
  Do_TableFields(ADOTable1);


  s := Format('SELECT * from  %s where xtype=''V'' and host=''%s'' order by name', [TBL_sys_objects, aHOST]);
  db_OpenQuery(ADOQuery1, s);
  Do_Views(ADOQuery1);


  s := Format('SELECT * from  %s where xtype=''TR'' and host=''%s'' order by name', [TBL_sys_objects, aHOST]);
  db_OpenQuery(ADOQuery1, s);
  Do_Table_Trigger(ADOQuery1);


  s := Format('SELECT * from  %s where xtype=''SP''  and host=''%s'' order by name', [TBL_sys_objects, aHOST]);
  db_OpenQuery(ADOQuery1, s);
  Do_StoredProcs(ADOQuery1);

  s := Format('SELECT * from  %s where xtype=''FN'' and host=''%s'' order by name', [TBL_sys_objects, aHOST]);
  db_OpenQuery(ADOQuery1, s);
//  Assert (ADOQuery1.RecordCount>0);

  Do_Func(ADOQuery1);

  s := Format('SELECT * from  %s where xtype=''index'' and host=''%s'' order by name', [TBL_sys_objects, aHOST]);
  db_OpenQuery(ADOQuery1, s);
//  Assert (ADOQuery1.RecordCount>0);


  Do_table_indexes(ADOQuery1);
  s := Format('SELECT * from  %s where xtype=''FK''  and host=''%s'' order by name', [TBL_sys_objects, aHOST]);
  db_OpenQuery(ADOQuery1, s);

  Do_FK(ADOQuery1);

//  db_OpenQuery(ADOQuery1, 'select * from views order by name');

  s := Format('SELECT * from  %s where xtype=''CK'' and host=''%s'' order by name', [TBL_sys_objects, aHOST]);
  db_OpenQuery(ADOQuery1, s);

  Do_CK(ADOQuery1);


  db_TableOpen1(ADOTable1, TBL_database_info);
  Do_Database_Info(ADOTable1);


end;

