object dlg_Structure_new: Tdlg_Structure_new
  Left = 365
  Top = 164
  BorderWidth = 3
  Caption = 'dlg_Structure_new'
  ClientHeight = 837
  ClientWidth = 1065
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 30
    Width = 1065
    Height = 499
    ActivePage = TabSheet_Tables
    Align = alTop
    TabOrder = 0
    object TabSheet8: TTabSheet
      Caption = 'Projects'
      ImageIndex = 12
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid11: TcxGrid
        Left = 0
        Top = 0
        Width = 473
        Height = 471
        Align = alLeft
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView9: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Projects
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBTableView9name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 301
          end
        end
        object cxGridLevel12: TcxGridLevel
          GridView = cxGridDBTableView9
        end
      end
    end
    object TabSheet_Tables: TTabSheet
      Caption = 'Tables'
      object cxGrid_Tables1: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 209
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGrid_Tables1DBTableView_Table: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Tables
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGrid_Tables1DBTableView_TableColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'SCHEMA'
            Width = 69
          end
          object cxGrid_Tables1DBTableView_Tablename: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            Width = 150
          end
          object cxGrid_Tables1DBTableView_Tablechecked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 78
          end
          object cxGrid_Tables1DBTableView_TableDBColumn1_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGrid_Tables1DBTableView_TableDBColumn1_Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 93
          end
          object cxGrid_Tables1DBTableView_TableDBColumn1_is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 79
          end
          object cxGrid_Tables1DBTableView_TableColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
            Width = 128
          end
          object cxGrid_Tables1DBTableView_TableColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
            Width = 140
          end
          object col_Owner1: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
          end
        end
        object cxGrid_Tables1Level1: TcxGridLevel
          GridView = cxGrid_Tables1DBTableView_Table
          Options.DetailTabsPosition = dtpTop
        end
      end
      object cxGrid2: TcxGrid
        Left = 0
        Top = 264
        Width = 1057
        Height = 207
        Align = alBottom
        TabOrder = 1
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        RootLevelOptions.DetailTabsPosition = dtpTop
        object cxGrid2DBTableView_Columns: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_TableFields
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = cxGrid2DBTableView_Columnsname
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView_Columnsname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 44
          end
          object cxGrid2DBTableView_Columnstype: TcxGridDBColumn
            DataBinding.FieldName = 'type'
            Width = 59
          end
          object cxGrid2DBTableView_ColumnsLength: TcxGridDBColumn
            DataBinding.FieldName = 'Length'
            Width = 51
          end
          object cxGrid2DBTableView_ColumnsVariable: TcxGridDBColumn
            DataBinding.FieldName = 'Variable'
          end
          object cxGrid2DBTableView_ColumnsIsNullable: TcxGridDBColumn
            DataBinding.FieldName = 'IsNullable'
          end
          object cxGrid2DBTableView_Columnsdefault_name: TcxGridDBColumn
            DataBinding.FieldName = 'default_name'
            Width = 70
          end
          object cxGrid2DBTableView_Columnsdefault_content: TcxGridDBColumn
            DataBinding.FieldName = 'default_content'
            Width = 80
          end
          object cxGrid2DBTableView_ColumnsIDENTITY: TcxGridDBColumn
            DataBinding.FieldName = 'IDENTITY'
          end
          object cxGrid2DBTableView_ColumnsIsComputed: TcxGridDBColumn
            DataBinding.FieldName = 'IsComputed'
          end
          object cxGrid2DBTableView_Columnscomputed_text: TcxGridDBColumn
            DataBinding.FieldName = 'computed_text'
            Width = 81
          end
          object cxGrid2DBTableView_Columnsis_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
        end
        object cxGrid2DBTableView_Index: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Indexes
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Kind = skCount
              Column = cxGrid2DBTableView_Indexname
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView_IndexSCHEMA: TcxGridDBColumn
            DataBinding.FieldName = 'SCHEMA'
            Width = 66
          end
          object cxGrid2DBTableView_Indexname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 92
          end
          object cxGrid2DBTableView_Indexdescription: TcxGridDBColumn
            DataBinding.FieldName = 'description'
          end
          object cxGrid2DBTableView_Indexchecked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
          end
          object cxGrid2DBTableView_Indexis_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGrid2DBTableView_Indexcontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
          end
          object cxGrid2DBTableView_Indexis_primary_key: TcxGridDBColumn
            DataBinding.FieldName = 'is_primary_key'
          end
          object cxGrid2DBTableView_Indexis_unique_constraint: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique_constraint'
          end
          object cxGrid2DBTableView_Indexis_unique: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique'
          end
          object cxGrid2DBTableView_Indexindex_keys: TcxGridDBColumn
            DataBinding.FieldName = 'index_keys'
            Width = 59
          end
          object cxGrid2DBTableView_Indexindex_description: TcxGridDBColumn
            DataBinding.FieldName = 'index_description'
            Width = 88
          end
          object cxGrid2DBTableView_Indexindex_type: TcxGridDBColumn
            DataBinding.FieldName = 'index_type'
            Width = 69
          end
          object cxGrid2DBTableView_Indexpriority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGrid2DBTableView_Indexsql_create: TcxGridDBColumn
            DataBinding.FieldName = 'sql_create'
          end
          object cxGrid2DBTableView_Indexsql_drop: TcxGridDBColumn
            DataBinding.FieldName = 'sql_drop'
          end
          object cxGrid2DBTableView_IndexDefinition: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
          end
        end
        object cxGrid2DBTableView3: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_CK
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView3SCHEMA: TcxGridDBColumn
            DataBinding.FieldName = 'SCHEMA'
            Width = 80
          end
          object cxGrid2DBTableView3name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 134
          end
          object cxGrid2DBTableView3description: TcxGridDBColumn
            DataBinding.FieldName = 'description'
          end
          object cxGrid2DBTableView3checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
          end
          object cxGrid2DBTableView3is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGrid2DBTableView3content: TcxGridDBColumn
            DataBinding.FieldName = 'content'
          end
          object cxGrid2DBTableView3priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGrid2DBTableView3sql_create: TcxGridDBColumn
            DataBinding.FieldName = 'sql_create'
            Width = 99
          end
          object cxGrid2DBTableView3sql_drop: TcxGridDBColumn
            DataBinding.FieldName = 'sql_drop'
            Width = 117
          end
          object cxGrid2DBTableView3Definition: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
          end
        end
        object cxGrid2DBTableView4: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Triggers
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView4SCHEMA: TcxGridDBColumn
            DataBinding.FieldName = 'SCHEMA'
            Width = 89
          end
          object cxGrid2DBTableView4name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 55
          end
          object cxGrid2DBTableView4description: TcxGridDBColumn
            DataBinding.FieldName = 'description'
          end
          object cxGrid2DBTableView4checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
          end
          object cxGrid2DBTableView4is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGrid2DBTableView4content: TcxGridDBColumn
            DataBinding.FieldName = 'content'
          end
          object cxGrid2DBTableView4priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGrid2DBTableView4sql_create: TcxGridDBColumn
            DataBinding.FieldName = 'sql_create'
          end
          object cxGrid2DBTableView4sql_drop: TcxGridDBColumn
            DataBinding.FieldName = 'sql_drop'
          end
          object cxGrid2DBTableView4Definition: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
          end
        end
        object cxGrid2Level1: TcxGridLevel
          Caption = 'columns'
          GridView = cxGrid2DBTableView_Columns
        end
        object cxGrid2Level2: TcxGridLevel
          Caption = 'Index'
          GridView = cxGrid2DBTableView_Index
        end
        object cxGrid2Level3: TcxGridLevel
          Caption = 'CK'
          GridView = cxGrid2DBTableView3
        end
        object cxGrid2Level4: TcxGridLevel
          Caption = 'Trigger'
          GridView = cxGrid2DBTableView4
        end
      end
      object cxSplitter1: TcxSplitter
        Left = 0
        Top = 256
        Width = 1057
        Height = 8
        HotZoneClassName = 'TcxSimpleStyle'
        AlignSplitter = salBottom
        Control = cxGrid2
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Views'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter4: TSplitter
        Left = 0
        Top = 293
        Width = 1057
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1065
      end
      object cxGrid_view: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 249
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView_View: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Views
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView_Viewid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
            Options.Filtering = False
          end
          object cxGridDBTableView_ViewColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'schema'
            Width = 60
          end
          object cxGridDBTableView_Viewname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 194
          end
          object cxGridDBTableView_Viewcontent: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 116
          end
          object cxGridDBTableView_ViewSQLCode: TcxGridDBColumn
            DataBinding.FieldName = 'SQLCode'
            Visible = False
            Options.Editing = False
            Width = 105
          end
          object cxGridDBTableView_ViewDBColumn1_checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
          end
          object cxGridDBTableView_ViewDBColumn1_text: TcxGridDBColumn
            DataBinding.FieldName = 'text'
            PropertiesClassName = 'TcxMemoProperties'
            Properties.ScrollBars = ssBoth
            Options.Filtering = False
          end
          object cxGridDBTableView_ViewDBColumn1_Status: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 90
          end
          object cxGridDBTableView_ViewDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object col_xtype1: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_ViewDBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGridDBTableView_ViewColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object cxGridDBTableView_ViewColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
          object col_Owner11: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView_View
        end
      end
      object PageControl_view: TPageControl
        Left = 0
        Top = 296
        Width = 1057
        Height = 175
        ActivePage = TabSheet22
        Align = alBottom
        TabOrder = 1
        object TabSheet22: TTabSheet
          Caption = 'sql_create'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo11: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_create'
            DataSource = ds_Views
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet16: TTabSheet
          Caption = 'sql_drop'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo4: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Views
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet17: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo5: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Views
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'FK'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 471
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView_FK: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_FK
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView_FKid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView_FKname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 226
          end
          object cxGridDBTableView_FKtablename: TcxGridDBColumn
            DataBinding.FieldName = 'pk_table_name'
            Options.Editing = False
            Width = 121
          end
          object cxGridDBTableView_FKDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'pk_column_name'
            Width = 81
          end
          object cxGridDBTableView_FKpk_tablename: TcxGridDBColumn
            DataBinding.FieldName = 'fk_table_name'
            Options.Editing = False
            Width = 114
          end
          object cxGridDBTableView_FKDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'fk_column_name'
            Width = 138
          end
          object cxGridDBTableView_FKDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 79
          end
          object cxGridDBTableView_FKDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView_FKDBColumn5_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView_FK
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Triggers'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter5: TSplitter
        Left = 0
        Top = 293
        Width = 1057
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1065
      end
      object cxGrid_tr: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 241
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView_Tr: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Triggers_all
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView_Trtablename: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 124
          end
          object cxGridDBTableView_Trname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 1
            SortOrder = soAscending
            Width = 147
          end
          object cxGridDBTableView_Trcontent: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 119
          end
          object cxGridDBTableView_TrDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 93
          end
          object cxGridDBTableView_TrDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView_TrDBColumn3_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
            Visible = False
          end
          object cxGridDBTableView_TrColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object cxGridDBTableView_TrColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView_Tr
        end
      end
      object PageControl_tr: TPageControl
        Left = 0
        Top = 296
        Width = 1057
        Height = 175
        ActivePage = TabSheet21
        Align = alBottom
        TabOrder = 1
        object TabSheet21: TTabSheet
          Caption = 'sql_create'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo10: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_create'
            DataSource = ds_Triggers
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet11: TTabSheet
          Caption = 'sql_drop'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo6: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Triggers
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet18: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo7: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Triggers
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'SP'
      ImageIndex = 5
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter2: TSplitter
        Left = 0
        Top = 293
        Width = 1057
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1065
      end
      object cxGrid_SP: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 249
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView_SP: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_SP
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView_SPColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'schema'
            Width = 69
          end
          object cxGridDBTableView_SPname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 287
          end
          object cxGridDBTableView_SPcontent: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
          end
          object cxGridDBTableView_SPDBColumn1_checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 101
          end
          object cxGridDBTableView_SPstatus_id: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Visible = False
            Width = 86
          end
          object cxGridDBTableView_SPDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 87
          end
          object cxGridDBTableView_SPDBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_SPDBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGridDBTableView_SPColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object cxGridDBTableView_SPColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
          object col_Owner2: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
          end
        end
        object cxGridLevel4: TcxGridLevel
          GridView = cxGridDBTableView_SP
        end
      end
      object PageControl_SP: TPageControl
        Left = 0
        Top = 296
        Width = 1057
        Height = 175
        ActivePage = TabSheet12
        Align = alBottom
        TabOrder = 1
        object TabSheet12: TTabSheet
          Caption = 'sql_drop'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo1: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_SP
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet13: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo_SP: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_SP
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Func'
      ImageIndex = 8
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter1: TSplitter
        Left = 0
        Top = 293
        Width = 1057
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1065
      end
      object cxGrid_func: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 225
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView_func: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Func
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBColumn7: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 270
          end
          object cxGridDBColumn8: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
          end
          object cxGridDBColumn9: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 101
          end
          object cxGridDBTableView_funcDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 86
          end
          object cxGridDBTableView_funcDBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_funcDBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
          object cxGridDBTableView_funcColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
          object cxGridDBTableView_funcColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object col_Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Width = 86
          end
        end
        object cxGridLevel8: TcxGridLevel
          GridView = cxGridDBTableView_func
        end
      end
      object PageControl_func: TPageControl
        Left = 0
        Top = 296
        Width = 1057
        Height = 175
        ActivePage = TabSheet23
        Align = alBottom
        TabOrder = 1
        object TabSheet23: TTabSheet
          Caption = 'sql_create'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo12: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_create'
            DataSource = ds_Func
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet14: TTabSheet
          Caption = 'sql_drop'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo2: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Func
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet15: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo3: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Func
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet_CK: TTabSheet
      Caption = 'CK'
      ImageIndex = 6
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid6: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 471
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView_CK: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_CK_all
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGridDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            Width = 121
          end
          object cxGridDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 226
          end
          object cxGridDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            Options.Editing = False
            Width = 355
          end
          object cxGridDBTableView_CKDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView_CKDBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
        end
        object cxGridLevel5: TcxGridLevel
          GridView = cxGridDBTableView_CK
        end
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'indexes'
      ImageIndex = 10
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Splitter3: TSplitter
        Left = 0
        Top = 293
        Width = 1057
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1065
      end
      object cxGrid_Index: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 257
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView_Index: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_indexes_all
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.Indicator = True
          object cxGridDBTableView_Indexid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
          end
          object cxGridDBTableView_Indextablename: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Width = 179
          end
          object cxGridDBTableView_Indexname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 201
          end
          object cxGridDBTableView_Indexindex_description: TcxGridDBColumn
            DataBinding.FieldName = 'sql_create'
            Width = 292
          end
          object cxGridDBTableView_Indexxtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_Indexis_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView_Indexindex_type: TcxGridDBColumn
            DataBinding.FieldName = 'index_type'
            Visible = False
            Width = 257
          end
          object cxGridDBTableView_IndexColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            Width = 258
          end
        end
        object cxGridLevel10: TcxGridLevel
          GridView = cxGridDBTableView_Index
        end
      end
      object PageControl_index: TPageControl
        Left = 0
        Top = 296
        Width = 1057
        Height = 175
        ActivePage = TabSheet19
        Align = alBottom
        TabOrder = 1
        object TabSheet19: TTabSheet
          Caption = 'sql_drop'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo8: TDBMemo
            Left = 0
            Top = 0
            Width = 761
            Height = 147
            Align = alLeft
            DataField = 'sql_drop'
            DataSource = ds_Indexes
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
        object TabSheet20: TTabSheet
          Caption = 'Definition'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBMemo9: TDBMemo
            Left = 0
            Top = 0
            Width = 825
            Height = 147
            Align = alLeft
            DataField = 'Definition'
            DataSource = ds_Indexes
            ScrollBars = ssBoth
            TabOrder = 0
          end
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'OBJECTS'
      ImageIndex = 7
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid_OBJECTS: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 161
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView6: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_OBJECTS
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBTableView6id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            SortIndex = 0
            SortOrder = soAscending
          end
          object cxGridDBTableView6checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 80
          end
          object cxGridDBTableView6name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            Width = 189
          end
          object cxGridDBTableView6display_name: TcxGridDBColumn
            DataBinding.FieldName = 'display_name'
            Visible = False
          end
          object cxGridDBTableView6table_name: TcxGridDBColumn
            DataBinding.FieldName = 'table_name'
            Visible = False
          end
          object cxGridDBTableView6folder_name: TcxGridDBColumn
            DataBinding.FieldName = 'folder_name'
            Visible = False
          end
          object cxGridDBTableView6view_name: TcxGridDBColumn
            DataBinding.FieldName = 'view_name'
            Visible = False
          end
        end
        object cxGridLevel6: TcxGridLevel
          GridView = cxGridDBTableView6
        end
      end
    end
    object TabSheet_Owner: TTabSheet
      Caption = 'Owner'
      ImageIndex = 8
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid7: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 455
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView7: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Owner
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            Width = 101
          end
          object cxGridDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Options.Editing = False
            Width = 80
          end
        end
        object cxGridLevel7: TcxGridLevel
          GridView = cxGridDBTableView7
        end
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'DatabaseInfo'
      ImageIndex = 9
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid9: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 471
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView4: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_DatabaseInfo
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object col_product_name: TcxGridDBColumn
            DataBinding.FieldName = 'product_name'
            Width = 141
          end
          object cxGridDBTableView4version: TcxGridDBColumn
            DataBinding.FieldName = 'version'
            Width = 114
          end
        end
        object cxGridLevel9: TcxGridLevel
          GridView = cxGridDBTableView4
        end
      end
    end
    object TabSheet_OBJECTS_main: TTabSheet
      Caption = 'OBJECTS_main'
      ImageIndex = 11
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid_OBJECTS_main: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 181
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_OBJECTS_main
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGridDBTableView2id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGridDBTableView2name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 161
          end
          object cxGridDBTableView2enabled: TcxGridDBColumn
            DataBinding.FieldName = 'enabled'
            Width = 84
          end
          object cxGridDBTableView2Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Width = 136
          end
        end
        object cxGridLevel11: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object TabSheet11_udp: TTabSheet
      Caption = 'UDP'
      ImageIndex = 13
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid_UDP: TcxGrid
        Left = 0
        Top = 0
        Width = 1057
        Height = 225
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView10_udp: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_UDT
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGridDBColumn11: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 124
          end
          object cxGridDBColumn12: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 1
            SortOrder = soAscending
            Width = 206
          end
          object cxGridDBColumn13: TcxGridDBColumn
            DataBinding.FieldName = 'Definition'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 222
          end
          object cxGridDBColumn14: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Width = 93
          end
          object cxGridDBColumn15: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 45
          end
          object cxGridDBColumn16: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object col_Owner3: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
          end
        end
        object cxGridLevel13: TcxGridLevel
          GridView = cxGridDBTableView10_udp
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1065
    Height = 30
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 6
      Top = 6
      Width = 33
      Height = 13
      Caption = 'Project'
    end
    object DBEdit1: TDBEdit
      Left = 49
      Top = 2
      Width = 168
      Height = 21
      DataField = 'name'
      DataSource = ds_Projects
      ParentColor = True
      ReadOnly = True
      TabOrder = 0
    end
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 272
      Top = 2
      Width = 249
      Height = 21
      KeyField = 'id'
      ListField = 'name'
      ListSource = ds_Projects
      TabOrder = 1
    end
  end
  object ds_Tables: TDataSource
    DataSet = qry_Tables
    Left = 24
    Top = 600
  end
  object ds_Views: TDataSource
    DataSet = qry_Views
    Left = 528
    Top = 600
  end
  object ds_CK: TDataSource
    DataSet = qry_CK
    Left = 200
    Top = 600
  end
  object ds_FK: TDataSource
    DataSet = qry_FK
    Left = 584
    Top = 600
  end
  object ds_Triggers: TDataSource
    DataSet = qry_Triggers
    Left = 344
    Top = 600
  end
  object ds_Indexes: TDataSource
    DataSet = qry_indexes
    Left = 264
    Top = 600
  end
  object ds_Owner: TDataSource
    DataSet = t_Owner
    Left = 824
    Top = 592
  end
  object ds_SP: TDataSource
    DataSet = qry_SP
    Left = 728
    Top = 600
  end
  object t_Owner: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'Owner'
    Left = 824
    Top = 552
  end
  object ds_TableFields: TDataSource
    DataSet = qry_table_fields
    Left = 120
    Top = 600
  end
  object MDBConnection: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=W:\RP' +
      'LS_DB update\bin\install_db.mdb;Mode=Share Deny None;Persist Sec' +
      'urity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry' +
      ' Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;' +
      'Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk ' +
      'Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Databas' +
      'e Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:E' +
      'ncrypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=Fal' +
      'se;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=' +
      'False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 16
    Top = 668
  end
  object ds_OBJECTS: TDataSource
    DataSet = t_OBJECTS
    Left = 120
    Top = 712
  end
  object t_OBJECTS: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = '_OBJECTS'
    Left = 120
    Top = 664
  end
  object ds_Func: TDataSource
    DataSet = qry_Func
    Left = 664
    Top = 600
  end
  object FormStorage2: TFormStorage
    IniFileName = 'Software\Onega\Install_DB\'
    UseRegistry = True
    StoredProps.Strings = (
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 896
    Top = 672
  end
  object ds_DatabaseInfo: TDataSource
    DataSet = t_Database_Info
    Left = 624
    Top = 720
  end
  object t_Database_Info: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Database_Info'
    Left = 624
    Top = 664
  end
  object ds_OBJECTS_main: TDataSource
    DataSet = t_OBJECTS_main
    Left = 920
    Top = 592
  end
  object t_OBJECTS_main: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = '_OBJECTS_main'
    Left = 920
    Top = 552
  end
  object ds_Projects: TDataSource
    DataSet = t_sys_Projects
    Left = 736
    Top = 720
  end
  object t_sys_Projects: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'Projects'
    Left = 736
    Top = 664
  end
  object qry_Tables: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'name'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = 'link'
      end>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'u'#39
      ' ')
    Left = 24
    Top = 552
  end
  object qry_CK: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    DataSource = ds_Tables
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Value = 317356
      end>
    SQL.Strings = (
      'select * from sys_objects'
      ''
      'where xtype='#39'ck'#39
      ''
      ''
      'and   object_id =:id')
    Left = 200
    Top = 552
  end
  object qry_Triggers: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    DataSource = ds_Tables
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Value = 317356
      end>
    SQL.Strings = (
      'select * from sys_objects   '
      'where xtype='#39'tr'#39'   '
      ''
      'and  object_id =:id')
    Left = 344
    Top = 552
  end
  object qry_indexes: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    DataSource = ds_Tables
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftInteger
        NumericScale = 255
        Precision = 255
        Value = 317356
      end>
    SQL.Strings = (
      'select * from sys_objects   '
      'where xtype='#39'index'#39'   '
      ''
      ''
      'and  object_id =:id')
    Left = 264
    Top = 552
  end
  object qry_Views: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'view'#39)
    Left = 528
    Top = 552
  end
  object qry_FK: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'fk'#39
      '')
    Left = 592
    Top = 552
  end
  object qry_Func: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'fn'#39)
    Left = 664
    Top = 552
  end
  object qry_SP: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'sp'#39' ')
    Left = 728
    Top = 552
  end
  object qry_table_fields: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    DataSource = ds_Tables
    Parameters = <
      item
        Name = 'id'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = '317356'
      end>
    SQL.Strings = (
      'select * from  sys_objects_table_columns'
      'where object_id =:id')
    Left = 120
    Top = 552
  end
  object ds_UDT: TDataSource
    DataSet = qry_UDT
    Left = 528
    Top = 720
  end
  object qry_UDT: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'ck'#39' ')
    Left = 528
    Top = 672
  end
  object ds_Triggers_all: TDataSource
    DataSet = qry_Triggers_all
    Left = 224
    Top = 712
  end
  object qry_Triggers_all: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'tr'#39' ')
    Left = 224
    Top = 664
  end
  object ds_indexes_all: TDataSource
    DataSet = qry_indexes_all
    Left = 320
    Top = 712
  end
  object qry_indexes_all: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'index'#39' '
      ''
      '')
    Left = 320
    Top = 664
  end
  object ds_CK_all: TDataSource
    DataSet = qry_CK_all
    Left = 400
    Top = 712
  end
  object qry_CK_all: TADOQuery
    Connection = MDBConnection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from sys_objects'
      'where xtype='#39'ck'#39
      ''
      ''
      '')
    Left = 400
    Top = 664
  end
end
