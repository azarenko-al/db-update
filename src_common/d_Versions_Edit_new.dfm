object dlg_Versions_new: Tdlg_Versions_new
  Left = 551
  Top = 305
  Caption = #1042#1077#1088#1089#1080#1080' (new)'
  ClientHeight = 489
  ClientWidth = 1103
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 454
    Width = 1103
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 0
    object Panel2: TPanel
      Left = 1008
      Top = 0
      Width = 95
      Height = 35
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
    end
    object Button1: TButton
      Left = 5
      Top = 8
      Width = 92
      Height = 23
      Action = act_Exec_SQL
      TabOrder = 1
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 649
    Height = 454
    ActivePage = TabSheet2
    Align = alLeft
    TabOrder = 1
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object Splitter2: TSplitter
        Left = 425
        Top = 33
        Width = 5
        Height = 393
        ResizeStyle = rsUpdate
        ExplicitHeight = 384
      end
      object cxDBTreeList11: TcxDBTreeList
        Left = 0
        Top = 33
        Width = 425
        Height = 393
        Align = alLeft
        Bands = <
          item
          end>
        DataController.DataSource = DataSource1
        DataController.ParentField = 'parent_id'
        DataController.KeyField = 'id'
        DragMode = dmAutomatic
        LookAndFeel.Kind = lfOffice11
        Navigator.Buttons.CustomButtons = <>
        OptionsBehavior.ImmediateEditor = False
        OptionsData.Appending = True
        OptionsData.Inserting = True
        OptionsSelection.HideFocusRect = False
        OptionsView.GridLines = tlglBoth
        OptionsView.Indicator = True
        RootValue = -1
        Styles.OnGetContentStyle = cxDBTreeList11StylesGetContentStyle
        TabOrder = 0
        OnDragOver = cxDBTreeList11DragOver
        object cxDBTreeList1name: TcxDBTreeListColumn
          DataBinding.FieldName = 'name'
          Width = 100
          Position.ColIndex = 3
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1caption: TcxDBTreeListColumn
          DataBinding.FieldName = 'caption'
          Width = 124
          Position.ColIndex = 2
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1enabled: TcxDBTreeListColumn
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssInactive
          DataBinding.FieldName = 'enabled'
          Width = 100
          Position.ColIndex = 0
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1priority: TcxDBTreeListColumn
          DataBinding.FieldName = 'priority'
          Width = 100
          Position.ColIndex = 4
          Position.RowIndex = 0
          Position.BandIndex = 0
          SortOrder = soAscending
          SortIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1created_date: TcxDBTreeListColumn
          Visible = False
          DataBinding.FieldName = 'created_date'
          Width = 100
          Position.ColIndex = 7
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1modify_date: TcxDBTreeListColumn
          Visible = False
          DataBinding.FieldName = 'modify_date'
          Width = 100
          Position.ColIndex = 8
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1is_run_before_all: TcxDBTreeListColumn
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssInactive
          DataBinding.FieldName = 'is_run_before_all'
          Width = 100
          Position.ColIndex = 1
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1id: TcxDBTreeListColumn
          Visible = False
          DataBinding.FieldName = 'id'
          Width = 100
          Position.ColIndex = 5
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
        object cxDBTreeList1parent_id: TcxDBTreeListColumn
          Visible = False
          DataBinding.FieldName = 'parent_id'
          Width = 100
          Position.ColIndex = 6
          Position.RowIndex = 0
          Position.BandIndex = 0
          Summary.FooterSummaryItems = <>
          Summary.GroupFooterSummaryItems = <>
        end
      end
      object ToolBar2: TToolBar
        Left = 0
        Top = 0
        Width = 641
        Height = 33
        BorderWidth = 1
        ButtonHeight = 25
        ButtonWidth = 58
        Caption = 'ToolBar1'
        TabOrder = 1
        object DBNavigator2: TDBNavigator
          Left = 0
          Top = 0
          Width = 135
          Height = 25
          VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel, nbRefresh]
          TabOrder = 0
        end
        object ToolButton2: TToolButton
          Left = 135
          Top = 0
          Width = 8
          Caption = 'ToolButton2'
          ImageIndex = 1
          Style = tbsSeparator
        end
        object Button4: TButton
          Left = 143
          Top = 0
          Width = 130
          Height = 25
          Action = act_Record_Add
          TabOrder = 1
        end
        object Button3: TButton
          Left = 273
          Top = 0
          Width = 75
          Height = 25
          Caption = 'Button3'
          TabOrder = 2
          OnClick = Button3Click
        end
      end
      object DBMemo1: TDBMemo
        Left = 496
        Top = 33
        Width = 145
        Height = 393
        Align = alRight
        DataField = 'content'
        DataSource = DataSource1
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu
        ScrollBars = ssBoth
        TabOrder = 2
        WordWrap = False
      end
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredProps.Strings = (
      'cxDBTreeList11.Width')
    StoredValues = <>
    Left = 1000
    Top = 42
  end
  object PopupMenu: TPopupMenu
    Left = 1000
    Top = 164
    object N1: TMenuItem
      Action = act_Exec_SQL
    end
    object SQL1: TMenuItem
      Action = act_Exec_All_SQL
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object SQL4: TMenuItem
      Action = act_Exec_SQL_test
    end
    object actExecAllSQLtest1: TMenuItem
      Action = act_Exec_All_SQL_test
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object LoadFromFile3: TMenuItem
      Action = act_LoadFromFile
    end
    object SaveAs1: TMenuItem
      Action = act_FileSaveAs
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object actTestloaddbframework1: TMenuItem
      Action = act_Test_load_SQL
      Caption = 'act_Test_load_SQL'
    end
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 1000
    Top = 108
    object act_Exec_SQL: TAction
      Caption = 'Exec_SQL'
      OnExecute = act_Exec_SQLExecute
    end
    object act_Exec_SQL_test: TAction
      Caption = 'Exec_SQL_test'
      OnExecute = act_Exec_SQLExecute
    end
    object act_Exec_All_SQL: TAction
      Caption = 'Exec_All_SQL'
      OnExecute = act_Exec_SQLExecute
    end
    object act_Exec_All_SQL_test: TAction
      Caption = 'Exec_All_SQL_test'
      OnExecute = act_Exec_SQLExecute
    end
    object act_LoadFromFile: TFileOpen
      Category = 'File'
      Caption = 'Load From File'
      Dialog.DefaultExt = 'sql'
      Dialog.Filter = '*.sql|*.sql'
      Hint = 'Open|Opens an existing file'
      ImageIndex = 7
      ShortCut = 16463
      OnAccept = act_LoadFromFileAccept
    end
    object act_FileSaveAs: TFileSaveAs
      Category = 'File'
      Caption = 'Save &As...'
      Dialog.DefaultExt = 'sql'
      Dialog.Filter = '*.sql|*.sql'
      Hint = 'Save As|Saves the active file with a new name'
      ImageIndex = 30
      OnAccept = act_FileSaveAsAccept
    end
    object act_Test_load_SQL: TAction
      Caption = 'act_Test_load_db_framework'
      OnExecute = act_Test_load_SQLExecute
    end
    object act_Record_Add: TAction
      Caption = 'act_Record_Add'
      OnExecute = act_Exec_SQLExecute
    end
  end
  object ADOTable1: TADOTable
    Connection = ADOConnection1
    OnNewRecord = ADOTable1NewRecord
    TableName = 'view_version_updates1'
    Left = 888
    Top = 40
  end
  object DataSource1: TDataSource
    DataSet = ADOTable1
    Left = 888
    Top = 128
  end
  object MainMenu1: TMainMenu
    Left = 1000
    Top = 224
    object dfg1: TMenuItem
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      object SQL2: TMenuItem
        Action = act_Exec_SQL
      end
      object SQL3: TMenuItem
        Action = act_Exec_All_SQL
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object ExecSQLtest1: TMenuItem
        Action = act_Exec_SQL_test
      end
      object ExecAllSQLtest1: TMenuItem
        Action = act_Exec_All_SQL_test
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object actLoadFromFile2: TMenuItem
        Caption = 'Load From File'
        OnClick = act_Exec_SQLExecute
      end
      object SaveToFile1: TMenuItem
        Caption = 'Save To File'
        OnClick = act_Exec_SQLExecute
      end
      object LoadFromFile2: TMenuItem
        Action = act_LoadFromFile
      end
    end
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=W:\RPLS_DB tools\_b' +
      'in\install_db.mdb;Persist Security Info=False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 888
    Top = 220
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 760
    Top = 120
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
  end
end
