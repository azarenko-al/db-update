object dmExportStructure: TdmExportStructure
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 799
  Top = 392
  Height = 460
  Width = 405
  object qry_Temp: TADOQuery
    Parameters = <>
    Left = 32
    Top = 16
  end
  object ADOTable1: TADOTable
    Left = 32
    Top = 88
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM INFORMATION_SCHEMA.views        '
      ' WHERE  (table_name NOT LIKE '#39'[_]%'#39')     AND'
      '        (table_name <> '#39#39'sysconstraints'#39#39') AND'
      '        (table_name <> '#39#39'syssegments'#39#39#9')      '
      ' ORDER BY table_name                          ')
    Left = 32
    Top = 152
  end
  object t_Objects: TADOTable
    Left = 168
    Top = 16
  end
  object t_Objects_columns: TADOTable
    Left = 168
    Top = 72
  end
  object t_schemas_allowed: TADOTable
    Left = 168
    Top = 160
  end
  object q_Depends: TADOQuery
    Parameters = <>
    Left = 24
    Top = 308
  end
  object q_Tables: TADOQuery
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM INFORMATION_SCHEMA.views        '
      ' WHERE  (table_name NOT LIKE '#39'[_]%'#39')     AND'
      '        (table_name <> '#39#39'sysconstraints'#39#39') AND'
      '        (table_name <> '#39#39'syssegments'#39#39#9')      '
      ' ORDER BY table_name                          ')
    Left = 176
    Top = 280
  end
  object t_Depands: TADOTable
    Left = 264
    Top = 16
  end
end
