unit d_Structure;

interface

uses
  dm_MDB,
  u_db,
  u_func,

  d_db_ViewDataset,

  StdCtrls, ExtCtrls,  Classes, Controls, Forms, SysUtils,
  DB, ADODB,  cxGridDBTableView, cxGridLevel, Dialogs,
  cxGrid, ComCtrls, rxPlacemnt,
  DBCtrls, Mask, cxGridCustomTableView, cxGridTableView, cxClasses,
  cxControls, cxGridCustomView, cxSplitter, Grids, DBGrids, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData,
  cxTextEdit, cxDBLookupComboBox, cxCheckBox, cxMemo, dxSkinsCore,
  dxSkinsDefaultPainters;


type

  Tdlg_Structure_new = class(TForm)
    PageControl1: TPageControl;

    ds_CK: TDataSource;
    ds_FK: TDataSource;
    ds_Indexes: TDataSource;
    ds_Tables: TDataSource;
    ds_Triggers: TDataSource;
    ds_Views: TDataSource;
    TabSheet3: TTabSheet;
    cxGrid3: TcxGrid;

    cxGrid_Tables1: TcxGrid;

    cxGridDBTableView_FK: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView_FKid: TcxGridDBColumn;
    cxGridDBTableView_FKname: TcxGridDBColumn;
    cxGridDBTableView_FKtablename: TcxGridDBColumn;
    cxGridDBTableView_FKpk_tablename: TcxGridDBColumn;
    TabSheet4: TTabSheet;
    cxGrid_tr: TcxGrid;
    cxGridDBTableView_Tr: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView_Trtablename: TcxGridDBColumn;
    cxGridDBTableView_Trname: TcxGridDBColumn;
    cxGridDBTableView_Trcontent: TcxGridDBColumn;
    ds_Owner: TDataSource;
    cxGrid_Tables1DBTableView_TableDBColumn1_Owner: TcxGridDBColumn;
    cxGridDBTableView_ViewDBColumn1_Status: TcxGridDBColumn;
    TabSheet6: TTabSheet;
    cxGrid_SP: TcxGrid;
    cxGridDBTableView_SP: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    ds_SP: TDataSource;
    cxGridDBTableView_SPname: TcxGridDBColumn;
    cxGridDBTableView_SPcontent: TcxGridDBColumn;
    cxGridDBTableView_SPstatus_id: TcxGridDBColumn;
    t_Owner: TADOTable;
    cxGrid_Tables1DBTableView_TableDBColumn1_xtype: TcxGridDBColumn;
    ds_TableFields: TDataSource;
    MDBConnection: TADOConnection;
    cxGridDBTableView_ViewDBColumn1_checked: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn1_checked: TcxGridDBColumn;
    TabSheet_CK: TTabSheet;
    cxGrid6: TcxGrid;
    cxGridDBTableView_CK: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridLevel5: TcxGridLevel;
    cxGridDBTableView_ViewDBColumn1_text: TcxGridDBColumn;
    TabSheet7: TTabSheet;
    cxGrid_OBJECTS: TcxGrid;
    cxGridDBTableView6: TcxGridDBTableView;
    cxGridLevel6: TcxGridLevel;
    ds_OBJECTS: TDataSource;
    t_OBJECTS: TADOTable;
    cxGridDBTableView6id: TcxGridDBColumn;
    cxGridDBTableView6name: TcxGridDBColumn;
    cxGridDBTableView6display_name: TcxGridDBColumn;
    cxGridDBTableView6table_name: TcxGridDBColumn;
    cxGridDBTableView6folder_name: TcxGridDBColumn;
    cxGridDBTableView6view_name: TcxGridDBColumn;
    cxGridDBTableView6checked: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn2: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn3: TcxGridDBColumn;
    cxGridDBTableView_TrDBColumn1: TcxGridDBColumn;
    TabSheet_Owner: TTabSheet;
    cxGrid7: TcxGrid;
    cxGridDBTableView7: TcxGridDBTableView;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridLevel7: TcxGridLevel;
    TabSheet5: TTabSheet;
    cxGrid_func: TcxGrid;
    cxGridDBTableView_func: TcxGridDBTableView;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    col_Owner: TcxGridDBColumn;
    cxGridLevel8: TcxGridLevel;
    ds_Func: TDataSource;
    cxGridDBTableView_funcDBColumn1: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn1: TcxGridDBColumn;
    cxGrid_Tables1DBTableView_TableDBColumn1_is_exists: TcxGridDBColumn;
    cxGridDBTableView_ViewDBColumn1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn4: TcxGridDBColumn;
    cxGridDBTableView_TrDBColumn2: TcxGridDBColumn;
    cxGridDBTableView_CKDBColumn1: TcxGridDBColumn;
    Splitter1: TSplitter;
    Splitter4: TSplitter;
    FormStorage2: TFormStorage;
    col_xtype1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn5_xtype: TcxGridDBColumn;
    cxGridDBTableView_TrDBColumn3_xtype: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn2_xtype: TcxGridDBColumn;
    cxGridDBTableView_CKDBColumn2_xtype: TcxGridDBColumn;
    cxGridDBTableView_funcDBColumn2_xtype: TcxGridDBColumn;
    TabSheet9: TTabSheet;
    ds_DatabaseInfo: TDataSource;
    t_Database_Info: TADOTable;
    cxGrid9: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel9: TcxGridLevel;
    TabSheet10: TTabSheet;
    cxGrid_Index: TcxGrid;
    cxGridDBTableView_Index: TcxGridDBTableView;
    cxGridLevel10: TcxGridLevel;
    cxGridDBTableView_Indexid: TcxGridDBColumn;
    cxGridDBTableView_Indextablename: TcxGridDBColumn;
    cxGridDBTableView_Indexname: TcxGridDBColumn;
    cxGridDBTableView_Indexindex_description: TcxGridDBColumn;
    cxGridDBTableView_Indexxtype: TcxGridDBColumn;
    cxGridDBTableView_Indexis_exists: TcxGridDBColumn;
    cxGridDBTableView_Indexindex_type: TcxGridDBColumn;
    cxGridDBTableView_ViewDBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView_funcDBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView4version: TcxGridDBColumn;
    col_product_name: TcxGridDBColumn;
    TabSheet_OBJECTS_main: TTabSheet;
    cxGrid_OBJECTS_main: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel11: TcxGridLevel;
    ds_OBJECTS_main: TDataSource;
    t_OBJECTS_main: TADOTable;
    cxGridDBTableView2id: TcxGridDBColumn;
    cxGridDBTableView2name: TcxGridDBColumn;
    cxGridDBTableView2enabled: TcxGridDBColumn;
    cxGridDBTableView2Owner: TcxGridDBColumn;
    TabSheet8: TTabSheet;
    cxGrid11: TcxGrid;
    cxGridDBTableView9: TcxGridDBTableView;
    cxGridLevel12: TcxGridLevel;
    ds_Projects: TDataSource;
    t_sys_Projects: TADOTable;
    cxGridDBTableView9name: TcxGridDBColumn;
    qry_Tables: TADOQuery;
    qry_CK: TADOQuery;
    qry_Triggers: TADOQuery;
    qry_indexes: TADOQuery;
    qry_Views: TADOQuery;
    qry_FK: TADOQuery;
    qry_Func: TADOQuery;
    qry_SP: TADOQuery;
    qry_table_fields: TADOQuery;
    Panel1: TPanel;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    TabSheet11_udp: TTabSheet;
    ds_UDT: TDataSource;
    qry_UDT: TADOQuery;
    cxGrid_UDP: TcxGrid;
    cxGridDBTableView10_udp: TcxGridDBTableView;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridLevel13: TcxGridLevel;
    cxGrid_Tables1DBTableView_TableColumn1: TcxGridDBColumn;
    cxGrid_Tables1DBTableView_TableColumn2: TcxGridDBColumn;
    cxGridDBTableView_ViewColumn1: TcxGridDBColumn;
    cxGridDBTableView_ViewColumn2: TcxGridDBColumn;
    cxGridDBTableView_TrColumn1: TcxGridDBColumn;
    cxGridDBTableView_TrColumn2: TcxGridDBColumn;
    cxGridDBTableView_SPColumn1: TcxGridDBColumn;
    cxGridDBTableView_SPColumn2: TcxGridDBColumn;
    cxGridDBTableView_funcColumn1: TcxGridDBColumn;
    cxGridDBTableView_funcColumn2: TcxGridDBColumn;
    PageControl_SP: TPageControl;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    DBMemo1: TDBMemo;
    Splitter2: TSplitter;
    DBMemo_SP: TDBMemo;
    PageControl_func: TPageControl;
    TabSheet14: TTabSheet;
    DBMemo2: TDBMemo;
    TabSheet15: TTabSheet;
    DBMemo3: TDBMemo;
    PageControl_view: TPageControl;
    TabSheet16: TTabSheet;
    DBMemo4: TDBMemo;
    TabSheet17: TTabSheet;
    DBMemo5: TDBMemo;
    cxGridDBTableView_IndexColumn1: TcxGridDBColumn;
    cxGrid_view: TcxGrid;
    Splitter5: TSplitter;
    PageControl_tr: TPageControl;
    TabSheet11: TTabSheet;
    DBMemo6: TDBMemo;
    TabSheet18: TTabSheet;
    DBMemo7: TDBMemo;
    PageControl_index: TPageControl;
    TabSheet19: TTabSheet;
    DBMemo8: TDBMemo;
    TabSheet20: TTabSheet;
    DBMemo9: TDBMemo;
    Splitter3: TSplitter;
    TabSheet21: TTabSheet;
    DBMemo10: TDBMemo;
    TabSheet22: TTabSheet;
    DBMemo11: TDBMemo;
    TabSheet23: TTabSheet;
    DBMemo12: TDBMemo;
    col_Owner1: TcxGridDBColumn;
    col_Owner11: TcxGridDBColumn;
    col_Owner2: TcxGridDBColumn;
    col_Owner3: TcxGridDBColumn;
    cxGridDBTableView_ViewColumn3: TcxGridDBColumn;
    cxGridDBTableView_SPColumn3: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBTableView_Columns: TcxGridDBTableView;
    cxGrid2Level2: TcxGridLevel;
    cxGrid2DBTableView_Index: TcxGridDBTableView;
    cxGrid2DBTableView_Columnsname: TcxGridDBColumn;
    cxGrid2DBTableView_Columnstype: TcxGridDBColumn;
    cxGrid2DBTableView_ColumnsLength: TcxGridDBColumn;
    cxGrid2DBTableView_ColumnsVariable: TcxGridDBColumn;
    cxGrid2DBTableView_ColumnsIsNullable: TcxGridDBColumn;
    cxGrid2DBTableView_Columnsdefault_name: TcxGridDBColumn;
    cxGrid2DBTableView_Columnsdefault_content: TcxGridDBColumn;
    cxGrid2DBTableView_ColumnsIDENTITY: TcxGridDBColumn;
    cxGrid2DBTableView_ColumnsIsComputed: TcxGridDBColumn;
    cxGrid2DBTableView_Columnscomputed_text: TcxGridDBColumn;
    cxGrid2DBTableView_Columnsis_exists: TcxGridDBColumn;
    cxGrid_Tables1DBTableView_TableColumn3: TcxGridDBColumn;
    cxGrid2Level3: TcxGridLevel;
    cxGrid2DBTableView3: TcxGridDBTableView;
    cxGrid2Level4: TcxGridLevel;
    cxGrid2DBTableView4: TcxGridDBTableView;
    cxGrid2DBTableView4SCHEMA: TcxGridDBColumn;
    cxGrid2DBTableView4name: TcxGridDBColumn;
    cxGrid2DBTableView4description: TcxGridDBColumn;
    cxGrid2DBTableView4checked: TcxGridDBColumn;
    cxGrid2DBTableView4is_exists: TcxGridDBColumn;
    cxGrid2DBTableView4content: TcxGridDBColumn;
    cxGrid2DBTableView4priority: TcxGridDBColumn;
    cxGrid2DBTableView4sql_create: TcxGridDBColumn;
    cxGrid2DBTableView4sql_drop: TcxGridDBColumn;
    cxGrid2DBTableView4Definition: TcxGridDBColumn;
    cxGrid2DBTableView3SCHEMA: TcxGridDBColumn;
    cxGrid2DBTableView3name: TcxGridDBColumn;
    cxGrid2DBTableView3description: TcxGridDBColumn;
    cxGrid2DBTableView3checked: TcxGridDBColumn;
    cxGrid2DBTableView3is_exists: TcxGridDBColumn;
    cxGrid2DBTableView3content: TcxGridDBColumn;
    cxGrid2DBTableView3priority: TcxGridDBColumn;
    cxGrid2DBTableView3sql_create: TcxGridDBColumn;
    cxGrid2DBTableView3sql_drop: TcxGridDBColumn;
    cxGrid2DBTableView3Definition: TcxGridDBColumn;
    cxGrid2DBTableView_IndexSCHEMA: TcxGridDBColumn;
    cxGrid2DBTableView_Indexname: TcxGridDBColumn;
    cxGrid2DBTableView_Indexdescription: TcxGridDBColumn;
    cxGrid2DBTableView_Indexchecked: TcxGridDBColumn;
    cxGrid2DBTableView_Indexis_exists: TcxGridDBColumn;
    cxGrid2DBTableView_Indexcontent: TcxGridDBColumn;
    cxGrid2DBTableView_Indexis_primary_key: TcxGridDBColumn;
    cxGrid2DBTableView_Indexis_unique_constraint: TcxGridDBColumn;
    cxGrid2DBTableView_Indexis_unique: TcxGridDBColumn;
    cxGrid2DBTableView_Indexindex_keys: TcxGridDBColumn;
    cxGrid2DBTableView_Indexindex_description: TcxGridDBColumn;
    cxGrid2DBTableView_Indexindex_type: TcxGridDBColumn;
    cxGrid2DBTableView_Indexpriority: TcxGridDBColumn;
    cxGrid2DBTableView_Indexsql_create: TcxGridDBColumn;
    cxGrid2DBTableView_Indexsql_drop: TcxGridDBColumn;
    cxGrid2DBTableView_IndexDefinition: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    ds_Triggers_all: TDataSource;
    qry_Triggers_all: TADOQuery;
    ds_indexes_all: TDataSource;
    qry_indexes_all: TADOQuery;
    ds_CK_all: TDataSource;
    qry_CK_all: TADOQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    procedure OpenData;

  public
    class procedure ExecDlg(aPrefix: string = '');
  end;



implementation

{$R *.dfm}




//-------------------------------------------------------------------
class procedure Tdlg_Structure_new.ExecDlg(aPrefix: string = '');
//-------------------------------------------------------------------
var
  dlg_Structure_new: Tdlg_Structure_new;

begin
  dlg_Structure_new:=Tdlg_Structure_new (FindForm(Tdlg_Structure_new.ClassName));


  if not Assigned(dlg_Structure_new) then
    Application.CreateForm(Tdlg_Structure_new, dlg_Structure_new);

  dlg_Structure_new.Show;


end;


// ---------------------------------------------------------------
procedure Tdlg_Structure_new.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin

  Caption :='Structure';
  PageControl1.Align:=alClient;


  if MDBConnection.Connected then
//  begin
    ShowMessage('Tdlg_Structure_new.FormCreate( - MDBConnection.Connected');
//    MDBConnection.Close;
//  end;


  t_OBJECTS.TableName       := '_OBJECTS';
  t_Owner.TableName         := 'Owner';

  t_OBJECTS_main.TableName  := '_OBJECTS_main';

  t_Database_Info.TableName  := 'Database_Info';
//  t_.TableName  := 'DatabaseInfo';


  cxGrid_OBJECTS.Align :=alClient;
  cxGrid_Tables1.Align :=alClient;

  cxGrid_UDP.Align :=alClient;
  cxGrid_SP.Align :=alClient;
  cxGrid_Func.Align :=alClient;
  cxGrid_View.Align :=alClient;
  cxGrid_Index.Align :=alClient;
  cxGrid_tr.Align :=alClient;

  OpenData;

 // TabSheet_sys_objects.Visible := False;

 cxGrid_OBJECTS_main.Align :=alClient;


  cxGrid7.Align:=alClient;
//  cxGrid11.Align:=alClient;

end;


// ---------------------------------------------------------------
procedure Tdlg_Structure_new.OpenData;
// ---------------------------------------------------------------
const
  DEF_SQL = 'select * from sys_objects where xtype=''%s'' and host=''local'' '; // and project_id = '':name''

//  DEF_SQL_FIELDS =  'select * from sys_objects_table_fields where project_name = '':name''';
 // DEF_SQL_FIELDS =  'select * from sys_objects_table_fields ';   qry_Triggers

  //order by priority desc, name
var
  s: string;
begin
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection_MDB);

 // db_TableOpen1()

  t_sys_Projects.Open;

//  s:=Format(DEF_SQL, ['tr']);
//  db_OpenQuery (qry_Triggers, Format(DEF_SQL, ['tr']));
//  db_View(qry_Triggers);

//  qry_table_fields.Open;

//select * from  sys_objects_table_columns
//where object_id =:id


//  db_OpenQuery (qry_table_fields,     DEF_SQL_FIELDS);
  db_OpenQuery (qry_Tables,   Format(DEF_SQL, ['u']));
  qry_table_fields.Open;
  qry_indexes.Open;
  qry_CK.Open;
  qry_Triggers.Open;

  //select * from sys_objects where xtype='tr' and  object_id =:id


  db_OpenQuery (qry_Views,    Format(DEF_SQL, ['view']));

  db_OpenQuery (qry_FK,       Format(DEF_SQL, ['fk']));
  db_OpenQuery (qry_Func,     Format(DEF_SQL, ['fn']));
  db_OpenQuery (qry_SP,       Format(DEF_SQL, ['sp']));
  db_OpenQuery (qry_UDT,      Format(DEF_SQL, ['UDT']));

  db_OpenQuery (qry_CK_all,       Format(DEF_SQL, ['ck']));
  db_OpenQuery (qry_Triggers_all, Format(DEF_SQL, ['tr']));
  db_OpenQuery (qry_indexes_all,  Format(DEF_SQL, ['index']));

//  db_View(qry_Triggers);


  t_OBJECTS.Open;

  t_Owner.Open;
  t_Database_Info.Open;

  t_OBJECTS_main.Open;


end;


procedure Tdlg_Structure_new.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action :=caFree;
end;

end.


