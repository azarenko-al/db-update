unit u_func_;

interface

uses SysUtils, Math, Classes
  ;

//  function Eq (Value1,Value2 : string): boolean;

function SqlCompare(aValue1, aValue2: String; var APos: Integer): Boolean;

//  function FullEq(aValue1, aValue2: String; var APos: Integer): Boolean;

implementation



// ---------------------------------------------------------------
function SqlCompare(aValue1, aValue2: String; var APos: Integer): Boolean;
// ---------------------------------------------------------------

// True - Equal;
var
  I: Integer;
  j: Integer;
  k: Integer;
  s1_: string;
  s2_: string;

  sl1: TStringList;
  sl2: TStringList;

begin
  // /****** Object:  User Defined Function dbo.fn_dBm_to_Wt    Script Date: 2013-05-13 13:35:29 ******/
               
  sl1:=TStringList.Create;
  sl2:=TStringList.Create;

  sl1.Text :=aValue1;
  sl2.Text :=aValue2;

  // remove AUTO GEN lines

  k:=0;

  Result := True;


  for I := 0 to Min(sl1.Count, sl2.Count)-1 do
  begin
    s1_:=sl1[i];
    s2_:=sl2[i];

    for j := 1 to Min(Length(s1_),Length(s2_)) do
      if s1_[j]<>s2_[j] then
      begin
        Result := False;
        APos:=k;
        Break;
        //Exit;
      end
        else Inc(k);

    if Result then
      Result := Length(s1_)=Length(s2_);

    if not Result then
      Break;
  end;

  aPos:=k + i*2;


//
//  for I := 1 to Min(Length(aValue1),Length(aValue2)) do
//    if aValue1[i]<>aValue2[i] then
//    begin
//      Result := False;
//      APos:=i;
//      Exit;
//    end;
//
//  Result := Length(aValue1)=Length(aValue2);


   FreeAndNil(sl1);
   FreeAndNil(sl2);

end;



// --------------------------------------------------------------
function SqlCompare_new1111111111111111111111(aValue1, aValue2: String;
    aDiffList: TStrings): Boolean;
// ---------------------------------------------------------------

  function DoCompStr(aStr1, aStr2: String; var aIndex : Integer): boolean;
  var
    j: Integer;
  begin
    result := True;


{    for j := 1 to Min(Length(aStr1),Length(aStr2)) do
      if s1_[j]<>s2_[j] then
      begin
        Result := False;
        APos:=k;
        Break;
        //Exit;
      end
        else Inc(k);
}

  end;



// True - Equal;
var
  I: Integer;
  j: Integer;
  k: Integer;
  s1_: string;
  s2_: string;

  sl1: TStringList;
  sl2: TStringList;

begin
  // /****** Object:  User Defined Function dbo.fn_dBm_to_Wt    Script Date: 2013-05-13 13:35:29 ******/
               
  sl1:=TStringList.Create;
  sl2:=TStringList.Create;

  sl1.Text :=aValue1;
  sl2.Text :=aValue2;

  // remove AUTO GEN lines

  k:=0;

  Result := True;


  for I := 0 to Min(sl1.Count, sl2.Count)-1 do
  begin
    s1_:=sl1[i];
    s2_:=sl2[i];

    for j := 1 to Min(Length(s1_),Length(s2_)) do
      if s1_[j]<>s2_[j] then
      begin
        Result := False;
     //   APos:=k;
        Break;
        //Exit;
      end
        else Inc(k);

    if Result then
      Result := Length(s1_)=Length(s2_);

    if not Result then
      Break;
  end;

 /// aPos:=k + i*2;


//
//  for I := 1 to Min(Length(aValue1),Length(aValue2)) do
//    if aValue1[i]<>aValue2[i] then
//    begin
//      Result := False;
//      APos:=i;
//      Exit;
//    end;
//
//  Result := Length(aValue1)=Length(aValue2);


   FreeAndNil(sl1);
   FreeAndNil(sl2);

end;





(*
function FullEq(S1, S2: String; var APos: Integer): Boolean;
var
  P1, P2: TParser;
  M1, M2: TMemoryStream;
  Src: PChar;
  L: Integer;
  C: Char;
begin
  C := #0;
  L := Length(S1);
  M1 := TMemoryStream.Create;
  M2 := TMemoryStream.Create;
  P1 := nil;
  P2 := nil;

  try
    M1.Size := L + 1;
    Src := @S1[1];
    M1.WriteBuffer(Src^, L);
    M1.WriteBuffer(C, 1);
    M1.Position := 0;

    L := Length(S2);
    M2.Size := L + 1;
    Src := @S2[1];
    M2.WriteBuffer(Src^, L);
    M2.WriteBuffer(C, 1);
    M2.Position := 0;

    try
      P1 := TParser.Create(M1);
      P2 := TParser.Create(M2);
      repeat
        if (P1.Token <> P2.Token) or not Eq(P1.TokenString, P2.TokenString) then
        begin
          APos := P1.SourcePos;
          Break;
        end;
        P1.NextToken;
        P2.NextToken;
      until (P1.Token = toEof) or (P2.Token = toEof);

      Result := (P1.Token = P2.Token) and Eq(P1.TokenString, P2.TokenString);
      if not Result then APos := P1.SourcePos;

    finally
      P1.Free;
      P2.Free;
    end;
  finally
    M1.Free;
    M2.Free;
  end;
end;


*)

var
  APos: Integer;
  b: Boolean;
begin

//  b:=SqlCompare('aaa', 'aassss', APos);



end.



(*
//--------------------------------------------------------
function Eq (Value1,Value2 : string): boolean;
//--------------------------------------------------------
begin
  Result := AnsiCompareText(Trim(Value1),Trim(Value2))=0;
end;
*)

(*
//-------------------------------------------------------------------
procedure FindFilesByMask1(aDir,aMask: ShortString; aList: TStrings;
    aNeedClear: boolean);
//-------------------------------------------------------------------
// example: FindFilesByMask1 ('t:\temp', '*.dcu', strList);
//-------------------------------------------------------------------

  procedure DoFind (aDir: string);
  var
    rInfoRec: TSearchRec;
    s       : string;
  begin
    aDir:=IncludeTrailingBackslash(aDir);

    if FindFirst(aDir + '*.*',faDirectory,rInfoRec)=0 then
    begin
      repeat
        s:=rInfoRec.Name;
        if (s<>'.') and (s<>'..') and ((rInfoRec.Attr and faDirectory)<>0)
          then DoFind (aDir + s);
      until
         FindNext(rInfoRec)<>0;

      SysUtils.FindClose(rInfoRec);
    end;


    if FindFirst(aDir + aMask,faAnyFile,rInfoRec)=0 then
    begin
      repeat
        s:=rInfoRec.Name;
        if (s<>'.') and (s<>'..') and ((rInfoRec.Attr and faDirectory)=0)
        then
          aList.Add(aDir + rInfoRec.Name);
      until
         FindNext(rInfoRec)<>0;

      SysUtils.FindClose(rInfoRec);
    end;

  end;


begin
  if aNeedClear then
    aList.Clear;

  if Trim(aDir)='' then
    Exit;

  DoFind (aDir);
end;
*)

