unit d_Versions_Edit;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,


  dm_MDB,

  u_files,

  u_func,

  u_db,

//  u_const,

  //dm_MDB,
  dm_Main_SQL,

  u_local_const,

  Dialogs, DB,  ADODB, cxGridCustomView, cxGridLevel,
  cxGrid, ComCtrls, StdActns, cxDBTL, cxInplaceContainer, cxTL, cxTLData,
  cxGridCustomTableView, cxGridTableView, cxClasses, cxControls, ToolWin,
  cxGridDBTableView, StdCtrls, ExtCtrls, DBCtrls, ActnList, Menus, rxPlacemnt,

  Grids, DBGrids, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, cxDBData, cxCheckBox, cxTextEdit, cxCalendar, System.Actions,
  dxSkinsCore, dxSkinsDefaultPainters
  ;

type
  Tdlg_Versions = class(TForm)
    FormStorage1: TFormStorage;
    PopupMenu: TPopupMenu;
    ActionList1: TActionList;
    act_Exec_SQL: TAction;
    N1: TMenuItem;
    act_Exec_All_SQL: TAction;
    SQL1: TMenuItem;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    MainMenu1: TMainMenu;
    dfg1: TMenuItem;
    SQL2: TMenuItem;
    SQL3: TMenuItem;
    N2: TMenuItem;
    actLoadFromFile2: TMenuItem;
    Panel1: TPanel;
    Panel2: TPanel;
    Button1: TButton;
    act_Exec_SQL_test: TAction;
    act_Exec_All_SQL_test: TAction;
    SQL4: TMenuItem;
    actExecAllSQLtest1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    ExecAllSQLtest1: TMenuItem;
    ExecSQLtest1: TMenuItem;
    SaveToFile1: TMenuItem;
    N5: TMenuItem;
    act_LoadFromFile: TFileOpen;
    LoadFromFile2: TMenuItem;
    LoadFromFile3: TMenuItem;
    act_FileSaveAs: TFileSaveAs;
    SaveAs1: TMenuItem;
    N6: TMenuItem;
    act_Test_load_SQL: TAction;
    actTestloaddbframework1: TMenuItem;
    ADOConnection1: TADOConnection;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    col_id: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2_enabled: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn6_caption: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1_name: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1_priority: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn4_modify_date: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn7_is_run_before_all: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Splitter3: TSplitter;
    DBMemo1: TDBMemo;
    ToolBar3: TToolBar;
    DBNavigator3: TDBNavigator;
    act_Record_Add: TAction;
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
//    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);
    procedure act_Exec_SQLExecute(Sender: TObject);
    procedure act_FileSaveAsAccept(Sender: TObject);
    procedure act_LoadFromFileAccept(Sender: TObject);
 //   procedure act_Test_load_SQLExecute(Sender: TObject);
    procedure ADOTable1NewRecord(DataSet: TDataSet);
//    procedure Button3Click(Sender: TObject);
    procedure cxDBTreeList1DragOver(Sender, Source: TObject; X, Y: Integer; State:
        TDragState; var Accept: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure qryAfterPost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
  //  procedure qryNewRecord(DataSet: TDataSet);
  private
    FGridRegPath: string;
   // FConnectRec: TLoginRec;
    FLib, FAddr: string;

    FUpdate: boolean;
//    procedure Export_view_Version_updates_enabled;

// TODO: OpenDbVersions
//  function  OpenDbVersions(): boolean;

  public
    class procedure ExecDlg();
  end;

(*
var
  dlg_Edit_versions: Tdlg_Edit_versions;
*)

//==============================================================
implementation
{$R *.dfm}

const
  FLD_DATETIME = 'datetime';
  TBL_view_Version_updates1 = 'view_Version_updates1';

var
  L_Form: Tdlg_Versions;

  //FindForm(aClassName: string): TForm;

class procedure Tdlg_Versions.ExecDlg;
begin
  if not Assigned(L_Form) then
    Application.CreateForm (Tdlg_Versions, L_Form);

  L_Form.Show;

{
  with Tdlg_Versions.Create(Application) do
  begin
  //  if OpenDbVersions() then
      ShowModal;

    Free;
  end;
  
}

end;

//--------------------------------------------------------------
procedure Tdlg_Versions.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
//  grid.Align:= alClient;
  DBMemo1.Align:= alClient;
 // cxDBTreeList1.Align:= alClient;
//  cxGrid1.Align:= alClient;

  PageControl1.Align:= alClient;


  if ADOConnection1.Connected then
    ShowMessage('procedure Tdlg_Versions.FormCreate(Sender: TObject);  Connection.Connected');


  db_SetComponentADOConnection(Self, dmMDB.ADOConnection_MDB);

  db_TableOpen1(ADOTable1, TBL_version_updates) ;

 // db_TableOpen1(view_Version_updates1, TBL_view_Version_updates1) ;

//  ADOTable1.Last;

//  ADOTable2.Open;


 // ADOTable1.Open;


(*
  dmMain.UseCustomConnection:= true;
  dmMain.CustomConnection:= AdoConnection1;

  cmd.Connection:= dmMain.CustomConnection;

  col_content_sql       .FieldName:= 'content_SqlServ_2000';
  col_enabled           .FieldName:= FLD_ENABLED;
  col_exec_each_time    .FieldName:= 'exec_each_time';
  col_date              .FieldName:= FLD_DATETIME;
  col_content_programmed.FieldName:= 'programmed_action_name';
  col_caption           .FieldName:= FLD_CAPTION;

  dbmemo1.DataField:= 'content_SqlServ_2000';

  FGridRegPath:= REGISTRY_FORMS + ClassName + grid.Name + '1';

  grid.LoadFromRegistry(FGridRegPath);
*)

//  FGridRegPath:= REGISTRY_FORMS + ClassName + cxGrid1DBTableView1.Name;
//  cxGrid1DBTableView1.RestoreFromRegistry(FGridRegPath);


end;


procedure Tdlg_Versions.FormDestroy(Sender: TObject);
//--------------------------------------------------------------
begin
  cxGrid1DBTableView1.StoreToRegistry(FGridRegPath);

  L_Form:=nil;


(*  dmMain.UseCustomConnection:= false;
  dmMain.CustomConnection:= nil;
*)
end;


//--------------------------------------------------------------
procedure Tdlg_Versions.act_Exec_SQLExecute(Sender: TObject);
//--------------------------------------------------------------
var
  sSql, sMsg: string;
  iRecords: Integer;

    // ---------------------------------------------------------------
    function DoExecRecord(aIsRollbackTrans: boolean): boolean;
    // ---------------------------------------------------------------
    var
      sErr: string;
      sSql: string;
    begin
      sSql:= ADOTable1.FieldByName('content').AsString;

      Result := dmMain_SQL.ExecScriptSQL_with_GO1(sSql, aIsRollbackTrans);

      if Result then
        ShowMessage('������ ��������.');

(*
     if aIsRollbackTrans then
       dmMain_SQL.ADOConnection1.BeginTrans;

     if aIsRollbackTrans then
       dmMain_SQL.ADOConnection1.RollbackTrans;

*)



     (* Result:= true;

      sSql:= qry.FieldByName(col_content_sql.FieldName).AsString;
      if sSql='' then exit;

      sSql:= Format('USE %s      '+
                    'BEGIN TRAN  '+
                    '%s          '+
                    'ROLLBACK    ', [ed_Test_DB.Text, sSql] );

      try
        cmd.CommandText := sSql;
        cmd.Execute (iRecords);
        sMsg:= Format('���������� ������� "%s" ������ �� �������. ��������� ������� %d',
                [qry.FieldByName(FLD_CAPTION).AsString, iRecords]);
        Result:= true;
      except
        on e: Exception do begin
          Result:= false;
          sMsg:= Format('������ ��� ����������� ������� "%s". �����: %s',
                   [qry.FieldByName(FLD_CAPTION).AsString, e.Message]);
        end;
      end;*)
    end;

var
  sRes: string;
begin
  if ADOTable1.RecordCount=0  then
    Exit;


  sMsg:= '';
  sRes:= '';
(*

  if Sender=act_LoadFromFile then
//    if ADOTable1.RecordCount>0  then
      if OpenDialog1.Execute then
      begin
        ADOTable1.Edit;

        DBMemo2.Lines.LoadFromFile(OpenDialog1.FileName);
      end;

  if Sender=act_SaveToFile then
  //  if ADOTable1.RecordCount>0  then
      if SaveDialog1.Execute then
      begin
        ADOTable1.Edit;

        DBMemo2.Lines.SaveToFile(SaveDialog1.FileName);
      end;

*)

  if Sender = act_Exec_SQL then
  begin
   // dm

    if DoExecRecord(False) then ;
    //  ShowMessage('Ok.');
  end;


  if Sender = act_Exec_SQL_test then
  begin
    if DoExecRecord(True) then  ;
     // ShowMessage('Ok.');
  end;

  //----------------------------------------------
  if Sender = act_Record_add then
  begin
  //  db_AddRecord_(view_Version_updates1, [FLD_NAME, '']);

   // dm

//    if DoExecRecord(False) then ;
    //  ShowMessage('Ok.');
  end;




(*
  if sender = act_Test_SQL then begin
    DoExecRecord();
    ShowMessage(sMsg);
  end;

  if sender = act_Test_All_SQL then begin
    CursorSql;
    qry.disableControls;
    with qry do begin
      First;
      while not EOF do
      begin
        if not DoExecRecord() then
          sRes:= sRes + sMsg + CRLF;

        Next;
      end;
    end;

    ShowMessage(sRes);
    CursorDefault;
    qry.enableControls;
  end;*)

end;

procedure Tdlg_Versions.ADOTable1NewRecord(DataSet: TDataSet);
begin
  DataSet[FLD_modify_date]:= Now;

 // DataSet[FLD_ID]:= 1000;



end;

//--------------------------------------------------------------
procedure Tdlg_Versions.qryAfterPost(DataSet: TDataSet);
//--------------------------------------------------------------
begin
(*  if FUpdate then
    exit;

  FUpdate:= true;
  db_SaveDatasetPosition([qry]);
  qry.requery;
  db_RestoreDatasetPosition([qry]);
  FUpdate:= false;*)

end;



procedure Tdlg_Versions.ActionList1Update(Action: TBasicAction; var Handled:
    Boolean);
var
  b: Boolean;
begin
  b:=ADOTable1.RecordCount>0;

  act_Exec_SQL.Enabled := b;
  act_Exec_SQL_test.Enabled := b;
  act_Exec_All_SQL.Enabled := b;
  act_Exec_All_SQL_test.Enabled := b;
end;


procedure Tdlg_Versions.act_FileSaveAsAccept(Sender: TObject);
begin
  ADOTable1.Edit;
  DBMemo1.Lines.SaveToFile(act_FileSaveAs.Dialog.FileName);
end;


procedure Tdlg_Versions.act_LoadFromFileAccept(Sender: TObject);
begin
  ADOTable1.Edit;
  DBMemo1.Lines.LoadFromFile(act_LoadFromFile.Dialog.FileName);
end;





procedure Tdlg_Versions.cxDBTreeList1DragOver(Sender, Source: TObject; X, Y:
    Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := True;
end;

procedure Tdlg_Versions.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;

end;

procedure Tdlg_Versions.ToolButton1Click(Sender: TObject);
begin
 //  db_AddRecord_(view_Version_updates1, [FLD_NAME, '']);
end;


end.



// TODO: OpenDbVersions
////--------------------------------------------------------------
//function  Tdlg_Versions.OpenDbVersions(): boolean;
////--------------------------------------------------------------
//begin
//
// (* dmMain.LoadConnectRecFromReg(FConnectRec);
//dmMain.LoadAdditionalConnectParamsFromReg(FConnectRec.Server, FLib, FAddr);
//
//with FConnectRec do begin
//  Result:= dmMain.OpenByParams(Server, User, Password,
//              'onega_version_updates', FLib, FAddr, IsUseWinAuth);
//
//  if not Result then begin
//    ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
//    exit;
//  end;
//end;
//
//Result:= db_OpenTable(qry, 'version_updates');
//if not Result then begin
//  ErrorDlg('������: '+ gl_DB.ErrorMsg);
//  exit;
//end;*)
//
//end;

      {

type
  TVarRec = record
    case Integer of
      0: ( FInt1, FInt2 : Integer; )
      1: ( FDouble : Double; )
  end;

function Convert(const ADouble: Double): Integer;
var
  arec : TVarRec;
begin
  arec.FDouble := ADouble;
  Result := arec.FInt1 xor arec.FInt2;
end;

//--------------------------------------------------------------
procedure Tdlg_Versions.qryNewRecord(DataSet: TDataSet);
//--------------------------------------------------------------
begin
 /// DataSet[FLD_DATETIME]:= AsString( Double( Now ) );
end;





//---------------------------------------------------------------------------
procedure Tdlg_Versions.Export_view_Version_updates_enabled;
//---------------------------------------------------------------------------
var
  oList: TstringList;
  oList_content: TstringList;
  is_run_before_all : boolean;

const
  DEF_view_Version_updates_checked1 = 'view_Version_updates_enabled1';
  FLD_ITEMS_COUNT = 'ITEMS_COUNT';


  //---------------------------------------
  procedure Do1 (aParent_id : Integer );
  //---------------------------------------
  var
    iID: Integer;
    iCount: Integer;
    oQuery: TADOQuery;
    s: string;
    s1: string;
  begin
    oQuery:=TADOQuery.Create(nil);
    oQuery.Connection:=ADOConnection1;

    if aParent_id=0 then
      s:= Format('SELECT * FROM %s WHERE (parent_id is null)', [DEF_view_Version_updates_checked1])
    else
      s:= Format('SELECT * FROM %s WHERE (parent_id=%d)', [DEF_view_Version_updates_checked1, aParent_id]);

    s1:=IIF(is_run_before_all, 'True','False');
    s:=s + Format(' and (is_run_before_all=%s)', [s1]);

    db_OpenQuery(oQuery, s);

  //  oQuery.SQL.Text:=s;
  //  oQuery.Open;

    with oQuery do
      while not EOF do
      begin
        oList.Add(oQuery.FieldByName(FLD_CAPTION).AsString);
        oList_content.Add(oQuery.FieldByName(FLD_CONTENT).AsString);

        iCount  :=oQuery.FieldByName(FLD_ITEMS_COUNT).AsInteger;
        iID:=oQuery.FieldByName(FLD_ID).AsInteger;

        if iCount>0 then
          Do1(iID);


        Next;
      end;



    FreeAndNil(oQuery);

  end;


begin
  oList:=TstringList.create;
  oList_content:=TstringList.create;

  is_run_before_all:=True;

  Do1 (0);

  ShellExec_Notepad_temp(oList.Text);


  FreeAndNil(oList);
  FreeAndNil(oList_content);

end;






// ---------------------------------------------------------------
procedure Tdlg_Versions.act_Test_load_SQLExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
{
  if ADOTable1.Locate(FLD_name, 'db_framework', []) then
  begin
    ADOTable1.Edit;
    DBMemo2.Lines.LoadFromFile('C:\DB_Scripts\Rostelecom\_common_func_new.sql');

  end;

  if ADOTable1.Locate(FLD_name, 'update_old', []) then
  begin
    ADOTable1.Edit;
    DBMemo2.Lines.LoadFromFile('C:\DB_Scripts\Rostelecom\_script_simple.sql');

  end;

}
end;



procedure Tdlg_Versions.Button3Click(Sender: TObject);
begin
//  Export_view_Version_updates_enabled();

end;

//FindForm(aClassName: string): TForm;
