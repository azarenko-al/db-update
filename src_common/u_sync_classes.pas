unit u_sync_classes;

interface

uses
  System.Generics.Collections,

  System.Generics.Defaults,  CodeSiteLogging,


  Classes, SysUtils, Math,

//u_const_db,
  u_local_const,

  u_func
  , DB;

const
 // DEF_GO      = 'go ---';
  DEF_GO1      = 'GO'+ CRLF;
  DEF_GO_CRLF = CRLF+ DEF_GO1 +CRLF;

//const
//  SQL_DROP_TABLE = 'exec _Drop_TABLE ''%s''';


type
  TDBStructure = class;
  TDBTableFieldList = class;
  TDBObjectList= class;

  TDBObjectType = (otNone,
                   otTABLE,
                   otTableField,
                   otVIEW,
                   otUDT,
                   otTRIGGER,
                   otStoredProc,
                   otStoredFunc,
                   otIndex,


                   otFK,
                   otCheckConstraint

                   );



//  TFlagSet = set of (flCheckExists, flNoDrop, flWithProc);

 // -------------------------------------------------------------------���
   TDBObject = class
  // -------------------------------------------------------------------
  private
  public
    TableFields : TDBTableFieldList;

    Indexes     : TDBObjectList;
    CK          : TDBObjectList;
    Triggers    : TDBObjectList;


  public
    Runtime: record
      IsProcessed : boolean;
    end;

    DB_id   : integer;


    Definition   : string;

    SCHEMA    : string;
    Name      : string;
    Name_full : string;

    ObjectType   : TDBObjectType;

    DateCreated: TDateTime;
    DateUpdated: TDateTime;

    TableName    : string;
    Text         : string; // for proc


    SQL_create : string;
    SQL_drop   : string;


//    Status : string;
//    Owner  : string;
    Priority : Integer;


    FK:     record

              PK_TABLE_NAME : string;
              PK_COLUMN_NAME : string;

              FK_TABLE_NAME : string;
              FK_COLUMN_NAME : string;


            end;


    constructor Create;
    destructor Destroy; override;

    function EqualTo(aObject: TDBObject): Boolean;
    function GetInfoText: string;
    function GetListByObject(aObjectType: TDBObjectType): TDBObjectList;
    procedure LoadFromDataset(aDataset: TDataSet);

    function MakeCreateSQL(): string;
    function MakeCreateSQL_simple(aList: TStringList): string;

    function MakeDropSQL: string;
    function MakeRenameSQL(aNewName: string): string;


  end;



  // ---------------------------------------------------------------
  TDBObjectList = class(TObjectList<TDBObject>)
  // ---------------------------------------------------------------
  private
    function MakeCreateSQL(aStrList: TStringList): string;

    function MakeDropSQL(aStrList: TStringList): string;

//    function GetItems(Index: Integer): TDBObject;
  public
    function AddItem(aObjectType: TDBObjectType): TDBObject;

    function FindByName(aName: string): TDBObject;
    function FindByNameFull(aName: string): TDBObject;

    function FindBy_Index_keys1(aObject: TDBObject): TDBObject;

    function IndexByName_111111111(aName: string): integer;




 //   property Items[Index: Integer]: TDBObject read GetItems; default;
  end;



 // -------------------------------------------------------------------
  TDBTableField = class
  // -------------------------------------------------------------------
  private
    function GetFieldTypeStr: string;
    function MakeDropSQL: string;

    function MakeAlterSQL(aDestField: TDBTableField; aFK: TDBObjectList): string;

  public
    Name            : string;

    sql_create: string;
    sql_drop: string;


    TableName_Full: string;

    IsNullable      : Boolean;
    IsIDENTITY      : Boolean;

    IsComputed : Boolean;
    ComputedText : string;

    Type_           : string;
    Length          : Integer;

    DEFAULT_CONTENT : string;

    function EqualTo(aField: TDBTableField): Boolean;
    function GetInfoText: string;


  end;

  // ---------------------------------------------------------------
  TDBTableFieldList = class(TObjectList<TDBTableField>)
  // ---------------------------------------------------------------
  public
    function AddItem: TDBTableField;

    function FindByName(aName: string): TDBTableField;
  end;


  // ---------------------------------------------------------------
  TDBStructure = class(TObject)
  // ---------------------------------------------------------------
  public
    UDT        : TDBObjectList;

    Tables     : TDBObjectList;
    Views      : TDBObjectList;
    StoredProcs: TDBObjectList;
    StoredFuncs: TDBObjectList;
    FK         : TDBObjectList;


    DB_Version : string;
    DB_Product_name : string;

    Exceptions: TStringList;


    constructor Create;
    destructor Destroy; override;

    procedure Clear;

    function GetListByObject(aObjectType: TDBObjectType): TDBObjectList;
  end;



function ObjectTypeToStr(aObjectType: TDBObjectType): string;


implementation



function CompareText_(aStr1, aStr2: string; aObjectName: string = ''): boolean;
    forward;

//function CompareText_constraints(aStr1, aStr2: string): boolean; forward;


function TDBObjectList.AddItem(aObjectType: TDBObjectType): TDBObject;
begin
  Result := TDBObject.Create;
  Add(Result);
  Result.ObjectType :=aObjectType;
end;

// ---------------------------------------------------------------
function TDBObjectList.FindByName(aName: string): TDBObject;
// ---------------------------------------------------------------
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName)
    then
    begin

      Result := Items[i];
      Break;
    end;
end;


// ---------------------------------------------------------------
function TDBObjectList.FindByNameFull(aName: string): TDBObject;
// ---------------------------------------------------------------
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Schema+'.'+Items[i].Name, aName) then
    begin

      Result := Items[i];
      Break;
    end;
end;




// ---------------------------------------------------------------
function TDBObjectList.IndexByName_111111111(aName: string): integer;
// ---------------------------------------------------------------
var I: Integer;
begin
 Result := -1;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := i;
      Break ;
    end;
end;


//
//function TDBObjectList.GetItems(Index: Integer): TDBObject;
//begin
//  Result := TDBObject(inherited Items[Index]);
//end;
//
//


function TDBObjectList.MakeCreateSQL(aStrList: TStringList): string;
var
  i: Integer;
begin
  Result:='';

  for i := 0 to Count - 1 do
    Result:=Result + Items[i].MakeCreateSQL()+ DEF_GO_CRLF;
end;


function TDBObjectList.MakeDropSQL(aStrList: TStringList): string;
var
  i: Integer;
begin
  Result:='';

  for i := 0 to Count - 1 do
    Result:=Result + Items[i].MakeDropSQL()+ DEF_GO_CRLF;
end;



// ---------------------------------------------------------------
constructor TDBStructure.Create;
// ---------------------------------------------------------------
begin
  inherited Create;

  Views       := TDBObjectList.Create();
  StoredProcs := TDBObjectList.Create();
  StoredFuncs := TDBObjectList.Create();
  FK          := TDBObjectList.Create();
  Tables      := TDBObjectList.Create();
  UDT         := TDBObjectList.Create();

//  Triggers   := TDBObjectList.Create();

//  CK          := TDBObjectList.Create();


  Exceptions := TStringList.Create();

end;


destructor TDBStructure.Destroy;
begin
  FreeAndNil(Exceptions);
  FreeAndNil(StoredFuncs);
  FreeAndNil(FK);
  FreeAndNil(StoredProcs);
  FreeAndNil(Views);
  FreeAndNil(Tables);
  FreeAndNil(UDT);
//  FreeAndNil(Triggers);
//  FreeAndNil(CK);

  inherited Destroy;
end;


procedure TDBStructure.Clear;
begin
  UDT.Clear;
  Tables.Clear;
  Views.Clear;
  StoredProcs.Clear;
  StoredFuncs.Clear;
  FK.Clear;
//  CK.Clear;
//  Triggers.Clear;
end;


function TDBStructure.GetListByObject(aObjectType: TDBObjectType):   TDBObjectList;
begin
  case aObjectType of
    otUDT:        Result := UDT;

    otTaBLE:      Result := TaBLEs;
    otView:       Result := Views;
    otStoredProc: Result := StoredProcs;
    otStoredFunc: Result := StoredFuncs;
    otFK:         Result := FK;
//    otCheckConstraint:         Result := CK;
//    otTRIGGER:    Result := TRIGGERs;

  ELSE
    raise Exception.Create('');
  end;

end;


function TDBTableFieldList.AddItem: TDBTableField;
begin
  Result := TDBTableField.Create;
  Add(Result);
end;


function TDBTableFieldList.FindByName(aName: string): TDBTableField;
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if Eq(Items[i].Name, aName) then
    begin
      Result := Items[i];
      Break;
    end;
end;

//
//function TDBTableFieldList.GetItems(Index: Integer): TDBTableField;
//begin
//  Result := TDBTableField(inherited Items[Index]);
//end;


constructor TDBObject.Create;
begin
  inherited Create;

  TableFields := TDBTableFieldList.Create();
  Indexes    := TDBObjectList.Create();
  CK          := TDBObjectList.Create();

  Triggers  := TDBObjectList.Create();


end;

// ---------------------------------------------------------------
destructor TDBObject.Destroy;
// ---------------------------------------------------------------
begin
  FreeAndNil(CK);
  FreeAndNil(Indexes);
  FreeAndNil(Triggers);

  FreeAndNil(TableFields);

  inherited Destroy;
end;


// ---------------------------------------------------------------
function TDBObject.EqualTo(aObject: TDBObject): Boolean;
// ---------------------------------------------------------------
var
  oTableField1: TDBTableField;
  oTableField2: TDBTableField;
  s: string;
  s1: string;
  s2: string;

begin
  case ObjectType of
    otCheckConstraint :  begin

              assert (SQL_create<>'');
              assert (aObject.SQL_create<>'');

               Result := SQL_create = aObject.SQL_create;

            end;

    otView,
    otStoredProc,
    otStoredFunc,

    otTRIGGER : Result := CompareText_(Text, aObject.Text, Name);


    otIndex   : begin
                // Result := SQL_create = aObject.SQL_create;

                  assert (Definition<>'');

                  Result := Definition = aObject.Definition;
                end;


    otUDT,
//    otCheckConstraint,
    otFk      : begin

                  assert (SQL_create<>'');
                  assert (aObject.SQL_create<>'');

//     = aObject.SQL_create;

                  Result := SQL_create = aObject.SQL_create;

                end;

    otTABLE: begin
       s1:=MakeCreateSQL ();
       s2:=aObject.MakeCreateSQL();

       Result := CompareText_(s1,s2);

    end;

   else
    raise Exception.Create('');
  end;
end;


// ---------------------------------------------------------------
function TDBObject.GetInfoText: string;
// ---------------------------------------------------------------
begin
  case ObjectType of
    otStoredProc,
    otStoredFunc,
    otView,
    otCheckConstraint,
    otTRIGGER :
          Result := Text;

    otTABLE,
    otFk:   Result := MakeCreateSQL();

    otUDT:
         Result := SQL_create;

    otIndex:
         Result := Definition;
   else
    raise Exception.Create('');
  end;

end;

function TDBObject.GetListByObject(aObjectType: TDBObjectType): TDBObjectList;
begin
  case aObjectType of
    otIndex:       Result := Indexes;
    otCheckConstraint:          Result := Ck;
  else
    raise Exception.Create('');
  end;

end;

// ---------------------------------------------------------------
function TDBObject.MakeCreateSQL(): string;
// ---------------------------------------------------------------
//--aFlags: TFlagSet = []
const
  SQL_CREATE_TABLE =
    'CREATE TABLE %s (' +CRLF+ '%s) ON [PRIMARY]';//+ DEF_GO_CRLF;

var
  I: Integer;
  s: string;
  s1: string;
  sIndexKeys: string;
  sON: string;
 // sIntegrity__Sql: string;
begin
 // Assert( not IsMakeCreateSQL_done );

//  IsMakeCreateSQL_done := True;

//  Result:='';

//  if (SCHEMA<>'') and (SCHEMA<>'dbo') then
 //   Result := Format('if schema_id(''%s'') is null  execute(''create schema %s'')', [SCHEMA, SCHEMA]) + DEF_GO_CRLF;



  case ObjectType of
    otView:    Result:= SQL_create + DEF_GO_CRLF ;

    // -------------------------
    otStoredProc:    Result:=  SQL_create + DEF_GO_CRLF ;


    // -------------------------
    otStoredFunc:    Result:=  SQL_create + DEF_GO_CRLF ;


    // -------------------------
    otTrigger   :    Result:=  SQL_create + DEF_GO_CRLF ;

    otFK:     Result:= SQL_create + DEF_GO_CRLF;

    otCheckConstraint:    Result:=SQL_create + DEF_GO_CRLF;


    // -------------------------
    otTABLE:
    // -------------------------
    begin

      Result:=SQL_create + DEF_GO_CRLF;


    //  if Assigned(aStrList) then
     //   aStrList.Add(s);

      for I := 0 to Indexes.Count - 1 do
        Result:=Result+ Indexes[i].MakeCreateSQL(); //[flNoDrop]

      for I := 0 to CK.Count - 1 do
        Result:=Result+ CK[i].MakeCreateSQL();

    end;

    // -------------------------
    otIndex:
    // -------------------------
      Result:=SQL_create + DEF_GO_CRLF;

    otUDT:
       Result:=SQL_create + DEF_GO_CRLF;


  else
    raise Exception.Create('');
  end;

  Assert(Result<>'');
end;


// ---------------------------------------------------------------
function TDBObject.MakeCreateSQL_simple(aList: TStringList): string;
// ---------------------------------------------------------------
const
  SQL_CREATE_TABLE =
    'CREATE TABLE %s (' +CRLF+ '%s) ON [PRIMARY]';//+ DEF_GO_CRLF;



var
  I: Integer;
  s: string;
  s1: string;
  
begin
  Result:='';


  case ObjectType of

    otView,
    otStoredProc,
    otStoredFunc,
    otTrigger:
      begin
//        Validate();

        if Assigned(aList) then
          aList.Add(Text);


        Result:= Text + DEF_GO_CRLF;

      end;


    // -------------------------
    otFK:
    // -------------------------
    begin
      s:= SQL_create;

      if Assigned(aList) then
         aList.Add(s);

      Result:=s + DEF_GO_CRLF;

    end;

    // -------------------------
    otCheckConstraint:
    // -------------------------
    begin
       Result:=SQL_create + DEF_GO_CRLF;

      if Assigned(aList) then
        aList.Add(s);


    //  if Assigned(aStrList) then aStrList.Add(s);
    end;


    // -------------------------
    otTABLE:
    // -------------------------
    begin
//      s1:=TableFields.MakeSQL();

Assert (SQL_create <> '');

  //    s:= Format(SQL_CREATE_TABLE, [ Name, s1]);
      Result:=SQL_create + DEF_GO_CRLF;

      if Assigned(aList) then
        aList.Add(s);

    //  if Assigned(aStrList) then
     //   aStrList.Add(s);

      for I := 0 to Indexes.Count - 1 do
        Result:=Result+ Indexes[i].MakeCreateSQL_simple (aList) ;

   //   for I := 0 to CK.Count - 1 do
    //    Result:=Result+ CK[i].MakeCreateSQL_simple;

    end;

    // -------------------------
    otIndex:
    // -------------------------
    begin
      Result:=SQL_create + DEF_GO_CRLF;

      if Assigned(aList) then aList.Add(s);
    end;
  else
    raise Exception.Create('');
  end;

  Assert(Result<>'');
end;



// ---------------------------------------------------------------
function TDBObject.MakeDropSQL: string;
// ---------------------------------------------------------------

(*
 const
  SQL_DROP_TABLE = 'exec _Drop_TABLE ''%s''';
*)

var
  s: string;
begin
  case ObjectType of

    otTABLE:        Result:= Format('exec service._Drop_TABLE ''%s''',  [Name]);
//      Result:= Format('Drop TABLE %s',  [Name]);

    //  Result:= SQL_drop; //  Format('if object_id(''%s'') is not null '+  CRLF +

    otView,  //:         Result:= SQL_drop;
    otStoredfUNC, //:   Result:=SQL_drop;
    otStoredProc, //:   Result:=SQL_drop;
    otTrigger, //:      Result:=SQL_drop;
    otFK, //:           Result:=SQL_drop;

    otCheckConstraint, //: Result:= SQL_drop;
    otIndex:            Result:= SQL_drop;

  else
    raise Exception.Create(Name);
  end;



 // else
  //  s:='';

//  s:=s+ '   '+ Result;

  //aStrList.Add(s);

  Result:=Result+ DEF_GO_CRLF;

end;


// ---------------------------------------------------------------
function TDBTableField.EqualTo(aField: TDBTableField): Boolean;
// ---------------------------------------------------------------
begin
  Result :=  CompareText (sql_create, aField.sql_create)=0 ;
end;


// ---------------------------------------------------------------
function TDBTableField.GetInfoText: string;
// ---------------------------------------------------------------
begin
  Result :=sql_create;
end;


// ---------------------------------------------------------------
function TDBTableField.MakeDropSQL: string;
// ---------------------------------------------------------------
const
  SQL_ALTER_TABLE_DROP_FIELD = 'exec service._ColumnDrop  ''%s'', ''%s'' ';// + CRLF+ DEF_GO +CRLF;

var
  s: string;
begin

  s:=Format(SQL_ALTER_TABLE_DROP_FIELD, [TableName_Full, Name]);

  Result := s + CRLF;
end;


// ---------------------------------------------------------------
function TDBTableField.MakeAlterSQL(aDestField: TDBTableField; aFK:
    TDBObjectList): string;
// ---------------------------------------------------------------

var
  I: Integer;
  s: string;
  oSList: TStringList;
  sFieldType: string;

  bExit : Boolean;
  sFlag: string;
  sNull: string;

begin
  bExit := False;
  //aError := '';

//  if Eq(TableName_Full,'link') then
//    s := '';

//  if Eq(TableName_Full,'lib_Bands') then
//    s := '';
             

  oSList:=TStringList.Create;

  Assert(Assigned(aDestField), 'Value not assigned');

  // ---------------------------------------------------------------
  if (IsComputed   <> aDestField.IsComputed) or
     (ComputedText <> aDestField.ComputedText)
  then
  // ---------------------------------------------------------------
  begin
    s:=Format('exec service._ColumnAddCalculated ''%s'',''%s'',''%s''',
        [ TableName_Full, Name, ComputedText]);

    oSList.Add(s);

  end else

  begin
{
      // ---------------------------------------------------------------
      if IsIDENTITY <> aDestField.IsIDENTITY then
      // ---------------------------------------------------------------
      begin
        sFlag:=IIF(IsIDENTITY, 'ON', 'OFF');

        s:=Format('exec service._ColumnSetIdentity ''%s'',''%s'',''%s''',
            [ TableName_Full, Name, sFlag]);

        oSList.Add(s);

      end;
}

      // -------------------------
      if (Type_  <> aDestField.Type_) or
         (Length <> aDestField.Length)
      // -------------------------
      then
      begin
        sFieldType:=GetFieldTypeStr();

     //   sNull := IIF(IsNullable, 'NULL', 'NOT NULL');

//        s:=Format('exec _ColumnAlter ''%s'',''%s'',''%s'',''%s'',''%s''',
        s:=Format('exec service._ColumnAlter  ''%s'',''%s'',''%s'',''%s'' ',
            [ TableName_Full, Name, sFieldType, ReplaceStr(DEFAULT_CONTENT,'''','''''')]);

        oSList.Add(s);
      end;

      // -------------------------
      if (DEFAULT_CONTENT <> aDestField.DEFAULT_CONTENT) then
      // -------------------------
      begin
        s:=Format('exec service._ColumnAddDefault ''%s'',''%s'',''%s''',
//        s:=Format('exec service._ColumnAddDefault ''%s'',''%s'',''%s''',
            [ TableName_Full, Name, ReplaceStr(DEFAULT_CONTENT,'''','''''') ]);

        oSList.Add(s);
      end;

      // -------------------------
      if (IsNullable <> aDestField.IsNullable) then
      // -------------------------
      begin
        sFieldType:=GetFieldTypeStr();

        if not Eq(sFieldType, 'sysname') then
        begin
          sNull := IIF(IsNullable, 'NULL', 'NOT NULL');

          s:=Format('exec service._ColumnSetNull ''%s'',''%s'',''%s''',
              [ TableName_Full, Name, sNull]);

          oSList.Add(s);

        end;
      end;


  end;


  for I := 0 to oSList.Count - 1 do
    oSList[i] := oSList[i] + DEF_GO_CRLF;

  Result := oSList.Text;

  FreeAndNil(oSList);

end;


// ---------------------------------------------------------------
function TDBTableField.GetFieldTypeStr: string;
// ---------------------------------------------------------------
var
  s: string;
begin
 // if Eq(Name,'id') then
  // ShowMessage('');


  Result:= Type_;


  if ( Eq(Type_,'varchar') or Eq(Type_,'nvarchar') )
       and ( Length = -1)
  then
    Result:=Result + '(max)'

//    Result:=Result + Format('(%d) COLLATE Cyrillic_General_CI_AS', [Length]);

  else

    if Eq(Type_,'char') or
       Eq(Type_,'varchar') or
       Eq(Type_,'nvarchar')
    then
      Result:=Result + Format('(%d)', [Length]);

//    Result:=Result + Format('(%d) COLLATE Cyrillic_General_CI_AS', [Length]);


//    s:=s;

 // else

//  if IsVariableLen and (Type_<>'sysname') then
//    Result:=Result + Format('(%d)', [Length]);

  if IsIdeNTITY then
    Result:=Result + ' IDENTITY(1,1)';

end;


// ---------------------------------------------------------------
function CompareSqlText_(aValue1, aValue2: String): Boolean;
// ---------------------------------------------------------------
// True - Equal;
var
  I: Integer;
  s1_: string;
  s2_: string;

  sl1: TStringList;
  sl2: TStringList;

begin
  sl1:=TStringList.Create;
  sl2:=TStringList.Create;

  sl1.Text :=aValue1;
  sl2.Text :=aValue2;

  Result := True;   

  for I := 0 to Min(sl1.Count, sl2.Count)-1 do
  begin
    s1_:=Trim(sl1[i]);
    s2_:=Trim(sl2[i]);

    Result :=AnsiLowerCase(s1_) = AnsiLowerCase(s2_);
//    Result :=CompareText(s1_,s2_) = 0;

    if not Result then
      Break;
  end;

  if sl1.Count<>sl2.Count then
    Result :=False;



   FreeAndNil(sl1);
   FreeAndNil(sl2);

end;


//---------------------------------------------------------
function CompareText_(aStr1, aStr2: string; aObjectName: string = ''): boolean;
//---------------------------------------------------------
var
  k: Integer;
begin
  aStr1 := Trim(aStr1);
  aStr2 := Trim(aStr2);


  if aObjectName<>'' then
  begin
    aStr1 := ReplaceStr(aStr1, '['+aObjectName+']', aObjectName);
    aStr2 := ReplaceStr(aStr2, '['+aObjectName+']', aObjectName);

  end;

  Result := CompareSqlText_(aStr1,aStr2);

end;



// ---------------------------------------------------------------
function ObjectTypeToStr(aObjectType: TDBObjectType): string;
// ---------------------------------------------------------------
begin
    case aObjectType of

      otTableField :  result:= 'TableField';
      otTABLE      :  result:= 'TABLE';
      otVIEW       :  result:= 'VIEW';
      otTRIGGER    :  result:= 'TRIGGER ';
      otStoredProc :  result:= 'StoredProc';
      otStoredFunc :  result:= 'StoredFunc';
      otIndex      :  result:= 'Index';

      otFK         :  result:= 'FK';
      otCheckConstraint :  result:= 'CK';
      otUDT        :  result:= 'UDT';

    else
      raise Exception.Create('');
    end;

end;



// ---------------------------------------------------------------
function TDBObjectList.FindBy_Index_keys1(aObject: TDBObject): TDBObject;
// ---------------------------------------------------------------
var I: Integer;
begin
 Result := nil;

  for I := 0 to Count - 1 do
    if (Items[i].ObjectType = aObject.ObjectType) and
       (Items[i].Definition = aObject.Definition) //and  Eq(Items[i].Index.INDEX_KEYS, aObject.Index.INDEX_KEYS)
    then begin
//      Assert (Items[i].Index.INDEX_KEYS <> '');


      Result := Items[i];
      Break;
    end;



end;

// ---------------------------------------------------------------
procedure TDBObject.LoadFromDataset(aDataset: TDataSet);
// ---------------------------------------------------------------
begin
  Name:=aDataset[FLD_Name];

  TableName := aDataset.FieldByName(FLD_TABLENAME).AsString;

  Text        := aDataset.FieldByName(FLD_Definition).AsString;

  SQL_create  := aDataset.FieldByName('SQL_create').AsString;
  SQL_drop    := aDataset.FieldByName('SQL_drop').AsString;

  //          obj.Owner := FieldByName(FLD_Owner).AsString;


  Priority := aDataset.FieldByName(FLD_Priority).AsInteger;

  Definition    := aDataset.FieldByName('Definition').AsString;;

  if ObjectType=otFK then
  begin
    FK.PK_TABLE_NAME   := aDataset[FLD_PK_TABLE_NAME];
    FK.PK_COLUMN_NAME  := aDataset[FLD_PK_COLUMN_NAME];

    FK.FK_TABLE_NAME   := aDataset[FLD_FK_TABLE_NAME];
    FK.FK_COLUMN_NAME  := aDataset[FLD_FK_COLUMN_NAME];

  end;         

end;

// ---------------------------------------------------------------
function TDBObject.MakeRenameSQL(aNewName: string): string;
// ---------------------------------------------------------------
begin
  case ObjectType of
  {
    otFK:
    begin
      Result:= Format('IF (OBJECT_ID(''%s'') IS not NULL) '+
                     'and (OBJECT_ID(''%s'') IS NULL)', [Name,aNewName]) + CRLF;
      Result:=Result+ Format('  exec sp_Rename ''%s'', ''%s''', [Name, aNewName]) + DEF_GO_CRLF;

    end;
   }

  //  Result:= Format('exec _rename '''', ''%s'', ''%s''', [Name, aNewName]) + DEF_GO_CRLF;

   // otPrimaryKey,
    otIndex:
    begin
      Assert (TableName <> '');

//      Result:= Format('exec service._Rename_index ''%s'', ''%s'', ''%s''', [TableName, Name, aNewName]) + DEF_GO_CRLF;

      Result:= Format('exec sp_RENAME ''%s.%s'', ''%s''', [TableName, Name, aNewName]) + DEF_GO_CRLF;


//            set @s = 'exec sp_RENAME '''+ @TABLE_NAME +'.'+ @INDEX_NAME +''', '''+ @INDEX_NAME_NEW + ''''


    end;
  end;

end;




end.

