object dlg_Structure111111111111: Tdlg_Structure111111111111
  Left = 601
  Top = 121
  BorderWidth = 3
  Caption = 'dlg_Structure111111111111'
  ClientHeight = 765
  ClientWidth = 1176
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 33
    Width = 1176
    Height = 416
    ActivePage = TabSheet1
    Align = alTop
    MultiLine = True
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Tables'
      object Splitter5: TSplitter
        Left = 0
        Top = 385
        Width = 1168
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1176
      end
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 385
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Tables
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGrid1DBTableView1checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 20
          end
          object cxGrid1DBTableView1name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            Width = 250
          end
          object cxGrid1DBTableView1DBColumn1_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGrid1DBTableView1DBColumn1_Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Width = 93
          end
          object cxGrid1DBTableView1DBColumn1_is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 72
          end
        end
        object cxGrid1DBTableView_CK: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_CK
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView_CKid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGrid1DBTableView_CKname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 100
          end
          object cxGrid1DBTableView_CKcontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
            Width = 1534
          end
        end
        object cxGrid1DBTableView_indexes: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Indexes
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView_indexesid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGrid1DBTableView_indexesname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 258
          end
          object cxGrid1DBTableView_indexes_is_primary_key: TcxGridDBColumn
            DataBinding.FieldName = 'is_primary_key'
            Width = 34
          end
          object cxGrid1DBTableView_indexes_is_unique_key: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique_key'
            Width = 34
          end
          object cxGrid1DBTableView_indexes_is_unique_index: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique_index'
            Width = 34
          end
          object cxGrid1DBTableView_indexesindex_keys: TcxGridDBColumn
            DataBinding.FieldName = 'index_keys'
            Width = 222
          end
          object cxGrid1DBTableView_indexescontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
          end
        end
        object cxGrid1DBTableView_Trigger: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Triggers
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView_Triggername: TcxGridDBColumn
            DataBinding.FieldName = 'name'
          end
          object cxGrid1DBTableView_Triggercontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
          end
          object cxGrid1DBTableView_TriggerDBColumn1_Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
          end
        end
        object cxGrid1DBTableView2_TableFields: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_TableFields
          DataController.DetailKeyFieldNames = 'TableName'
          DataController.KeyFieldNames = 'id'
          DataController.MasterKeyFieldNames = 'name'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid1DBTableView2_TableFieldsDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 83
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'type'
            Width = 68
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'Length'
            Width = 52
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'PK'
            Width = 36
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'default_name'
            Width = 112
          end
          object cxGrid1DBTableView2_TableFieldsDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'default_content'
            Width = 105
          end
          object COL_IDENTITY: TcxGridDBColumn
            DataBinding.FieldName = 'IDENTITY'
            Width = 34
          end
          object COL_IsNullable: TcxGridDBColumn
            DataBinding.FieldName = 'IsNullable'
            Width = 34
          end
          object col_IsComputed: TcxGridDBColumn
            DataBinding.FieldName = 'IsComputed'
          end
          object col_computed_text: TcxGridDBColumn
            DataBinding.FieldName = 'computed_text'
            Width = 30
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
          Options.DetailTabsPosition = dtpTop
          object cxGrid1Level2: TcxGridLevel
            Caption = 'Table Fields'
            GridView = cxGrid1DBTableView2_TableFields
          end
          object cxGrid1Level4: TcxGridLevel
            Caption = 'Indexes'
            GridView = cxGrid1DBTableView_indexes
          end
          object cxGrid1Level5: TcxGridLevel
            Caption = 'Triggers'
            GridView = cxGrid1DBTableView_Trigger
          end
          object cxGrid1Level3: TcxGridLevel
            Caption = 'CK'
            GridView = cxGrid1DBTableView_CK
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Views'
      ImageIndex = 1
      object Splitter4: TSplitter
        Left = 0
        Top = 296
        Width = 1168
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1176
      end
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 296
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Views
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
            Options.Filtering = False
          end
          object cxGridDBTableView1DBColumn1_checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Width = 49
          end
          object cxGridDBTableView1name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 194
          end
          object cxGridDBTableView1content: TcxGridDBColumn
            DataBinding.FieldName = 'content'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 116
          end
          object cxGridDBTableView1SQLCode: TcxGridDBColumn
            DataBinding.FieldName = 'SQLCode'
            Visible = False
            Options.Editing = False
            Width = 105
          end
          object cxGridDBTableView1DBColumn1_text: TcxGridDBColumn
            DataBinding.FieldName = 'text'
            PropertiesClassName = 'TcxMemoProperties'
            Properties.ScrollBars = ssBoth
            Options.Filtering = False
          end
          object cxGridDBTableView1DBColumn1_Status: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Width = 90
          end
          object cxGridDBTableView1DBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object col_xtype1: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView1DBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object DBMemo_View: TDBMemo
        Left = 0
        Top = 299
        Width = 1168
        Height = 89
        Align = alBottom
        DataField = 'content'
        DataSource = ds_Views
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'FK'
      ImageIndex = 2
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 388
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView_FK: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_FK
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView_FKid: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView_FKname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            SortIndex = 0
            SortOrder = soAscending
            Width = 104
          end
          object cxGridDBTableView_FKtablename: TcxGridDBColumn
            DataBinding.FieldName = 'pk_table_name'
            Width = 121
          end
          object cxGridDBTableView_FKDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'pk_column_name'
            Width = 81
          end
          object cxGridDBTableView_FKpk_tablename: TcxGridDBColumn
            DataBinding.FieldName = 'fk_table_name'
            Width = 114
          end
          object cxGridDBTableView_FKDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'fk_column_name'
            Width = 115
          end
          object cxGridDBTableView_FKDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Width = 64
          end
          object cxGridDBTableView_FKDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView_FKDBColumn5_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_FKDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'IsDeleteCascade'
          end
          object cxGridDBTableView_FKDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'IsUpdateCascade'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView_FK
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Triggers'
      ImageIndex = 3
      object Splitter3: TSplitter
        Left = 0
        Top = 296
        Width = 1168
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1176
      end
      object cxGrid4: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 296
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView3: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Triggers
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView3Column2: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
          end
          object cxGridDBTableView3tablename: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 124
          end
          object cxGridDBTableView3name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 1
            SortOrder = soAscending
            Width = 147
          end
          object cxGridDBTableView3content: TcxGridDBColumn
            DataBinding.FieldName = 'content'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
            Width = 119
          end
          object cxGridDBTableView3DBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Width = 93
          end
          object cxGridDBTableView3DBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView3DBColumn3_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView3
        end
      end
      object DBMemo_Trigger: TDBMemo
        Left = 0
        Top = 299
        Width = 1168
        Height = 89
        Align = alBottom
        DataField = 'content'
        DataSource = ds_Triggers
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'SP'
      ImageIndex = 5
      object Splitter2: TSplitter
        Left = 0
        Top = 296
        Width = 1168
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1176
      end
      object cxGrid5: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 296
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView_SP: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_SP
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView_SPDBColumn1_checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Width = 62
          end
          object cxGridDBTableView_SPname: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 270
          end
          object cxGridDBTableView_SPcontent: TcxGridDBColumn
            DataBinding.FieldName = 'content'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Editing = False
          end
          object cxGridDBTableView_SPstatus_id: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Width = 86
          end
          object cxGridDBTableView_SPDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 87
          end
          object cxGridDBTableView_SPDBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_SPDBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
        end
        object cxGridLevel4: TcxGridLevel
          GridView = cxGridDBTableView_SP
        end
      end
      object DBMemo_SP: TDBMemo
        Left = 0
        Top = 299
        Width = 1168
        Height = 89
        Align = alBottom
        DataField = 'content'
        DataSource = ds_SP
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'Func'
      ImageIndex = 8
      object Splitter1: TSplitter
        Left = 0
        Top = 300
        Width = 1168
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        ExplicitWidth = 1176
      end
      object cxGrid8: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 300
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView_func: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Func
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBColumn9: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Width = 54
          end
          object cxGridDBColumn7: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 270
          end
          object cxGridDBColumn8: TcxGridDBColumn
            DataBinding.FieldName = 'content'
            PropertiesClassName = 'TcxMemoProperties'
          end
          object cxGridDBColumn10: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsFixedList
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListOptions.ShowHeader = False
            Properties.ListSource = ds_Owner
            Width = 86
          end
          object cxGridDBTableView_funcDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
            Width = 86
          end
          object cxGridDBTableView_funcDBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView_funcDBColumn2_priority: TcxGridDBColumn
            DataBinding.FieldName = 'priority'
          end
        end
        object cxGridLevel8: TcxGridLevel
          GridView = cxGridDBTableView_func
        end
      end
      object DBMemo_func: TDBMemo
        Left = 0
        Top = 303
        Width = 1168
        Height = 85
        Align = alBottom
        DataField = 'content'
        DataSource = ds_Func
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
    object TabSheet_CK: TTabSheet
      Caption = 'CK'
      ImageIndex = 6
      object cxGrid6: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 388
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView5: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_CK
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGridDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Options.Editing = False
            Width = 121
          end
          object cxGridDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            SortIndex = 0
            SortOrder = soAscending
            Width = 226
          end
          object cxGridDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'content'
            Options.Editing = False
            Width = 210
          end
          object cxGridDBTableView5DBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView5DBColumn2_xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
        end
        object cxGridLevel5: TcxGridLevel
          GridView = cxGridDBTableView5
        end
      end
    end
    object TabSheet10: TTabSheet
      Caption = 'indexes'
      ImageIndex = 10
      object cxGrid10: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 388
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView8: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Indexes
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView8id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGridDBTableView8tablename: TcxGridDBColumn
            DataBinding.FieldName = 'tablename'
            Visible = False
            GroupIndex = 0
            Width = 128
          end
          object cxGridDBTableView8name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 189
          end
          object cxGridDBTableView8is_primary_key: TcxGridDBColumn
            DataBinding.FieldName = 'is_primary_key'
            Width = 84
          end
          object cxGridDBTableView8is_unique_key: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique_key'
            Width = 95
          end
          object cxGridDBTableView8is_unique_index: TcxGridDBColumn
            DataBinding.FieldName = 'is_unique_index'
            Width = 101
          end
          object cxGridDBTableView8index_keys: TcxGridDBColumn
            DataBinding.FieldName = 'index_keys'
            Width = 183
          end
          object cxGridDBTableView8index_description: TcxGridDBColumn
            DataBinding.FieldName = 'index_description'
            Width = 128
          end
          object cxGridDBTableView8xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView8is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView8index_type: TcxGridDBColumn
            DataBinding.FieldName = 'index_type'
          end
        end
        object cxGridLevel10: TcxGridLevel
          GridView = cxGridDBTableView8
        end
      end
    end
    object TabSheet7: TTabSheet
      Caption = 'OBJECTS'
      ImageIndex = 7
      object cxGrid_OBJECTS: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 161
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView6: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_OBJECTS
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBTableView6id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            SortIndex = 0
            SortOrder = soAscending
          end
          object cxGridDBTableView6checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Width = 80
          end
          object cxGridDBTableView6name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            Width = 189
          end
          object cxGridDBTableView6display_name: TcxGridDBColumn
            DataBinding.FieldName = 'display_name'
            Visible = False
          end
          object cxGridDBTableView6table_name: TcxGridDBColumn
            DataBinding.FieldName = 'table_name'
            Visible = False
          end
          object cxGridDBTableView6folder_name: TcxGridDBColumn
            DataBinding.FieldName = 'folder_name'
            Visible = False
          end
          object cxGridDBTableView6view_name: TcxGridDBColumn
            DataBinding.FieldName = 'view_name'
            Visible = False
          end
        end
        object cxGridLevel6: TcxGridLevel
          GridView = cxGridDBTableView6
        end
      end
    end
    object TabSheet_Owner: TTabSheet
      Caption = 'Owner'
      ImageIndex = 8
      object cxGrid7: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 409
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView7: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Owner
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.GroupByBox = False
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Options.Editing = False
            Width = 101
          end
          object cxGridDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.NullStyle = nssUnchecked
            Options.Editing = False
            Width = 80
          end
        end
        object cxGridLevel7: TcxGridLevel
          GridView = cxGridDBTableView7
        end
      end
    end
    object TabSheet9: TTabSheet
      Caption = 'DatabaseInfo'
      ImageIndex = 9
      object cxGrid9: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 388
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView4: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_DatabaseInfo
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object col_product_name: TcxGridDBColumn
            DataBinding.FieldName = 'product_name'
            Width = 141
          end
          object cxGridDBTableView4version: TcxGridDBColumn
            DataBinding.FieldName = 'version'
            Width = 114
          end
        end
        object cxGridLevel9: TcxGridLevel
          GridView = cxGridDBTableView4
        end
      end
    end
    object TabSheet_OBJECTS_main: TTabSheet
      Caption = 'OBJECTS_main'
      ImageIndex = 11
      object cxGrid_OBJECTS_main: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 181
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_OBJECTS_main
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGridDBTableView2id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGridDBTableView2name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 161
          end
          object cxGridDBTableView2enabled: TcxGridDBColumn
            DataBinding.FieldName = 'enabled'
            Width = 84
          end
          object cxGridDBTableView2Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.KeyFieldNames = 'name'
            Properties.ListColumns = <
              item
                FieldName = 'name'
              end>
            Properties.ListSource = ds_Owner
            Width = 136
          end
        end
        object cxGridLevel11: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object TabSheet8: TTabSheet
      Caption = 'Project'
      ImageIndex = 12
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 657
        Height = 388
        Align = alLeft
        DataSource = ds_Project
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
    object TabSheet11: TTabSheet
      Caption = 'UDT'
      ImageIndex = 13
      object cxGrid11: TcxGrid
        Left = 0
        Top = 0
        Width = 1168
        Height = 388
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        object cxGridDBTableView9: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_UDT
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsView.Indicator = True
          object cxGridDBTableView9id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            Width = 50
          end
          object cxGridDBTableView9name: TcxGridDBColumn
            DataBinding.FieldName = 'name'
            Width = 280
          end
          object cxGridDBTableView9checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
            Width = 54
          end
          object cxGridDBTableView9xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
          end
          object cxGridDBTableView9Owner: TcxGridDBColumn
            DataBinding.FieldName = 'Owner'
          end
          object cxGridDBTableView9is_exists: TcxGridDBColumn
            DataBinding.FieldName = 'is_exists'
          end
          object cxGridDBTableView9create_date: TcxGridDBColumn
            DataBinding.FieldName = 'create_date'
          end
          object cxGridDBTableView9modify_date: TcxGridDBColumn
            DataBinding.FieldName = 'modify_date'
          end
          object cxGridDBTableView9project_id: TcxGridDBColumn
            DataBinding.FieldName = 'project_id'
          end
          object cxGridDBTableView9object_id: TcxGridDBColumn
            DataBinding.FieldName = 'object_id'
          end
          object cxGridDBTableView9sql_create: TcxGridDBColumn
            DataBinding.FieldName = 'sql_create'
          end
          object cxGridDBTableView9sql_drop: TcxGridDBColumn
            DataBinding.FieldName = 'sql_drop'
          end
        end
        object cxGridLevel12: TcxGridLevel
          GridView = cxGridDBTableView9
          Options.DetailTabsPosition = dtpTop
        end
      end
    end
  end
  object pn_Buttons: TPanel
    Left = 0
    Top = 730
    Width = 1176
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    Constraints.MaxHeight = 35
    Constraints.MinHeight = 35
    TabOrder = 1
    DesignSize = (
      1176
      35)
    object Bevel1: TBevel
      Left = 0
      Top = 0
      Width = 1176
      Height = 2
      Align = alTop
      Shape = bsTopLine
      Visible = False
      ExplicitWidth = 1184
    end
    object btn_Cancel: TButton
      Left = 1105
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 0
      OnClick = btn_CancelClick
    end
  end
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1176
    Height = 33
    ButtonHeight = 21
    Caption = 'ToolBar1'
    TabOrder = 2
    object DBLookupComboBox1: TDBLookupComboBox
      Left = 0
      Top = 0
      Width = 249
      Height = 21
      KeyField = 'id'
      ListField = 'name'
      ListSource = ds_Project
      TabOrder = 0
    end
  end
  object ds_Tables: TDataSource
    DataSet = tbl_Tables
    Left = 48
    Top = 576
  end
  object ds_Views: TDataSource
    DataSet = tbl_Views
    Left = 408
    Top = 576
  end
  object ds_CK: TDataSource
    DataSet = tbl_CK
    Left = 208
    Top = 576
  end
  object ds_FK: TDataSource
    DataSet = tbl_FK
    Left = 464
    Top = 576
  end
  object ds_Triggers: TDataSource
    DataSet = tbl_Triggers
    Left = 272
    Top = 576
  end
  object ds_Indexes: TDataSource
    DataSet = tbl_indexes
    Left = 336
    Top = 576
  end
  object ds_Owner: TDataSource
    DataSet = t_Owner
    Left = 776
    Top = 576
  end
  object ds_SP: TDataSource
    DataSet = tbl_SP
    Left = 720
    Top = 576
  end
  object t_Owner: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableName = 'Owner'
    Left = 776
    Top = 520
  end
  object tbl_Triggers: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'Triggers'
    Left = 264
    Top = 520
  end
  object tbl_SP: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableName = 'StoredProcs'
    Left = 720
    Top = 520
  end
  object tbl_FK: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'FK'
    Left = 472
    Top = 520
  end
  object tbl_indexes: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'Indexes'
    Left = 336
    Top = 520
  end
  object tbl_CK: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'CK'
    Left = 200
    Top = 520
  end
  object tbl_Views: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'Views'
    Left = 408
    Top = 516
  end
  object tbl_Tables: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'Tables'
    Left = 48
    Top = 516
  end
  object tbl_TableFields: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'Table_Fields'
    Left = 128
    Top = 520
  end
  object ds_TableFields: TDataSource
    DataSet = tbl_TableFields
    Left = 128
    Top = 576
  end
  object MDBConnection: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=C:\ON' +
      'EGA\RPLS_DB\bin\install_db.mdb;Mode=Share Deny None;Persist Secu' +
      'rity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry ' +
      'Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;J' +
      'et OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk O' +
      'ps=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database' +
      ' Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:En' +
      'crypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=Fals' +
      'e;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=F' +
      'alse;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 48
    Top = 680
  end
  object ds_OBJECTS: TDataSource
    DataSet = t_OBJECTS
    Left = 992
    Top = 568
  end
  object t_OBJECTS: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableName = '_OBJECTS'
    Left = 992
    Top = 520
  end
  object ds_Func: TDataSource
    DataSet = t_Func
    Left = 656
    Top = 576
  end
  object t_Func: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableName = 'Func'
    Left = 656
    Top = 520
  end
  object FormStorage2: TFormStorage
    IniFileName = 'Software\Onega\Install_DB\'
    UseRegistry = True
    StoredProps.Strings = (
      'DBMemo_View.Height'
      'DBMemo_Trigger.Height'
      'DBMemo_SP.Height'
      'DBMemo_func.Height'
      'PageControl1.ActivePage')
    StoredValues = <>
    Left = 776
    Top = 680
  end
  object ds_DatabaseInfo: TDataSource
    DataSet = t_Database_Info
    Left = 640
    Top = 688
  end
  object t_Database_Info: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableDirect = True
    TableName = 'Database_Info'
    Left = 640
    Top = 640
  end
  object ds_OBJECTS_main: TDataSource
    DataSet = t_OBJECTS_main
    Left = 872
    Top = 576
  end
  object t_OBJECTS_main: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableName = '_OBJECTS_main'
    Left = 872
    Top = 520
  end
  object ds_Project: TDataSource
    DataSet = t_Project
    Left = 1112
    Top = 576
  end
  object t_Project: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    TableName = 'Projects'
    Left = 1112
    Top = 528
  end
  object ds_UDT: TDataSource
    DataSet = t_UDT
    Left = 536
    Top = 576
  end
  object t_UDT: TADOTable
    Connection = MDBConnection
    CursorType = ctStatic
    MasterFields = 'id'
    TableDirect = True
    TableName = 'UDT'
    Left = 536
    Top = 520
  end
end
