unit dm_MDB;

interface

uses
  SysUtils, Classes, Forms, Dialogs, DB, ADODB,
  IOUtils,

//  u_log_,

  u_files,
  u_db,
  u_db_mdb

  ;

type
  TdmMDB = class(TDataModule)
    ADOConnection_MDB: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
  private
    FMDBFileName: string;

    function OpenDB1: Boolean;
// TODO: UpdateIniFile1
//  procedure UpdateIniFile1(aChecksum: Integer; aIniFileName: string);

  public

    procedure CompactDatabase1;
    procedure EmptyTable(aTableName: string);

    procedure PublishToDir1(aDir: string);

    procedure OpenQuery(aQuery : TADOQuery; aSQL : string);


//    db_OpenQuery (qry_Views,    Format(DEF_SQL, ['view']));

    class procedure Init1;
    procedure OpenTable(aTable : TADOTable; aTableName : string);
  end;

var
  dmMDB: TdmMDB;

implementation
{$R *.dfm}
(*
const
  DEF_install_db_mdb = 'install_db.mdb';
*)


class procedure TdmMDB.Init1;
begin
  if not Assigned(dmMDB) then
    dmMDB := TdmMDB.Create(Application);
end;


procedure TdmMDB.DataModuleCreate(Sender: TObject);
begin
  if ADOConnection_MDB.Connected then
//  begin
    ShowMessage('procedure TdmMDB.DataModuleCreate(Sender: TObject); - ADOConnection_MDB.Connected');

  //  ADOConnection_MDB.Close;
//  end;


  FMDBFileName := GetApplicationDir()+ 'install_db.mdb';

//  mdb_CompactDatabase(FMDBFileName);

  OpenDB1;
end;


procedure TdmMDB.OpenQuery(aQuery : TADOQuery; aSQL : string);
begin
  aQuery.Connection:= ADOConnection_MDB;
  db_OpenQuery (aQuery, aSQL, []);
end;


procedure TdmMDB.OpenTable(aTable : TADOTable; aTableName : string);
begin
  aTable.Connection:= ADOConnection_MDB;
  db_TableOpen1 (aTable, aTableName);
end;


// ---------------------------------------------------------------
procedure TdmMDB.CompactDatabase1;
// ---------------------------------------------------------------
begin
//  Exit;

  {$DEFINE CompactDatabase1}
  {$IFDEF CompactDatabase1}

  ADOConnection_MDB.Close;

 // mdb_CompactDatabase(FMDBFileName);
  {$ENDIF}
end;


//------------------------------------------------------------------------------
function TdmMDB.OpenDB1: Boolean;
//------------------------------------------------------------------------------

begin
  Result := True;

  if ADOConnection_MDB.Connected then
    Exit;

 // FMDBFileName := GetApplicationDir()+ 'onega_tables.mdb';
  //ir :=  GetApplicationDir()

//  FMDBFileName := GetApplicationDir()+ 'install_db.mdb';

//  FMDBFileName := GetApplicationDir()+ DEF_install_db_mdb;


//  {$DEFINE CompactDatabase1}
//  {$IFDEF CompactDatabase1}
//  mdb_CompactDatabase(FMDBFileName);
//  {$ENDIF}


  if not ADOConnection_MDB.Connected then
    if not mdb_OpenConnection(ADOConnection_MDB, FMDBFileName) then
    begin
      ShowMessage('������ �������� ����� MS Access: '+ FMDBFileName);
      Result := False;
    end;


 // Filecopy (FMDBFileName, 'd:\aaaa.mdb');

end;



procedure TdmMDB.EmptyTable(aTableName: string);
begin
  ADOConnection_MDB.Execute(Format('DELETE FROM %s', [aTableName]));
end;

// ---------------------------------------------------------------
procedure TdmMDB.PublishToDir1(aDir: string);
// ---------------------------------------------------------------
begin

  ADOConnection_MDB.Close;

  aDir := IncludeTrailingBackslash(aDir);

  TFile.Copy(GetApplicationDir()+ 'install_db.mdb',  aDir + 'install_db.mdb' );
  TFile.Copy(GetApplicationDir()+ 'install_db.exe',  aDir + 'install_db.exe' );

//  FileCopy_Src_Dest (GetApplicationDir()+ 'install_db.mdb',  aDir + 'install_db.mdb' );
//  FileCopy_Src_Dest (GetApplicationDir()+ 'install_db.exe',  aDir + 'install_db.exe' );

  Assert (FileExists(aDir + 'install_db.mdb'));
  Assert (FileExists(aDir + 'install_db.exe'));


end;


end.




