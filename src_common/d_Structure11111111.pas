unit d_Structure11111111;

interface

uses
  StdCtrls, ExtCtrls,  Classes, Controls, Forms,
  DB, ADODB,  cxGridDBTableView, cxGridLevel, Dialogs,
  cxGrid, ComCtrls, rxPlacemnt,

  u_func,

  dm_MDB,

  u_db,

  DBCtrls, ToolWin, cxGridCustomTableView, cxGridTableView, cxClasses,
  cxControls, cxGridCustomView, Grids, DBGrids, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, cxDBData, cxTextEdit,
  cxDBLookupComboBox, cxCheckBox, cxMemo, dxSkinsCore, dxSkinsDefaultPainters;


type

  Tdlg_Structure111111111111 = class(TForm)
    PageControl1: TPageControl;

    ds_CK: TDataSource;
    ds_FK: TDataSource;
    ds_Indexes: TDataSource;
    ds_Tables: TDataSource;
    ds_Triggers: TDataSource;
    ds_Views: TDataSource;
    cxGrid1DBTableView_Triggername: TcxGridDBColumn;
    cxGrid1DBTableView_Triggercontent: TcxGridDBColumn;
    cxGrid1DBTableView_indexesid: TcxGridDBColumn;
    cxGrid1DBTableView_indexesname: TcxGridDBColumn;
    cxGrid1DBTableView_indexes_is_primary_key: TcxGridDBColumn;
    cxGrid1DBTableView_indexes_is_unique_key: TcxGridDBColumn;
    cxGrid1DBTableView_indexesindex_keys: TcxGridDBColumn;                                                                          
    cxGrid1DBTableView_indexescontent: TcxGridDBColumn;
    TabSheet3: TTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView_FK: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView_FKid: TcxGridDBColumn;
    cxGridDBTableView_FKname: TcxGridDBColumn;
    cxGridDBTableView_FKtablename: TcxGridDBColumn;
    cxGridDBTableView_FKpk_tablename: TcxGridDBColumn;
    TabSheet4: TTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView3tablename: TcxGridDBColumn;
    cxGridDBTableView3name: TcxGridDBColumn;
    cxGridDBTableView3content: TcxGridDBColumn;
    pn_Buttons: TPanel;
    Bevel1: TBevel;
    btn_Cancel: TButton;
    ds_Owner: TDataSource;
    cxGrid1DBTableView1DBColumn1_Owner: TcxGridDBColumn;
    cxGridDBTableView1DBColumn1_Status: TcxGridDBColumn;
    TabSheet6: TTabSheet;
    cxGrid5: TcxGrid;
    cxGridDBTableView_SP: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    ds_SP: TDataSource;
    cxGridDBTableView_SPname: TcxGridDBColumn;
    cxGridDBTableView_SPcontent: TcxGridDBColumn;
    cxGridDBTableView_SPstatus_id: TcxGridDBColumn;
    t_Owner: TADOTable;
    tbl_Triggers: TADOTable;
    tbl_SP: TADOTable;
    tbl_FK: TADOTable;
    tbl_indexes: TADOTable;
    tbl_CK: TADOTable;
    tbl_Views: TADOTable;
    tbl_Tables: TADOTable;
    tbl_TableFields: TADOTable;
    cxGrid1DBTableView1DBColumn1_xtype: TcxGridDBColumn;
    ds_TableFields: TDataSource;
    MDBConnection: TADOConnection;
    cxGrid1Level3: TcxGridLevel;
    cxGrid1DBTableView2_TableFields: TcxGridDBTableView;
    cxGrid1DBTableView2_TableFieldsDBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn4: TcxGridDBColumn;
    cxGridDBTableView1DBColumn1_checked: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn1_checked: TcxGridDBColumn;
    TabSheet_CK: TTabSheet;
    cxGrid6: TcxGrid;
    cxGridDBTableView5: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridLevel5: TcxGridLevel;
    cxGridDBTableView1DBColumn1_text: TcxGridDBColumn;
    TabSheet7: TTabSheet;
    cxGrid_OBJECTS: TcxGrid;
    cxGridDBTableView6: TcxGridDBTableView;
    cxGridLevel6: TcxGridLevel;
    ds_OBJECTS: TDataSource;
    t_OBJECTS: TADOTable;
    cxGridDBTableView6id: TcxGridDBColumn;
    cxGridDBTableView6name: TcxGridDBColumn;
    cxGridDBTableView6display_name: TcxGridDBColumn;
    cxGridDBTableView6table_name: TcxGridDBColumn;
    cxGridDBTableView6folder_name: TcxGridDBColumn;
    cxGridDBTableView6view_name: TcxGridDBColumn;
    cxGridDBTableView6checked: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView2_TableFieldsDBColumn6: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn3: TcxGridDBColumn;
    cxGridDBTableView3DBColumn1: TcxGridDBColumn;
    TabSheet_Owner: TTabSheet;
    cxGrid7: TcxGrid;
    cxGridDBTableView7: TcxGridDBTableView;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridLevel7: TcxGridLevel;
    TabSheet5: TTabSheet;
    cxGrid8: TcxGrid;
    cxGridDBTableView_func: TcxGridDBTableView;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridLevel8: TcxGridLevel;
    ds_Func: TDataSource;
    t_Func: TADOTable;
    cxGridDBTableView_funcDBColumn1: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1_is_exists: TcxGridDBColumn;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn4: TcxGridDBColumn;
    cxGridDBTableView3DBColumn2: TcxGridDBColumn;
    cxGridDBTableView5DBColumn1: TcxGridDBColumn;
    DBMemo_SP: TDBMemo;
    DBMemo_func: TDBMemo;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    DBMemo_Trigger: TDBMemo;
    Splitter4: TSplitter;
    DBMemo_View: TDBMemo;
    FormStorage2: TFormStorage;
    COL_IDENTITY: TcxGridDBColumn;
    COL_IsNullable: TcxGridDBColumn;
    col_xtype1: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn5_xtype: TcxGridDBColumn;
    cxGridDBTableView3DBColumn3_xtype: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn2_xtype: TcxGridDBColumn;
    cxGridDBTableView5DBColumn2_xtype: TcxGridDBColumn;
    cxGridDBTableView_funcDBColumn2_xtype: TcxGridDBColumn;
    cxGrid1DBTableView_indexes_is_unique_index: TcxGridDBColumn;
    TabSheet9: TTabSheet;
    ds_DatabaseInfo: TDataSource;
    t_Database_Info: TADOTable;
    cxGrid9: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel9: TcxGridLevel;
    TabSheet10: TTabSheet;
    cxGrid10: TcxGrid;
    cxGridDBTableView8: TcxGridDBTableView;
    cxGridLevel10: TcxGridLevel;
    cxGridDBTableView8id: TcxGridDBColumn;
    cxGridDBTableView8tablename: TcxGridDBColumn;
    cxGridDBTableView8name: TcxGridDBColumn;
    cxGridDBTableView8is_primary_key: TcxGridDBColumn;
    cxGridDBTableView8is_unique_key: TcxGridDBColumn;
    cxGridDBTableView8is_unique_index: TcxGridDBColumn;
    cxGridDBTableView8index_keys: TcxGridDBColumn;
    cxGridDBTableView8index_description: TcxGridDBColumn;
    cxGridDBTableView8xtype: TcxGridDBColumn;
    cxGridDBTableView8is_exists: TcxGridDBColumn;
    cxGridDBTableView8index_type: TcxGridDBColumn;
    cxGridDBTableView1DBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView_funcDBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView_SPDBColumn2_priority: TcxGridDBColumn;
    cxGridDBTableView4version: TcxGridDBColumn;
    col_product_name: TcxGridDBColumn;
    TabSheet_OBJECTS_main: TTabSheet;
    cxGrid_OBJECTS_main: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel11: TcxGridLevel;
    ds_OBJECTS_main: TDataSource;
    t_OBJECTS_main: TADOTable;
    cxGridDBTableView2id: TcxGridDBColumn;
    cxGridDBTableView2name: TcxGridDBColumn;
    cxGridDBTableView2enabled: TcxGridDBColumn;
    cxGridDBTableView2Owner: TcxGridDBColumn;
    cxGrid1DBTableView_TriggerDBColumn1_Owner: TcxGridDBColumn;
    col_IsComputed: TcxGridDBColumn;
    col_computed_text: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn5: TcxGridDBColumn;
    cxGridDBTableView_FKDBColumn6: TcxGridDBColumn;
    Splitter5: TSplitter;
    cxGridDBTableView3Column2: TcxGridDBColumn;
    ToolBar1: TToolBar;
    ds_Project: TDataSource;
    t_Project: TADOTable;
    DBLookupComboBox1: TDBLookupComboBox;
    TabSheet8: TTabSheet;
    DBGrid1: TDBGrid;
    TabSheet11: TTabSheet;
    cxGrid11: TcxGrid;
    cxGridDBTableView9: TcxGridDBTableView;
    cxGridLevel12: TcxGridLevel;
    ds_UDT: TDataSource;
    t_UDT: TADOTable;
    cxGridDBTableView9id: TcxGridDBColumn;
    cxGridDBTableView9name: TcxGridDBColumn;
    cxGridDBTableView9checked: TcxGridDBColumn;
    cxGridDBTableView9xtype: TcxGridDBColumn;
    cxGridDBTableView9Owner: TcxGridDBColumn;
    cxGridDBTableView9is_exists: TcxGridDBColumn;
    cxGridDBTableView9create_date: TcxGridDBColumn;
    cxGridDBTableView9modify_date: TcxGridDBColumn;
    cxGridDBTableView9project_id: TcxGridDBColumn;
    cxGridDBTableView9object_id: TcxGridDBColumn;
    cxGridDBTableView9sql_create: TcxGridDBColumn;
    cxGridDBTableView9sql_drop: TcxGridDBColumn;
//    procedure FormDestroy(Sender: TObject);
    procedure btn_CancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    procedure Open;

  public
//    class procedure ExecDlg(aProject_id: Integer);
  end;

//var
 // dlg_Structure: Tdlg_Structure;

implementation

{$R *.dfm}


// ---------------------------------------------------------------
procedure Tdlg_Structure111111111111.FormCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  if MDBConnection.Connected then
  begin
    ShowMessage('MDBConnection.Connected');
    MDBConnection.Close;
  end;


  Caption :='Structure';
  PageControl1.Align:=alClient;




  tbl_Tables.TableName      := 'Tables';
  tbl_TableFields.TableName := 'Table_Fields';
  tbl_Views.TableName       := 'Views';
  tbl_Triggers.TableName    := 'Triggers';
  tbl_SP.TableName          := 'StoredProcs';
  t_Func.TableName          := 'Func';
  tbl_indexes.TableName     := 'indexes';
  tbl_FK.TableName          := 'FK';
  tbl_CK.TableName          := 'CK';

  t_OBJECTS.TableName       := '_OBJECTS';
  t_Owner.TableName         := 'Owner';
 
  t_OBJECTS_main.TableName  := '_OBJECTS_main';

  t_Database_Info.TableName  := 'Database_Info';
//  t_.TableName  := 'DatabaseInfo';


  cxGrid_OBJECTS.Align :=alClient;

  Open;

 // TabSheet_sys_objects.Visible := False;

 cxGrid_OBJECTS_main.Align :=alClient;


//  cxGrid1.Align:=alClient;
  cxGrid7.Align:=alClient;
//  cxGrid11.Align:=alClient;

end;



// ---------------------------------------------------------------
procedure Tdlg_Structure111111111111.Open;
// ---------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection1);

 // db_TableOpen1()

  t_Project.Open;


  tbl_Tables.Open;
  tbl_TableFields.Open;
  tbl_Views.Open;
  tbl_Triggers.Open;
  tbl_SP.Open;
  t_Func.Open;
  tbl_indexes.Open;
  tbl_FK.Open;
  tbl_CK.Open;
  t_UDT.Open;

  t_OBJECTS.Open;

  t_Owner.Open;
  t_Database_Info.Open;

  t_OBJECTS_main.Open;





 // t__sys_objects.Open;

(*
  tbl_Tables.TableName        := 'Tables';
  tbl_TableFields.TableName   := 'Table_Fields';
  tbl_Views.TableName         := 'Views';
  tbl_Triggers.TableName      := 'Triggers';
  tbl_StoredProcs.TableName   := 'StoredProcs';
  tbl_indexes.TableName       := 'indexes';
  tbl_FK.TableName            := 'FK';
  tbl_CK.TableName            := 'CK';

  t_OBJECTS.TableName         := '_OBJECTS';
  t_Status.TableName          := 'Status';

*)

 // qry_Status.Open;

end;


procedure Tdlg_Structure111111111111.btn_CancelClick(Sender: TObject);
begin
  Close;
end;

procedure Tdlg_Structure111111111111.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action :=caFree;
end;


end.


{



procedure Tdlg_Structure.FormDestroy(Sender: TObject);
begin
  dlg_Structure  := nil;
end;




//-------------------------------------------------------------------
class procedure Tdlg_Structure111111111111.ExecDlg(aProject_id: Integer);
//-------------------------------------------------------------------
var
  dlg_Structure: Tdlg_Structure

begin
  dlg_Structure:=Tdlg_Structure (FindForm(Tdlg_Structure.ClassName));


  if not Assigned(dlg_Structure) then
    Application.CreateForm(Tdlg_Structure, dlg_Structure);

  dlg_Structure.Show;

  dlg_Structure.t_Project.Locate(FLD_ID, aProject_id, []);


(*  else
  with Tdlg_Structure.Create(Application) do
  begin
    Open();

    Show;
  end;
*)

end;


