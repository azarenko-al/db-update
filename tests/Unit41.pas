unit Unit41;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Grids, Vcl.DBGrids,


 codeSiteLogging,
 
 
  StrUtils,
  
  Data.Win.ADODB, Data.DB, Vcl.Mask, RxToolEdit, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, RxStrHlder,


  u_func


//  u_mdb
  ;

type
  TForm41 = class(TForm)
    ADOConnection_DB: TADOConnection;
    DataSource1: TDataSource;
    ADOTable1: TADOTable;
    ADOQuery1: TADOQuery;
    DBGrid1: TDBGrid;
    Button1: TButton;
    ADOConnection_MDB: TADOConnection;
    ds_tables_for_export: TDataSource;
    t_tables_for_export: TADOTable;
    FilenameEdit1: TFilenameEdit;
    Button2: TButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1name: TcxGridDBColumn;
    cxGrid1DBTableView1index_: TcxGridDBColumn;
    cxGrid1DBTableView1filter: TcxGridDBColumn;
    cxGrid1DBTableView1enable: TcxGridDBColumn;
    t_Project: TADOTable;
    ds_Project: TDataSource;
    DBGrid2: TDBGrid;
    q_Tables: TADOQuery;
    ds_Tables: TDataSource;
    DBGrid3: TDBGrid;
    q_tables_with_identity: TADOQuery;
    ds_tables_with_identity: TDataSource;
    DBGrid4: TDBGrid;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    procedure Add_table(aDataSet: TDataSet; aTableName: string; aSList:
        TstringList);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form41: TForm41;

implementation

{$R *.dfm}


procedure TForm41.Button1Click(Sender: TObject);
var
  bEnable: Boolean;

  oSList: TstringList;
  sFilter: string;
  sName: string;
  sSQL: string;

begin
  oSList:=TstringList.Create;

//  oSList.Add('begin transaction');
//  oSList.Add('GO');
//  oSList.Add('');
//
  
  
  oSList.Add('SET ANSI_WARNINGS  OFF');
  oSList.Add('GO');
  oSList.Add('');

  
  oSList.Add('EXEC sp_MSforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"');
  oSList.Add('GO');
  oSList.Add('');

   

  
//
//  
//if exists (SELECT * FROM sys.databases  where name='kpt' and compatibility_level <100 )
//
//
//--SELECT @level=compatibility_level FROM sys.databases
//--where name='kpt'
//
//--if @level<110 
//  ALTER DATABASE kpt   
//    SET COMPATIBILITY_LEVEL = 110 --{ 130 | 120 | 110 | 100 | 90 }  


  
  
//
//-- disable all constraints
//EXEC sp_MSforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"
//To switch them back on, run: (the print is optional of course and it is just listing the tables)
//
//-- enable all constraints
//exec sp_MSforeachtable @command1="print '?'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
//I fi 


 
  
  q_Tables.Close;
  q_Tables.Open;
   

  q_tables_with_identity.Close;
  q_tables_with_identity.Open;

  with q_Tables do
    while not EOF do
    begin
   {
      bEnable:= FieldBYName('Enable').AsBoolean;
      if not bEnable then
      begin
        Next;
        Continue;
      end;
   }   


      sFilter:= FieldBYName('Filter').AsString;
      sName  := Format('[dbo].[%s]',[FieldBYName('name').AsString]);      

      sSQL:=  'select * from '+ sName;
      if sFilter<>'' then
        sSQL:=sSQL + ' WHERE ' + sFilter;
      

      ADOQuery1.SQL.Text:=sSQL;
      ADOQuery1.Open;

//      
//      ADOTable1.Close;
//      ADOTable1.TableName:= sName;
//      ADOTable1.Open;      
    

      Add_table(ADOQuery1, sName, oSList);

      Next;

 //     break;/////////////////////////////////
    end;
  
//
  oSList.Add('exec sp_MSforeachtable @command1="print ''?''", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"');
  oSList.Add('GO');
  oSList.Add('');  

  
  oSList.SaveToFile(FilenameEdit1.FileName);

  
//
//  Memo1.Lines.Assign(oSList);
//
//
//  Memo1.Lines.
//
// d:\script.sql

//
//  aSList.Add('begin transaction');
//  aSList.Add('GO');
//  aSList.Add('');

 
  FreeAndNil(oSList);
//  FreeAndNil()
//  FreeAndNil ()
//
//  
//  oSList_Names :=TstringList.Create;  
//  oSList_Values:=TstringList.Create;  
//  
  
end;


// ---------------------------------------------------------------
procedure TForm41.Add_table(aDataSet: TDataSet; aTableName: string; aSList:
    TstringList);
// ---------------------------------------------------------------
var
  bIdentity: Boolean;
  I: Integer;
 // oSList: TstringList;
  oSList_Names: TstringList;
  oSList_Values: TstringList;
  s: string;
  s1: string;
  s2: string;
  sFieldName: string;
//  sTableName: string;
  sNames: string;  

  sValue: Ansistring;
//  sWideValue: Widestring;
  
  sValues: string;

  
begin

  oSList_Names :=TstringList.Create;  
  oSList_Values:=TstringList.Create;  

  oSList_Names.Delimiter :=',';
  oSList_Values.Delimiter:=',';  

  
    
   bIdentity:=q_tables_with_identity.Locate('table_name',aTableName,[]);
   
      
  
  if bIdentity then
  begin
    aSList.Add( Format('SET IDENTITY_INSERT %s ON',[aTableName]) );
    aSList.Add('GO');
    aSList.Add('');     
  end;

  
   codeSite.Send( 'aDataSet.RecordCount - ' +  IntToStr(aDataSet.RecordCount) );
  


with aDataSet do  
  while not EOF do
  begin
    
    sNames :='';
    sValues:='';   

    oSList_Names.Clear; 
    oSList_Values.Clear;
     

    for I := 0 to FieldCount - 1 do
      if Fields[i].Value <> null then        
      begin
        sFieldName:= LowerCase(Fields[i].FieldName);


        if Fields[i].DataType = ftGuid then 
          Continue;
          

        if
           (LeftStr(sFieldName,1)='_') or 
           (RightStr(sFieldName,2)='__') or 

           (sFieldName='user_created_id') or 
           (sFieldName='user_modify_id') or 

           (sFieldName='user_modify') or 
           (sFieldName='date_modify') or  
           (sFieldName='user_created') or   
           (sFieldName='date_created') 
        then
          Continue;
      
        

        case Fields[i].DataType of

          ftBoolean: if Fields[i].Value=true then sValue:='1' else sValue:='0' ;


          ftWideString,        
          ftMemo,
          ftString:  begin
                       sValue:=  Fields[i].Value;

                       if sValue='' then
                          Continue;    

                       sValue:=  ReplaceStr(sValue, '''', '''''');

                       sValue:=  ReplaceStr(sValue, '    ', ' ');
                                                                                              
                       sValue:='N''' +sValue + '''';

                     end;


          ftFloat:   sValue:= ReplaceStr(FloatToStr(Fields[i].Value), ',', '.' );
        
          ftAutoInc,
          ftSmallint, 
          ftInteger, 
          ftWord:   sValue:=IntToStr(Fields[i].Value);
          else
            raise Exception.Create( aTableName +'-'+  Fields[i].FieldName + '-'+  IntToStr( Integer( Fields[i].DataType) ))
//            ShowMessage( IntToStr( Integer( Fields[i].DataType) ))

//         (ftUnknown, ftString, ftSmallint, ftInteger, ftWord, // 0..4
//    ftBoolean, ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime, // 5..11
//    ftBytes, ftVarBytes, ftAutoInc, ftBlob, ftMemo, ftGraphic, ftFmtMemo, // 12..18
//    ftParadoxOle, ftDBaseOle, ftTypedBinary, ftCursor, ftFixedChar, ftWideString, // 19..24
//    ftLargeint, ftADT, ftArray, ftReference, ftDataSet, ftOraBlob, ftOraClob, // 25..31
//    ftVariant, ftInterface, ftIDispatch, ftGuid, ftTimeStamp, ftFMTBcd, // 32..37
//    ftFixedWideChar, ftWideMemo, ftOraTimeStamp, ftOraInterval, // 38..41
//    ftLongWord, ftShortint, ftByte, ftExtended, ftConnection, ftParams, ftStream, //42..48
//    ftTimeStampOffset, ftObject, ftSingle); //49..51
//        
          
        end;
        
        oSList_Names.Add(Format('[%s]',[sFieldName]));
        oSList_Values.Add(sValue);
    

//CodeSite.Send(sValue);


      end;
  


    if oSList_Names.Count>0 then
    begin
        s1:=oSList_Names.DelimitedText;

        oSList_Values.QuoteChar:=' ';
        s2:=oSList_Values.DelimitedText;  
        s2:=  ReplaceStr(s2, '    ', ' ');
        
  
        aSList.Add(Format('INSERT INTO %s (%s) ',[aTableName,s1]));
        aSList.Add('VALUES');
        aSList.Add(Format('  (%s)',[s2 ]));
        aSList.Add('GO');
        aSList.Add('');
        
    end;
  
  

//  --break;
    Next;
  end;         

  
  if bIdentity then
  begin
    aSList.Add( Format('SET IDENTITY_INSERT %s OFF',[aTableName]) );
    aSList.Add('GO');
    aSList.Add('');     
  end;

  
end;

procedure TForm41.Button2Click(Sender: TObject);
begin
  t_tables_for_export.Open;

end;

procedure TForm41.Button3Click(Sender: TObject);
begin
//   Button1Click (nil)  ;
end;


end.
