unit dm_Compare_and_Modify_v2;

interface

uses
  dm_Main_SQL,
  u_db,

  System.SysUtils, Forms,  System.Classes, DB;

type
  TdmCompare_and_Modify_v2 = class(TDataModule)
  private

  public
    class function Init: TdmCompare_and_Modify_v2;

    procedure ExecSingleRecord_Table(aDataset, aDataset_columns: DB.TDataset);
    procedure ExecSingleRecord(aDataset: DB.TDataset);

  end;

var
  dmCompare_and_Modify_v2: TdmCompare_and_Modify_v2;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}


class function TdmCompare_and_Modify_v2.Init: TdmCompare_and_Modify_v2;
begin
  if not Assigned(dmCompare_and_Modify_v2) then
    dmCompare_and_Modify_v2:=TdmCompare_and_Modify_v2.Create(Application);

  Result:=dmCompare_and_Modify_v2;
end;


//---------------------------------------------------------
procedure TdmCompare_and_Modify_v2.ExecSingleRecord_Table(aDataset,
    aDataset_columns: DB.TDataset);
//---------------------------------------------------------
var
  I: Integer;
  sErr: string;

 // oStrList: TStringList;
  s: string;

 // oADOConnection: TADOConnection;
  b : Boolean;
  sName_local: string;
  sName_local_: string;
//  sName_local: string;
  sName_remote: string;
  sName_remote_: string;
  sSQL: string;
  sType: string;
//  sType: string;
begin


  {
  if sSQL='' then
  begin
    ShowMessage('UPDATE_SQL = ''''');
    Exit;
  end;
  }

 // FExecutedScript.Add(sSQL);

 // oStrList:=StringToStringList(sSQL, DEF_GO);
 // i :=oStrList.Count;


//  b:=dmMain_SQL.ExecScript(oStrList, sErr);


//  b:=dmMain_SQL.ExecSQL(sSQL, sErr);
  sType        :=LowerCase (aDataset.FieldByName('xType').AsString );
  sName_remote :=aDataset.FieldByName('Name_remote').AsString;


  //-------------------------------------
  //
  //-------------------------------------
  if  (sName_remote<>'') and (sType='u') then
  begin

    aDataset_columns.First;

    with aDataset_columns do
      while not EOF do
      begin
        sName_local_  :=aDataset_columns.FieldByName('Name_local').AsString;
        sName_remote_ :=aDataset_columns.FieldByName('Name_remote').AsString;

     //   if True then
        if sName_remote_='' then
        begin
          sSQL := aDataset_columns.FieldByName('SQL_create').AsString;

          b:=dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);

          if b then
  //          aDataset_columns.Delete
            db_UpdateRecord(aDataset_columns, ['Name_remote', sName_local_])
          else
            db_UpdateRecord(aDataset_columns, [FLD_ERROR, dmMain_SQL.LastErrorMsg]);

        end;

        Next;
      end;
    end;

  //-------------------------------------
  // aDataset main
  //-------------------------------------

  if  (sName_remote='') or (sType<>'u') then
  begin
    sSQL := aDataset.FieldByName('SQL_create').AsString;

    b:=dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);

    if b then
      aDataset.Delete
    else
      db_UpdateRecord(aDataset, [FLD_ERROR, dmMain_SQL.LastErrorMsg]);

  end;

end;


//---------------------------------------------------------
procedure TdmCompare_and_Modify_v2.ExecSingleRecord(aDataset: TDataset);
//---------------------------------------------------------
var
  I: Integer;
  sErr: string;

 // oStrList: TStringList;
  s: string;

 // oADOConnection: TADOConnection;
  b : Boolean;
  sSQL: string;
begin

  sSQL := aDataset.FieldByName('SQL_create').AsString;

  {
  if sSQL='' then
  begin
    ShowMessage('UPDATE_SQL = ''''');
    Exit;
  end;
  }

 // FExecutedScript.Add(sSQL);

 // oStrList:=StringToStringList(sSQL, DEF_GO);
 // i :=oStrList.Count;


//  b:=dmMain_SQL.ExecScript(oStrList, sErr);


//  b:=dmMain_SQL.ExecSQL(sSQL, sErr);
  b:=dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);

  if b then
    aDataset.Delete
  else
    db_UpdateRecord(aDataset, [FLD_ERROR, dmMain_SQL.LastErrorMsg]);

end;




end.
