object dlg_Compare: Tdlg_Compare
  Left = 1126
  Top = 103
  Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093' RPLS DB'
  ClientHeight = 739
  ClientWidth = 1003
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 664
    Width = 1003
    Height = 75
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      1003
      75)
    object Button2: TButton
      Left = 915
      Top = 38
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = btn_CancelClick
    end
    object Button1: TButton
      Left = 5
      Top = 8
      Width = 87
      Height = 23
      Action = act_UpdateAllViews
      TabOrder = 1
    end
    object Button3: TButton
      Left = 111
      Top = 8
      Width = 130
      Height = 23
      Action = act_Export_Objects
      TabOrder = 2
      Visible = False
    end
    object Button6: TButton
      Left = 256
      Top = 8
      Width = 75
      Height = 25
      Action = act_Structure
      TabOrder = 3
      Visible = False
    end
    object Button7: TButton
      Left = 344
      Top = 7
      Width = 65
      Height = 25
      Action = act_Versions
      TabOrder = 4
      Visible = False
    end
    object b_Exec_object: TButton
      Left = 7
      Top = 36
      Width = 75
      Height = 25
      Caption = 'Exec_object'
      TabOrder = 5
      OnClick = b_Exec_objectClick
    end
    object b_Exec_column: TButton
      Left = 88
      Top = 37
      Width = 81
      Height = 25
      Caption = 'Exec_column'
      TabOrder = 6
      OnClick = b_Exec_columnClick
    end
    object Button8: TButton
      Left = 416
      Top = 7
      Width = 81
      Height = 25
      Action = act_Versions_new
      TabOrder = 7
      Visible = False
    end
    object bRunAll: TButton
      Left = 256
      Top = 39
      Width = 75
      Height = 25
      Caption = 'Run All'
      TabOrder = 8
      OnClick = bRunAllClick
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 645
    Width = 1003
    Height = 19
    Panels = <>
  end
  object PageControl_top: TPageControl
    Left = 0
    Top = 0
    Width = 1003
    Height = 249
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Data'
      ImageIndex = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 995
        Height = 169
        Align = alTop
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Data_Compare
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGrid1DBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
            SortIndex = 0
            SortOrder = soAscending
            Width = 55
          end
          object cxGrid1DBTableView1checked: TcxGridDBColumn
            DataBinding.FieldName = 'checked'
          end
          object cxGrid1DBTableView1group_name: TcxGridDBColumn
            DataBinding.FieldName = 'group_name'
            Visible = False
            GroupIndex = 0
            Width = 74
          end
          object cxGrid1DBTableView1xtype: TcxGridDBColumn
            DataBinding.FieldName = 'xtype'
            Options.Editing = False
            Width = 31
          end
          object cxGrid1DBTableView1schema: TcxGridDBColumn
            DataBinding.FieldName = 'schema'
            Options.Editing = False
            Width = 101
          end
          object cxGrid1DBTableView1name_local: TcxGridDBColumn
            DataBinding.FieldName = 'name_local'
            Options.Editing = False
            Width = 183
          end
          object cxGrid1DBTableView1name_remote: TcxGridDBColumn
            DataBinding.FieldName = 'name_remote'
            Options.Editing = False
            Width = 180
          end
          object cxGrid1DBTableView1action: TcxGridDBColumn
            DataBinding.FieldName = 'action'
            Width = 35
          end
          object cxGrid1DBTableView1action_111111111: TcxGridDBColumn
            DataBinding.FieldName = 'action_111111111'
            Visible = False
            Width = 95
          end
          object cxGrid1DBTableView1error: TcxGridDBColumn
            DataBinding.FieldName = 'error'
          end
          object cxGrid1DBTableView1Definition_local: TcxGridDBColumn
            DataBinding.FieldName = 'Definition_local'
            Options.Editing = False
          end
          object cxGrid1DBTableView1Definition_remote: TcxGridDBColumn
            DataBinding.FieldName = 'Definition_remote'
            Options.Editing = False
          end
          object cxGrid1DBTableView1Enabled: TcxGridDBColumn
            DataBinding.FieldName = 'Enabled'
          end
          object cxGrid1DBTableView1owner: TcxGridDBColumn
            DataBinding.FieldName = 'owner'
          end
          object cxGrid1DBTableView1priority111: TcxGridDBColumn
            DataBinding.FieldName = 'priority111'
          end
          object cxGrid1DBTableView1is_updated: TcxGridDBColumn
            DataBinding.FieldName = 'is_updated'
          end
          object cxGrid1DBTableView1SQL_update: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_update'
          end
          object cxGrid1DBTableView1SQL_create: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_create'
          end
          object cxGrid1DBTableView1SQL_drop: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_drop'
          end
          object cxGrid1DBTableView1parent_id: TcxGridDBColumn
            DataBinding.FieldName = 'parent_id'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object PageControl_bottom: TPageControl
    Left = 0
    Top = 257
    Width = 521
    Height = 388
    ActivePage = TabSheet3
    Align = alLeft
    TabOrder = 3
    object TabSheet3: TTabSheet
      Caption = 'Columns'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 513
        Height = 142
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.ScrollbarMode = sbmClassic
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = ds_Compare_v1_table_columns
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Editing = False
          OptionsView.GroupByBox = False
          object cxGridDBTableView1id: TcxGridDBColumn
            DataBinding.FieldName = 'id'
          end
          object cxGridDBTableView1parent_id: TcxGridDBColumn
            DataBinding.FieldName = 'parent_id'
            Visible = False
          end
          object cxGridDBTableView1table_name: TcxGridDBColumn
            DataBinding.FieldName = 'table_name'
            Width = 92
          end
          object cxGridDBTableView1name_local: TcxGridDBColumn
            DataBinding.FieldName = 'name_local'
            Width = 135
          end
          object cxGridDBTableView1name_remote: TcxGridDBColumn
            DataBinding.FieldName = 'name_remote'
            Width = 135
          end
          object cxGridDBTableView1action: TcxGridDBColumn
            DataBinding.FieldName = 'action'
            Width = 35
          end
          object cxGridDBTableView1action_: TcxGridDBColumn
            DataBinding.FieldName = 'action_'
            Width = 41
          end
          object cxGridDBTableView1error: TcxGridDBColumn
            DataBinding.FieldName = 'error'
          end
          object cxGridDBTableView1checked11: TcxGridDBColumn
            DataBinding.FieldName = 'checked11'
          end
          object cxGridDBTableView1update_SQL: TcxGridDBColumn
            DataBinding.FieldName = 'update_SQL'
          end
          object cxGridDBTableView1SQL_create: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_create'
          end
          object cxGridDBTableView1SQL_drop: TcxGridDBColumn
            DataBinding.FieldName = 'SQL_drop'
          end
          object cxGridDBTableView1Enabled: TcxGridDBColumn
            DataBinding.FieldName = 'Enabled'
          end
          object cxGridDBTableView1is_updated: TcxGridDBColumn
            DataBinding.FieldName = 'is_updated'
          end
          object cxGridDBTableView1Definition_local: TcxGridDBColumn
            DataBinding.FieldName = 'Definition_local'
          end
          object cxGridDBTableView1Definition_remote: TcxGridDBColumn
            DataBinding.FieldName = 'Definition_remote'
          end
          object cxGridDBTableView1local_column_id: TcxGridDBColumn
            DataBinding.FieldName = 'local_column_id'
          end
          object cxGridDBTableView1remote_column_id: TcxGridDBColumn
            DataBinding.FieldName = 'remote_column_id'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 152
        Width = 513
        Height = 208
        ActivePage = TabSheet2
        Align = alBottom
        TabOrder = 1
        object TabSheet2: TTabSheet
          Caption = 'TabSheet2'
          object cxDBVerticalGrid1: TcxDBVerticalGrid
            Left = 0
            Top = 0
            Width = 174
            Height = 180
            Align = alLeft
            OptionsView.RowHeaderWidth = 106
            Navigator.Buttons.CustomButtons = <>
            TabOrder = 0
            DataController.DataSource = ds_local_column
            Version = 1
            object cxDBVerticalGrid1id: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'id'
              Visible = False
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid1host: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'host'
              Visible = False
              ID = 1
              ParentID = -1
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid1object_id: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'object_id'
              Visible = False
              ID = 2
              ParentID = -1
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid1schema: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'schema'
              Visible = False
              ID = 3
              ParentID = -1
              Index = 3
              Version = 1
            end
            object cxDBVerticalGrid1TableName: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'TableName'
              Visible = False
              ID = 4
              ParentID = -1
              Index = 4
              Version = 1
            end
            object cxDBVerticalGrid1name: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'name'
              Visible = False
              ID = 5
              ParentID = -1
              Index = 5
              Version = 1
            end
            object cxDBVerticalGrid1type: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'type'
              ID = 6
              ParentID = -1
              Index = 6
              Version = 1
            end
            object cxDBVerticalGrid1Length: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Length'
              ID = 7
              ParentID = -1
              Index = 7
              Version = 1
            end
            object cxDBVerticalGrid1Variable: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Variable'
              ID = 8
              ParentID = -1
              Index = 8
              Version = 1
            end
            object cxDBVerticalGrid1IsNullable: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IsNullable'
              ID = 9
              ParentID = -1
              Index = 9
              Version = 1
            end
            object cxDBVerticalGrid1default_name: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'default_name'
              ID = 10
              ParentID = -1
              Index = 10
              Version = 1
            end
            object cxDBVerticalGrid1default_content: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'default_content'
              ID = 11
              ParentID = -1
              Index = 11
              Version = 1
            end
            object cxDBVerticalGrid1IDENTITY: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IDENTITY'
              ID = 12
              ParentID = -1
              Index = 12
              Version = 1
            end
            object cxDBVerticalGrid1IsComputed: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IsComputed'
              ID = 13
              ParentID = -1
              Index = 13
              Version = 1
            end
            object cxDBVerticalGrid1computed_text: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'computed_text'
              ID = 14
              ParentID = -1
              Index = 14
              Version = 1
            end
            object cxDBVerticalGrid1is_exists: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'is_exists'
              Visible = False
              ID = 15
              ParentID = -1
              Index = 15
              Version = 1
            end
            object cxDBVerticalGrid1sql_create: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'sql_create'
              Visible = False
              ID = 16
              ParentID = -1
              Index = 16
              Version = 1
            end
            object cxDBVerticalGrid1sql_drop: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'sql_drop'
              Visible = False
              ID = 17
              ParentID = -1
              Index = 17
              Version = 1
            end
          end
          object cxDBVerticalGrid2: TcxDBVerticalGrid
            Left = 174
            Top = 0
            Width = 174
            Height = 180
            Align = alLeft
            OptionsView.RowHeaderWidth = 75
            Navigator.Buttons.CustomButtons = <>
            TabOrder = 1
            DataController.DataSource = ds_remote_column
            Version = 1
            object cxDBVerticalGrid2id: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'id'
              Visible = False
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object cxDBVerticalGrid2host: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'host'
              Visible = False
              ID = 1
              ParentID = -1
              Index = 1
              Version = 1
            end
            object cxDBVerticalGrid2object_id: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'object_id'
              Visible = False
              ID = 2
              ParentID = -1
              Index = 2
              Version = 1
            end
            object cxDBVerticalGrid2schema: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'schema'
              Visible = False
              ID = 3
              ParentID = -1
              Index = 3
              Version = 1
            end
            object cxDBVerticalGrid2TableName: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'TableName'
              Visible = False
              ID = 4
              ParentID = -1
              Index = 4
              Version = 1
            end
            object cxDBVerticalGrid2name: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'name'
              Visible = False
              ID = 5
              ParentID = -1
              Index = 5
              Version = 1
            end
            object cxDBVerticalGrid2type: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'type'
              ID = 6
              ParentID = -1
              Index = 6
              Version = 1
            end
            object cxDBVerticalGrid2Length: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Length'
              ID = 7
              ParentID = -1
              Index = 7
              Version = 1
            end
            object cxDBVerticalGrid2Variable: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'Variable'
              ID = 8
              ParentID = -1
              Index = 8
              Version = 1
            end
            object cxDBVerticalGrid2IsNullable: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IsNullable'
              ID = 9
              ParentID = -1
              Index = 9
              Version = 1
            end
            object cxDBVerticalGrid2default_name: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'default_name'
              ID = 10
              ParentID = -1
              Index = 10
              Version = 1
            end
            object cxDBVerticalGrid2default_content: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'default_content'
              ID = 11
              ParentID = -1
              Index = 11
              Version = 1
            end
            object cxDBVerticalGrid2IDENTITY: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IDENTITY'
              ID = 12
              ParentID = -1
              Index = 12
              Version = 1
            end
            object cxDBVerticalGrid2IsComputed: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'IsComputed'
              ID = 13
              ParentID = -1
              Index = 13
              Version = 1
            end
            object cxDBVerticalGrid2computed_text: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'computed_text'
              ID = 14
              ParentID = -1
              Index = 14
              Version = 1
            end
            object cxDBVerticalGrid2is_exists: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'is_exists'
              Visible = False
              ID = 15
              ParentID = -1
              Index = 15
              Version = 1
            end
            object cxDBVerticalGrid2sql_create: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'sql_create'
              Visible = False
              ID = 16
              ParentID = -1
              Index = 16
              Version = 1
            end
            object cxDBVerticalGrid2sql_drop: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'sql_drop'
              Visible = False
              ID = 17
              ParentID = -1
              Index = 17
              Version = 1
            end
          end
          object DBMemo5: TDBMemo
            Left = 348
            Top = 0
            Width = 249
            Height = 180
            Align = alLeft
            DataField = 'error'
            DataSource = ds_Compare_v1_table_columns
            ScrollBars = ssBoth
            TabOrder = 2
          end
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'SQL_create'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 513
        Height = 326
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        DataField = 'SQL_create'
        DataSource = ds_Data_Compare
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Definition'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBMemo2: TDBMemo
        Left = 257
        Top = 0
        Width = 236
        Height = 360
        Align = alLeft
        DataField = 'Definition_remote'
        DataSource = ds_Data_Compare
        ScrollBars = ssBoth
        TabOrder = 0
      end
      object DBMemo3: TDBMemo
        Left = 0
        Top = 0
        Width = 257
        Height = 360
        Align = alLeft
        DataField = 'Definition_local'
        DataSource = ds_Data_Compare
        ScrollBars = ssBoth
        TabOrder = 1
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 249
    Width = 1003
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salTop
    Control = PageControl_top
    ExplicitWidth = 8
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 744
    Top = 536
    object act_Ok: TAction
      Category = 'Main'
      Caption = #1057#1086#1079#1076#1072#1090#1100
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
      OnExecute = _Action
    end
    object act_Compare: TAction
      Caption = #1057#1088#1072#1074#1085#1080#1090#1100
      OnExecute = _Action
    end
    object act_Modify: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1041#1044
      OnExecute = _Action
    end
    object act_Run_Dict_Import: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
      OnExecute = _Action
    end
    object act_UpdateViews: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1087#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1103
      OnExecute = _Action
    end
    object act_UpdateVer: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1074#1077#1088#1089#1080#1102
      OnExecute = _Action
    end
    object act_UpdateObjFields: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1086#1090#1086#1073#1088#1072#1078#1072#1077#1084#1099#1077' '#1087#1086#1083#1103
      OnExecute = _Action
    end
    object act_ShowLog: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1083#1086#1075' '#1086#1096#1080#1073#1086#1082
      OnExecute = _Action
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredProps.Strings = (
      'PageControl_top.ActivePage'
      'PageControl_bottom.ActivePage'
      'PageControl_top.Height')
    StoredValues = <>
    Left = 688
    Top = 448
  end
  object ActionList2: TActionList
    Left = 648
    Top = 536
    object act_Export_Objects: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1086#1073#1098#1077#1082#1090#1086#1074
      OnExecute = act_Export_ObjectsExecute
    end
    object act_Exec_record: TAction
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      ShortCut = 16471
      OnExecute = act_Exec_recordExecute
    end
    object act_Compare_SQL: TAction
      Caption = #1057#1088#1072#1074#1085#1080#1090#1100' SQL'
      ShortCut = 16465
      OnExecute = act_Exec_recordExecute
    end
    object act_Show_Executed_script: TAction
      Caption = 'Show_Executed_script'
      OnExecute = act_Exec_recordExecute
    end
    object act_Versions: TAction
      Caption = 'Versions'
      OnExecute = act_Exec_recordExecute
    end
    object act_Check_all: TAction
      Caption = 'Check_all'
      OnExecute = act_Exec_recordExecute
    end
    object act_UnCheck_all: TAction
      Caption = 'UnCheck_all'
      OnExecute = act_Exec_recordExecute
    end
    object act_Show_checked_script: TAction
      Caption = 'Show_checked_script'
      OnExecute = act_Exec_recordExecute
    end
    object act_Structure: TAction
      Caption = 'Structure'
      OnExecute = act_Exec_recordExecute
    end
    object act_Versions_new: TAction
      Caption = 'Versions_new'
      OnExecute = act_Exec_recordExecute
    end
    object act_Dlg_Details: TAction
      Caption = 'act_Dlg_Details'
    end
    object act_UpdateAllViews: TAction
      Caption = 'Update AllViews'
      OnExecute = act_Exec_recordExecute
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 568
    Top = 536
    object MenuItem1: TMenuItem
      Action = act_Exec_record
    end
    object MenuItem2: TMenuItem
      Action = act_Compare_SQL
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = act_Show_Executed_script
    end
    object actCheckall1: TMenuItem
      Action = act_Check_all
    end
    object ScriptCheckedToNotepad1: TMenuItem
      Action = act_UnCheck_all
    end
    object ScriptToNotepad1: TMenuItem
      Action = act_Show_checked_script
    end
    object N3: TMenuItem
      Caption = '-'
    end
  end
  object ds_Data_Compare: TDataSource
    DataSet = t_Data_Compare
    Left = 568
    Top = 352
  end
  object t_Data_Compare: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    TableName = '_Compare_v1'
    Left = 568
    Top = 296
  end
  object ADOConnection_MDB: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=W:\RP' +
      'LS_DB update\bin\install_db.mdb;Mode=Share Deny None;Persist Sec' +
      'urity Info=False;Jet OLEDB:System database="";Jet OLEDB:Registry' +
      ' Path="";Jet OLEDB:Database Password="";Jet OLEDB:Engine Type=5;' +
      'Jet OLEDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk ' +
      'Ops=2;Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Databas' +
      'e Password="";Jet OLEDB:Create System Database=False;Jet OLEDB:E' +
      'ncrypt Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=Fal' +
      'se;Jet OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=' +
      'False;'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 560
    Top = 452
  end
  object ds_Compare_v1_table_columns: TDataSource
    DataSet = t_Compare_v1_table_columns
    Left = 712
    Top = 360
  end
  object t_Compare_v1_table_columns: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'parent_id'
    MasterFields = 'id'
    MasterSource = ds_Data_Compare
    TableName = '_Compare_v1_table_columns'
    Left = 712
    Top = 296
  end
  object ds_local_column: TDataSource
    DataSet = t_local_column
    Left = 840
    Top = 360
  end
  object t_local_column: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'local_column_id'
    MasterSource = ds_Compare_v1_table_columns
    TableName = 'sys_objects_table_columns'
    Left = 840
    Top = 296
  end
  object ds_remote_column: TDataSource
    DataSet = t_remote_column
    Left = 936
    Top = 360
  end
  object t_remote_column: TADOTable
    Connection = ADOConnection_MDB
    CursorType = ctStatic
    IndexFieldNames = 'id'
    MasterFields = 'remote_column_id'
    MasterSource = ds_Compare_v1_table_columns
    TableName = 'sys_objects_table_columns'
    Left = 936
    Top = 296
  end
end
