unit f_Main;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs,  StdActns, DBCtrls, cxVGrid,
  ActnList, StdCtrls, ExtCtrls, Db, ADODB, ComCtrls,
  rxToolEdit, cxDropDownEdit,

  dm_CompareAndModify_v1,

  dm_ExportStructure,

  d_Compare,

//d_Actions,

  u_sql,


  dm_Connection,

//  I_DB_login,

  //d_Database_tables,

  u_cx_VGrid_export,

  d_Wizard,

  u_Log,

  d_versions_Edit,

  dm_MDB,

  dm_Main_SQL,
  dm_SQL_TO_MDB,

//  d_Structure,
  d_Structure,

  fra_DB_Login,

 // fr_Tables,

  u_db,

//  u_cx_vgrid_export,
  u_cx_vgrid,

  u_func,


  ToolWin, cxControls, cxInplaceContainer, Mask, cxPropertiesStore,
  rxPlacemnt, cxDBVGrid, Grids, DBGrids, cxLookAndFeels, cxGraphics,
  cxLookAndFeelPainters, cxStyles, cxEdit, cxButtonEdit, cxCheckBox,
   cxMemo, cxClasses, System.Actions, dxSkinsCore

  //d_SelectObjectFields,

  ;

type
  Tfrm_Main = class(Tdlg_Wizard)
    act_SetupTables: TAction;
    Button1: TButton;
    act_Structure: TAction;
    act_Versions: TAction;
    GroupBox1_Log: TGroupBox;
    RichEdit1: TRichEdit;
    Button3: TButton;
    act_Database_tables: TAction;
    ActionList2: TActionList;
    act_FileOpen1: TFileOpen;
    Button4: TButton;
    act_Structure_new: TAction;
    ds_Projects: TDataSource;
    t_Projects: TADOTable;
    FileSaveAsIni: TFileSaveAs;
    ADOConnection1: TADOConnection;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    cxVerticalGrid1: TcxVerticalGrid;
    row_SQL_script: TcxEditorRow;
    row_Include_Library: TcxEditorRow;
    row_Library_filename: TcxEditorRow;
    cxVerticalGrid1EditorRow3: TcxEditorRow;
    cxVerticalGrid1EditorRow4: TcxEditorRow;
    row_Is_Export_Objects_and_PickList: TcxEditorRow;
    row_Is_Export_DatabaseInfo_New: TcxEditorRow;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    row_server: TcxDBEditorRow;
    row_database: TcxDBEditorRow;
    row_auth: TcxDBEditorRow;
    row_Login: TcxDBEditorRow;
    row_password: TcxDBEditorRow;
    cxDBVerticalGrid1library_enabled: TcxDBEditorRow;
    cxDBVerticalGrid1library_path: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow1: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow;
    cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow2: TcxCategoryRow;
    cxDBVerticalGrid1CategoryRow3: TcxCategoryRow;
    cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow;
    cxDBVerticalGrid1CategoryRow4: TcxCategoryRow;
    Button6: TButton;
    act_Actions: TAction;
    btn_Save_Service_Script: TButton;
    Button9: TButton;
    row_Is_Save_Service_ScriptToFile: TcxEditorRow;
    cxPropertiesStore1: TcxPropertiesStore;
    PageControl_connection: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    pn_main: TPanel;
    gb_ConnectSQL: TGroupBox;
    GroupBox2: TGroupBox;
    ed_db_version1: TFilenameEdit;
    ed_db_version2: TFilenameEdit;
    GroupBox3: TGroupBox;
    ed_Script: TFilenameEdit;
    Button5: TButton;
    GroupBox1: TGroupBox;
    ed_Publish_dir: TDirectoryEdit;
    GroupBox4: TGroupBox;
    ed_Library: TFilenameEdit;
    cb_Include_Library: TCheckBox;
    GroupBox_Connect_remote: TGroupBox;
    act_Load_Remote: TAction;
    Button7: TButton;
    Button8: TButton;
    bCompare: TButton;
    b_d_Compare: TButton;
    Button2: TButton;
    b_Remote_copy: TButton;
    procedure FormCreate(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);
//    procedure ed_MSAccessFileName1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure act_OkExecute(Sender: TObject);
//    prcxocedure act_SelectObjectFieldsExecute(Sender: TObject);
//    procedure act_SetupTablesExecute(Sender: TObject);
    procedure act_StructureExecute(Sender: TObject);
    procedure bCompareClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure btn_Save_Service_ScriptClick(Sender: TObject);
    procedure b_Remote_copyClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure b_d_CompareClick(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure cxDBVerticalGrid1serverEditPropertiesInitPopup(
      Sender: TObject);
    procedure cxDBVerticalGrid1library_pathEditPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure Panel3Click(Sender: TObject);
    procedure row_databaseEditPropertiesInitPopup(Sender: TObject);
//    procedure Button2Click(Sender: TObject);

  private
    Fframe_DB_Login_local: Tframe_DB_Login;
    Fframe_DB_Login_remote: Tframe_DB_Login;

    procedure CheckScript;
// TODO: ConnectONEGA
////    FLoginRec : TdbLoginRec;
//
//  procedure ConnectONEGA;
    procedure DoProceedException(Sender: TObject; E: Exception);

    procedure Exec_local;
    procedure Exec_remote;

    procedure UpdateDatabaseList;
    procedure UpdateServerList;
  public
    procedure SaveToClass;
  end;

var
  frm_Main: Tfrm_Main;


implementation {$R *.DFM}

uses
  u_sync_classes, dm_Object_Fields;



const
  DEF_REG_PATH  = 'Software\Onega\Install_DB\';



//--------------------------------------------------------------
procedure Tfrm_Main.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;

   if ADOConnection1.Connected then
     ShowMessage('procedure TfSQL_TO_MDB.DataModuleCreate(Sender: TObject); - MDBConnection.Connected');

  Caption := Caption + GetAppVersionStr;

  //-----------------------------------------------------------------
  CreateChildForm_(Tframe_DB_Login, Fframe_DB_Login_local, gb_ConnectSQL);

  Fframe_DB_Login_local.IniFileName := ChangeFileExt(Application.ExeName, '.ini');
  Fframe_DB_Login_local.LoadFromIni();

  gb_ConnectSQL.Height := Fframe_DB_Login_local.MaxHeight;

  //-----------------------------------------------------------------
  CreateChildForm_(Tframe_DB_Login, Fframe_DB_Login_remote, GroupBox_Connect_remote);

  Fframe_DB_Login_remote.IniFileName := ChangeFileExt(Application.ExeName, '_remote.ini');
  Fframe_DB_Login_remote.LoadFromIni();

  GroupBox_Connect_remote.Height := Fframe_DB_Login_remote.MaxHeight;

  //-----------------------------------------------------------------

  g_Log.RichEdit := RichEdit1;

  TdmMain_SQL.Init;
  TdmMDB.Init1;
  dmMDB.CompactDatabase1;


  TdmCompareAndModify_v1.Init;

  pn_main.Align:=alClient;

  PageControl_connection.Align:=alClient;

 // cxVerticalGrid1.Align:=alClient;


//  TdmMain.Init;
 // dmMain.OpenDB();
//  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;


  //OpenDB(True) then


  Application.OnException:= DoProceedException;

 // gl_Reg.InitRegistry1 ('GuidesExportImport');

 // with gl_Reg.RegIni do
//    ed_MSAccessFileName.Text:= ReadString (Name, ed_MSAccessFileName.Name, '');

  if not FileExists(ed_db_version1.FileName) then
    ed_db_version1.FileName :=GetApplicationDir()+ 'db_version.ini';

  if not FileExists(ed_Script.FileName) then
    ed_Script.FileName :=GetApplicationDir()+ 'onega.sql';




/////  if not FileExists(FileName1.FileName) then
///////    FileName1.FileName:= GetApplicationDir()+ 'onega_tables.mdb';

  //dmMain.CustomDbLoginRegPath:= 'DBSyncPrepare';

//  Fframe_DB_Connect_Params:= Tframe_DB_Connect_Params.CreateChildForm (Self, pn_Connect);
//  Fframe_DB_Connect_Params.IsShowBorder:= false;
//  Fframe_DB_Connect_Params.pn_Connect.Visible:= false;

  pn_Main.Align:= alClient;
 ////// gb_ConnectSQL.Align:= alClient;
//  SetActionName ('������� ������ �� SQL-������� Infotel � ���� MS ACCESS');

//  t_sys_Projects.Connection:=dmMDB.ADOConnection1;

  db_SetComponentADOConnection(Self, dmMDB.ADOConnection_MDB);

  db_TableOpen1 (t_Projects,  'Projects' );

  cx_VerticalGrid_LoadFromReg (cxVerticalGrid1, DEF_REG_PATH);

//
//  procedure cx_VerticalGrid_SaveToReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
//    string);
//
//procedure cx_VerticalGrid_LoadFromReg(aVerticalGrid: TcxVerticalGrid; aRegPath:
//    string);




 //////////////// SetDefaultSize;
end;


// -------------------------------------------------------------------
procedure Tfrm_Main.FormDestroy(Sender: TObject);
// -------------------------------------------------------------------
begin
//  dmMDB.CompactDatabase1;

  cx_VerticalGrid_SaveToReg (cxVerticalGrid1, DEF_REG_PATH);



  if Fframe_DB_Login_local.IsModified then
    Fframe_DB_Login_local.SaveToIni();

  if Fframe_DB_Login_remote.IsModified then
    Fframe_DB_Login_remote.SaveToIni();


(*  with gl_Reg.RegIni do
    WriteString (Name, ed_MSAccessFileName.Name,  ed_MSAccessFileName.Text);

  gl_Reg.SaveAndClearGroup (Self);
*)
  inherited;
end;


//--------------------------------------------------------------
procedure Tfrm_Main.ActionList1Update(Action: TBasicAction;  var Handled: Boolean);
//--------------------------------------------------------------
begin
 // act_Ok.Enabled:= (FileName1.FileName  <> '');
 // act_SelectObjectFields.Enabled:= (RadioButton2.Checked);
end;

procedure Tfrm_Main.act_OkExecute(Sender: TObject);
begin
  Exec_local;
end;

//--------------------------------------------------------------
procedure Tfrm_Main.Exec_local;
//--------------------------------------------------------------
//var
 // s: string;
var
  oDBStructure: TDBStructure;
begin
//  s:=Fframe_DB_Login_local.GetADOConnectionString;

  if not dmMain_SQL.OpenDB(Fframe_DB_Login_local.GetLoginRec()) then
    Exit;

 // begin
//    ErrorDlg('������ �����������:');
 // end;
 {

 function TdmMain_SQL.OpenDB(aRec: TdbLoginRec): Boolean;
begin
  LoginRec:=aRec;

  //TdmConnection.Init;
  Result := dmConnection1.OpenDatabase (aRec, ADOConnection1);

//  ADOConnection1.Da

//  ADOConnection1.DefaultDatabase :=aRec.DataBase;

end;

 }


 // dmMDB.OpenDB1(); //(FileName1.FileName);



(*  with Fframe_DB_Connect_Params do
  begin
    if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then begin
      ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
      ModalResult:= mrNone;
      exit;
    end;

    dmMain.SaveConnectRecToReg(Server, DefaultDb, User, Password, UseWinAuth);
//    dmMain.Add_ODBC_Source(Server, DefaultDb, User);
  end;

  if not dmMain.Open then
    ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
*)
  //-------------------------------------
  //      �������
 // dmSQL_to_MDB.Params.MDBFileName:= FileName1.FileName;

(*
  if (dmSQL_to_MDB.Params.UnloadObjectFieldIDs.Count=0) AND (act_SelectObjectFields.Enabled) then
  begin
    ShowMessage('������� �������� ����');
    exit;
  end;
*)


(*  if FileExists(ed_db_version.FileName) then
    dmSQL_to_MDB.Params.IniFileName :=ed_db_version.FileName
  else
    dmSQL_to_MDB.Params.IniFileName :='';

*)


//   dmSQL_to_MDB.Params. IniFileName :=ed_db_version.FileName;

//   dmSQL_to_MDB1.Params.Project_id := 2; //row_Project_name.Properties.Value;

   dmSQL_to_MDB1.Params.IniFileName_ :=ed_db_version1.FileName;
   dmSQL_to_MDB1.Params.IniFileName2 :=ed_db_version2.FileName;

   dmSQL_to_MDB1.Params.FileName_Script :=ed_Script.FileName;

   dmSQL_to_MDB1.Params.Is_Export_Objects_and_PickList := row_Is_Export_Objects_and_PickList.Properties.Value;
   dmSQL_to_MDB1.Params.is_Export_DatabaseInfo         := row_Is_Export_DatabaseInfo_New.Properties.Value;


   dmSQL_to_MDB1.Params.Is_Save_Service_ScriptToFile   := row_Is_Save_Service_ScriptToFile.Properties.Value;


//   True;


{
   if cb_Include_Library.Checked then
     dmSQL_to_MDB1.Params.FileName_Library :=ed_Library.FileName
   else
     dmSQL_to_MDB1.Params.FileName_Library :='';
 }


 // if RadioButton1.Checked then
  //  dmSQL_to_MDB.Params.IsUnloadObjectFields:=cb_Load_user_columns.Checked;  //RadioGroup1.ItemIndex=0;

 // if RadioButton2.Checked then
   // dmSQL_to_MDB.Params.UnloadObjectFieldsType:= uotCustom;

 // if RadioButton3.Checked then
  //  dmSQL_to_MDB.Params.UnloadObjectFieldsType:= uotNone;

(*  case rg_Objects.ItemIndex of
    0: dmSQL_to_MDB.Params.UnloadObjectFieldsType:= uotAll;
    1: dmSQL_to_MDB.Params.UnloadObjectFieldsType:= uotCustom;
    2: dmSQL_to_MDB.Params.UnloadObjectFieldsType:= uotNone;
  end;
*)
(*
  dmSQL_to_MDB.Params.UpdateStructure   := cb_UpdateStructure.Checked;
  dmSQL_to_MDB.Params.UpdateRRL         := cb_Link.Checked;
  dmSQL_to_MDB.Params.Update2G          := cb_2G.Checked;
  dmSQL_to_MDB.Params.Update3G          := cb_3G.Checked;
  dmSQL_to_MDB.Params.UpdateOtherObjects:= cb_others.Checked;
*)
  //-------------------------------------

//  Screen.Cursor:= crHourGlass;
//  dmSQL_TO_MDB.Params.DatabaseName := Fframe_DB_Login_local.GetLoginRec.DataBase;

//  dmSQL_TO_MDB.ExecuteDlg ('������� ��������...');
//  dmSQL_TO_MDB1.Params.HOST:='local';
  dmSQL_TO_MDB1.Params.HostType:=ht_Local;
  dmSQL_TO_MDB1.Execute;

{
  oDBStructure:=TDBStructure.create;

 // Assert(Assigned(dmExportStructure));
  TdmExportStructure.Init().Load_DBStructure( oDBStructure, 'local');
//  TdmExportStructure.Init().Load_DBStructure(dmMain_SQL.ADOConnection1, oDBStructure, 'local');

  FreeAndNil(oDBStructure);
}
  //   dmExportStructure.
//    aDBStructure: TDBStructure; aHOST: string);


  dmMain_SQL.Close;

//////////////// ............. dmMDB.PublishToDir(ed_Publish_dir.InitialDir);

//  dmExportStructure_2005.Save_Service_ScriptToFile(ADOConnection1,
//     ChangeFileExt(Application.ExeName, '.service.sql') );


  ShowMessage('���������.');




//  Screen.Cursor:= crDefault;

//  Close;
end;



//--------------------------------------------------------------
procedure Tfrm_Main.Exec_remote;
//--------------------------------------------------------------
//var
 // s: string;
var
  oDBStructure: TDBStructure;
begin
//  s:=Fframe_DB_Login_local.GetADOConnectionString;

  if not dmMain_SQL.OpenDB(Fframe_DB_Login_remote.GetLoginRec()) then
    Exit;



//  dmSQL_TO_MDB.ExecuteDlg ('������� ��������...');
//  dmSQL_TO_MDB1.Params.HOST:='remote';
  dmSQL_TO_MDB1.Params.HostType:=ht_Remote;
  dmSQL_TO_MDB1.Execute;


  dmMain_SQL.Close;

//////////////// ............. dmMDB.PublishToDir(ed_Publish_dir.InitialDir);

//  dmExportStructure_2005.Save_Service_ScriptToFile(ADOConnection1,
//     ChangeFileExt(Application.ExeName, '.service.sql') );


  ShowMessage('���������.');


//  Screen.Cursor:= crDefault;

//  Close;
end;


//--------------------------------------------------------------
procedure Tfrm_Main.DoProceedException(Sender: TObject; E: Exception);
//--------------------------------------------------------------
begin
  ShowMessage('���������� ' + E.Message);
end;


// ---------------------------------------------------------------
procedure Tfrm_Main.act_StructureExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  s: string;
begin
//  if Sender=act_Actions then
//    Tdlg_Actions.ShowForm else


  // -------------------------
//  if Sender=act_Structure then
  // -------------------------
//  begin
   // if not dmMDB.ADOConnection1.Connected then
     // dmMDB.OpenDB1(); //(FileName1.FileName);

//    Tdlg_Structure.ExecDlg(2);
//  end else


  // -------------------------
  if Sender=act_Structure_new then
  // -------------------------
  begin
  //  if not dmMDB.ADOConnection1.Connected then
  //    dmMDB.OpenDB1(); //(FileName1.FileName);

    Tdlg_Structure_new.ExecDlg();
  end else



{  if Sender=act_Database_tables then
  begin
    if dmMain_SQL.OpenDB(Fframe_DB_Login_local.GetLoginRec()) then
      Tdlg_Database_tables.ExecDlg;

  end else
}

  // -------------------------
  if Sender=act_Versions then
  // -------------------------
  begin
   // if not dmMDB.ADOConnection1.Connected then
   //   dmMDB.OpenDB1();  // (FileName1.FileName);

//    dmMDB.ConnectMDB(FileName1.FileName);

    if not dmMain_SQL.OpenDB(Fframe_DB_Login_local.GetLoginRec()) then

//    s:=Fframe_DB_Login_local.GetADOConnectionString;

//    if not dmMain_SQL.OpenConnectionString(s) then
   // begin
  //    ErrorDlg('������ �����������:');
      Exit;
   // end;


   // dmSQL_TO_MDB.ConnectMDB(FileName1.FileName);
    Tdlg_versions.ExecDlg();

  end;


end;


procedure Tfrm_Main.bCompareClick(Sender: TObject);
begin
//  if not dmMain_SQL.OpenDB(Fframe_DB_Login_remote.GetLoginRec()) then
//    Exit;

  TdmCompareAndModify_v1.Init().Execute;
end;


procedure Tfrm_Main.Button5Click(Sender: TObject);
begin
  CheckScript;
end;

procedure Tfrm_Main.btn_Save_Service_ScriptClick(Sender: TObject);
begin
  if not dmMain_SQL.OpenDB(Fframe_DB_Login_local.GetLoginRec()) then
    Exit;

  TdmExportStructure.Init;

  dmExportStructure.Save_Service_ScriptToFile_(dmMain_SQL.AdoConnection1,
      ExtractFilePath (Application.ExeName) + 'install_db.service.sql') ;

  FreeAndNil(dmExportStructure);



//  dmExportStructure_2005.Save_Service_ScriptToFile(ADOConnection1,
 //    ChangeFileExt(Application.ExeName, '.service.sql') );


end;

procedure Tfrm_Main.b_Remote_copyClick(Sender: TObject);
begin
  if not dmMain_SQL.OpenDB(Fframe_DB_Login_remote.GetLoginRec()) then
  Exit;


  TdmObject_Fields.Init.remote_CopyObjects;
  /////////////////

end;

procedure Tfrm_Main.Button2Click(Sender: TObject);
begin
  if not dmMain_SQL.OpenDB(Fframe_DB_Login_local.GetLoginRec()) then
    Exit;


  TdmObject_Fields.Init.Local_Create_ObjectTables;
  /////////////////

end;

procedure Tfrm_Main.b_d_CompareClick(Sender: TObject);
begin
  if not dmMain_SQL.OpenDB(Fframe_DB_Login_remote.GetLoginRec) then
    Exit;

  Tdlg_Compare.ExecDlg;
end;

procedure Tfrm_Main.Button7Click(Sender: TObject);
begin
  Exec_Local;
end;

procedure Tfrm_Main.Button8Click(Sender: TObject);
begin
  Exec_remote;
//  inherited;

  //DBLookupComboBox1.KeyValue :=1;

end;

procedure Tfrm_Main.Button9Click(Sender: TObject);
begin
  Tdlg_Structure_new.ExecDlg();

end;


// ---------------------------------------------------------------
procedure Tfrm_Main.CheckScript;
// ---------------------------------------------------------------
var
//  oSList1: TStringList;
  oSList: TStringList;
//  DEF_DATABASE: string;
  sSQL: string;

  rec: TdbLoginRec;
const
  DEF_DATABASE='aaaaa123121131';

begin
  rec:=Fframe_DB_Login_local.GetLoginRec();
  //----------------------------------

  sSQL:=
    Format('IF db_id(''%s'') IS NOT NULL DROP DATABASE %s', [DEF_DATABASE,DEF_DATABASE]) + CRLF +
    'go' + CRLF +
    Format('CREATE DATABASE %s', [DEF_DATABASE]) + CRLF +
    'go';


//
//
//  oSList1:=TStringList.Create;
//
//  oSList1.Add(Format('IF db_id(''%s'') IS NOT NULL DROP DATABASE %s', [DEF_DATABASE,DEF_DATABASE]));
//  oSList1.Add('go');
//
//  oSList1.Add(Format('CREATE DATABASE %s', [DEF_DATABASE]));
//  oSList1.Add('go');
//
//  sSQL:=oSList1.Text;
//
//  FreeAndNil(oSList1);

  //----------------------------------

  if dmMain_SQL.OpenDB(rec) then
    dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);



  oSList:=TStringList.Create;
  oSList.LoadFromFile(ed_Script.FileName);



//  IF db_id('SQLServerPlanet') IS NOT NULL
//BEGIN
//    SELECT 'database does exist'
//END
//
//  oSList.Insert(0,Format('IF db_id(''%s'') IS NOT NULL DROP DATABASE %s', [DEF_DATABASE,DEF_DATABASE]));
//  oSList.Insert(1,'go');
//
//
//  oSList.Insert(2,Format('CREATE DATABASE %s', [DEF_DATABASE]));
//  oSList.Insert(3,'go');
//
//  oSList.Add(Format('DROP DATABASE %s', [DEF_DATABASE]));
//  oSList.Add('go');

  oSList.SaveToFile(ed_Script.FileName+'_');

  sSQL := oSList.Text;

  FreeAndNil(oSList);

  ShellExec_Notepad(ed_Script.FileName+'_');

//  dmMain_SQL.

//  rec:=Fframe_DB_Login_local.GetLoginRec();
  rec.DataBase:=DEF_DATABASE;


  if dmMain_SQL.OpenDB(rec) then
    dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);



//  oSList1.Add(Format('CREATE DATABASE %s', [DEF_DATABASE]));




//osql -S %server%   -U %user% -P %pass%   -Q "DROP DATABASE %test_DATABAS%"
//
//osql -S %server%   -U %user% -P %pass%   -Q "CREATE DATABASE %test_DATABASE%"

//  if act_FileOpen1.Dialog.Execute then
 //   ShowMessage(act_FileOpen1.Dialog.FileName);

end;



procedure Tfrm_Main.cxDBVerticalGrid1DBEditorRow1EditPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  inherited;

//  if act_FileOpen1.Dialog.Execute then
 //   ShowMessage(act_FileOpen1.Dialog.FileName);

end;


procedure Tfrm_Main.cxDBVerticalGrid1serverEditPropertiesInitPopup(
  Sender: TObject);
begin
  inherited;

  //rec.Server  := AsString(row_server.Properties.Value);

  UpdateServerList();

//  SaveToClass();

 // ShowMessage('cxDBVerticalGrid1serverEditPropertiesInitPopup');

end;

//---------------------------------------------------------------------------
procedure Tfrm_Main.UpdateServerList;
//---------------------------------------------------------------------------
var
  oComboBoxProperties: TcxComboBoxProperties;
begin

  oComboBoxProperties:=TcxComboBoxProperties(row_server.Properties.EditProperties);

  if oComboBoxProperties.Items.Count=0 then
    db_ListAvailableSQLServers(oComboBoxProperties.Items);


//  cx_BrowseFile(Sender);
end;



//---------------------------------------------------------------------------
procedure Tfrm_Main.UpdateDatabaseList;
//---------------------------------------------------------------------------
var
  oComboBoxProperties: TcxComboBoxProperties;
  rec: TdbLoginRec;
  s: string;
begin
  oComboBoxProperties:=TcxComboBoxProperties(row_database.Properties.EditProperties);
//  if oComboBoxProperties.Items.Count>0 then
 //   exit;


  FillChar(rec, SizeOf(rec), 0);

  rec.Server  := AsString(row_server.Properties.Value);
  rec.Login   := AsString(row_Login.Properties.Value);
  rec.DataBase:= AsString(row_DataBase.Properties.Value);
  rec.Password:= AsString(row_Password.Properties.Value);

  s:=AsString(row_auth.Properties.Value);

  rec.IsUseWinAuth:=Eq(s,'win');



  dmConnection.SQLServer_GetDatabaseList(rec, oComboBoxProperties.Items);


//  cx_BrowseFile(Sender);
end;




procedure Tfrm_Main.cxDBVerticalGrid1library_pathEditPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  cx_BrowseFile_(Sender);
end;

procedure Tfrm_Main.Panel3Click(Sender: TObject);
begin
  inherited;
end;

//---------------------------------------------------------------------------
procedure Tfrm_Main.SaveToClass;
//---------------------------------------------------------------------------
var
  rec: TdbLoginRec;
begin
  rec.Server  := AsString(row_server.Properties.Value);
  rec.Login   := AsString(row_Login.Properties.Value);
  rec.DataBase:= AsString(row_DataBase.Properties.Value);
  rec.Password:= AsString(row_Password.Properties.Value);


  // TODO -cMM: TfSQL_TO_MDB.SaveToClass default body inserted
end;

procedure Tfrm_Main.row_databaseEditPropertiesInitPopup(
  Sender: TObject);
begin
  UpdateDatabaseList();

end;

end.



{


procedure TfSQL_TO_MDB.Button2Click(Sender: TObject);
begin
//  dmSQL_TO_MDB.ConnectMDB(FileName1.FileName);
  //Tdlg_Structure.ExecDlg();
end;


// TODO: ConnectONEGA
//procedure TfSQL_TO_MDB.ConnectONEGA;
//begin
// // if dmMain.OpenDB(True) then
////  StatusBar1.SimpleText:=dmMain.LoginRec.ConnectionStatusStr;
//end;


//--------------------------------------------------------------
procedure TfSQL_TO_MDB.act_SetupTablesExecute(Sender: TObject);
//--------------------------------------------------------------
begin
(*  with Fframe_DB_Connect_Params do
  begin
    if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then
    begin
      ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
      ModalResult:= mrNone;
      exit;
    end;

    dmMain.SaveConnectRecToReg(Server, DefaultDb, User, Password, UseWinAuth);
  end;

  if not dmMain.Open then
    ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
*)

 // Tfrm_Tables1.ExecDlg;

end;


//--------------------------------------------------------------
procedure TfSQL_TO_MDB.act_SelectObjectFieldsExecute(Sender: TObject);
//--------------------------------------------------------------
begin

 (* with Fframe_DB_Connect_Params do
  begin
    if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then begin
      ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
      exit;
    end;

    dmMain.SaveConnectRecToReg(Server, DefaultDb, User, Password, UseWinAuth);
//    dmMain.Add_ODBC_Source(Server, DefaultDb, User);
  end;

  if not dmMain.Open then
    ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);

  if dmMain.ADOConnection.Connected then
    TDlg_SelectObjectFields.ExecDlg (dmSQL_to_MDB.Params.UnloadObjectFieldIDs);
*)

end;



  if FileSaveAs1.Execute then
    cx_SetButtonEditText (Sender, FileSaveAs1.Dialog.FileName);

