unit dm_SQL_to_MDB;

interface

uses
  dm_ExportStructure,

  dm_Main_SQL,
  u_db,
  u_func,
  u_sync_classes,

  DB, SysUtils, Classes, Forms, ADODB, rxStrHlder;


type
//  TUnloadObjectFieldsType = (uotAll, uotNone); //uotCustom,
  THostType = (ht_Local, ht_Remote);


  TdmSQL_TO_MDB1 = class(TDataModule)
    qry_Objects: TADOQuery;
    qry_Object_Fields: TADOQuery;
    t_Objects: TADOTable;
    t_Object_fields: TADOTable;
    qry_Temp: TADOQuery;
    ADOTable_dest: TADOTable;
    ADOQuery1: TADOQuery;
    ADOTable_SQL: TADOTable;
    StrHolder1: TStrHolder;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

  private
  //  FMDB: TDBManager;

    FDBStructure: TDBStructure;
    FHOST: string;

  //  procedure Create_ObjectTables;

//    procedure CLearMDB;

 //   procedure Export_Lib_Tables;
  public
    Params: record

   //   UnloadObjectFieldIDs:   TIDlist;
//      IsUnloadObjectFields: Boolean;: string;
      HostType: THostType;

      IniFileName_: string;
      IniFileName2: string;

      FileName_Script: string;

//      FileName_Library: string;

//      Project_id: integer;

      Is_Export_Objects_and_PickList : boolean;
      is_Export_DatabaseInfo : boolean;

      Is_Save_Service_ScriptToFile : boolean;
    end;

    procedure Execute;

// TODO: ConnectMDB
//  procedure ConnectMDB(aMDBFileName: string);
  end;

var
  FdmSQL_TO_MDB: TdmSQL_TO_MDB1;

function dmSQL_TO_MDB1: TdmSQL_TO_MDB1;

const
  THostType_str : array[THostType] of string = ('local', 'remote');


//--------------------------------------------------------------------
implementation

 {$R *.DFM}


//--------------------------------------------------------------------
function dmSQL_TO_MDB1: TdmSQL_TO_MDB1;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmSQL_TO_MDB) then
    FdmSQL_TO_MDB:= TdmSQL_TO_MDB1.Create(Application);

  Result:= FdmSQL_TO_MDB;
end;


//--------------------------------------------------------------------
procedure TdmSQL_TO_MDB1.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
 // TdmMDB.Init;

  db_SetComponentADOConnection(Self, dmMain_SQL.AdoConnection1);

  FDBStructure := TDBStructure.Create();
end;


//----------------------------------------------------------
procedure TdmSQL_TO_MDB1.DataModuleDestroy(Sender: TObject);
//----------------------------------------------------------
begin
  FreeAndNil(FDBStructure);
//  Params.UnloadObjectFieldIDs.Free;

 // FreeAndNil(FMDB);
end;


//----------------------------------------------------------
procedure TdmSQL_TO_MDB1.Execute;
//----------------------------------------------------------
//var
//  s: string;
//  s1: string;
//  sIDs: string;
begin
  FHOST:= LowerCase (THostType_str[Params.HostType]);

 // DoProgress(0, 9);

//  CursorSql;

 // ConnectSQLServ;
 // ConnectMDB(Params.MDBFileName);

  if Params.Is_Export_Objects_and_PickList then
  begin
//    TdmExport_Lib.Init;
 //   dmExport_Lib.Exec;

//    Create_ObjectTables;
 //   Export_Lib_Tables();

  end;



 // ClearMDB();

//  if Params.IsUnloadObjectFields then
 // begin
(*

    db_OpenQuery(qry_Objects,  SQL_SELECT_OBJECTS);

    db_OpenQuery(qry_Object_Fields, SQL_SELECT_ALL_OBJECT_FIELDS);

*)
//    case Params.UnloadObjectFieldsType of
 //     uotAll:

 (*     uotCustom: begin
        if Params.UnloadObjectFieldIDs.Count = 0 then
          Raise Exception.Create('');

        sIDs:= Params.UnloadObjectFieldIDs.ValuesToString(',');
        db_OpenQuery(qry_Object_Fields,
                          Format(SQL_SELECT_CUSTOM_OBJECT_FIELDS, [sIDs]));


      end;
 *)
  //    uotNone: qry_Object_Fields.Close;
  //  end;
 // end;


 // CursorDefault;

  //FMDB.ExecCommand('Update [OPTIONS] SET [value]=:value where [name]=''UpdateStructure'' ',
    //              [db_Par(FLD_VALUE, Params.UpdateStructure)]   );

//  FMDB.ADOConnection.BeginTrans;

//  DBStructure.Clear;

  TdmExportStructure.Init;
//  dmExportStructure.DBStructure_ref :=DBStructure;

//  Assert(Params.Project_name<>'');

  dmExportStructure.Params.is_Export_DatabaseInfo_New := Params.is_Export_DatabaseInfo;


  dmExportStructure.Params.IniFileName_ := Params.IniFileName_;
  dmExportStructure.Params.IniFileName2 := Params.IniFileName2;

 // dmExportStructure.Params.Project_ID := Params.Project_ID;

  dmExportStructure.ExecDlg(FHost, FDBStructure);
//..  dmExportStructure.ExecDlg('local', DBStructure);


//  dmExportStructure_2005.

  if Params.FileName_Script <> '' then
  begin
 {
    dmExportStructure_2005.Load_DBStructure_Version(dmMain_SQL.AdoConnection1, DBStructure);

//   db_SetComponentsADOConn([ADOQuery1], dmMain_SQL.AdoConnection1);

    s:=DBStructure.GetSQL1();
    s:=s+ 'INSERT INTO project (name) VALUES (N''������1'') ' + DEF_GO_CRLF ;


    if Params.FileName_Library <> '' then
    begin
      TextFileToStr (Params.FileName_Library , s1);

      s:=s+ s1;
    end;

    StrToFile (s, Params.FileName_Script);
 }
  end;


 //ShellExec_Notepad_temp ();


//////////  dmExportStructure_2005.Save_Service_ScriptToFile( dmMain_SQL.AdoConnection1, 'install_db.sql');

  if Params.Is_Save_Service_ScriptToFile then
    dmExportStructure.Save_Service_ScriptToFile_(dmMain_SQL.AdoConnection1,
        ExtractFilePath (Application.ExeName) + 'install_db.service.sql' );

//      ChangeFileExt(Application.ExeName, '.service.sql') );



  FreeAndNil(dmExportStructure);

(*
  procedure TdmMDB.UpdateIniFile1(aMax_modify_date: TDateTime; aChecksum:
    Integer; aIniFileName: string);
*)


(*
  dmMDB.UpdateIniFile1(
//    DBStructure.Max_modify_date,
    DBStructure.Checksum,
    Params.IniFileName);
*)

  //  IniFileName




(*
  if Params.IsUnloadObjectFields then
  begin
    Progress_SetProgressMsg1('_objects');

//    DoProgressMsg('_objects');

//    DoLog('_objects');
    Export_Lib_Tables();
 //   DoProgress(8, 9);
  end;

*)

end;



end.



