unit d_Compare;

interface

uses
  dm_Compare_and_Modify_v2,

   dm_MDB,
   u_db,


// dm_Auto_Update,
// dm_MDB_to_SQL_install,

  d_CompareSQL,

  d_Structure,

//  d_UpdateDB_Hand,

  u_dlg,

  fra_DB_Login,

  d_Versions_Edit_new,
  d_Versions_Edit,

  dm_CompareAndModify_v1,
  dm_ExportStructure,

//  dm_ObjectFields,

  dm_Main_SQL,

  u_log,

  u_sql,

 // dm_SQLServ,
//  dm_MDB,

//  u_const,

  u_func,
 // u_dlg,
  u_files,


  SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, rxPlacemnt, ActnList, Menus,
//  rxToolEdit, ComCtrls,   TB2Item, TB2Dock, TB2Toolbar,
  cxClasses, cxControls, cxGridCustomView, cxInplaceContainer,
  cxTL, cxDBTL,  cxGraphics,  cxGridLevel,
  cxGridDBTableView, cxGrid, Mask, cxGridCustomTableView, cxGridTableView,

  cxTLData, Grids, DBGrids, cxLookAndFeels, cxLookAndFeelPainters,
  cxCustomData, cxStyles, cxTextEdit, cxMemo, cxCheckBox, cxMaskEdit,
  cxTLdxBarBuiltInMenu, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  dxDateRanges, Data.DB, cxDBData, System.Actions, Vcl.ComCtrls, dxSkinsCore,
   Data.Win.ADODB, cxSplitter, cxVGrid, cxDBVGrid,
  Vcl.DBCtrls;

type
  Tdlg_Compare = class(TForm)
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormStorage1: TFormStorage;
    act_Compare: TAction;
    act_Modify: TAction;
    act_Run_Dict_Import: TAction;
    act_UpdateViews: TAction;
    act_UpdateVer: TAction;
    act_UpdateObjFields: TAction;
    Panel1: TPanel;
    Button2: TButton;
    Button1: TButton;
    act_ShowLog: TAction;
    Button3: TButton;
    ActionList2: TActionList;
    act_Export_Objects: TAction;
    act_Exec_record: TAction;
    act_Compare_SQL: TAction;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    act_Show_Executed_script: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    act_Check_all: TAction;
    actCheckall1: TMenuItem;
    ScriptToNotepad1: TMenuItem;
    ScriptCheckedToNotepad1: TMenuItem;
    N3: TMenuItem;
    act_Versions: TAction;
    act_UnCheck_all: TAction;
    act_Show_checked_script: TAction;
    Button6: TButton;
    act_Structure: TAction;
    Button7: TButton;
    act_Versions_new: TAction;
    StatusBar1: TStatusBar;
    PageControl_top: TPageControl;
    TabSheet1: TTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    ds_Data_Compare: TDataSource;
    t_Data_Compare: TADOTable;
    act_Dlg_Details: TAction;
    ADOConnection_MDB: TADOConnection;
    cxGrid1DBTableView1id: TcxGridDBColumn;
    cxGrid1DBTableView1checked: TcxGridDBColumn;
    cxGrid1DBTableView1group_name: TcxGridDBColumn;
    cxGrid1DBTableView1xtype: TcxGridDBColumn;
    cxGrid1DBTableView1schema: TcxGridDBColumn;
    cxGrid1DBTableView1name_local: TcxGridDBColumn;
    cxGrid1DBTableView1name_remote: TcxGridDBColumn;
    cxGrid1DBTableView1action: TcxGridDBColumn;
    cxGrid1DBTableView1action_111111111: TcxGridDBColumn;
    cxGrid1DBTableView1error: TcxGridDBColumn;
    cxGrid1DBTableView1Definition_local: TcxGridDBColumn;
    cxGrid1DBTableView1Definition_remote: TcxGridDBColumn;
    cxGrid1DBTableView1Enabled: TcxGridDBColumn;
    cxGrid1DBTableView1owner: TcxGridDBColumn;
    cxGrid1DBTableView1priority111: TcxGridDBColumn;
    cxGrid1DBTableView1is_updated: TcxGridDBColumn;
    cxGrid1DBTableView1SQL_update: TcxGridDBColumn;
    cxGrid1DBTableView1SQL_create: TcxGridDBColumn;
    cxGrid1DBTableView1SQL_drop: TcxGridDBColumn;
    cxGrid1DBTableView1parent_id: TcxGridDBColumn;
    ds_Compare_v1_table_columns: TDataSource;
    t_Compare_v1_table_columns: TADOTable;
    PageControl_bottom: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet5: TTabSheet;
    DBMemo1: TDBMemo;
    TabSheet6: TTabSheet;
    DBMemo2: TDBMemo;
    DBMemo3: TDBMemo;
    ds_local_column: TDataSource;
    t_local_column: TADOTable;
    ds_remote_column: TDataSource;
    t_remote_column: TADOTable;
    cxSplitter1: TcxSplitter;
    b_Exec_object: TButton;
    b_Exec_column: TButton;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1id: TcxGridDBColumn;
    cxGridDBTableView1parent_id: TcxGridDBColumn;
    cxGridDBTableView1table_name: TcxGridDBColumn;
    cxGridDBTableView1name_local: TcxGridDBColumn;
    cxGridDBTableView1name_remote: TcxGridDBColumn;
    cxGridDBTableView1action: TcxGridDBColumn;
    cxGridDBTableView1action_: TcxGridDBColumn;
    cxGridDBTableView1error: TcxGridDBColumn;
    cxGridDBTableView1checked11: TcxGridDBColumn;
    cxGridDBTableView1update_SQL: TcxGridDBColumn;
    cxGridDBTableView1SQL_create: TcxGridDBColumn;
    cxGridDBTableView1SQL_drop: TcxGridDBColumn;
    cxGridDBTableView1Enabled: TcxGridDBColumn;
    cxGridDBTableView1is_updated: TcxGridDBColumn;
    cxGridDBTableView1Definition_local: TcxGridDBColumn;
    cxGridDBTableView1Definition_remote: TcxGridDBColumn;
    cxGridDBTableView1local_column_id: TcxGridDBColumn;
    cxGridDBTableView1remote_column_id: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    cxDBVerticalGrid1: TcxDBVerticalGrid;
    cxDBVerticalGrid1id: TcxDBEditorRow;
    cxDBVerticalGrid1host: TcxDBEditorRow;
    cxDBVerticalGrid1object_id: TcxDBEditorRow;
    cxDBVerticalGrid1schema: TcxDBEditorRow;
    cxDBVerticalGrid1TableName: TcxDBEditorRow;
    cxDBVerticalGrid1name: TcxDBEditorRow;
    cxDBVerticalGrid1type: TcxDBEditorRow;
    cxDBVerticalGrid1Length: TcxDBEditorRow;
    cxDBVerticalGrid1Variable: TcxDBEditorRow;
    cxDBVerticalGrid1IsNullable: TcxDBEditorRow;
    cxDBVerticalGrid1default_name: TcxDBEditorRow;
    cxDBVerticalGrid1default_content: TcxDBEditorRow;
    cxDBVerticalGrid1IDENTITY: TcxDBEditorRow;
    cxDBVerticalGrid1IsComputed: TcxDBEditorRow;
    cxDBVerticalGrid1computed_text: TcxDBEditorRow;
    cxDBVerticalGrid1is_exists: TcxDBEditorRow;
    cxDBVerticalGrid1sql_create: TcxDBEditorRow;
    cxDBVerticalGrid1sql_drop: TcxDBEditorRow;
    cxDBVerticalGrid2: TcxDBVerticalGrid;
    cxDBVerticalGrid2id: TcxDBEditorRow;
    cxDBVerticalGrid2host: TcxDBEditorRow;
    cxDBVerticalGrid2object_id: TcxDBEditorRow;
    cxDBVerticalGrid2schema: TcxDBEditorRow;
    cxDBVerticalGrid2TableName: TcxDBEditorRow;
    cxDBVerticalGrid2name: TcxDBEditorRow;
    cxDBVerticalGrid2type: TcxDBEditorRow;
    cxDBVerticalGrid2Length: TcxDBEditorRow;
    cxDBVerticalGrid2Variable: TcxDBEditorRow;
    cxDBVerticalGrid2IsNullable: TcxDBEditorRow;
    cxDBVerticalGrid2default_name: TcxDBEditorRow;
    cxDBVerticalGrid2default_content: TcxDBEditorRow;
    cxDBVerticalGrid2IDENTITY: TcxDBEditorRow;
    cxDBVerticalGrid2IsComputed: TcxDBEditorRow;
    cxDBVerticalGrid2computed_text: TcxDBEditorRow;
    cxDBVerticalGrid2is_exists: TcxDBEditorRow;
    cxDBVerticalGrid2sql_create: TcxDBEditorRow;
    cxDBVerticalGrid2sql_drop: TcxDBEditorRow;
    DBMemo5: TDBMemo;
    act_UpdateAllViews: TAction;
    Button8: TButton;
    bRunAll: TButton;
    procedure FormDestroy(Sender: TObject);
//    procedure ed_Server1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_CancelClick(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_Exec_recordExecute(Sender: TObject);
    procedure act_Export_ObjectsExecute(Sender: TObject);
    procedure bRunAllClick(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure b_Exec_objectClick(Sender: TObject);
    procedure b_d_CompareClick(Sender: TObject);
    procedure b_Exec_columnClick(Sender: TObject);
 //   procedure Button5Click(Sender: TObject);
//    procedure Button3Click(Sender: TObject);
//    procedure cxDBTreeList11CustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
  //      AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
//    procedure cxDBTreeList2_newCustomDrawCell(Sender: TObject; ACanvas: TcxCanvas;
//        AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
//    procedure PageControl1Change(Sender: TObject);
//    procedure TBDock2ContextPopup(Sender: TObject; MousePos: TPoint; var Handled:
   //     Boolean);
    procedure _Action(Sender: TObject);
//    procedure treeHotTrackNode(Sender: TObject;
    //  AHotTrackInfo: TdxTreeListHotTrackInfo; var ACursor: TCursor);
//    procedure treeKeyPress(Sender: TObject; var Key: Char);

  private
    FIsShowUpdateObjectsCheckbox: boolean;

    Fframe_DB_Login: Tframe_DB_Login;

//    FIniFileName : string;

 //   Fframe_DB_Connect_Params: Tframe_DB_Connect_Params;

    procedure ExecuteUpdateDB (aIsModifyFields: boolean);
    procedure Export_Objects;

    procedure OpenConfigFile_MDB;
    procedure Run_Auto_Update;

  public
    class function ExecDlg: boolean;
  end;

//var
//  dlg_Compare: Tdlg_Compare;

//=====================================================================
implementation


 {$R *.DFM}


(*const
  REGISTRY_COMMON_FORMS = 'Software\Onega\Install_DB';
*)



procedure Tdlg_Compare.FormDestroy(Sender: TObject);
begin
//..  if Fframe_DB_Login.IsModified then
//    Fframe_DB_Login.SaveToIni();

end;

// ---------------------------------------------------------------
class function Tdlg_Compare.ExecDlg: boolean;
// ---------------------------------------------------------------
begin
  with Tdlg_Compare.Create(Application) do
  begin
    ShowModal

  end;
end;

//--------------------------------------------------------------
procedure Tdlg_Compare.FormCreate(Sender: TObject);
//--------------------------------------------------------------
var
  sCurAppPath: string;
begin
  if ADOConnection_MDB.Connected then
     ShowMessage ('ADOConnection_MDB.Connected');


  TdmMDB.Init1;
  TdmCompare_and_Modify_v2.Init;

  ADOConnection_MDB.Close;

 // dmMDB.OpenDB1;

//  TdmExportStructure.Init;
//  TdmCompareAndModify_new.Init;

//  DBGrid1.Align:=alClient;


//  PageControl1.Align:=alClient;
  PageControl_bottom.Align:=alClient;


  cxGrid1.Align:=alClient;

//  dmCompareAndModify_new.Params.ADOConnection_SQL := dmMain_SQL.ADOConnection1;


  Caption:= '������ ��������� �� RPLS';
  Caption :=Caption+ GetAppVersionStr();

//  lb_Action.Caption:= '���������� ������������ �� (������������ ���� MS Access)';

//  FIniFileName :=ChangeFileExt(Application.ExeName, '.ini');

//
//  CreateChildForm_(Tframe_DB_Login, Fframe_DB_Login, gb_ConnectToSQL);
//
//  Fframe_DB_Login.IniFileName := ChangeFileExt(Application.ExeName, '.ini');
//  Fframe_DB_Login.LoadFromIni();
//
//
//  gb_ConnectToSQL.Height:= Fframe_DB_Login.MaxHeight;


//  Height:=  636;

//  FormStorage1.IniFileName:=REGISTRY_FORMS + ClassName +'\' + FormStorage1.name;
//  FormStorage1.IniSection:='Window';
 // FormStorage1.Active:=True;


(*  Fframe_DB_Connect_Params:=
     Tframe_DB_Connect_Params.CreateChildForm (Self, pn_Connect_Params);
  Fframe_DB_Connect_Params.IsShowImage:= false;
*)

//  Fframe_DB_Connect_Params.pn_Connect.Visible:= false;
//  Fframe_DB_Connect_Params.IsShowBorder := false;

//  gb_ConnectToSQL.Height:= Fframe_DB_Connect_Params.CurHeight-10;

  //PageControl11.Align:= alClient;
//  PageControl2.Align:= alClient;
//  cxdbTreeList11.Align:= alClient;
 // cxDBTreeList2_new.Align:= alClient;

//  PageControl11.ActivePageIndex:= 1;

//  act_Create_Object.Caption:= '��������� ��������';
  act_Compare_SQL.Caption:= '�������� SQL';

  sCurAppPath:= ExtractFilePath(Application.ExeName);
  sCurAppPath:= IncludeTrailingBackslash(sCurAppPath)+ 'onega_tables.mdb';

  FIsShowUpdateObjectsCheckbox:= false;

//  gb_Options.Visible:= FIsShowUpdateObjectsCheckbox;

 // if FileExists (sCurAppPath) then
  //  ed_FilenameEdit.Text:= sCurAppPath;

//  g_Log.RichEdit := RichEdit1;

//  TdmCompareAndModify.Init;

   db_SetComponentADOConnection(Self, dmMDB.ADOConnection_MDB);

   t_Data_Compare.Open;
   t_Compare_v1_table_columns.Open;
   t_local_column.Open;
   t_remote_column.Open;

 // tree.dataSource := dmCompareAndModify.ds_Data_Compare;
//  cxDBTreeList11.DataController.dataSource := dmCompareAndModify.ds_Data_Compare;
//  cxDBTreeList2_new.DataController.dataSource := dmCompareAndModify_new.ds_Data_Compare;

//  cxGridDBTableView_Owner.DataController.dataSource := dmCompareAndModify_new.ds_Owner;



//  OpenConfigFile_MDB;

end;


//----------------------------------------------------------------------
procedure Tdlg_Compare.FormClose(Sender: TObject; var Action: TCloseAction);
//----------------------------------------------------------------------
begin
 // db_Clear(dmCompareAndModify.t_Data);
  Action:= caFree;
end;


//----------------------------------------------------------------------
procedure Tdlg_Compare.OpenConfigFile_MDB;
//----------------------------------------------------------------------
begin
 // dmMDB.OpenDB1;//(ed_FilenameEdit.Text);

//  if not dmMDB.ConnectMDB(FilenameEdit1.Filename) then
//  begin
//    ErrorDlg('������ �������� ����� MS Access');
  //  exit;
 // end;

//  dmCompareAndModify.OpenData;
//  dmCompareAndModify_new.OpenCompareData;

end;


//----------------------------------------------------------------------
procedure Tdlg_Compare.btn_CancelClick(Sender: TObject);
//----------------------------------------------------------------------
begin
  Close;
end;

//----------------------------------------------------------------------
procedure Tdlg_Compare.ExecuteUpdateDB (aIsModifyFields: boolean);
//----------------------------------------------------------------------

(*
const
  STR_RESULT =
    '��������� ���������� ��: '+ CRLF +
    '   ����������� ������ (���������� � ������������������� ���������) : %d '+  CRLF +
    '   ������ ������ (��������� ������ ��������� ������������) : %d '+  CRLF +
    '��������: ��� ������� ����������� ������ ���������� � ������������� ��� ���������� ��'+  CRLF +
    '�������� ������ ������?';

*)
var
  I: Integer;
  b: boolean;

 // rec: TdbLoginRec;

begin
 // rec:=Fframe_DB_Login.GetLoginRec;

  if not dmMain_SQL.OpenDB(Fframe_DB_Login.GetLoginRec) then
    Exit;

//  dmCompareAndModify.Params.LoginRec := Fframe_DB_Login.GetLoginRec;

//  dmCompareAndModify.Params.IsModifyFields1:= aIsModifyFields;


                                      
(*  if FIsShowUpdateObjectsCheckbox then
    dmCompareAndModify.IsModifyObjectFields:= cb_RenewObjectFields.Checked
  else
    dmCompareAndModify.IsModifyObjectFields:= true;*)

(*

  with Fframe_DB_Connect_Params do
  begin
    if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then begin
      ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
      exit;
    end;

    dmMain.SaveConnectRecToReg(Server, DefaultDb, User, Password, UseWinAuth);
///    dmMain.Add_ODBC_Source(Server, DefaultDb, User);
  end;

  if not dmMain.Open then begin
    ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
    exit;
  end;
*)


(*  if aIsModifyFields then
    if not dmMain.TestForAdminWrites then begin
      ErrorDlg(MSG_NOT_ENOUGH_RIGHTS);
      exit;
    end;
*)

(*
  if not dmMain.AdoConnection.Connected then
    exit;
*)

//  dmMDB.OpenDB1;//(ed_FilenameEdit.Text);

(*  if not dmMDB.ConnectMDB(FilenameEdit1.Filename) then
  begin
   // ErrorDlg('������ �������� ����� MS Access');
    exit;
  end;
*)
{
   Assert(Assigned(dmMDB), 'Value not assigned');
   Assert(Assigned(dmCompareAndModify_new), 'Value not assigned');


  TdmExportStructure.Init;

//  dmExportStructure_2005.Params.Project_name1:='link';

//  dmExportStructure.Load_DBStructure(dmMDB.ADOConnection1, dmCompareAndModify.DBStructure_MDB);
  dmExportStructure.Load_DBStructure(dmCompareAndModify_new.DBStructure_MDB, 'local');

  dmCompareAndModify_new.DBStructure_SQL.clear;

//  TdmExportStructure_2005.Init;
  dmExportStructure.ExecDlg('sql_', dmCompareAndModify_new.DBStructure_SQL);
  FreeAndNil(dmExportStructure);

  }

(*  Screen.Cursor:= crHourGlass;
  Screen.Cursor:= crDefault;


//
//  if aIsModifyFields then
//    with dmMDB.tbl_StoredProcs do
//    begin
//      if not gl_DB.ObjectExists('sp__SetDboOwner') then
//        if Locate(FLD_NAME, 'sp__SetDboOwner', []) then
//          try
//            gl_DB.ExecCommandSimple(FieldByName(FLD_CONTENT).AsString);
//          except end;
//
//      dmMain_SQL.SetDboOwner;
//    end;




(*
  dmCompareAndModify.UpdateObjectFields:=
    AsBoolean(dmMdb.MDB.GetFieldValue('Options', FLD_VALUE,
        [db_Par(FLD_NAME, 'UpdateObjectFields')]));
*)

(*
  if dmCompareAndModify.Params.IsModifyFields1 then
    g_Log.Add('�������� 1');
*)

(*
  dmCompareAndModify.ExportStructure;
  dmCompareAndModify.ExecuteDlg('');

*)

  ////dmCompareAndModify_new.Params.DatabaseName := Fframe_DB_Login.GetLoginRec.DataBase;


 // dmCompareAndModify_new.ExportStructure11111111111111;
 // dmCompareAndModify_new.ExecDlg;

//  dmCompareAndModify_new.ExecuteDlg('');

 // Memo1.Lines.Assign(dmCompareAndModify_new.Script);


(*  if dmCompareAndModify.Params.IsModifyFields1 then
  begin
    g_Log.Add('�������� 2');

    b:= dmCompareAndModify.Params.UpdateObjectFields;

    dmCompareAndModify.Params.UpdateObjectFields:= false;
   // dmSQLServ.ExptortSructure;
    dmCompareAndModify.ExecuteDlg('');
    dmCompareAndModify.Params.UpdateObjectFields:= b;
  end;

*)

  // ---------------------------
//  cxdbTreeList11.FullCollapse;

//  for i:=0 to tree.Count-1 do
 //   if Eq_Any_variant(tree.DataSource.DataSet[FLD_DXTREE_ID], [-1,1,2,3,4,5,6,7,8]) then
  //    tree.Items[i].Expand(false);
  // ---------------------------

 // cxdbTreeList11.ApplyBestFit();

 // col_Error.Width:= 50;
 // col_Action.Width:= 100;

  if aIsModifyFields then
  begin
    CursorSql;
  //  dmCompareAndModify.RefreshViews1;
   // dmVersionUpdate.UpdateByCustomSqlUpdates;
    CursorDefault;

  //  if g_Log.ErrorCount=0 then
  //    dmVersionUpdate.UpdateDBVersion;

(*    with g_Log do
      if ConfirmDlg( Format(STR_RESULT, [ErrorCount, ErrorCount ]) ) then
      ;
      //  ShowLogOnDisk;
*)

  end;
end;

//----------------------------------------------------------------------
procedure Tdlg_Compare.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//----------------------------------------------------------------------
var
  sNameSQL, sAction, sType: string;
begin
(*
  act_Compare.Enabled := FilenameEdit1.FileName <> '';
  act_Modify.Enabled  := act_Compare.Enabled;

  act_Create_Object.Enabled := false;
  act_Compare_SQL.Enabled   := false;

*)
(*  if Assigned (cxdbTreeList11.FocusedNode) then
  begin
    sAction:=  AsString(cxdbTreeList11.FocusedNode.Values[col_action.ItemIndex]);
    sType:=    AsString(cxdbTreeList11.FocusedNode.Values[col_type.ItemIndex])  ;
    sNameSQL:= AsString(cxdbTreeList11.FocusedNode.Values[col_Name_SQL.ItemIndex]) ;

    act_Compare_SQL.Enabled :=
      ((sType=TYPE_PROC) or
       (sType=TYPE_VIEW) or
       (sType=TYPE_TRIGGER) or
       (sType=TYPE_FIELD) or
       (sType=TYPE_INDEX) or
       (sType=TYPE_FK) or
       (sType=TYPE_CK) or

       (sType=TYPE_OBJECT_FIELD));

    act_Create_Object.Enabled :=
       ((act_Compare_SQL.Enabled) or
        (sType=TYPE_FIELD) or
        (sType=TYPE_TABLE))
           and (sAction<>'');
  end;
*)
end;

// ---------------------------------------------------------------
procedure Tdlg_Compare.act_Exec_recordExecute(Sender: TObject);
// ---------------------------------------------------------------
begin
  //----------------------------------------
  if Sender = act_UpdateAllViews then
  //----------------------------------------
  begin
    dmMain_SQL.ExecScriptSQL_with_GO1('dbo._UpdateAllViews');
  //  dmCompareAndModify.DlgCompareSQL();
  end else


  if Sender=act_Structure then
  begin
   // dmMDB.OpenDB1;//(ed_FilenameEdit.Text);
    Tdlg_Structure_new.ExecDlg();

  end else


 // if Sender = act_Show_Executed_script then
   // Memo2.Lines.Assign(dmCompareAndModify_new.ExecutedScript);

   // act_Show_Executed_script

   {
  if Sender = act_Check_all then
    dmCompareAndModify_new.Check_All;

  if Sender = act_UnCheck_all then
    dmCompareAndModify_new.UnCheck_All;

  if Sender = act_Show_checked_script then
  begin
    dmCompareAndModify_new.ShowScriptForChecked;
    Exit;
  end;
    }


  if not dmMain_SQL.ADOConnection1.Connected then
    if not dmMain_SQL.OpenDB(Fframe_DB_Login.GetLoginRec) then
      Exit;

  if Sender = act_versions then
    Tdlg_Versions.ExecDlg else


  if Sender = act_versions_new then
    Tdlg_Versions_new.ExecDlg else


  //----------------------------------------
  if Sender = act_Exec_record then
  //----------------------------------------
  begin
(*    if not dmMain.TestForAdminWrites then begin
      ErrorDlg(MSG_NOT_ENOUGH_RIGHTS);
      exit;
    end;
*)
  //  dmCompareAndModify_new.ExecSingleRecord();
  end else

  //----------------------------------------
  if Sender = act_Compare_SQL then
  //----------------------------------------
  begin
   // dmCompareAndModify_new.Exec_Dlg_Compare;
   // dmCompareAndModify_new.DlgCompareSQL();
  end else

end;


procedure Tdlg_Compare.act_Export_ObjectsExecute(Sender: TObject);
begin
  if Sender=act_Export_Objects then
    Export_Objects;
end;

procedure Tdlg_Compare.bRunAllClick(Sender: TObject);
const
//  DEF_STR : array [0..0] of string = ('UDT');
  DEF_STR : array [0..4] of string = ('u','UDT','fn','view','sp');

var
  I: Integer;
begin
//  sType        :=LowerCase (aDataset.FieldByName('xType').AsString );
 // sName_remote :=aDataset.FieldByName('Name_remote').AsString;


  for I := 0 to High(DEF_STR) do
  begin
    t_Data_Compare.Filter:=Format('xType=''%s''', [ DEF_STR[i] ]);

    t_Data_Compare.First;

    while not t_Data_Compare.EOF do
    begin
      try
        dmCompare_and_Modify_v2.ExecSingleRecord_Table(t_Data_Compare, t_Compare_v1_table_columns);

      except on E: Exception do
      end;

      t_Data_Compare.Next;
    end;

    if DEF_STR[i]='u' then
      act_UpdateAllViews.Execute;
  end;


  t_Data_Compare.Filtered:=False;
  t_Data_Compare.First;

//  dmCompare_and_Modify_v2.ExecSingleRecord_Table(t_Data_Compare, t_Compare_v1_table_columns);

  //
end;

procedure Tdlg_Compare.Button4Click(Sender: TObject);
begin
  Run_Auto_Update

 (* if not ConfirmDlg('�������� ����?') then
    Exit;


  if not dmMain_SQL.OpenDB(Fframe_DB_Login.GetLoginRec) then
  begin
    Exit;

  end;

  TdmAuto_Update.Init;
  dmAuto_Update.Params.IsTestMode := True;
  dmAuto_Update.Execute;
  FreeAndNil(dmAuto_Update);
*)
end;


procedure Tdlg_Compare.Run_Auto_Update;
begin
  if not ConfirmDlg('�������� ����?') then
    Exit;


  if not dmMain_SQL.OpenDB(Fframe_DB_Login.GetLoginRec) then
  begin
    Exit;

  end;

  {
  TdmAuto_Update.Init;
  dmAuto_Update.Params.IsTestMode1 := True;
  dmAuto_Update.Execute;
  FreeAndNil(dmAuto_Update);
  }


end;



procedure Tdlg_Compare.Button5Click(Sender: TObject);
begin
  if not dmMain_SQL.OpenDB(Fframe_DB_Login.GetLoginRec) then
    Exit;

 // Tdlg_UpdateDB_Hand.CreateForm;

end;

procedure Tdlg_Compare.Button9Click(Sender: TObject);
begin
  Tdlg_CompareSQL.ExecDlg(t_Data_Compare);
end;

procedure Tdlg_Compare.b_Exec_objectClick(Sender: TObject);
begin
  dmCompare_and_Modify_v2.ExecSingleRecord_Table(t_Data_Compare, t_Compare_v1_table_columns);

end;


procedure Tdlg_Compare.b_d_CompareClick(Sender: TObject);
begin
  Tdlg_CompareSQL.ExecDlg( t_Data_Compare);
end;


procedure Tdlg_Compare.b_Exec_columnClick(Sender: TObject);

begin
  dmCompare_and_Modify_v2.ExecSingleRecord(t_Compare_v1_table_columns);

end;


// ---------------------------------------------------------------
procedure Tdlg_Compare.Export_Objects;
// ---------------------------------------------------------------
begin
  if not dmMain_SQL.OpenDB(Fframe_DB_Login.GetLoginRec) then
    Exit;

 // dmMDB.OpenDB1;//(ed_FilenameEdit.Text);

(*  if not dmMDB.ConnectMDB(FilenameEdit1.Filename) then
  begin
    ErrorDlg('������ �������� ����� MS Access');
    exit;
  end;
*)

{
  // ---------------------------------------------------------------
  TdmMDB_to_SQL_install.Init;
  dmMDB_to_SQL_install.Exec;
  FreeAndNil(dmMDB_to_SQL_install);

}  // ---------------------------------------------------------------


  // ---------------------------------------------------------------

  {
  TdmObjectFields.Init;
 // dmObjectFields.Params.IsOnlyLinkObjects:=True;
  dmObjectFields.Exec_Progress;
  FreeAndNil(dmObjectFields);
  }


end;

//----------------------------------------------------------------------
procedure Tdlg_Compare._Action(Sender: TObject);
//----------------------------------------------------------------------
var
  iCode: Integer;
begin
  //----------------------------------------
  //if Sender = act_Create_Object then
  //----------------------------------------
 // begin
(*    if not dmMain.TestForAdminWrites then begin
      ErrorDlg(MSG_NOT_ENOUGH_RIGHTS);
      exit;
    end;
*)

  //  dmCompareAndModify.CreateObject();

 // end else

//


  //----------------------------------------
  if Sender = act_Compare_SQL then
  //----------------------------------------
  begin
  //  dmCompareAndModify.DlgCompareSQL();
  end else

  //----------------------------------------
  if Sender = act_Compare then
  //----------------------------------------
  begin
    ExecuteUpdateDB (false);
//    tree.visible:= true;
  end else

  //----------------------------------------
  if Sender = act_Modify then
  //----------------------------------------
  begin
    g_Log.Clear;
  //  g_Log.AddCreationInfo;

(*    if (not dmMain.DEBUG) or
       (dmMain.DEBUG) and ConfirmDlg('�� �������?') then
    begin
*)
      ExecuteUpdateDB (true);
 //   end;
  end else

  //----------------------------------------
  if Sender = act_Run_Dict_Import then
  //----------------------------------------
  begin
//    if ConfirmDlg('��������� ������ ������������?') then
//      RunApp (GetParentFileDir(GetApplicationDir) + 'bin\GuidesImport.exe', AnsiQuotedStr (GetApplicationDir+ 'Guides.mdb', '"'), iCode);
  end else

  //----------------------------------------
  if Sender = act_UpdateViews then
  //----------------------------------------
  begin(*
    with Fframe_DB_Connect_Params do begin
      if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then
      begin
        ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
        exit;
      end;

      dmMain.SaveConnectRecToReg(Server, DefaultDb, User, Password, UseWinAuth);
    end;

    if not dmMain.Open then begin
      ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
      exit;
    end;

    dmCompareAndModify.RefreshViews;

    MsgDlg('���������');*)

  end else


  //----------------------------------------
  if Sender = act_UpdateVer then
  //----------------------------------------
  begin
(*
    with Fframe_DB_Connect_Params do begin
      if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then
      begin
        ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
        exit;
      end;

      dmMain.SaveConnectRecToReg(Server, DefaultDb, User, Password, UseWinAuth);

      if not dmMain.Open then begin
        ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
        exit;
      end;
    end;

    if not dmMDB.Connect(ed_MSAccessFileName.Text) then
    begin
      ErrorDlg('������ �������� ����� MS Access');
      exit;
    end;

    CursorSql;
    dmVersionUpdate.UpdateByCustomSqlUpdates;
    CursorDefault;*)


  end else


  //----------------------------------------
  if Sender = act_UpdateObjFields then
  //----------------------------------------
  begin
(*
    if ConfirmDlg('�������� ������������ ����?') then begin
      with Fframe_DB_Connect_Params do begin
        if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then
        begin
          ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
          exit;
        end;

        dmMain.SaveConnectRecToReg(Server, DefaultDb, User, Password, UseWinAuth);

        if not dmMain.Open then begin
          ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
          exit;
        end;
      end;

      if not dmMDB.Connect(ed_MSAccessFileName.Text) then
      begin
        ErrorDlg('������ �������� ����� MS Access');
        exit;
      end;

      CursorSql;
      dmObjectFields.FillTableObjects(nil);
      dmObjectFields.FillTableObjectFields(nil);
      CursorDefault;

      MsgDlg('���������');

    end;

     *)
  end else

  //----------------------------------------
  if Sender = act_ShowLog then begin
  //----------------------------------------
   // cus_ErrorLog.ShowLogOnDisk;
  end else

end;



end.

(*


//
//procedure Tdlg_UpdateDB.cxDBTreeList11CustomDrawCell(Sender: TObject; ACanvas:
//    TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo; var ADone: Boolean);
//var
//  v: Variant;
//begin
//  (*
//  if AViewInfo.Column = col_action then
//  begin
//    v:=AViewInfo.Node.Values[col_action.ItemIndex] ;
//
//    if v='��������' then acanvas.Brush.Color:= clYellow  else
//    if v='�������'  then acanvas.Brush.Color:= clRed;
//
//  end;
//*)
//end;


//----------------------------------------------------------------------
procedure Tdlg_Compare.treeKeyPress(Sender: TObject;  var Key: Char);
//----------------------------------------------------------------------
begin
(*  if Key = 'm' then begin
    dmCompareAndModify.CreateObject();
  end else

  if Key = 'c' then begin
    dmCompareAndModify.DlgCompareSQL();
  end else
*)
end;


procedure Tdlg_Compare.PageControl1Change(Sender: TObject);
begin
//  if PageControl1.ActivePage=TabSheet_Data then
  begin
//    pn_Connection.Caption:=
//       db_GetConnectionStatusStr_new (Fframe_DB_Login.GetLoginRec());
  end;

end;




// ---------------------------------------------------------------
procedure Tdlg_Compare.cxDBTreeList2_newCustomDrawCell(Sender: TObject;
    ACanvas: TcxCanvas; AViewInfo: TcxTreeListEditCellViewInfo; var ADone:
    Boolean);
// ---------------------------------------------------------------
var
  v: Variant;
begin
{
  if AViewInfo.Column = col_action_ then
  begin
    v:=AViewInfo.Node.Values[col_action_.ItemIndex] ;

    if v='CREATE' then acanvas.Brush.Color:= clGreen  else
    if v='DROP'   then acanvas.Brush.Color:= clRed  else
    if v='ALTER'  then acanvas.Brush.Color:= clYellow;

  end;
 }

end;

