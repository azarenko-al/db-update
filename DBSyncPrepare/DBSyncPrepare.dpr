program DBSyncPrepare;

uses
  {$IFDEF EurekaLog}
  EMemLeaks,
  EResLeaks,
  EDebugExports,
  EDebugJCL,
  EFixSafeCallException,
  EMapWin32,
  EAppVCL,
  EDialogWinAPIMSClassic,
  EDialogWinAPIEurekaLogDetailed,
  EDialogWinAPIStepsToReproduce,
  ExceptionLog7,
  {$ENDIF EurekaLog}
  d_Structure in '..\src_common\d_Structure.pas' {dlg_Structure_new},
  d_Versions_Edit_new in '..\src_common\d_Versions_Edit_new.pas' {dlg_Versions_new},
  d_Wizard in 'W:\common XE\Package_Common\d_Wizard.pas' {dlg_Wizard},
  dm_ExportStructure in '..\src_common\dm_ExportStructure.pas' {dmExportStructure: TDataModule},
  dm_Main_SQL in '..\src_common\dm_Main_SQL.pas' {dmMain_SQL: TDataModule},
  dm_MDB in '..\src_common\dm_MDB.pas' {dmMDB: TDataModule},
  dm_SQL_to_MDB in 'Src\dm_SQL_to_MDB.pas' {dmSQL_TO_MDB1: TDataModule},
  f_Main in 'Src\f_Main.pas' {frm_Main},
  Forms,
  u_local_const in '..\src_common\u_local_const.pas',
  u_sync_classes in '..\src_common\u_sync_classes.pas',
  d_Versions_Edit in '..\src_common\d_Versions_Edit.pas' {dlg_Versions},
  u_Sql_Parser in '..\Install_DB\src\u_Sql_Parser.pas',
  dm_CompareAndModify_v1 in '..\Install_DB\src\dm_CompareAndModify_v1.pas' {dmCompareAndModify_v1: TDataModule},
  u_install_db_classes in '..\Install_DB\src\u_install_db_classes.pas',
  d_Compare in 'Src\d_Compare.pas' {dlg_Compare},
  d_CompareSQL in '..\Install_DB\src\d_CompareSQL.pas' {dlg_CompareSQL},
  u_func_ in '..\src_common\u_func_.pas',
  u_sync_classes_new in 'Src\u_sync_classes_new.pas',
  dm_Compare_and_Modify_v2 in 'Src\dm_Compare_and_Modify_v2.pas' {dmCompare_and_Modify_v2: TDataModule},
  dm_Object_Fields in '..\Install_DB\src\dm_Object_Fields.pas' {dmObject_Fields: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(Tfrm_Main, frm_Main);
  //  Application.CreateForm(Tdlg_Compare, dlg_Compare);
  //  Application.CreateForm(Tdlg_Actions, dlg_Actions);
  // Application.CreateForm(TfSQL_TO_MDB, fSQL_TO_MDB);
  Application.Run;


//if bIsConverted then
//    ExitCode := 8;
//  else
 //   ExitProcess(0);}

end.

