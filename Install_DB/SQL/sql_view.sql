
select *,   
  
IIF (schema_name = 'dbo', '',
  replace( 'if schema_id('':name'') is null   execute (''create schema :name '')  ', ':name', schema_name )  + char(10) + 'GO ' + char(10) 
  ) + definition as sql_create,
     
  

  replace( 'if object_id('':name'') is not null   drop view :name  ', ':name', schema_name+'.'+NAME )
    + char(10) + 'GO ' + char(10)   as sql_drop
   

--  'if object_id(''' +NAME +''') is not null   drop view '+ NAME as sql_drop


from 
(


  select   
   o.object_id,
   

  
  o.NAME,
  o.create_date,
  o.modify_date,
  
  schema_name (o.schema_id) as schema_name,
  
  definition
  
  
from sys.objects     o
  join sys.sql_modules m on m.object_id = o.object_id


where
  
   o.type      = 'V'
   
   
   and o.NAME not LIKE '%[_][_]'
   and o.NAME not LIKE '[_][_]%'
   and o.NAME not LIKE '%11'
   
   and schema_name (o.schema_id) <> 'sync'
   and schema_name (o.schema_id) <> 'ui'


   and schema_name (o.schema_id) not in ( 'service', 'sync','test')
   
) M
