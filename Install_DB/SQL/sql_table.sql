
select top 9999
--*,

Object_id,

-- schema_name, name,

name_full,


create_date,
modify_date,



IIF (schema_name ='dbo', '',
  replace( 'if schema_id('':name'') is null   execute (''create schema :name '')', ':name', schema_name)  + char(10) + 'GO' + char(10) 
  ) +

'create table ' + name_full + ' ( ' +  sql_columns + char(10) +') ' + char(10)  as  [sql_create]  ,

'drop table ' + name_full   as  [sql_drop] 


from 
(


    select 


schema_name(schema_id)  +'.'+ name as name_full,

     schema_name(schema_id) as schema_name,
     *,     
     
 
   
     SUBSTRING(
          (
              SELECT ',' + char(10) + '  '+   sql_create  AS [text()]
            --  FROM sync.view_UDT_columns 
                            
               FROM               
                  (

                      select top 9999999


                      obj.object_id,


                      --'ALTER TABLE '+ col.table_schema +'.'+col.table_name + ' ADD COLUMN '+

                      case
                        when sc.Is_computed=1 then '[' + column_name + '] as ' + cc.definition
                        else '[' + column_name + '] ' + data_type +
                             case
                                 when   data_type='varchar'  and     col.character_maximum_length = -1 then '(MAX)'
                                 when IsNull(col.character_maximum_length ,0) between 1 and 10000 then  '('+  cast( col.character_maximum_length as varchar(10)) +')'
                                 else ''
                                 
                             end +   
                               
                           --  IIF( IsNull( col.character_maximum_length ,0) between 1 and 10000, '('+  cast( col.character_maximum_length as varchar(10)) +')' , '')
                             + IIF (col.IS_NULLABLE = 'NO', ' not null', '')
                             
                             + IIF (col.column_default is not null, ' DEFAULT '+ col.column_default, '')
                             
                             + IIF (sc.Is_Identity = 1, ' IDENTITY(1,1) ', '')
                             
                      end  
                      as [sql_create]  

                         


                       from  INFORMATION_SCHEMA.COLUMNS  col

                      INNER JOIN sys.columns AS sc ON sc.object_id = object_id(table_schema + '.' + table_name) AND sc.NAME = col.COLUMN_NAME

                      inner join sys.objects obj on obj.object_Id=sc.object_Id 

                      left join  sys.computed_columns cc on cc.name = col.COLUMN_NAME and cc.object_id = obj.object_Id

                      where obj.[type]='u'

                        and col.COLUMN_NAME not like'[_]%'
                        and col.COLUMN_NAME not like'%[_][_]'
                        and col.COLUMN_NAME not like'%[11]'


                      order by 

                       -- sc.object_id, 
                        ordinal_position  
                                        
                  
                  /*
                   SELECT 
                      tt.name as table_name, 
                      schema_name(tt.schema_id) as schema_name,         
                      c.name + ' '+

                  case
                    when st.name='varchar' then st.name +'('+ cast(c.max_length as varchar(10)) +  ')'
                    else st.name
                  end 

                  as sql_create
                          
                    FROM sys.columns c
                      JOIN sys.table_types tt ON c.[object_id] = tt.type_table_object_id  
                      JOIN sys.types st ON st.system_type_id  = c.system_type_id
                           
                      */
                      
                   ) S   WHERE S.object_id = M.object_id
                
          FOR XML PATH ('')
      ), 2, 1000) [sql_columns]     
     
     
     
   --  'create table ' + schema_name +'.'+ name + ' ( ' + sql_columns +')' as  [sql_create] 
     
  /*   SUBSTRING(
          (
              SELECT ','+ char(10) + '  '+ sql_create  AS [text()]
                FROM sync.view_TABLE_COLUMNS  S   
                WHERE S.column_object_id = M.object_id
                
              FOR XML PATH ('')
              
      ), 3, 10000000000) [sql_columns1] 
    */
    
     from sys.tables M
     
     
    where
     -- (table_type='base table') 
          (name not like '[_][_]%')         
--      and (name not like '%[_]history')  
      and (name not like '%111')  

      and (name <> 'sysdiagrams')  
      AND (name <> 'dtproperties') 
      and name <> 'msfavorites'

      and (name not like '%[_][_]')      


          
) M
   
where 
  schema_name not in ('service','test', 'sync')

order by 
  schema_name,  name
  
