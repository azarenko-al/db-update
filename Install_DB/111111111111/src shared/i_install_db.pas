unit i_install_db;

interface

uses ADOInt,
  u_dll;

type

  IInstall_db_X = interface(IInterface)
  ['{870E1CAC-FE79-46B4-9832-984D9F3C6E16}']

    procedure InitADO(aConnection: _Connection); stdcall;
    procedure InitAppHandle(aHandle: integer); stdcall;

  //  procedure LoadFromFile(aFileName: WideString);  stdcall;

    procedure Execute (aFileName: WideString);  stdcall;
  end;

var
  IInstall_db: IInstall_db_X;

  function Load_IInstall_db: Boolean;
  procedure UnLoad_IInstall_db;

implementation

const
//  DEF_BPL_FILENAME = 'bpl_LOS.bpl';
  DEF_FILENAME = 'dll_Install_db.dll';

var
  LHandle : Integer;


function Load_IInstall_db: boolean;
begin
  Result := GetInterface_DLL(DEF_FILENAME, LHandle, IInstall_db_X, IInstall_db)=0;
end;


procedure UnLoad_IInstall_db;
begin
  IInstall_db := nil;
  UnloadPackage_dll(LHandle);
end;


end.



