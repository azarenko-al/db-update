unit dm_CompareAndModify;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, AdoDB, dxmdaset,

  u_func_DB,
  u_func,
  u_func_,
  u_func_dlg,

  u_classes,
  u_const,
  u_const_db,
  u_local_const,

  dm_Main,

  d_CompareSQL,

/////////////  dm_Install_,
  dm_ObjectFields,
  dm_CustomErrorLog,
  dm_CustomerSQLServToMem,
  dm_MDBConnect,

  dm_Progress

  ;

type
  TProcessAction = (maAdd, maDel, maUpdate);

  TdmCompareAndModify = class(TdmProgress)
    mem_Data: TdxMemData;
    DataSource1: TDataSource;
    sp_Views: TADOStoredProc;
    qry_Views: TADOQuery;
    qry_Defaults: TADOQuery;
    mem_Temp: TdxMemData;
    procedure DataModuleCreate(Sender: TObject);
  private
    FiTreeID,
    FiCellID: integer; // FIndex
    FDataSet, FMdbDataSet, FSqlDataSet: TDataSet;
    FType: string;
    FObjName, FModuleName, FMdbObjName, FSqlObjName: string;
    FDiffers, FExists: boolean;

    FProcessAction: TProcessAction;

    FSql: string;

    procedure UpdateAction();

    procedure DoSimpleAddObject();
    function  DoMakeDropSql(aType: string; aDataSet: TDataSet): string;

    procedure DoApplyDataset(aType: string);
    procedure DoAddDefaults();

    procedure AddTablesAndFields();
    procedure AddViews();
    procedure AddTriggers();
    procedure AddIndexes();
    procedure AddFK();

    procedure AddStoredProcs();
    procedure AddFields(abTableAdded: boolean; abTableCreated: boolean);

    procedure ExecuteProc; override;

  public
    IsModifyFields: boolean;

    procedure DoAddToInterface (vNameMDB, vNameSQL: variant; aAction: string;
        aType: string; iIDMdb: integer = -1; iIDSql: integer = -1);

    procedure SetDboOwner ();
    procedure RefreshViews ();
    procedure DlgCompareSQL();
    procedure CreateObject();
  end;

const
  TYPE_TABLE    = 'tbl';
  TYPE_FIELD    = 'fld';
  TYPE_PROC     = 'proc';
  TYPE_TRIGGER  = 'trigger';
  TYPE_INDEX    = 'index';
  TYPE_FK       = 'fk';
  TYPE_VIEW     = 'view';
  TYPE_OBJECT_FIELD = 'OBJECT_FIELD';

  STR_UPDATE      = '��������';
  STR_UPDATE_DONE = '��������� ����������';
  STR_UPDATED     = STR_UPDATE_DONE;
  STR_ADD         = '��������';
  STR_ADDED       = '��������� ����������';
  STR_CREATE      = '�������';
  STR_CREATE_DONE = '��������� ��������';
  STR_CREATED     = STR_CREATE_DONE;
  STR_DEL         = '�������';
  STR_DELETED     = '��������� ��������';
  STR_ERR_DELETE  = '������ ��������';
  STR_ERR_DEL_WHILE_UPD = '������ ��������  (��� ����������)';
  STR_ERR_ADD_WHILE_UPD = '������ ���������� (��� ����������)';


var
  FdmCompareAndModify: TdmCompareAndModify;

function dmCompareAndModify: TdmCompareAndModify;

//===================================================================
implementation {$R *.DFM}
//===================================================================

CONST
  MAX_NUMBER_OF_TABLES = 1000;

  ERR_MSG_OBJECT_ALREADY_IN_DB = 'Object "%s" already exist in the DataBase. Cannot create object with same name.';

//--------------------------------------------------------------------
function dmCompareAndModify: TdmCompareAndModify;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmCompareAndModify) then
    FdmCompareAndModify:=TdmCompareAndModify.Create(Application);

  Result:= FdmCompareAndModify;
end;



//---------------------------------------------------------
procedure TdmCompareAndModify.DataModuleCreate(Sender: TObject);
//---------------------------------------------------------
begin
  IsModifyFields := false;
  db_CreateField(mem_Data,
    [db_Field(FLD_ID,               ftInteger),
     db_Field(FLD_TYPE,             ftString),
     db_Field(FLD_DXTREE_id,        ftInteger),
     db_Field(FLD_DXTREE_parent_id, ftInteger),
     db_Field(FLD_ID_MDB,           ftInteger),
     db_Field(FLD_ID_SQL,           ftInteger),
     db_Field(FLD_NAME_MDB,         ftString),
     db_Field(FLD_NAME_SQL,         ftString),
     db_Field(FLD_ACTION,           ftString),
     db_Field(FLD_FIELDS,           ftString)(*,
     db_Field(FLD_CHECKED,          ftBoolean)*)
    ]);

  db_SetComponentsADOConn([qry_Views, sp_Views], dmMain.AdoConnection);

  sp_Views.ProcedureName:= 'sp_RefreshView';
  sp_Views.Parameters.AddParameter;
end;

//------------------------------------------------------------------------------------
procedure TdmCompareAndModify.DoAddToInterface;
//------------------------------------------------------------------------------------
var
  iParentID: Integer;
begin
  if Eq(aType, TYPE_TABLE) then
    iParentID:= 1
  else
  if Eq(aType, TYPE_VIEW) then
    iParentID:= 2
  else
  if Eq(aType, TYPE_TRIGGER) then
    iParentID:= 3
  else
  if Eq(aType, TYPE_PROC) then
    iParentID:= 4
  else
  if Eq(aType, TYPE_OBJECT_FIELD) then
    iParentID:= 5
  else
  if Eq(aType, TYPE_INDEX) then
    iParentID:= 6
  else
  if Eq(aType, TYPE_FK) then
    iParentID:= 7
  else
    Raise Exception.Create('not recognizable type '+aType);

  Inc(FiTreeID);

  db_AddRecord (mem_Data,
                [db_Par (FLD_NAME_MDB,         vNameMDB),
                 db_Par (FLD_NAME_SQL,         vNameSQL),
                 db_Par (FLD_ID_MDB,           IIF(iIDMdb=-1, NULL, iIDMdb) ),
                 db_Par (FLD_ID_SQL,           IIF(iIDSql=-1, NULL, iIDSql)),
                 db_Par (FLD_TYPE,             aType),
                 db_Par (FLD_ACTION,           aAction),
                 db_Par (FLD_DXTREE_ID,        FiTreeID),
                 db_Par (FLD_DXTREE_PARENT_ID, iParentID)   ]);
end;

//---------------------------------------------------------
function  TdmCompareAndModify.DoMakeDropSql(aType: string; aDataSet: TDataSet): string;
//---------------------------------------------------------
var
  sTblName, sObjName: string;
begin
  sObjName:= aDataSet[FLD_NAME];
  sTblName:= aDataSet[FLD_TABLENAME];

  if Eq(aType, TYPE_VIEW) then
    Result:='DROP VIEW ' + sObjName
  else
  if Eq(aType, TYPE_TRIGGER) then
    Result:='DROP TRIGGER ' + sObjName
  else
  if Eq(aType, TYPE_PROC) then begin
    if Pos(UpperCase('create procedure'), UpperCase(aDataSet[FLD_CONTENT]))>0 then
      Result:= Format('DROP %s %s', ['PROCEDURE', sObjName]);
    if Pos(UpperCase('create function'), UpperCase(aDataSet[FLD_CONTENT]))>0 then
      Result:= Format('DROP %s %s', ['FUNCTION', sObjName]);
  end
  else
  if Eq(aType, TYPE_INDEX) then
    Result:=Format('DROP INDEX [%s].[%s]', [sTblName, sObjName])
  else
  if Eq(aType, TYPE_FK) then
    Result:=Format('ALTER TABLE [%s] DROP CONSTRAINT [%s]', [sTblName, sObjName])
  else

    Raise Exception.Create('not recognizable type '+ aType);
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.AddTablesAndFields();
//---------------------------------------------------------
var
  i: integer;
  bTableCreatedArr: array [0..MAX_NUMBER_OF_TABLES] of boolean;
  bTableAdded: boolean;         // ���������� �� ������� � TreeList
begin
  for i:=0 to MAX_NUMBER_OF_TABLES do
    bTableCreatedArr[i]:=false;

//  db_OpenQuery(qry_Defaults, SQL_SELECT_TABLE_DEFAULTS);

  with dmCustomerSQLServToMem, dmMDBConnect do
  begin
    tbl_Tables.First;
    i:=0;

    while not tbl_Tables.EOF do
    begin
      DoProgress2(tbl_Tables.RecNo, tbl_Tables.RecordCount);

      bTableAdded := false;
      tbl_Table_Fields.First;
      FiCellID:=FiTreeID+1;

      if not mem_Tables.Locate(FLD_NAME,
                               dmMDBConnect.tbl_Tables[FLD_NAME],
                               [loCaseInsensitive]) then
      begin
        db_AddRecord (mem_Data,
          [db_Par (FLD_NAME_MDB,           tbl_Tables[FLD_NAME]),
           db_Par (FLD_TYPE,               TYPE_TABLE),
           db_Par (FLD_DXTREE_ID,          FiTreeID),
           db_Par (FLD_DXTREE_PARENT_ID,   1),
           db_Par (FLD_ACTION,             STR_CREATE) ]);

        bTableAdded := true;

        if dmCompareAndModify.IsModifyFields = true then
        begin
          db_UpdateRecord (mem_Data,
            [db_Par (FLD_NAME_SQL,         tbl_Tables[FLD_NAME]),
/////////////             db_Par (FLD_TYPE,               TYPE_TABLE),
             db_Par (FLD_ACTION,           STR_CREATE_DONE)           ]);

          if not gl_DB.ExecCommandSimple(FORMAT('CREATE TABLE [dbo].%s (_temp int)',
                                    [tbl_Tables[FLD_NAME] ])) then
          begin
            ShowMessage('CREATE TABLE [dbo].%s (_temp int) - �� ���������'+CRLF+gl_DB.ErrorMsg);
            custom_ErrorLog.AddError('TdmCompareAndModify.AddTablesAndFields', gl_DB.ErrorMsg);
          end;
          bTableCreatedArr[i]:=true;
        end;
      end;

      AddFields(bTableAdded, bTableCreatedArr[i]);

      bTableAdded := false;
      FiTreeID:=FiCellID;
      tbl_Tables.Next;
      Inc(FiTreeID);
      Inc(i);
    end;    // while


    if IsModifyFields then begin
/////////      FillFieldsMem;

      with tbl_Table_Fields do begin
        First;
        while not EOF do begin
          DoAddDefaults();

          Next;
        end;
      end;
    end;


    //------------------------------------------------------------------
    if IsModifyFields then
    begin
//      if tbl_tables.Locate(FLD_NAME, TABLE_OBJECTS, []) then
        dmObjectFields.FillTableObjects();

//      if tbl_tables.Locate(FLD_NAME, TABLE_OBJECT_FIELDS, []) then
    end;
//////////////////    FillTableObjectFields();
    //------------------------------------------------------------------


    //----------------------------------------------------------------
    //        �������� ���� _temp �� ������, ��� ��� ����
    tbl_Tables.First;
    i:=0;
    while not tbl_Tables.EOF do
    begin
      if bTableCreatedArr[i] = true then
        gl_DB.DropTableField(tbl_Tables[FLD_NAME], '_temp');
//        gl_DB.ExecCommand(Format('ALTER TABLE %s DROP COLUMN _temp',
//                                 [ tbl_Tables.FieldValues[FLD_NAME] ]),
//                                 [EMPTY_PARAM]);
      tbl_Tables.Next;
      Inc(i);
    end;
    //----------------------------------------------------------------

  end;    // with
end;


//---------------------------------
procedure TdmCompareAndModify.DoAddDefaults;
//---------------------------------
var
  sMdbDef, sSqlDef, sSql, sSql1, sError: string;
  bDel, bAdd: boolean;
begin
  with dmCustomerSQLServToMem, dmMDBConnect do begin
    if mem_TableFields.Locate(FLD_TABLENAME+';'+FLD_NAME,
                    VarArrayOf([tbl_Table_Fields.FieldValues[FLD_TABLENAME],
                                tbl_Table_Fields.FieldValues[FLD_NAME] ]),
                    [loCaseInsensitive]) then
    begin
      sMdbDef:= tbl_Table_Fields.FieldByName(FLD_DEFAULT).AsString;
      sSqlDef:= mem_TableFields.FieldByName(FLD_DEFAULT).AsString;

      bDel:= false;  bAdd:= false;

      if (sMdbDef=sSqlDef) then
        exit
      else
      if (not Eq(sMdbDef, '')) AND Eq(sSqlDef, '') then
        bAdd:= true
      else
      if (Eq(sMdbDef, '')) AND (not Eq(sSqlDef, '')) then
        bDel:= true
      else
      if not Eq(sMdbDef, sSqlDef) then  begin
        bDel:= true;
        bAdd:= true;
      end;

      if bDel then begin
        sSql:= Format('ALTER TABLE %s DROP CONSTRAINT %s',
                     [tbl_Table_Fields.FieldByName(FLD_TABLENAME).AsString,
                      mem_TableFields.FieldByName(FLD_DEFAULT_NAME).AsString
                     ]);
        if not gl_DB.ExecCommandSimple(sSql) then begin
          ShowMessage(sSql+' - �� ���������'+CRLF+gl_DB.ErrorMsg);
          custom_ErrorLog.AddError('TdmCompareAndModify.AddFields.DoAddColumnsPrimaryKeysDefaults5', gl_DB.ErrorMsg);
        end;
      end;

      if bAdd then begin
        sSql:= Format('ALTER TABLE %s ADD CONSTRAINT DF_%s_%s DEFAULT %s FOR [%s]',
                              [tbl_Table_Fields.FieldValues[FLD_TABLENAME],
                               tbl_Table_Fields.FieldValues[FLD_TABLENAME],
                               tbl_Table_Fields.FieldValues[FLD_NAME],
                               tbl_Table_Fields.FieldValues[FLD_DEFAULT],
                               tbl_Table_Fields.FieldValues[FLD_NAME]
                              ]);
        if not gl_DB.ExecCommandSimple(sSql) then begin
          sSql1:= Format('EXEC sp_unbindefault ''%s.%s''',
                            [tbl_Table_Fields.FieldValues[FLD_TABLENAME],
                             tbl_Table_Fields.FieldValues[FLD_NAME]]);
          sError:= gl_DB.ErrorMsg;
          if gl_DB.ExecCommandSimple(sSql1) then
            gl_DB.ExecCommandSimple(sSql)
          else begin
            ShowMessage(sSql+' - �� ���������'+CRLF+sError);
            custom_ErrorLog.AddError('TdmCompareAndModify.AddFields.DoAddColumnsPrimaryKeysDefaults5', sError);
          end;
        end;
      end;

    end else
    begin
      bDel:= false;  bAdd:= false;
    end;

  end;
end;


//------------------------------------------------------------------------------
procedure TdmCompareAndModify.AddFields(abTableAdded: boolean; abTableCreated: boolean);
//------------------------------------------------------------------------------

  //------------------------------------------------------------------
  procedure DoAddColumnsPrimaryKeysDefaults(aIsColumnTypeVariable: boolean);
  //------------------------------------------------------------------
  const
    SQL_ADD_FIELD   = 'ALTER TABLE %s ADD [%s] %s%s';
    SQL_ALTER_FIELD = 'ALTER TABLE %s ALTER COLUMN [%s] %s%s';
  var
    sSql: string;
  begin

    with dmCustomerSQLServToMem, dmMDBConnect, tbl_Table_Fields do
    begin
      //--------------------------------------
      // IDENTITY
      if aIsColumnTypeVariable then
        sSql:= Format(SQL_ADD_FIELD,
                [tbl_Tables[FLD_NAME],  FieldValues[FLD_NAME],
                 FieldValues[FLD_TYPE], '('+FieldValues[FLD_SIZE]+')']   )
      else
        sSql:= Format(SQL_ADD_FIELD,
                   [tbl_Tables[FLD_NAME],  FieldValues[FLD_NAME],
                    FieldValues[FLD_TYPE], ' ']   );

      if Eq(FieldValues[FLD_NAME], FLD_ID) then
        sSql:= sSql+' IDENTITY';

      sSql:= ReplaceStr(sSql, 'sysname', 'Varchar');

      if gl_DB.ExecCommandSimple(sSql) then
      begin
        //-------------------
        // Update mem for defaults processing
(*        sSql:= Format(SQL_SELECT_TABLE_FIELDS, [SQL_SELECT_CUSTOM_TABLE_FIELDS]);
        db_OpenQuery (qry_CLient_TableFields1, sSql,
             [db_Par(FLD_TABLENAME,  tbl_Tables[FLD_NAME]    ),
              db_Par(FLD_NAME,       FieldValues[FLD_NAME]   )]);
        if qry_CLient_TableFields1.RecordCount <> 1 then
          Raise Exception.Create('qry_CLient_TableFields1.RecordCount <> 1')
        else*)
          db_AddRecord (mem_TableFields,
            [db_Par(FLD_TABLE_ID,  tbl_Table_Fields[FLD_TABLE_ID]),
             db_Par(FLD_TABLENAME, tbl_Table_Fields[FLD_TABLENAME]),
             db_Par(FLD_NAME,      tbl_Table_Fields[FLD_NAME]),
             db_Par(FLD_TYPE,      tbl_Table_Fields[FLD_TYPE]),
             db_Par(FLD_SIZE,      tbl_Table_Fields[FLD_SIZE]),
             db_Par(FLD_NULLS,     tbl_Table_Fields[FLD_NULLS]),
             db_Par(FLD_VARIABLE,  tbl_Table_Fields[FLD_VARIABLE]),
             db_Par(FLD_USERTYPE,  tbl_Table_Fields[FLD_USERTYPE])    ]);
        //-------------------
      end else
      begin
        ShowMessage(sSql+' - �� ���������'+CRLF+gl_DB.ErrorMsg);
        custom_ErrorLog.AddError('TdmCompareAndModify.AddFields.DoAddColumnsPrimaryKeysDefaults1', gl_DB.ErrorMsg);
      end;
      //--------------------------------------



      if (not FieldByName(FLD_NULLS).AsBoolean) then begin
        if aIsColumnTypeVariable then
          sSql:= Format(SQL_ALTER_FIELD,
                    [tbl_Tables[FLD_NAME],  FieldValues[FLD_NAME],
                     FieldValues[FLD_TYPE], '('+FieldValues[FLD_SIZE]+')'  ])
        else
          sSql:= Format(SQL_ALTER_FIELD,
                        [tbl_Tables[FLD_NAME],  FieldValues[FLD_NAME],
                         FieldValues[FLD_TYPE], ' '  ]);

        sSql:= sSql + ' NOT NULL';
        sSql:= ReplaceStr(sSql, 'sysname', 'Varchar');

        if not gl_DB.ExecCommandSimple(sSql) then
        begin
          ShowMessage(sSql+' - �� ���������'+CRLF+gl_DB.ErrorMsg);
          custom_ErrorLog.AddError('TdmCompareAndModify.AddFields.DoAddColumnsPrimaryKeysDefaults2', gl_Db.ErrorMsg);
        end;
      end;
      //--------------------------------------

    end; // with
  end; // procedure
  //------------------------------------------------------------------
var
  strArrKeyFields: TstrArray;
  sArrKeyFields, s: string;
//////  iTtbl1RecNo: integer;
/////  b: boolean;
begin

  with dmCustomerSQLServToMem, dmMDBConnect do
  begin
    while (not tbl_Table_Fields.EOF) do
    begin

      if (tbl_Tables[FLD_TABLE_ID] = tbl_Table_Fields[FLD_TABLE_ID]) then
      begin
        if not (mem_TableFields.Locate(FLD_TABLENAME+';'+FLD_NAME,
                     VarArrayOf([ tbl_Tables[FLD_NAME], tbl_Table_Fields[FLD_NAME] ]),
                    [loCaseInsensitive]))
        then begin
          if not abTableAdded then  // ���� ������� �� ���������� � Treelist
          begin                         // �������� � ����
            db_AddRecord (mem_Data,
              [db_Par (FLD_NAME_MDB,         tbl_Tables[FLD_NAME]),
               db_Par (FLD_NAME_SQL,         tbl_Tables[FLD_NAME]),
  ////////////////////             db_Par (FLD_TYPE,             TYPE_TABLE),
               db_Par (FLD_DXTREE_ID,        FiTreeID),
               db_Par (FLD_DXTREE_PARENT_ID, 1)            ]);

            abTableAdded := true;
          end;
          db_AddRecord (mem_Data,
            [db_Par (FLD_NAME_MDB,           tbl_Table_Fields[FLD_NAME]),
             db_Par (FLD_TYPE,               TYPE_FIELD),
             db_Par (FLD_DXTREE_ID,          FiCellID+1),
             db_Par (FLD_DXTREE_PARENT_ID,   FiTreeID),
             db_Par (FLD_ACTION,             STR_ADD)    ]);

          if IsModifyFields then
          begin
            db_UpdateRecord (mem_Data,
              [db_Par (FLD_ACTION,     STR_ADD),
  /////////////             db_Par (FLD_TYPE,       TYPE_FIELD),
               db_Par (FLD_NAME_SQL,   tbl_Table_Fields[FLD_NAME])  ]);

              if (tbl_Table_Fields[FLD_VARIABLE]) AND
                 (tbl_Table_Fields[FLD_USERTYPE] < 250)
              then
                DoAddColumnsPrimaryKeysDefaults(true)
              else
                DoAddColumnsPrimaryKeysDefaults(false);
          end;
        end;

        Inc(FiCellID);
        if tbl_Table_Fields[FLD_PK] then
          AddValueToStrArray(tbl_Table_Fields[FLD_NAME], strArrKeyFields);
      end
      else
        if (StrArrayToString(strArrKeyFields,',') <> '') then
          sArrKeyFields := StrArrayToString(strArrKeyFields,',');

      tbl_Table_Fields.Next;

    end;   // while

    if (IsModifyFields) AND (abTableCreated) AND (sArrKeyFields <> '') then
    begin
      s:= Format('ALTER TABLE %s ADD CONSTRAINT [PK_%s] PRIMARY KEY (%s)',
              [tbl_Tables[FLD_NAME], tbl_Tables[FLD_NAME], sArrKeyFields]);

      s:= ReplaceStr(s, 'sysname', 'Varchar');

      if not gl_DB.ExecCommandSimple(s) then begin
        custom_ErrorLog.AddError('TdmCompareAndModify.AddFields', gl_DB.ErrorMsg);
        ShowMessage(s+' - �� ���������'+CRLF+gl_DB.ErrorMsg);
      end;
    end;

  end; // with

end;

//---------------------------------------------------------
procedure TdmCompareAndModify.AddViews();
//---------------------------------------------------------
begin
  FMdbDataSet:= dmMDBConnect.tbl_Views;
  FSqlDataSet:= dmCustomerSQLServToMem.mem_Views;
  FModuleName:= 'TdmCompareAndModify.AddViews';
  FType      := TYPE_VIEW;

  FMdbDataSet.First;
  while not FMdbDataSet.EOF do
  begin
    FExists:= FSqlDataSet.Locate(FLD_NAME, FMdbDataSet[FLD_NAME], [loCaseInsensitive]);
    FDiffers:= not Eq(AsString(FSqlDataSet[FLD_CONTENT]), AsString(FMdbDataSet[FLD_CONTENT]));

    if FExists and (not FDiffers) then
    begin FMdbDataSet.Next; continue; end;

    DoSimpleAddObject;

(*    FMdbObjName:= FMdbDataSet[FLD_NAME];
    FSqlObjName:= FSqlDataSet[FLD_NAME];

    if IsModifyFields then
    begin
      if FExists then
      begin
        FSql:= DoMakeDropSql(FType, FMdbDataSet);

        if not gl_DB.ExecCommandSimple(FSql) then
        begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_DEL_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; Continue;
        end;
      end;

      if not gl_DB.ObjectExists(FMdbObjName) then begin
        FSql:= FMdbDataSet[FLD_CONTENT];

        if not gl_DB.ExecCommandSimple(FSql) then begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_ADD_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; Continue;
        end else
          DoAddToInterface(FMdbObjName, FSqlObjName,
              IIF(FExists, STR_UPDATE_DONE, STR_CREATE_DONE), FType);
      end else
        custom_ErrorLog.AddError(FModuleName,
              Format(ERR_MSG_OBJECT_ALREADY_IN_DB, [FMdbObjName]));
    end else
      if FExists then
        DoAddToInterface(FMdbObjName, FSqlObjName, STR_UPDATE, FType)
      else
        DoAddToInterface(FMdbObjName, NULL, STR_CREATE, FType);
*)
    FMdbDataSet.Next;
  end;



(*    if IsModifyFields then
    begin
      if bViewExists then
        if not gl_DB.ExecCommandSimple('DROP VIEW ' +tbl_Views[FLD_NAME]) then
        begin
          ShowMessage('Error Dropping View '+tbl_Views[FLD_NAME]+CRLF+'ErrorMsg: '+gl_DB.ErrorMsg);
          custom_ErrorLog.AddError('TdmCompareAndModify.AddViews', gl_DB.ErrorMsg);
          tbl_Views.Next;
          continue;
        end;

      if not gl_DB.ObjectExists(dmMDBConnect.tbl_Views[FLD_NAME]) then begin
        if not gl_DB.ExecCommandSimple(tbl_Views[FLD_CONTENT]) then begin
          ShowMessage('Error Creating View '+tbl_Views[FLD_NAME]+CRLF+'ErrorMsg: '+gl_DB.ErrorMsg);
          custom_ErrorLog.AddError('TdmCompareAndModify.AddViews', gl_DB.ErrorMsg);
          tbl_Views.Next;
          continue;
        end;
      end else
        ShowMessage(Format(ERR_MSG_OBJECT_ALREADY_IN_DB, ['View '+tbl_Views[FLD_NAME]]));


      if not bViewExists then
        db_AddRecord (mem_Data,
           [db_Par (FLD_NAME_MDB,           tbl_Views[FLD_NAME]),
            db_Par (FLD_NAME_SQL,           tbl_Views[FLD_NAME]),
/////////              db_Par (FLD_TYPE,               TYPE_VIEW),
            db_Par (FLD_ACTION,             '�������'),
            db_Par (FLD_DXTREE_ID,          FiTreeID),
            db_Par (FLD_DXTREE_PARENT_ID,   2)              ])
      else
        if bViewDiffers then
          db_AddRecord (mem_Data,
             [db_Par (FLD_NAME_MDB,         tbl_Views[FLD_NAME]),
              db_Par (FLD_NAME_SQL,         tbl_Views[FLD_NAME]),
              db_Par (FLD_ACTION,           '���������'),
              db_Par (FLD_DXTREE_ID,        FiTreeID),
              db_Par (FLD_DXTREE_PARENT_ID, 2)                ]);
    end  else
    begin
      if not bViewExists then
        db_AddRecord (mem_Data,
             [db_Par (FLD_NAME_MDB,         tbl_Views[FLD_NAME]),
              db_Par (FLD_TYPE,             TYPE_VIEW),
              db_Par (FLD_ACTION,           STR_CREATE),
              db_Par (FLD_DXTREE_ID,        FiTreeID),
              db_Par (FLD_DXTREE_PARENT_ID, 2)                ])
      else
        if bViewDiffers then
          db_AddRecord (mem_Data,
             [db_Par (FLD_NAME_MDB,         tbl_Views[FLD_NAME]),
              db_Par (FLD_NAME_SQL,         tbl_Views[FLD_NAME]),
              db_Par (FLD_TYPE,             TYPE_VIEW),
              db_Par (FLD_ACTION,           STR_UPDATE),
              db_Par (FLD_DXTREE_ID,        FiTreeID),
              db_Par (FLD_DXTREE_PARENT_ID, 2)                ]);
    end;

    FMdbDataSet.Next;
    Inc(FiTreeID);
  end;*)
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.DoSimpleAddObject();
//---------------------------------------------------------
begin
  FMdbObjName:= FMdbDataSet[FLD_NAME];
  FSqlObjName:= FSqlDataSet[FLD_NAME];                                            

  if IsModifyFields then                                                          
  begin                                                                           
    if FExists then                                                               
    begin                                                                         
      FSql:= DoMakeDropSql(FType, FMdbDataSet);                                   

      if not gl_DB.ExecCommandSimple(FSql) then                                   
      begin                                                                       
        DoAddToInterface(FMdbObjName, NULL, STR_ERR_DEL_WHILE_UPD, FType);        
        custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);   
        FMdbDataSet.Next; continue;                                               
      end;                                                                        
    end;                                                                          

    if not gl_DB.ObjectExists(FMdbObjName) then begin                             
      FSql:= FMdbDataSet[FLD_CONTENT];                                            

      if not gl_DB.ExecCommandSimple(FSql) then begin                             
        DoAddToInterface(FMdbObjName, NULL, STR_ERR_ADD_WHILE_UPD, FType);        
        custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);   
        FMdbDataSet.Next; continue;                                               
      end else                                                                    
        DoAddToInterface(FMdbObjName, FSqlObjName,                                
            IIF(FExists, STR_UPDATED, STR_CREATE_DONE), FType);                   
    end else                                                                      
      custom_ErrorLog.AddError(FModuleName,                                       
            Format(ERR_MSG_OBJECT_ALREADY_IN_DB, [FMdbObjName]));                 
  end else                                                                        
    DoAddToInterface(FMdbObjName, IIF(FExists, FSqlObjName, NULL),                
                     IIF(FExists, STR_UPDATE, STR_CREATE), FType);
end;                                                                             
                                                                                 
//---------------------------------------------------------
procedure TdmCompareAndModify.AddStoredProcs();
//---------------------------------------------------------
begin
  FMdbDataSet:= dmMDBConnect.tbl_StoredProcs;
  FSqlDataSet:= dmCustomerSQLServToMem.mem_StoredProcs;
  FType      := TYPE_PROC;
  FModuleName:= 'TdmCompareAndModify.AddStoredProcs';

  FMdbDataSet.First;
  while not FMdbDataSet.EOF do
  begin
    FExists:= FSqlDataSet.Locate(FLD_NAME, FMdbDataSet[FLD_NAME], [loCaseInsensitive]);
    FDiffers:= not Eq(AsString(FSqlDataSet[FLD_CONTENT]), AsString(FMdbDataSet[FLD_CONTENT]));

    if FExists and (not FDiffers) then
    begin FMdbDataSet.Next; continue; end;

    DoSimpleAddObject();

(*    if IsModifyFields then
    begin
      if FExists then
      begin
        FSql:= DoMakeDropSql(FType, FMdbDataSet);

        if not gl_DB.ExecCommandSimple(FSql) then
        begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_DEL_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; continue;
        end;
      end;

      if not gl_DB.ObjectExists(FMdbObjName) then begin
        FSql:= FMdbDataSet[FLD_CONTENT];

        if not gl_DB.ExecCommandSimple(FSql) then begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_ADD_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; continue;
        end else
          DoAddToInterface(FMdbObjName, FSqlObjName,
              IIF(FExists, STR_UPDATED, STR_CREATE_DONE), FType);
      end else
        custom_ErrorLog.AddError(FModuleName,
              Format(ERR_MSG_OBJECT_ALREADY_IN_DB, [FMdbObjName]));
    end else
      DoAddToInterface(FMdbObjName, IIF(FExists, FSqlObjName, NULL),
                       IIF(FExists, STR_UPDATE, STR_CREATE), FType);
*)
    FMdbDataSet.Next;
  end;

(*      if IsModifyFields then
      begin
        if bStoredProcExists then
        begin
          if Pos(UpperCase('create procedure'), UpperCase(tbl_StoredProcs[FLD_CONTENT]))>0 then
            sObject:= 'PROCEDURE';
          if Pos(UpperCase('create function'), UpperCase(tbl_StoredProcs[FLD_CONTENT]))>0 then
            sObject:= 'FUNCTION';

          FSql:= Format('DROP %s %s', [sObject, tbl_StoredProcs[FLD_NAME]]);

          if not gl_DB.ExecCommandSimple(FSql) then
          begin
            DoAddToInterface(tbl_StoredProcs[FLD_NAME], NULL,
                '������ ��������  (��� ����������)', TYPE_PROC);
            custom_ErrorLog.AddExceptionWithSQL('TdmCompareAndModify.AddStoredProcs', gl_DB.ErrorMsg, FSql);
          end;
        end;

        if not gl_DB.ObjectExists(tbl_StoredProcs[FLD_NAME]) then begin
          FSql:= dmMDBConnect.tbl_StoredProcs[FLD_CONTENT];

          if not gl_DB.ExecCommandSimple(FSql) then begin
            DoAddToInterface(tbl_StoredProcs[FLD_NAME], NULL,
                '������ ���������� (��� ����������)', TYPE_PROC);
            custom_ErrorLog.AddExceptionWithSQL('TdmCompareAndModify.AddStoredProcs', gl_DB.ErrorMsg, FSql);
            tbl_StoredProcs.Next;
            continue;
          end else
            DoAddToInterface(tbl_StoredProcs[FLD_NAME], mem_StoredProcs[FLD_NAME],
                IIF(bStoredProcExists, '���������', '�������'), TYPE_PROC);
        end else
          custom_ErrorLog.AddError('TdmCompareAndModify.AddStoredProcs',
                Format(ERR_MSG_OBJECT_ALREADY_IN_DB, ['��������� '+tbl_StoredProcs[FLD_NAME]]));
      end else
        if bStoredProcExists then
          DoAddToInterface(tbl_StoredProcs[FLD_NAME],
                mem_StoredProcs[FLD_NAME], STR_UPDATE, TYPE_PROC)
        else
          DoAddToInterface(tbl_StoredProcs[FLD_NAME], NULL, STR_CREATE, TYPE_PROC);

      FMdbDataSet.Next;
    end;
*)

end;

//---------------------------------------------------------
procedure TdmCompareAndModify.AddTriggers();
//---------------------------------------------------------
begin
  FMdbDataSet:= dmMDBConnect.tbl_triggers;
  FSqlDataSet:= dmCustomerSQLServToMem.mem_triggers;
  FType      := TYPE_TRIGGER;
  FModuleName:= 'TdmCompareAndModify.AddTriggers';


  //-----����� ��������� ���������----
  FSqlDataSet.First;
  while not FSqlDataSet.EOF do
  begin
    FSqlObjName:= FSqlDataSet[FLD_NAME];

    if (not FMdbDataSet.Locate(FLD_NAME, FSqlObjName, [])) then
    begin
      if IsModifyFields then begin
        FSql:='DROP TRIGGER ' +FSqlObjName;
        if gl_DB.ExecCommandSimple(FSql) then
          DoAddToInterface(NULL, FSqlObjName, STR_DELETED, FType)
        else begin
          DoAddToInterface(NULL, FSqlObjName, STR_ERR_DELETE, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
        end;
      end
      else
        DoAddToInterface(NULL, FSqlObjName, STR_DEL, FType);
    end;

    FSqlDataSet.Next;
  end;





  //-----����� ����������� � ����������� ���������
  FMdbDataSet.First;
  while not FMdbDataSet.EOF do
  begin
    FExists:= FSqlDataSet.Locate(FLD_NAME, FMdbDataSet[FLD_NAME], [loCaseInsensitive]);
    FDiffers:= not Eq(AsString(FSqlDataSet[FLD_CONTENT]), AsString(FMdbDataSet[FLD_CONTENT]));

    if FExists and (not FDiffers) then
    begin FMdbDataSet.Next; continue; end;

    DoSimpleAddObject;

(*    FMdbObjName:= FMdbDataSet[FLD_NAME];
    FSqlObjName:= FSqlDataSet[FLD_NAME];

    if IsModifyFields then
    begin
      if FExists then
      begin
        FSql:= DoMakeDropSql(FType, FMdbDataSet);

        if not gl_DB.ExecCommandSimple(FSql) then
        begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_DEL_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; continue;
        end;
      end;

      if not gl_DB.ObjectExists(FMdbObjName) then begin
        FSql:= FMdbDataSet[FLD_CONTENT];

        if not gl_DB.ExecCommandSimple(FSql) then begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_ADD_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; continue;
        end else
          DoAddToInterface(FMdbObjName, FSqlObjName,
              IIF(FExists, STR_UPDATED, STR_CREATE_DONE), FType);
      end else
        custom_ErrorLog.AddError(FModuleName, Format(ERR_MSG_OBJECT_ALREADY_IN_DB, [FMdbObjName]));
    end else
      DoAddToInterface(FMdbObjName, IIF(FExists, FSqlObjName, NULL),
                       IIF(FExists, STR_UPDATE, STR_CREATE), FType);
*)
    FMdbDataSet.Next;
  end;


(*  //-----����� ����������� � ����������� ���������
  tbl_triggers.First;
  while not tbl_triggers.EOF do
  begin
    FExists:= mem_triggers.Locate(FLD_NAME,
                        dmMDBConnect.tbl_triggers.FieldByName(FLD_NAME).AsString,
                                   [loCaseInsensitive]);
    FDiffers:= not Eq(AsString(dmCustomerSQLServToMem.mem_triggers.FieldValues[FLD_CONTENT]),
                          AsString(dmMDBConnect.tbl_triggers[FLD_CONTENT]));

    if FExists and (not FDiffers) then begin
      tbl_triggers.Next; continue;
    end;

    if IsModifyFields then
    begin
      if FExists then
      begin
        FSql:='DROP TRIGGER ' +mem_triggers[FLD_NAME];
        if not gl_DB.ExecCommandSimple(FSql) then begin
          DoAddToInterface(tbl_triggers[FLD_NAME], NULL, '������ �������� (��� ����������)', TYPE_TRIGGER);
          custom_ErrorLog.AddExceptionWithSQL('TdmCompareAndModify.AddTriggers', gl_DB.ErrorMsg, FSql);
          tbl_triggers.Next;
          continue;
        end;
      end;

      if not gl_DB.ObjectExists(tbl_triggers[FLD_NAME]) then begin
        FSql:= dmMDBConnect.tbl_triggers[FLD_CONTENT];
        if not gl_DB.ExecCommandSimple(FSql) then begin
          DoAddToInterface(tbl_triggers[FLD_NAME], NULL, '������ ���������� (��� ����������)', TYPE_TRIGGER);
          custom_ErrorLog.AddExceptionWithSQL('TdmCompareAndModify.AddTriggers', gl_DB.ErrorMsg, FSql);
          tbl_triggers.Next;
          continue;
        end else
          DoAddToInterface(tbl_triggers[FLD_NAME], mem_triggers[FLD_NAME], IIF(bExists, STR_UPDATE_DONE, STR_CREATED), TYPE_TRIGGER);
      end else
        custom_ErrorLog.AddError('TdmCompareAndModify.AddTriggers', Format(ERR_MSG_OBJECT_ALREADY_IN_DB, ['������� '+tbl_triggers[FLD_NAME]]));
    end else
      if FExists then
        DoAddToInterface(tbl_triggers[FLD_NAME], mem_triggers[FLD_NAME], STR_UPDATE, TYPE_TRIGGER)
      else
        DoAddToInterface(tbl_triggers[FLD_NAME], NULL, STR_CREATE, TYPE_TRIGGER);

    tbl_triggers.Next;
    Inc(FiTreeID);
  end;*)
end;



//----------------------------------------------------------------------------------
procedure TdmCompareAndModify.AddIndexes;
//----------------------------------------------------------------------------------
var
  sName: string;
begin
  FMdbDataSet:= dmMDBConnect.tbl_indexes;
  FSqlDataSet:= dmCustomerSQLServToMem.mem_indexes;
  FType      := TYPE_INDEX;
  FModuleName:= 'TdmCompareAndModify.AddIndexes';


  //-----����� ��������� ��������----
  FSqlDataSet.First;
  while not FSqlDataSet.EOF do
  begin
    FSqlObjName:= FSqlDataSet[FLD_NAME];

    sName:= FSqlDataSet[FLD_TABLENAME];

    if ((sName[1]='_') and (sName[2]='_')) or
         (Pos('pk_',LowerCase(FSqlObjName))<>0) then
    begin FSqlDataSet.Next; continue; end;

    if (not FMdbDataSet.Locate(FLD_NAME, FSqlObjName, [])) then
    begin
      if IsModifyFields then begin
        FSql:='DROP INDEX '+sName+'.'+FSqlObjName;
        if gl_DB.ExecCommandSimple(FSql) then
          DoAddToInterface(NULL, FSqlObjName, STR_DELETED, FType)
        else begin
          DoAddToInterface(NULL, FSqlObjName, STR_ERR_DELETE, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
        end;
      end
      else
        DoAddToInterface(NULL, FSqlObjName, STR_DEL, FType);
    end;

    FSqlDataSet.Next;
  end;




  //-----����� ����������� � ����������� ��������
  FMdbDataSet.First;
  while not FMdbDataSet.EOF do
  begin
    FMdbObjName:= FMdbDataSet[FLD_NAME];

    //���������� ��������� ��������� ������
    if Pos('pk_',LowerCase(FMdbObjName))<>0 then
      begin FMdbDataSet.Next; continue; end;

    FExists:= FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_NAME,
                VarArrayOf([FMdbDataSet[FLD_TABLENAME], FMdbObjName]),
                                 [loCaseInsensitive]);
    FDiffers:= not Eq(AsString(FSqlDataSet[FLD_CONTENT]), AsString(FMdbDataSet[FLD_CONTENT]));

    if FExists and (not FDiffers) then
    begin FMdbDataSet.Next; continue; end;

    DoSimpleAddObject();

(*    FSqlObjName:= FSqlDataSet[FLD_NAME];

    if IsModifyFields then
    begin
      if FExists then
      begin
        FSql:= DoMakeDropSql(FType, FMdbDataSet);

        if not gl_DB.ExecCommandSimple(FSql) then
        begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_DEL_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; continue;
        end;
      end;

      if not gl_DB.ObjectExists(FMdbObjName) then begin
        FSql:= FMdbDataSet[FLD_CONTENT];

        if not gl_DB.ExecCommandSimple(FSql) then begin
          DoAddToInterface(FMdbObjName, NULL, STR_ERR_ADD_WHILE_UPD, FType);
          custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
          FMdbDataSet.Next; continue;
        end else
          DoAddToInterface(FMdbObjName, FSqlObjName,
              IIF(FExists, STR_UPDATED, STR_CREATE_DONE), FType);
      end else
        custom_ErrorLog.AddError(FModuleName, Format(ERR_MSG_OBJECT_ALREADY_IN_DB, [FMdbObjName]));
    end else
      DoAddToInterface(FMdbObjName, IIF(FExists, FSqlObjName, NULL),
                       IIF(FExists, STR_UPDATE, STR_CREATE), FType);
*)
    FMdbDataSet.Next;
  end;




(*  with dmCustomerSQLServToMem, dmMDBConnect do
  begin

    //-----����� ��������� ��������----
    mem_indexes.First;
    while not mem_indexes.EOF do
    begin
      sName:= mem_Indexes[FLD_TABLENAME];

      if ((sName[1]='_') and (sName[2]='_')) or
         (Pos('pk_',LowerCase(mem_indexes[FLD_NAME]))<>0) then
      begin mem_Indexes.Next; continue; end;

      if (not tbl_indexes.Locate(FLD_NAME, mem_indexes[FLD_NAME], [])) then
      begin
        if IsModifyFields then begin
          FSql:='DROP INDEX '+mem_indexes[FLD_TABLENAME]+'.'+mem_indexes[FLD_NAME];
          if gl_DB.ExecCommandSimple(FSql) then
            DoAddToInterface(mem_indexes[FLD_NAME], NULL, '������', TYPE_INDEX)
          else begin
            DoAddToInterface(mem_indexes[FLD_NAME], NULL, STR_ERR_DELETE, TYPE_INDEX);
            custom_ErrorLog.AddExceptionWithSQL('TdmCompareAndModify.AddIndexes', gl_DB.ErrorMsg, FSql);
          end;
        end
        else
          DoAddToInterface(tbl_Indexes[FLD_NAME], NULL, STR_DEL, TYPE_INDEX);
      end;

      mem_indexes.Next;
      Inc(FiTreeID);
    end;

    //-----����� ����������� � ����������� ��������
    tbl_indexes.First;
    while not tbl_indexes.EOF do
    begin
      //���������� ��������� ��������� ������
      if Pos('pk_',LowerCase(tbl_indexes[FLD_NAME]))<>0 then
      begin tbl_indexes.Next; continue; end;

      bIndexExists:= mem_indexes.Locate(FLD_TABLENAME+';'+FLD_NAME,
                      VarArrayOf([dmMDBConnect.tbl_indexes[FLD_TABLENAME],
                                  dmMDBConnect.tbl_indexes[FLD_NAME]]),
                                 [loCaseInsensitive]);

      bIndexDiffers:= not Eq(AsString(dmCustomerSQLServToMem.mem_Indexes.FieldValues[FLD_CONTENT]),
                        AsString(dmMDBConnect.tbl_Indexes[FLD_CONTENT]));

      if bIndexExists and (not bIndexDiffers) then
      begin tbl_Indexes.Next; continue; end;

      if not IsModifyFields then
        DoAddToInterface(tbl_Indexes[FLD_NAME], NULL,
                 IIF(bIndexExists, STR_UPDATE, STR_CREATE), TYPE_INDEX)
      else
      begin
        if bIndexExists then begin
          FSqL:='DROP INDEX '+tbl_indexes[FLD_TABLENAME]+'.'+tbl_indexes[FLD_NAME];
          if not gl_DB.ExecCommandSimple(FSqL) then
          begin
            DoAddToInterface(tbl_Indexes[FLD_NAME], NULL, STR_ERR_UPDATE, TYPE_INDEX);
            custom_ErrorLog.AddExceptionWithSQL('TdmCompareAndModify.AddIndexes', gl_DB.ErrorMsg, FSqL);
            mem_indexes.Next;
            Inc(FiTreeID);
            continue;
          end;
        end;

        if not gl_DB.ObjectExists(tbl_Indexes[FLD_NAME]) then
        begin
          FSqL:=tbl_indexes[FLD_CONTENT];
          if gl_DB.ExecCommandSimple(FSqL) then
            DoAddToInterface(tbl_Indexes[FLD_NAME], tbl_indexes[FLD_NAME],
                   IIF(bIndexExists, STR_UPDATE_DONE, STR_CREATED), TYPE_INDEX)
          else
          begin
            DoAddToInterface(tbl_Indexes[FLD_NAME], NULL,
                  IIF(bIndexExists, '������ ����������', STR_ERR_CREATE),
                  TYPE_INDEX);
            custom_ErrorLog.AddExceptionWithSQL('TdmCompareAndModify.AddIndexes', gl_DB.ErrorMsg, FSqL);
          end;
        end;
      end;

      tbl_Indexes.Next;
      Inc(FiTreeID);
    end;

  end;  // with*)
end;


//---Foreign Keys--------------------------------------------------------------------
procedure TdmCompareAndModify.AddFK;
//------------------------------------------------------------------------------------
var
  bCommit: boolean;
  sTableName: string;
  oTablesDataset: TDataSet;

    //---------------------------------------------
    procedure DoCompare ();
    begin
      // ����� ��������� FK
      FSqlDataSet.First;
      while not FSqlDataSet.EOF do
      begin
        if (not FMdbDataSet.Locate(FLD_NAME, FSqlDataSet[FLD_NAME], [])) then
          DoAddToInterface(NULL, FSqlDataSet[FLD_NAME], STR_DEL, TYPE_FK);

        FSqlDataSet.Next;
      end;

      //����� ����������� � ����������� FK
      FMdbDataSet.First;
      while not FMdbDataSet.EOF do
      begin
        FExists:= FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_NAME,
                      VarArrayOf([FMdbDataSet[FLD_TABLENAME],
                                  FMdbDataSet[FLD_NAME]]),
                                 [loCaseInsensitive]);

        FDiffers:= not Eq(AsString(FSqlDataSet.FieldValues[FLD_CONTENT]),
                            AsString(FMdbDataSet[FLD_CONTENT]));

        if FExists and (not FDiffers) then
        begin FMdbDataSet.Next; continue; end;

        DoAddToInterface(FMdbDataSet[FLD_NAME], IIF(FExists, FSqlDataSet[FLD_NAME], NULL),
              IIF(FExists, STR_UPDATE, STR_CREATE), TYPE_FK);

        FMdbDataSet.Next;
      end;
    end;

begin
  FMdbDataSet   := dmMDBConnect.tbl_FK;
  FSqlDataSet   := dmCustomerSQLServToMem.mem_FK;
  FType         := TYPE_FK;
  FModuleName   := 'TdmCompareAndModify.AddFK';
  oTablesDataset:= dmCustomerSQLServToMem.mem_Tables;


  if not IsModifyFields then
    DoCompare
  else

  // �������� ��� ������� ������ �������������� ------------------------------
  begin
    oTablesDataset.First;

    while not oTablesDataset.Eof do
    begin
      FMdbDataSet.Filtered:=false;           //��������� �������
      FMdbDataSet.Filter:=FLD_TABLENAME+' = '+QuotedStr(oTablesDataset[FLD_NAME]);
      FMdbDataSet.Filtered:=true;

      gl_DB.BeginTrans;
      bCommit:=true;


      //���������� � ����������� ������� ������
      FMdbDataSet.First;
      while not FMdbDataSet.EOF do
      begin
        FExists:= FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_NAME,
                       VarArrayOf([FMdbDataSet[FLD_TABLENAME],
                                   FMdbDataSet[FLD_NAME]]),
                                   [loCaseInsensitive]);

        FDiffers:= not Eq(AsString(FSqlDataSet[FLD_CONTENT]),
                              AsString(FMdbDataSet[FLD_CONTENT]));

        if FExists and (not FDiffers) then
        begin FMdbDataSet.Next; continue; end;

        FMdbObjName:= FMdbDataSet[FLD_NAME];
        FSqlObjName:= FSqlDataSet[FLD_NAME];

        bCommit:=true;
        if FExists then
        begin
          FSql:='ALTER TABLE ['+FMdbDataSet[FLD_TABLENAME]+'] DROP CONSTRAINT ['+FMdbObjName+']';
          if not gl_DB.ExecCommandSimple(FSql) then
            begin
              DoAddToInterface(FMdbObjName, NULL, STR_ERR_DELETE, FType);
              custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
              bCommit:=false;
              break;
            end;
        end;

        if not gl_DB.ObjectExists(FMdbObjName) then
        begin
          FSql:=FMdbDataSet[FLD_CONTENT];
          if gl_DB.ExecCommandSimple(FSql) then begin
            DoAddToInterface(FMdbObjName, FMdbObjName, IIF(FExists, STR_UPDATED, STR_CREATED), FType);
          end else
          begin
            DoAddToInterface(FMdbObjName, NULL, STR_ERR_ADD_WHILE_UPD, FType);
            custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
            bCommit:=false;
            break;
          end;
        end;

        FMdbDataSet.Next;
      end;


      if not bCommit then
      begin
        gl_DB.RollbackTrans;
        oTablesDataset.Next;
        continue;
      end;

      //�������� ������� ������
      FSqlDataSet.First;
      while not FSqlDataSet.EOF do
      begin
        if FSqlDataSet[FLD_TABLENAME]<>FMdbDataSet[FLD_TABLENAME] then
        begin FSqlDataSet.Next; continue; end;

        bCommit:= true;
        sTableName:= FSqlDataSet[FLD_TABLENAME];
        FSqlObjName:= FSqlDataSet[FLD_NAME];

        if not FMdbDataSet.Locate(FLD_NAME, FSqlObjName, []) then
        begin
          FSql:='ALTER TABLE ['+sTableName+ '] DROP CONSTRAINT ['+FSqlObjName+']';
          if gl_DB.ExecCommandSimple(FSql) then
            DoAddToInterface(NULL, FSqlObjName, STR_DELETED, FType)
          else
          begin
            DoAddToInterface(FSqlObjName, NULL, STR_ERR_DELETE, FType);
            custom_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
            bCommit:=false;
            break;
          end;
        end;
        FSqlDataSet.Next;
      end;

      if not bCommit then
      begin
        gl_DB.RollbackTrans;
        FSqlDataSet.Next;
        continue;
      end;

      gl_DB.CommitTrans;

      FSqlDataSet.Next;
    end;

  end;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.DoApplyDataset(aType: string);
//---------------------------------------------------------
begin
  with dmCustomerSQLServToMem, dmMDBConnect do begin
    if (aType = TYPE_VIEW) then
    begin
      FMdbDataSet:= tbl_Views;
      FSqlDataSet:= mem_Views;
    end else

    if (aType = TYPE_PROC) then
    begin
      FMdbDataSet:= tbl_StoredProcs;
      FSqlDataSet:= mem_StoredProcs;
    end else

    if (aType = TYPE_TRIGGER) then
    begin
      FMdbDataSet:= tbl_Triggers;
      FSqlDataSet:= mem_Triggers;
    end else

    if (aType = TYPE_FK) then
    begin
      FMdbDataSet:= tbl_Fk;
      FSqlDataSet:= mem_FK;
    end else

    if (aType = TYPE_INDEX) then
    begin
      FMdbDataSet:= tbl_indexes;
      FSqlDataSet:= mem_Indexes;
    end;
  end;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.DlgCompareSQL();
//---------------------------------------------------------
var
  sServSql, sMdbSql, sType, sName: string;
begin
  UpdateAction();

  with dmCustomerSQLServToMem, dmMDBConnect do
  begin
    DoApplyDataset(FType);

    if (FType = TYPE_VIEW) or
       (FType = TYPE_PROC) or
       (FType = TYPE_TRIGGER) or
       (FType = TYPE_INDEX) or
       (FType = TYPE_FK)
    then begin
      if FMdbDataSet.Locate(FLD_NAME, FObjName, [loCaseInsensitive]) then
        sMdbSql := FMdbDataSet.FieldByName(FLD_CONTENT).AsString
      else
        sMdbSql := '';

      if FSqlDataSet.Locate(FLD_NAME, FObjName, [loCaseInsensitive]) then
        sServSql:= FSqlDataSet.FieldByName(FLD_CONTENT).AsString
      else
        sServSql:= '';

      Tdlg_CompareSQL.ExecDlg(Trim(sMdbSql), Trim(sServSql));
    end else

(*    if sType = TYPE_PROC then begin
      if mem_StoredProcs.Locate(FLD_NAME, sName, [loCaseInsensitive]) then
        if tbl_StoredProcs.Locate(FLD_NAME, sName, [loCaseInsensitive]) then
        begin
          sMdbSql := tbl_StoredProcs.FieldByName(FLD_CONTENT).AsString;
          sServSql:= mem_StoredProcs.FieldByName(FLD_CONTENT).AsString;

          Tdlg_CompareSQL.ExecDlg(Trim(sMdbSql), Trim(sServSql));
        end;
    end else

    if sType = TYPE_TRIGGER then begin
      if mem_Triggers.Locate(FLD_NAME, sName, [loCaseInsensitive]) then
        if tbl_Triggers.Locate(FLD_NAME, sName, [loCaseInsensitive]) then
        begin
          sMdbSql := tbl_Triggers.FieldByName(FLD_CONTENT).AsString;
          sServSql:= mem_Triggers.FieldByName(FLD_CONTENT).AsString;
          Tdlg_CompareSQL.ExecDlg(Trim(sMdbSql), Trim(sServSql));
        end;
    end else*)

    if sType = TYPE_OBJECT_FIELD then begin
      qry_object_fields_Mdb.Filtered:= true;
      qry_object_fields_Mdb.Filter:= 'id = '+ mem_Data.FieldByName(FLD_ID_MDB).AsString;

      qry_object_fields.Filtered:= true;
      qry_object_fields.Filter:= 'id = '+ mem_Data.FieldByName(FLD_ID_SQL).AsString;

      db_View(qry_object_fields_Mdb, qry_object_fields);

      qry_object_fields_Mdb.Filtered:= false;
      qry_object_fields.Filtered:= false;
    end;

  end;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.UpdateAction();
//---------------------------------------------------------
var
  sAction: string;
begin
  FType:=       mem_Data.FieldByName(FLD_TYPE).AsString;
  sAction:=     mem_Data.FieldByName(FLD_ACTION).AsString;

  if (sAction=STR_CREATE) or
     (sAction=STR_ADD)
  then
    FProcessAction:= maAdd
  else

  if (sAction=STR_UPDATE) then
    FProcessAction:= maUpdate
  else
  
  if (sAction=STR_DEL) then
    FProcessAction:= maDel
  else
    Raise Exception.Create('not recognizible action: '+sAction);

  case FProcessAction of
    maAdd,
    maUpdate: FObjName:= mem_Data.FieldByName(FLD_NAME_MDB).AsString;
    maDel:    FObjName:= mem_Data.FieldByName(FLD_NAME_SQL).AsString;
  end;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.CreateObject(); 
//---------------------------------------------------------

  function DoDelObject(): boolean;
  begin
    FSql:= DoMakeDropSql(FType, FDataSet);
    Result:= gl_DB.ExecCommandSimple(FSql);
  end;

  function DoAddObject(): boolean;
  begin
    Result:= gl_DB.ExecCommandSimple(FDataSet[FLD_CONTENT]);
  end;

var
  sParentName, sSql: string;
  iID, iParentID: Integer;
begin
  UpdateAction();

  iParentID:=   mem_Data.FieldByName(FLD_DXTREE_PARENT_ID).AsInteger;
  iID:=         mem_Data.FieldByName(FLD_DXTREE_ID).AsInteger;

  //-----------------------------------------------
  if FType = TYPE_TABLE then
  begin
    MsgDlg('����������� �� �����������');
  end else

  //-----------------------------------------------
  if (FType = TYPE_PROC) or
     (FType = TYPE_TRIGGER) or
     (FType = TYPE_VIEW) or
     (FType = TYPE_FK) or
     (FType = TYPE_INDEX)
  then begin
    FModuleName:= 'TdmCompareAndModify.CreateObject.'+FType;

    if not (FProcessAction in [maAdd, maUpdate, maDel]) then
      Raise Exception.Create('��� �������� �� �������: ' +AsString(FProcessAction));

    with dmCustomerSQLServToMem, dmMDBConnect do begin
      if (FType = TYPE_PROC) then
        if FProcessAction=maDel then FDataSet:=mem_StoredProcs else FDataSet:=tbl_StoredProcs;
      if (FType = TYPE_TRIGGER) then
        if FProcessAction=maDel then FDataSet:=mem_Triggers else FDataSet:=tbl_Triggers;
      if (FType = TYPE_VIEW) then
        if FProcessAction=maDel then FDataSet:=mem_Views else FDataSet:=tbl_views;
      if (FType = TYPE_FK) then
        if FProcessAction=maDel then FDataSet:=mem_FK else FDataSet:=tbl_FK;
      if (FType = TYPE_INDEX) then
        if FProcessAction=maDel then FDataSet:=mem_Indexes else FDataSet:=tbl_indexes;
    end;

    if FDataSet.Locate(FLD_NAME, FObjName, [loCaseInsensitive]) then
    begin
      gl_DB.ErrorMsg:= '';

      case FProcessAction of
        maDel: if DoDelObject() then mem_Data.Delete;

        maAdd: if DoAddObject() then mem_Data.Delete;

        maUpdate: if DoDelObject() then
                    if DoAddObject() then mem_Data.Delete;
      end;

      if gl_DB.ErrorMsg<>'' then begin
        ShowMessage(Format('������. ��� �������: %s, ��� %s. ����� ������: ',
                [FType, FDataSet[FLD_NAME], gl_DB.ErrorMsg]));
        custom_ErrorLog.AddError(FModuleName, gl_DB.ErrorMsg);
      end;
    end else begin
      ShowMessage(FObjName);
      db_View(FDataSet);
    end;
  end else

  //-----------------------------------------------
  if FType = TYPE_FIELD then
  begin
    if mem_Data.Locate(FLD_DXTREE_ID, iParentID, []) then
    begin
      sParentName:= mem_Data[FLD_NAME_MDB];
      if dmMDBConnect.tbl_table_fields.Locate(FLD_TABLENAME+';'+FLD_NAME,
                         VarArrayOf([sParentName, FObjName]), []) then
      begin
        sSql:= Format('ALTER TABLE %s ADD [%s] %s',
                      [sParentName,
                       dmMDBConnect.tbl_Table_Fields[FLD_NAME],
                       dmMDBConnect.tbl_Table_Fields[FLD_TYPE]  ]);

        if (dmMDBConnect.tbl_Table_Fields[FLD_VARIABLE]) AND
           (dmMDBConnect.tbl_Table_Fields[FLD_USERTYPE] < 250)
        then
          AppendStr(sSql, Format('(%s)', [dmMDBConnect.tbl_Table_Fields.FieldByName(FLD_SIZE).AsString]));

        if (dmMDBConnect.tbl_Table_Fields[FLD_NULLS] = false) then
          AppendStr(sSql, ' NOT NULL');

        try
          if gl_DB.ExecCommandSimple(sSql) then
            if mem_Data.Locate(FLD_DXTREE_ID, iID, []) then
              mem_Data.Delete;
        except
          on E: Exception do begin
            MsgDlg('������. ���� �� �������. �����: '+ E.Message);
            custom_ErrorLog.AddError('TdmCompareAndModify.CreateObject', E.Message);
          end;
        end;
      end;

(*      if mem_Data.Locate(FLD_DXTREE_ID, iID, []) then
        mem_Data.Delete;*)
    end;
  end else

end;

//---------------------------------------------------------
procedure TdmCompareAndModify.SetDboOwner ();
//---------------------------------------------------------
begin
  if gl_DB.ObjectExists('sp__SetDboOwner') then
    if not db_ExecStoredProc('sp__SetDboOwner') then
      ShowMessage(gl_DB.ErrorMsg);
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.RefreshViews ();
//---------------------------------------------------------
(*var
  sViewName: string;*)
begin
  CursorHourGlass;
  gl_DB.UpdateViews;
  CursorDefault;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify.ExecuteProc ();
//---------------------------------------------------------

  procedure DoAdd (sName: string; iInd: Integer);
  begin
    db_AddRecord (mem_Data,
     [db_Par (FLD_NAME_MDB,             sName),
      db_Par (FLD_DXTREE_ID,            iInd),
      db_Par (FLD_DXTREE_PARENT_ID,     -1)   ]);
  end;

begin
  mem_Data.DisableControls;

  db_Clear(mem_Data);

  DoAdd('�������',              1);
  DoAdd('�������',              2);
  DoAdd('��������',             3);
  DoAdd('�������� ���������',   4);
  if dmMain.DEBUG then begin
    DoAdd('������������ ����',  5);
    DoAdd('�������',            6);
    DoAdd('������� �����',      7);
  end;

  FiTreeID:=10;

  DoProgress(0,6);
  AddTablesAndFields ();
  DoProgress(1,6);

  Inc(FiTreeID);  // �� ������ ��������

  AddViews ();
  DoProgress(2,6);

  AddTriggers ();
  DoProgress(3,6);

  AddStoredProcs ();
  DoProgress(4,6);

  AddIndexes ();
  DoProgress(5,6);

  AddFK ();
  DoProgress(6,6);

  mem_Data.EnableControls;
end;



end.
