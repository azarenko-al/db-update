unit dm_MDB;

interface

uses
  SysUtils, Classes, Forms, ADODB, Dialogs,

  u_dlg,

  u_local_const,

  u_sync_classes,

  u_db_manager,
  u_db,
  u_db_mdb, DB
  ;

type
  TdmMDB1 = class(TDataModule)
    qry_object_fields_Mdb: TADOQuery;
    tbl_mem_Data: TADOTable;
    tbl_version: TADOTable;
    qry_Temp_Mdb: TADOQuery;
    ADOConnection1: TADOConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
//    Version: TDateTime;

  public
    TableNameList: TStringList;
    MDB: TDBManager;

    function Connect_MDB1(aMdbFile: string): Boolean;
    procedure EmptyTable1(aTableName: string);
// TODO: Load_DBStructure111111
//  procedure Load_DBStructure111111(aDBStructure: TDBStructure);

    function TableExists1(aTableName: string): Boolean;

    class procedure Init;

  end;


const
  TYPE_TABLE    = 'TABLE';
  TYPE_FIELD    = 'FIELD';
  TYPE_PROC     = 'proc';
  TYPE_TRIGGER  = 'trigger';
  TYPE_INDEX    = 'index';
  TYPE_FK       = 'fk';
  TYPE_CK       = 'ck';
  TYPE_VIEW     = 'view';

  TYPE_OBJECT_FIELD = 'OBJECT_FIELD';

var
  dmMDB1: TdmMDB1;

//function dmMDB: TdmMDB;


implementation {$R *.DFM}



class procedure TdmMDB1.Init;
begin
  if not Assigned(dmMDB1) then
    dmMDB1 := TdmMDB1.Create(Application);

end;

//------------------------------------------------------------------------------
procedure TdmMDB1.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin

//  sCurAppPath:= ExtractFilePath(Application.ExeName);
//  sCurAppPath:= IncludeTrailingBackslash(sCurAppPath)+ 'onega_tables.mdb';

  if ADOConnection1.Connected then
    ShowMessage('TdmMDB.DataModuleCreate ADOConnection1.Connected');



  TableNameList:= TStringList.Create;

  MDB := TDBManager.Create;
  MDB.ADOConnection := ADOConnection1;

  db_SetComponentADOConnection(Self, ADOConnection1);

(*
  tbl_objects.TableName       := '_objects';
 // if TableNameList.IndexOf('_object_fields')>=0 then
  tbl_object_fields.TableName := '_object_fields';

  tbl_tables1.TableName       := 'tables';
  tbl_table_fields.TableName  := 'table_fields';
  tbl_views.TableName         := 'views';
  tbl_triggers.TableName      := 'triggers';
  tbl_StoredProcs.TableName   := 'StoredProcs';
  tbl_indexes.TableName       := 'indexes';
  tbl_CK.TableName            := 'CK';
  tbl_FK.TableName            := 'FK';*)


end;

//------------------------------------------------------------------------------
procedure TdmMDB1.DataModuleDestroy(Sender: TObject);
//------------------------------------------------------------------------------
begin
  FreeAndNil(TableNameList);
  FreeAndNil(MDB);
end;

//------------------------------------------------------------------------------
function TdmMDB1.Connect_MDB1(aMdbFile: string): Boolean;
//------------------------------------------------------------------------------
var
  i: Integer;
begin
  Result:= mdb_OpenConnection(ADOConnection1, aMdbFile);

  if not Result then
  begin
    ErrorDlg('������ �������� ����� MS Access');
    exit;
  end;


  ADOConnection1.GetTableNames(TableNameList);

 (*
  tbl_objects.TableName       := '_objects';
 // if TableNameList.IndexOf('_object_fields')>=0 then
  tbl_object_fields.TableName := '_object_fields';

  db_TableReOpen(tbl_tables1, 'tables');

  i:=tbl_tables1.RecordCount;

  db_TableReOpen(tbl_table_fields, 'table_fields');
  db_TableReOpen(tbl_views, 'views');
  db_TableReOpen(tbl_triggers, 'triggers');
  db_TableReOpen(tbl_StoredProcs, 'StoredProcs');
  db_TableReOpen(tbl_indexes, 'indexes');
  db_TableReOpen(tbl_FK, 'FK');
  db_TableReOpen(tbl_CK, 'CK');
*)

(*  if TableExists1('_objects') then
    db_TableReOpen (tbl_objects, '_objects');

  if TableExists1('_object_fields') then
  begin
    db_TableReOpen (tbl_object_fields, '_object_fields');
    tbl_object_fields.Sort:= 'object_id ASC';
  end;
*)

(*
  db_Clear (tbl_indexes);
  db_Clear (tbl_FK);
  db_Clear (tbl_CK);
*)

 // db_Clear (tbl_mem_Data);
//  db_Clear (tbl_version);

//  tbl_version.Filter:= 'enabled = 1';
 // tbl_version.Filtered:= true;


(*  Version:= 0;
  with tbl_version do begin
    First;
    while not EOF do
    begin

      if tbl_version.FieldByName('datetime').AsDateTime>Version then
        Version:= tbl_version.FieldByName('datetime').AsDateTime;

      Next;
    end;
  end;
*)


(*
  if TableNameList.IndexOf('_objects')>=0 then
    db_OpenQuery(qry_object_fields_Mdb, 'SELECT * FROM view_objects');
*)

end;


procedure TdmMDB1.EmptyTable1(aTableName: string);
begin
  db_DeleteRecords(ADOConnection1, aTableName);
end;



// TODO: Load_DBStructure111111
//// ---------------------------------------------------------------
//procedure TdmMDB.Load_DBStructure111111(aDBStructure: TDBStructure);
//// ---------------------------------------------------------------
//
//// ---------------------------------------------------------------
//  procedure Do_CK(aDataset: TDataSet);
//  // ---------------------------------------------------------------
//  var obj: TDBObject;
//    i: Integer;
//     sTableName: string;
//     oTable: TDBObject;
//
//  begin
//    i:=aDataset.RecordCount;
//
//    aDataset.First;
//
//    with aDataset do
//       while not EOF do
//     begin
//        sTableName :=FieldValues[FLD_TABLENAME];
//
//        oTable := aDBStructure.Tables.FindByName(sTableName);
//        Assert(Assigned(oTable), 'Value not assigned');
//
//        obj:=oTable.CK.AddItem;
//        obj.ObjectType := otCK;
//        obj.Name := FieldValues[FLD_Name];
//
//        obj.TableName :=FieldValues[FLD_TableName];
//      //  obj.TABLE_ID  := FieldValues[FLD_TABLE_ID];
//
//      //  obj.FK.PK_TABLENAME := FieldValues[FLD_PK_TABLENAME];
//      //  obj.FK.integrity_SQL := FieldValues[FLD_integrity_SQL];
//
//        obj.Text := FieldValues[FLD_CONTENT];
////          obj.Text := FieldValues[FLD_TEXT];
////          obj.SQL_create := FieldValues[FLD_CONTENT];
//        
////          obj.SQL_create := FieldValues[FLD_SQL_Create];
//
//        Next;
//     end;
//  end;
//
//  // ---------------------------------------------------------------
//  procedure Do_FK(aDataset: TDataSet);
//  // ---------------------------------------------------------------
//  var obj: TDBObject;
//    i: Integer;
//  begin
//    aDataset.First;
//
//    with aDataset do
//       while not EOF do
//     begin
//        obj:=aDBStructure.FK.AddItem;
//        obj.ObjectType := otFK;
//        obj.Name      := FieldValues[FLD_Name];
//        obj.TableName :=FieldValues[FLD_TableName];
//
//        obj.FK.PK_TABLENAME  := FieldValues[FLD_PK_TABLENAME];
//        obj.FK.integrity_SQL := FieldValues[FLD_integrity_SQL];
//
////          obj.SQL_create := FieldValues[FLD_SQL_Create];
//        obj.Text := FieldValues[FLD_CONTENT];
//
//        Next;
//     end;
//  end;
//
//
//  // ---------------------------------------------------------------
//  procedure Do_Views(aDataset: TDataSet);
//  // ---------------------------------------------------------------
//  var obj: TDBObject;
//    i: Integer;
//  begin
//    aDataset.First;
//
//    i:=aDataset.RecordCount;
//
//    with aDataset do
//       while not EOF do
//     begin
//        obj := aDBStructure.Views.AddItem;
//        obj.ObjectType := otView;
//        obj.Name    :=FieldValues[FLD_Name];
//      //  obj.TABLE_ID:=FieldValues[FLD_TABLE_ID];
//
//        obj.Text:=FieldValues[FLD_CONTENT];
//      //  obj.XTYPE   :=FieldValues[FLD_xtype];
//
//       Next;
//     end;
//  end;
//
//  // ---------------------------------------------------------------
//  procedure Do_StoredProcs(aDataset: TDataSet);
//  // ---------------------------------------------------------------
//  var obj: TDBObject;
//  begin
//    aDataset.First;
//
//    with aDataset do
//       while not EOF do
//     begin
//        obj := aDBStructure.StoredProcs.AddItem;
//        obj.ObjectType := otStoredPROC;
//        obj.Name    :=FieldValues[FLD_Name];
//      //  obj.TABLE_ID:=FieldValues[FLD_TABLE_ID];
//
//        obj.Text:=FieldValues[FLD_CONTENT];
//      //  obj.XTYPE   :=FieldValues[FLD_xtype];
//
//       Next;
//     end;
//  end;
//
//
//  // ---------------------------------------------------------------
//  procedure Do_tables(aDataset: TDataSet);
//  // ---------------------------------------------------------------
//  var obj: TDBObject;
//    i: Integer;
//  begin
//    i:=aDataset.RecordCount;
//
//    aDataset.First;
//
//    with aDataset do
//       while not EOF do
//     begin
//        obj := aDBStructure.Tables.AddItem;
//        obj.ObjectType := otTable;
//        obj.Name    :=FieldValues[FLD_Name];
//    //    obj.TABLE_ID:=FieldValues[FLD_TABLE_ID];
//      //  obj.XTYPE   :=FieldValues[FLD_xtype];
//
//       Next;
//     end;
//  end;
//
//
//  // ---------------------------------------------------------------
//  procedure Do_indexes(aDataset: TDataSet);
//  // ---------------------------------------------------------------
//  var obj: TDBObject;
//    bPrimaryKey: Boolean;
//     sTableName: string;
//     oTable: TDBObject;
//
//  //   oTable: TDBObject;
//  //  sTableName: string;
//  begin
//    aDataset.First;
//
//    with aDataset do
//       while not EOF do
//     begin
//        sTableName :=FieldValues[FLD_TABLENAME];
//
//        oTable := aDBStructure.Tables.FindByName(sTableName);
//        Assert(Assigned(oTable), 'Value not assigned');
//
//       bPrimaryKey:= FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean;
//
//       if bPrimaryKey then
//         obj:=oTable.PrimaryKeys.AddItem
//       else
//         obj:=oTable.Indexes1.AddItem;
//
//
//       obj.Name := FieldValues[FLD_NAME];
//       obj.ObjectType := otIndex;
//       obj.TableName :=FieldValues[FLD_TABLENAME];
//      // obj.SQL := FieldValues[FLD_TEXT];
//     //  obj.XTYPE := aDataset.FieldByName(FLD_xtype).AsString;
//     //  obj.TABLE_ID :=FieldValues[FLD_TABLE_ID];
//
//    //   obj.SQL_Create := sSQL_create;
////         obj.SQL_Create := FieldValues[FLD_SQL_create];
//       obj.Text := FieldValues[FLD_CONTENT];
//
//       obj.Index.IS_PRIMARY_KEY := FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean;
//       obj.Index.IS_UNIQUE_KEY  := FieldByName(FLD_IS_UNIQUE_KEY).AsBoolean;
//       obj.Index.INDEX_KEYS     := FieldByName(FLD_INDEX_KEYS).AsString;
//    //   obj.Index.IS_CLUSTERED   := FieldByName(FLD_IS_CLUSTERED).AsBoolean;
//
//      // obj.Index.FILL_FACTOR    := FieldByName(FLD_FILL_FACTOR).AsInteger;
//
//
//       Next;
//     end;
//  end;
//
//
//  // ---------------------------------------------------------------
//  procedure Do_TableFields(aDataset: TDataSet);
//  // ---------------------------------------------------------------
//  var obj: TDBTableField;
//     sTableName: string;
//     oTable: TDBObject;
//  begin
//    aDataset.First;
//
//    with aDataset do
//       while not EOF do
//     begin
//        sTableName :=FieldValues[FLD_TABLENAME];
//
//        oTable := aDBStructure.Tables.FindByName(sTableName);
//
//        if Assigned(oTable) then
//        begin
//          obj:=oTable.TableFields.AddItem;
//
//          obj.Name := FieldValues[FLD_NAME];
//
//          obj.TableName :=FieldValues[FLD_TABLENAME];
//          obj.TYPE_        :=FieldValues[FLD_TYPE];
//          obj.Size         :=FieldValues[FLD_Size];
//          obj.IsVariableLen     :=FieldValues[FLD_VARIABLE];
//        //  obj.USERTYPE     :=FieldValues[FLD_USERTYPE];
//          obj.AllowNULLS        :=FieldValues[FLD_NULLS];
//       //   obj.IsRowGuidCol :=FieldValues[FLD_IsRowGuidCol];
//
//          obj.IsPrimaryKey   := FieldValues[FLD_PrimaryKey];
//          obj.IsIdentity  := FieldByName(FLD_IDENTITY).AsBoolean;
//       //   obj.default_is_bound := FieldByName(FLD_default_is_bound).AsBoolean;
//        end;
//
//        Next;
//     end;
//  end;
//  // ---------------------------------------------------------------
//
//
//begin
////  aDBStructure.Clear;
//Do_tables(tbl_tables1);
//Do_TableFields(tbl_table_fields);
//Do_Views(tbl_views);
//Do_indexes(tbl_indexes);
//Do_StoredProcs(tbl_StoredProcs);
//Do_CK(tbl_ck);
//Do_FK(tbl_fk);
//
//end;


function TdmMDB1.TableExists1(aTableName: string): Boolean;
begin
  Result := TableNameList.IndexOf(aTableName)>=0;
end;

end.





