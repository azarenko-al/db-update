unit dm_VersionUpdate;

interface

uses
  Classes, Forms, ADODB, SysUtils,  Dialogs,

  u_DB,

  u_files,
  u_func,
//  u_geo,

  u_local_const,

//  u_geo_types,

  //u_progress_form,

  dm_MDB,

 // dm_Main_SQL,
   DB

  ;

type
  TdmVersionUpdate = class(TDataModule)
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    ADOTable2: TADOTable;
    procedure DataModuleCreate(Sender: TObject);
  private
// TODO: GetChecksum11
//  function GetChecksum11: Integer;

    function GetSQLAll(var aBeforeSQL, aAfterSQL: string): Boolean;

    procedure OpenDB;

    function GetSqlByName1(aName: string; var aSQL: string): boolean;

  public

  end;

var
  FdmVersionUpdate: TdmVersionUpdate;

function  dmVersionUpdate: TdmVersionUpdate;

//===================================================================
implementation  {$R *.dfm}
//===================================================================



(*const
  FLD_programmed_action_name = 'programmed_action_name';
*)


//--------------------------------------------------------------------
function dmVersionUpdate: TdmVersionUpdate;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmVersionUpdate) then
    FdmVersionUpdate:=TdmVersionUpdate.Create(Application);

  Result:= FdmVersionUpdate;
end;

//--------------------------------------------------------------------
procedure TdmVersionUpdate.DataModuleCreate(Sender: TObject);
//--------------------------------------------------------------------
begin
//  db_SetComponentADOConnection(Self, dmMain_SQL.AdoConnection1);
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection_MDB);

end;


// ---------------------------------------------------------------
function TdmVersionUpdate.GetSqlByName1(aName: string; var aSQL: string):
    boolean;
// ---------------------------------------------------------------
const
  DEF_SQL = 'SELECT * FROM ' + TBL_Version_updates + ' WHERE name=:name';
begin

(*
  if Eq(aName, DEF_RFP_to_LINK) then
  begin
    Result := FileToStr('C:\DB_Scripts\Rostelecom\_script_.sql', aSQL);
    Exit;
  end;

*)

  db_OpenQuery(ADOQuery1, DEF_SQL, [FLD_NAME, aName]);

  Result := not ADOQuery1.IsEmpty;

  if Result then
    aSQL := ADOQuery1.FieldByName('content').AsString
  else
    raise Exception.Create('function TdmVersionUpdate.GetSqlByName1:  '+ aName);
//    ShowMessage('function TdmVersionUpdate.GetSqlByName1:  '+ aName);
end;



procedure TdmVersionUpdate.OpenDB;
begin
//  db_TableOpen1(ADOTable1, 'version_updates')

  ADOTable1.Open;
end;

// ---------------------------------------------------------------
function TdmVersionUpdate.GetSQLAll(var aBeforeSQL, aAfterSQL: string): Boolean;
// ---------------------------------------------------------------
var
  I: Integer;
  is_run_before_all: Boolean;

  oAfterSQL: TStringList;
  oBeforeSQL: TStringList;
  s: string;
  sCaption: string;

  sCONTENT: string;

begin
  Result := False;

  oBeforeSQL:=TStringList.Create;
  oAfterSQL:=TStringList.Create;


  s:=TxtFileToStr('install_db.sql');
  Assert (s<>'');



  db_TableOpen1(ADOTable2, view_Version_updates_enabled);

//////  db_View(ADOTable2);

//  aBeforeSQL:= '';
//  aAfterSQL:= '';

    with ADOTable2 do
      while not EOF do
    begin
      sCaption:=FieldBYName(FLD_Caption).AsString;;
      sCONTENT:=FieldBYName(FLD_CONTENT).AsString;;

    //  aBeforeSQL := aBeforeSQL + sCONTENT + CRLF


      is_run_before_all:=FieldBYName('is_run_before_all').AsBoolean;

      if is_run_before_all then begin
        oBeforeSQL.Add('-------- ' + sCaption + '------------');
        oBeforeSQL.Add(sCONTENT)

      //   aBeforeSQL := aBeforeSQL + sCONTENT + CRLF
      end else begin
        oAfterSQL.Add('-------- ' + sCaption + '------------');
        oAfterSQL.Add(sCONTENT) ;
      end;
       //  aAfterSQL := aAfterSQL + sCONTENT + CRLF;

      Result := True;

      Next;
    end;

  ADOTable2.Close;


  aBeforeSQL:= oBeforeSQL.Text;
  aAfterSQL := oAfterSQL.Text;

end;



end.

