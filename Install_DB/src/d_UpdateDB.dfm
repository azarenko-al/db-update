object dlg_UpdateDB: Tdlg_UpdateDB
  Left = 1162
  Top = 164
  Caption = #1059#1089#1090#1072#1085#1086#1074#1082#1072' '#1073#1072#1079#1099' '#1076#1072#1085#1085#1099#1093' RPLS DB'
  ClientHeight = 685
  ClientWidth = 819
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 527
    Width = 819
    Height = 3
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 538
    ExplicitWidth = 827
  end
  object pn_Header: TPanel
    Left = 0
    Top = 0
    Width = 819
    Height = 60
    Align = alTop
    BevelOuter = bvLowered
    BorderWidth = 6
    Color = clInfoBk
    Constraints.MaxHeight = 60
    Constraints.MinHeight = 60
    TabOrder = 0
    Visible = False
    object lb_Action: TLabel
      Left = 8
      Top = 8
      Width = 44
      Height = 13
      Caption = 'lb_Action'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 629
    Width = 819
    Height = 56
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      819
      56)
    object Button2: TButton
      Left = 747
      Top = 8
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Caption = #1047#1072#1082#1088#1099#1090#1100
      TabOrder = 0
      OnClick = btn_CancelClick
    end
    object Button1: TButton
      Left = 5
      Top = 8
      Width = 87
      Height = 23
      Action = act_Modify
      TabOrder = 1
    end
    object Button3: TButton
      Left = 111
      Top = 8
      Width = 130
      Height = 23
      Action = act_Export_Objects
      TabOrder = 2
    end
    object Button6: TButton
      Left = 256
      Top = 8
      Width = 75
      Height = 25
      Action = act_Structure
      TabOrder = 3
    end
    object Button7: TButton
      Left = 344
      Top = 7
      Width = 65
      Height = 25
      Action = act_Versions
      TabOrder = 4
    end
    object Button4: TButton
      Left = 520
      Top = 5
      Width = 89
      Height = 25
      Caption = 'Auto_Update'
      TabOrder = 5
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 664
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Tdlg_UpdateDB_Hand'
      TabOrder = 6
      OnClick = Button5Click
    end
    object cb_Show_Scrpit: TCheckBox
      Left = 520
      Top = 34
      Width = 137
      Height = 17
      Caption = 'cb_Show_Scrpit'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object Button8: TButton
      Left = 416
      Top = 7
      Width = 81
      Height = 25
      Action = act_Versions_new
      TabOrder = 8
    end
  end
  object GroupBox1_Log: TGroupBox
    Left = 0
    Top = 530
    Width = 819
    Height = 80
    Align = alBottom
    Caption = ' '#1046#1091#1088#1085#1072#1083' '#1089#1086#1073#1099#1090#1080#1081' '
    TabOrder = 2
    object RichEdit1: TRichEdit
      Left = 2
      Top = 15
      Width = 815
      Height = 63
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 60
    Width = 819
    Height = 377
    ActivePage = TabSheet_Data
    Align = alTop
    TabOrder = 3
    OnChange = PageControl1Change
    object TabSheet_Connection: TTabSheet
      Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' '#1041#1044
      object gb_ConnectToSQL: TGroupBox
        Left = 0
        Top = 0
        Width = 811
        Height = 165
        Align = alTop
        Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' '#1041#1044
        Constraints.MinHeight = 165
        TabOrder = 0
      end
    end
    object TabSheet_Data: TTabSheet
      Caption = 'Data'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object PageControl2: TPageControl
        Left = 0
        Top = 25
        Width = 811
        Height = 172
        ActivePage = TabSheet4
        Align = alTop
        TabOrder = 1
        object TabSheet4: TTabSheet
          Caption = 'new'
          ImageIndex = 1
          object cxDBTreeList2_new: TcxDBTreeList
            Left = 0
            Top = 0
            Width = 803
            Height = 121
            Align = alTop
            Bands = <
              item
                Caption.AlignHorz = taCenter
              end>
            DataController.ParentField = 'parent_id'
            DataController.KeyField = 'id'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            LookAndFeel.Kind = lfFlat
            Navigator.Buttons.CustomButtons = <>
            OptionsBehavior.CellHints = True
            OptionsBehavior.GoToNextCellOnTab = True
            OptionsBehavior.AutoDragCopy = True
            OptionsBehavior.DragCollapse = False
            OptionsBehavior.ExpandOnIncSearch = True
            OptionsBehavior.ShowHourGlass = False
            OptionsBehavior.Sorting = False
            OptionsCustomizing.BandCustomizing = False
            OptionsCustomizing.BandVertSizing = False
            OptionsCustomizing.ColumnVertSizing = False
            OptionsData.CaseInsensitive = True
            OptionsData.Deleting = False
            OptionsSelection.HideFocusRect = False
            OptionsSelection.MultiSelect = True
            OptionsView.CellTextMaxLineCount = -1
            OptionsView.ShowEditButtons = ecsbFocused
            OptionsView.FixedSeparatorColor = clBlack
            OptionsView.GridLineColor = clBlack
            ParentColor = False
            ParentFont = False
            PopupMenu = PopupMenu2
            Preview.AutoHeight = False
            Preview.MaxLineCount = 2
            RootValue = -1
            TabOrder = 0
            object col_Name_MDB_: TcxDBTreeListColumn
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Caption.Text = #1060#1072#1081#1083' '#1041#1044' MS Access'
              DataBinding.FieldName = 'Name_MDB'
              Options.Editing = False
              Width = 145
              Position.ColIndex = 0
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_action_: TcxDBTreeListColumn
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Caption.Text = #1044#1077#1081#1089#1090#1074#1080#1077
              DataBinding.FieldName = 'action'
              Options.Editing = False
              Width = 100
              Position.ColIndex = 2
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object cxDBTreeListColumn5: TcxDBTreeListColumn
              PropertiesClassName = 'TcxMemoProperties'
              Caption.Text = #1054#1096#1080#1073#1082#1080
              DataBinding.FieldName = 'Error'
              Options.Editing = False
              Width = 100
              Position.ColIndex = 4
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_Name_SQL_: TcxDBTreeListColumn
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              Caption.Text = 'SQL-'#1089#1077#1088#1074#1077#1088' Onega'
              DataBinding.FieldName = 'Name_SQL'
              Options.Editing = False
              Width = 100
              Position.ColIndex = 5
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_Type_: TcxDBTreeListColumn
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.MaxLength = 0
              Properties.ReadOnly = True
              DataBinding.FieldName = 'Type'
              Options.Editing = False
              Width = 58
              Position.ColIndex = 1
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_update_SQL: TcxDBTreeListColumn
              PropertiesClassName = 'TcxMemoProperties'
              DataBinding.FieldName = 'update_SQL'
              Options.Editing = False
              Width = 100
              Position.ColIndex = 6
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_text_MDB: TcxDBTreeListColumn
              PropertiesClassName = 'TcxMemoProperties'
              DataBinding.FieldName = 'text_MDB'
              Options.Editing = False
              Width = 100
              Position.ColIndex = 7
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_text_SQL: TcxDBTreeListColumn
              PropertiesClassName = 'TcxMemoProperties'
              DataBinding.FieldName = 'text_SQL'
              Options.Editing = False
              Width = 100
              Position.ColIndex = 8
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_Enabled: TcxDBTreeListColumn
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.NullStyle = nssUnchecked
              DataBinding.FieldName = 'Enabled'
              Width = 51
              Position.ColIndex = 3
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_id: TcxDBTreeListColumn
              Visible = False
              DataBinding.FieldName = 'id'
              Width = 100
              Position.ColIndex = 13
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_parent_id: TcxDBTreeListColumn
              Visible = False
              DataBinding.FieldName = 'parent_id'
              Width = 100
              Position.ColIndex = 12
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_status: TcxDBTreeListColumn
              Caption.Text = 'status'
              DataBinding.FieldName = 'Owner'
              Width = 100
              Position.ColIndex = 9
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_priority: TcxDBTreeListColumn
              DataBinding.FieldName = 'priority'
              Width = 100
              Position.ColIndex = 10
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
            object col_is_updated: TcxDBTreeListColumn
              DataBinding.FieldName = 'is_updated'
              Width = 100
              Position.ColIndex = 11
              Position.RowIndex = 0
              Position.BandIndex = 0
              Summary.FooterSummaryItems = <>
              Summary.GroupFooterSummaryItems = <>
            end
          end
        end
        object TabSheet_Owners: TTabSheet
          Caption = 'Owners'
          ImageIndex = 2
          object cxGrid2: TcxGrid
            Left = 0
            Top = 0
            Width = 433
            Height = 144
            Align = alLeft
            TabOrder = 0
            LookAndFeel.Kind = lfFlat
            object cxGridDBTableView_Owner: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsBehavior.ImmediateEditor = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsView.GroupByBox = False
              object cxGridDBTableView_Ownername: TcxGridDBColumn
                DataBinding.FieldName = 'name'
                Width = 81
              end
              object cxGridDBTableView_Ownerchecked: TcxGridDBColumn
                DataBinding.FieldName = 'checked'
                Width = 84
              end
              object cxGridDBTableView_Ownerallow_DROP: TcxGridDBColumn
                DataBinding.FieldName = 'allow_DROP'
                Width = 66
              end
              object cxGridDBTableView_Ownerallow_ALTER: TcxGridDBColumn
                DataBinding.FieldName = 'allow_ALTER'
                Width = 67
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBTableView_Owner
            end
          end
          object DBGrid1: TDBGrid
            Left = 512
            Top = 0
            Width = 291
            Height = 144
            Align = alRight
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
      object pn_Connection: TPanel
        Left = 0
        Top = 0
        Width = 811
        Height = 25
        Align = alTop
        Alignment = taLeftJustify
        BevelOuter = bvLowered
        Color = 10930928
        TabOrder = 0
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 610
    Width = 819
    Height = 19
    Panels = <>
  end
  object ActionList1: TActionList
    OnUpdate = ActionList1Update
    Left = 256
    Top = 8
    object act_Ok: TAction
      Category = 'Main'
      Caption = #1057#1086#1079#1076#1072#1090#1100
    end
    object act_Cancel: TAction
      Category = 'Main'
      Caption = #1054#1090#1084#1077#1085#1072
      OnExecute = _Action
    end
    object act_Compare: TAction
      Caption = #1057#1088#1072#1074#1085#1080#1090#1100
      OnExecute = _Action
    end
    object act_Modify: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1041#1044
      OnExecute = _Action
    end
    object act_Run_Dict_Import: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1086#1074
      OnExecute = _Action
    end
    object act_UpdateViews: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1087#1088#1077#1076#1089#1090#1072#1074#1083#1077#1085#1080#1103
      OnExecute = _Action
    end
    object act_UpdateVer: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1074#1077#1088#1089#1080#1102
      OnExecute = _Action
    end
    object act_UpdateObjFields: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1086#1090#1086#1073#1088#1072#1078#1072#1077#1084#1099#1077' '#1087#1086#1083#1103
      OnExecute = _Action
    end
    object act_ShowLog: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1083#1086#1075' '#1086#1096#1080#1073#1086#1082
      OnExecute = _Action
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    UseRegistry = True
    StoredValues = <>
    Left = 328
    Top = 8
  end
  object ActionList2: TActionList
    Left = 328
    Top = 464
    object act_Export_Objects: TAction
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1086#1073#1098#1077#1082#1090#1086#1074
      OnExecute = act_Export_ObjectsExecute
    end
    object act_Exec_record: TAction
      Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
      ShortCut = 16471
      OnExecute = act_Exec_recordExecute
    end
    object act_Compare_SQL: TAction
      Caption = #1057#1088#1072#1074#1085#1080#1090#1100' SQL'
      ShortCut = 16465
      OnExecute = act_Exec_recordExecute
    end
    object act_Show_Executed_script: TAction
      Caption = 'Show_Executed_script'
      OnExecute = act_Exec_recordExecute
    end
    object act_Versions: TAction
      Caption = 'Versions'
      OnExecute = act_Exec_recordExecute
    end
    object act_Check_all: TAction
      Caption = 'Check_all'
      OnExecute = act_Exec_recordExecute
    end
    object act_UnCheck_all: TAction
      Caption = 'UnCheck_all'
      OnExecute = act_Exec_recordExecute
    end
    object act_Show_checked_script: TAction
      Caption = 'Show_checked_script'
      OnExecute = act_Exec_recordExecute
    end
    object act_Structure: TAction
      Caption = 'Structure'
      OnExecute = act_Exec_recordExecute
    end
    object act_Versions_new: TAction
      Caption = 'Versions_new'
      OnExecute = act_Exec_recordExecute
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 240
    Top = 464
    object MenuItem1: TMenuItem
      Action = act_Exec_record
    end
    object MenuItem2: TMenuItem
      Action = act_Compare_SQL
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object N1: TMenuItem
      Action = act_Show_Executed_script
    end
    object actCheckall1: TMenuItem
      Action = act_Check_all
    end
    object ScriptCheckedToNotepad1: TMenuItem
      Action = act_UnCheck_all
    end
    object ScriptToNotepad1: TMenuItem
      Action = act_Show_checked_script
    end
    object N3: TMenuItem
      Caption = '-'
    end
  end
end
