unit d_Start;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs,
  ExtCtrls,

  d_Wizard,


  d_NewDB,
  d_UpdateDB,
  d_UpdateDB_FromSQL, StdCtrls, cxPropertiesStore, rxPlacemnt, ActnList,
  cxLookAndFeels, cxClasses, System.Actions;

type
  Tdlg_Start = class(Tdlg_Wizard)
    rg_NextAction: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
  private
    procedure DoProceedException(Sender: TObject; E: Exception);
  end;

var
  dlg_Start: Tdlg_Start;

//==================================================================
implementation {$R *.DFM}
//==================================================================

// -------------------------------------------------------------------
procedure Tdlg_Start.FormCreate(Sender: TObject);
// -------------------------------------------------------------------
var
  sMode: string;
begin
  inherited;

 // dmCustomErrorLog.CreateCustomLog('ErrorLog\InstallDB_Error_Log.log');

 // gl_Reg.InitRegistry1('Software\Onega\Install_DB\');

  Caption:= '������ ��������� �� RPLS';
  SetActionName('�������� ��������');

  Application.OnException:= DoProceedException;

  sMode:= ParamStr(1);

  if sMode<>'' then
  begin
    Hide;
    Tdlg_UpdateDB.CreateForm1(sMode);
    Show;
  end;
end;

// -------------------------------------------------------------------
procedure Tdlg_Start.act_OkExecute(Sender: TObject);
// -------------------------------------------------------------------
//var
//  iInd: Integer;
begin
 // iInd:= ;

  Hide;

  case rg_NextAction.ItemIndex of
//    0: TDlg_CustomLogin.ShowModalForm();

    0: Tdlg_NewDB.CreateForm();
    1: Tdlg_UpdateDB.CreateForm1();
    2: Tdlg_UpdateDB_FromSQL.CreateForm();

  ////////  4: if ConfirmDlg('��������� ������ ������������?') then
    /////////     RunApp (GetParentFileDir(GetApplicationDir) + 'bin\GuidesImport.exe', AnsiQuotedStr (GetApplicationDir+ 'Guides.mdb', '"'));
  end;

  Show;
end;

// -------------------------------------------------------------------
procedure Tdlg_Start.DoProceedException(Sender: TObject; E: Exception);
// -------------------------------------------------------------------
begin
  ShowMessage(Format('����������� ������, �����: %s', [E.Message]));
end;


end.
