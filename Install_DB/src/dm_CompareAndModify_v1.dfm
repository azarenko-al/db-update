object dmCompareAndModify_v1: TdmCompareAndModify_v1
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1263
  Top = 368
  Height = 341
  Width = 387
  object ds_Data_Compare: TDataSource
    DataSet = t_Data_Compare
    Left = 64
    Top = 80
  end
  object t_Data_Compare: TADOTable
    CursorType = ctStatic
    TableName = '_Compare'
    Left = 64
    Top = 24
  end
  object ADOTable1: TADOTable
    TableName = '_Compare'
    Left = 288
    Top = 96
  end
  object ActionList1: TActionList
    Left = 288
    Top = 32
    object act_Script_ToNotepad: TAction
      Caption = 'Script To Notepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_ScriptNoDrop_ToNotepad: TAction
      Caption = 'ScriptNoDrop_ToNotepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_ScriptChecked_ToNotepad: TAction
      Caption = 'ScriptCheckedToNotepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_Check_All: TAction
      Caption = 'Check_All'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_ExecutedScript: TAction
      Caption = 'Executed Script'
      OnExecute = act_Script_ToNotepadExecute
    end
    object Action1: TAction
      Caption = 'Action1'
    end
    object act_Script_new_ToNotepad: TAction
      Caption = 'Script_new_ToNotepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_Script_new_exec: TAction
      Caption = 'Script_new_exec'
      OnExecute = act_Script_ToNotepadExecute
    end
  end
  object ADOQuery1: TADOQuery
    Parameters = <>
    Left = 64
    Top = 160
  end
  object q_Tables: TADOQuery
    Parameters = <>
    Left = 184
    Top = 160
  end
end
