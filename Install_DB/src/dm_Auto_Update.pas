unit dm_Auto_Update;

interface

 {$DEFINE test}

uses
  Forms, SysUtils, Classes, Dialogs, Controls,

//  dm_MDB_to_SQL_install,


  u_func,
  u_files,
  u_dlg,

  u_cx_vgrid,

  u_local_const,

  dm_MDB,                                                       
  dm_ExportStructure,
  dm_Main_SQL,
  dm_ObjectFields,
  dm_VersionUpdate,
  dm_CompareAndModify_new;



type
  TdmAuto_Update = class(TDataModule)
    SaveDialog1: TSaveDialog;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private
    FLogList : TstringList;

    procedure Log(aMsg: string);

  public
    Params: record
              IsTestMode1 : Boolean;
            end;

    procedure Execute;
    class procedure Init;

  end;



var
  dmAuto_Update: TdmAuto_Update;

implementation


{$R *.dfm}


class procedure TdmAuto_Update.Init;
begin
  if not Assigned(dmAuto_Update) then
    dmAuto_Update := TdmAuto_Update.Create(Application);
end;


procedure TdmAuto_Update.DataModuleCreate(Sender: TObject);
begin
  FLogList:=TStringList.Create;
end;


procedure TdmAuto_Update.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FLogList);
end;


// ---------------------------------------------------------------
procedure TdmAuto_Update.Execute;
// ---------------------------------------------------------------
var
  iRes: Integer;
  s: string;

  sError: string;
  sFileName: string;
//  sBeforeSQL, sAfterSQL: string;
  sSQL: string;
  sUpdateSQL: string;
begin
 /// Check_RFP_upgrade();

 // Result:=False;

  TdmCompareAndModify_new.Init;

  TdmMDB.Init1;
 // dmMDB.OpenDB1;


  TdmExportStructure.Init;
//  dmExportStructure_2005.Params.Project_name1:='link';


//  dmExportStructure.Load_DBStructure(dmMDB.ADOConnection1, dmCompareAndModify.DBStructure_MDB);
  dmExportStructure.Load_DBStructure(dmCompareAndModify_new.DBStructure_MDB, 'local');

//  TdmExportStructure_2005.Init;
  dmExportStructure.ExecDlg('sql_', dmCompareAndModify_new.DBStructure_SQL);
  FreeAndNil(dmExportStructure);


 // dmCompareAndModify_new.Params.IsCreateOnly := True;
//  dmCompareAndModify_new.Params.
  dmCompareAndModify_new.ExecDlg;


//  dmVersionUpdate.GetSQLAll(sBeforeSQL, sAfterSQL);

  sUpdateSQL:=dmCompareAndModify_new.Script.Text;

  {.$DEFINE test}

//
//  {$IFDEF test}
//    sSQL:=sUpdateSQL;
//  {$ELSE}
//    sSQL:=sBeforeSQL+ sUpdateSQL + sAfterSQL;
//  {$ENDIF}


{
if exists (SELECT * FROM sys.databases  where name='kpt' and compatibility_level <110 )


--SELECT @level=compatibility_level FROM sys.databases
--where name='kpt'

--if @level<110
  ALTER DATABASE kpt
    SET COMPATIBILITY_LEVEL = 110 -- 130 | 120 | 110 | 100 | 90
}


 s:=dmMain_SQL.LoginRec.DataBase;


//   sSQL:=Format('if exists (SELECT * FROM sys.databases  where name=''%s'' and compatibility_level <100)' +
//            'EXEC sp_dbcmptlevel %s, 90',
  //         // '   ALTER DATABASE %s  SET COMPATIBILITY_LEVEL = 100',
    //        [dmMain_SQL.LoginRec.DataBase, dmMain_SQL.LoginRec.DataBase]   );



//   sSQL:=sSQL + sBeforeSQL + sUpdateSQL + sAfterSQL;

  sFileName:=ExtractFilePath (Application.ExeName) + 'install_db.service.sql';

  Assert(FileExists(sFileName));

  sSQL:=TxtFileToStr (sFileName);


   sSQL:=sSQL + sUpdateSQL; // + sAfterSQL;


//   StrToTxtFile(sSQL, 'd:\1111.sql');

//   TxtFileToStr (sFileName);



   ////////////
 //  sSQL:=sBeforeSQL+ sUpdateSQL;



 // sSQL:=sBeforeSQL+ sUpdateSQL + sAfterSQL;
//  sSQL:=sBeforeSQL + sUpdateSQL + sAfterSQL;



   {.$DEFINE test}
   {$IFDEF test}

   if params.IsTestMode1 then
   begin
    repeat
        iRes:=MyMessageDialog('�������� ����?', mtConfirmation,
                   [mbYes,mbNo,mbRetry], ['��','���','C�����...']);

        if iRes=mrRetry then
        begin
          SaveDialog1.FileName:='script.sql';

          if SaveDialog1.Execute then
             StrToTxtFile(sSQL, SaveDialog1.FileName);

   //       ShellExec_Notepad_temp(sSQL, '_main.sql');

  //
  //        {$IFDEF test}
  //        ShellExec_Notepad_temp(sUpdateSQL, '_Update.sql');
  //        {$ELSE}
  //        ShellExec_Notepad_temp(sBeforeSQL, '_Before.sql');
  //        ShellExec_Notepad_temp(sUpdateSQL, '_Update.sql');
  //        ShellExec_Notepad_temp(sAfterSQL,  '_After.sql');
  //
  //        {$ENDIF}

        end;

      until iRes in [mrYes,mrNo] ;

      if iRes=mrYes then
        dmMain_SQL.ExecScriptSQL_with_GO1(sSQL)
      else
        Exit;


   end else
    dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);


  {$ELSE}
//    dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);
  {$ENDIF}



  //  dmMain_SQL.ExecScriptSQL_with_GO(s);

  // ---------------------------------------
 // if ConfirmDlg('��������� ���������� ��������?') then
  // ---------------------------------------
 // begin
  TdmObjectFields.Init;
  dmObjectFields.Exec_Progress;
  FreeAndNil(dmObjectFields);
 // end;


{
  TdmMDB_to_SQL_install.Init;
  dmMDB_to_SQL_install.Exec;
  FreeAndNil(dmMDB_to_SQL_install);

}
 // Result:=True;

end;



procedure TdmAuto_Update.Log(aMsg: string);
begin
  FLogList.Add(aMsg);
end;


end.


{
if exists (SELECT * FROM sys.databases  where name='kpt' and compatibility_level <110 )


--SELECT @level=compatibility_level FROM sys.databases
--where name='kpt'

--if @level<110
  ALTER DATABASE kpt
    SET COMPATIBILITY_LEVEL = 110 --{ 130 | 120 | 110 | 100 | 90 }



}
