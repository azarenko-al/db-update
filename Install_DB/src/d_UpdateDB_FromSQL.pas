unit d_UpdateDB_FromSQL;

interface

uses
  SysUtils, Classes, Controls, Forms,ActnList, StdCtrls, ExtCtrls,
  d_Wizard,

  u_func,

  fra_DB_Login,

 // fr_Db_Connect_Params,

  rxToolEdit, Mask, cxPropertiesStore, rxPlacemnt, cxLookAndFeels, cxClasses,
  System.Actions;

type
  Tdlg_UpdateDB_FromSQL = class(Tdlg_Wizard)
    pn_Connect: TPanel;
    gb_ConnectToSQL: TGroupBox;
    gb_Script: TGroupBox;
    pn_Connect_Params: TPanel;
    btn_Hand: TButton;
    FilenameEdit1: TFilenameEdit;
    procedure FormDestroy(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure act_OkExecute(Sender: TObject);
    procedure ed_FilePropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure btn_HandClick(Sender: TObject);
  private
    Fframe_DB_Login: Tframe_DB_Login;

//    Ffr_Db_Connect_Params: Tframe_Db_Connect_Params;
  public
    class function CreateForm: boolean;
  end;

//
//var
//  dlg_UpdateDB_FromSQL: Tdlg_UpdateDB_FromSQL;


//==============================================================
implementation {$R *.DFM}
//==============================================================


class function Tdlg_UpdateDB_FromSQL.CreateForm: boolean;
begin
  with Tdlg_UpdateDB_FromSQL.Create(Application) do begin
    Result:= (ShowModal=mrOk);
  end;
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_FromSQL.FormCreate(Sender: TObject);
//--------------------------------------------------------------
var
  sDir:  string;
begin
  Caption:= '������ ��������� �� RPLS';
  Caption :=Caption+ GetAppVersionStr();


  SetActionName('���������� ������������ �� (������������ SQL-������ (.sql))');
  btn_Cancel.Caption:= '�������';

  pn_Connect.Align:= alClient;


 // FIniFileName :=ChangeFileExt(Application.ExeName, '.ini');


  CreateChildForm_(Tframe_DB_Login, Fframe_DB_Login, gb_ConnectToSQL);

  Fframe_DB_Login.IniFileName := ChangeFileExt(Application.ExeName, '.ini');
  Fframe_DB_Login.LoadFromIni();


(*
  Ffr_Db_Connect_Params:= Tframe_Db_Connect_Params.CreateChildForm(Self, pn_Connect_Params);
  Ffr_Db_Connect_Params.IsShowImage:= false;
*)

//  Ffr_Db_Connect_Params.pn_Connect.Visible:= false;
//  Ffr_Db_Connect_Params.IsShowBorder:= false;

 // gb_ConnectToSQL.Height:= Ffr_Db_Connect_Params.CurHeight-10;

 // ed_File.Text:= gl_Reg.RegIni.ReadString('', 'ed_File.Text', '');

  inherited;
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_FromSQL.FormDestroy(Sender: TObject);
//--------------------------------------------------------------
begin
 // gl_Reg.RegIni.WriteString('', 'ed_File.Text', ed_File.Text);

 // Ffr_Db_Connect_Params.Free;

  inherited;
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_FromSQL.act_OkExecute(Sender: TObject);
//--------------------------------------------------------------
var
  oList, oStrFile1: TStringList;
  sDir, sSql:  string;
  i: Integer;
  bUpdated: boolean;
begin
(*

  ModalResult:= mrNone;

  bUpdated:= false;

  with Ffr_Db_Connect_Params do
  begin
    if dmMain.TestConnect (Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then
    begin
      dmMain.SaveConnectRecToReg (Server, DefaultDB, User, Password, UseWinAuth);
      if not dmMain.Open then
        ErrorDlg(Format(MSG_CONNECT_ERROR_F, [dmMain.ErrorMsg]));
    end else begin
      ErrorDlg(Format(MSG_CONNECT_ERROR_F, [dmMain.ErrorMsg]));
      exit;
    end;
  end;

  sDir:= ed_File.Text;

  if not FileExists(sDir) then begin
    ErrorDlg ('���� �� ����������');
    exit;
  end;
*)

(*
  if not dmMain.TestForAdminWrites then begin
    ErrorDlg('������������ ���� ��� ���������� �������� � ��');
    exit;
  end;
*)

(*
  oList:=     TStringList.Create;
  oStrFile1:= TStringList.Create;

  CursorSql;

  oList.LoadFromFile(sDir);

  oStrFile1.Clear;

  for i := 0 to oList.Count -1 do
  begin
    if (Pos('GO', UpperCase(oList[i])) = 1) then
    begin
      if oStrFile1.Text='' then begin
        oStrFile1.Clear;
        continue;
      end;

      if not gl_DB.ExecCommandSimple(oStrFile1.Text) then begin
        ErrorDlg('���������� �� ���������. ���������� � �������������. ������: '+gl_DB.ErrorMsg);
        CursorDefault;
        exit;
      end
      else
        bUpdated:= true;

      oStrFile1.Clear;
      Continue;
    end;

    oStrFile1.Add(oList[i]);
  end;

  if oStrFile1.Text<>'' then
    if not gl_DB.ExecCommandSimple(oStrFile1.Text) then begin
      ErrorDlg('���������� �� ���������. ���������� � �������������. ������: '+gl_DB.ErrorMsg);
      CursorDefault;
      exit;
    end
    else
      bUpdated:= true;

  CursorDefault;

  if bUpdated then
    MsgDlg('���������� ���������')
  else
    MsgDlg('���������� �� ���������. ������� ���������� �� ����������� �� ����');

  ModalResult:= mrOk;

  oList.Free;
  oStrFile1.Free;

*)
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_FromSQL.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//--------------------------------------------------------------
begin
  act_Ok.Enabled:= FilenameEdit1.FileName <> '';
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_FromSQL.ed_FilePropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
//--------------------------------------------------------------
var
  sFileName: string;
begin
(*
  sFileName:= ShowOpenDialog (Self, '�������� �����','onega.sql','SQL files|*.sql', ed_File.Text);
  if sFileName <> '' then
    ed_File.Text:= sFileName;
*)
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_FromSQL.btn_HandClick(Sender: TObject);
//--------------------------------------------------------------
begin
(*  inherited;

  with Ffr_Db_Connect_Params do
  begin
    if dmMain.TestConnect (Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then
    begin
      dmMain.SaveConnectRecToReg (Server, DefaultDB, User, Password, UseWinAuth);
      if not dmMain.Open then
        ErrorDlg(Format(MSG_CONNECT_ERROR_F, [dmMain.ErrorMsg]));
    end else begin
      ErrorDlg(Format(MSG_CONNECT_ERROR_F, [dmMain.ErrorMsg]));
      exit;
    end;
  end;

  Tdlg_UpdateDB_Hand.CreateForm();
*)

end;

end.
