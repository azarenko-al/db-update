unit d_Install_;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, GSTabs,
  StdCtrls, ExtCtrls, rxPlacemnt, ActnList, Registry, Db, ADODB, Grids, DBGrids, Menus,

  dxEdLib, dxCntner, dxEditor, dxExEdtr, dxTL, dxDBCtrl, dxDBTL, dxInspRw, dxInspct,

  cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxButtonEdit,

  dm_Install_,

  dm_Main,

  dm_CustomerSQLServToMem,
  dm_MDBConnect,
  dm_CompareAndModify,

  fr_DB_Connect_Params,

  u_const,
  u_const_db,

  u_func,
  u_func_dlg,
  u_func_files,
  u_func_db,
  u_func_reg,
  u_func_dx

  ;

type
  Tdlg_Install_ = class(TForm)
    pn_Header: TPanel;
    lb_Action: TLabel;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormStorage1: TFormStorage;
    act_Compare: TAction;
    act_Modify: TAction;
    GSPages1: TGSPages;
    page_ExistDB: TGSPage;
    dx_Compare_Results: TdxDBTreeList;
    col_Access: TdxDBTreeListColumn;
    col_Action: TdxDBTreeListColumn;
    col_tree_id: TdxDBTreeListColumn;
    col_Onega: TdxDBTreeListColumn;
    col_parent_id: TdxDBTreeListColumn;
    GroupBox1: TGroupBox;
    Page_NewBD1: TGSPage;
    gb_Database: TGroupBox;
    Label1: TLabel;
    ed_MSAccessFileName: TcxButtonEdit;
    Label2: TLabel;
    ed_NewDatabase: TdxEdit;
    Pn3: TPanel;
    page_Setup: TGSPage;
    pn_Main: TPanel;
    gb_Options: TGroupBox;
    cb_RenewObjectFields: TCheckBox;
    Bevel1: TBevel;
    Panel1: TPanel;
    Button3: TButton;
    Button4: TButton;
    Bevel3: TBevel;
    Panel2: TPanel;
    Button5: TButton;
    Button1: TButton;
    act_Run_Dict_Import: TAction;
    PopupMenu1: TPopupMenu;
    act_Create_Object: TAction;
    actCreateObject1: TMenuItem;
    col_Type: TdxDBTreeListColumn;
    procedure ed_Server1ButtonClick(Sender: TObject; AbsoluteIndex: Integer);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_CancelClick(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
    procedure act_Run_Dict_ImportExecute(Sender: TObject);
    procedure _Action(Sender: TObject);
  private
/////////////    FIsShowUpdateObjectsCheckbox: boolean;

    Fframe_DB_Connect_Params: Tframe_DB_Connect_Params;

    procedure ExecuteNewDB();
    procedure ExecuteUpdateDB (aIsModifyFields: boolean);
  end;

var
  dlg_Install_: Tdlg_Install_;

//=====================================================================
implementation {$R *.DFM}
//=====================================================================


//--------------------------------------------------------------
procedure Tdlg_Install_.FormCreate(Sender: TObject);
//--------------------------------------------------------------
var
  sCurAppPath: string;
begin
  Caption:= '��������� ���� ������ RPLS DB';
  Height:=  370;

///////////  gl_ErrorLog.ShowLog;

  Pn3.Caption:= '';

  Fframe_DB_Connect_Params:= Tframe_DB_Connect_Params.CreateChildForm (Self, page_Setup);

  dx_Compare_Results.Align:= alClient;
  GSPages1.Align:=           alClient;
  pn_Main.Align:=            alClient;

  GSPages1.ActivePageIndex:= 0;

  act_Create_Object.Caption:= '������� ������';

  sCurAppPath:= ExtractFilePath(Application.ExeName);
  sCurAppPath:= IncludeTrailingBackslash(sCurAppPath)+ 'onega_tables.mdb';

///////  FIsShowUpdateObjectsCheckbox:= true; //false;

////////  gb_Options.Visible:= FIsShowUpdateObjectsCheckbox;

  if FileExists (sCurAppPath) then
    ed_MSAccessFileName.Text:= sCurAppPath;
end;


//----------------------------------------------------------------------
procedure Tdlg_Install_.ed_Server1ButtonClick(Sender: TObject;  AbsoluteIndex: Integer);
//----------------------------------------------------------------------
begin
  if Sender = ed_MSAccessFileName then
  begin
    with TOpenDialog.Create(Application) do
    begin
      Filter    :='(*.mdb)|*.mdb';
      FileName  :=ed_MSAccessFileName.Text;
      InitialDir:=ExtractFileDir(FileName);

      if Execute then
        ed_MSAccessFileName.Text:=FileName;

      Free;
    end;
  end;
end;

//----------------------------------------------------------------------
procedure Tdlg_Install_.FormClose(Sender: TObject; var Action: TCloseAction);
//----------------------------------------------------------------------
begin
  Action:= caFree;
end;

//----------------------------------------------------------------------
procedure Tdlg_Install_.btn_CancelClick(Sender: TObject);
//----------------------------------------------------------------------
begin
  Close;
end;

//----------------------------------------------------------------------
procedure Tdlg_Install_.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//----------------------------------------------------------------------
var
  sType: string;
begin
  case GSPages1.ActivePageIndex of
    0: begin
      act_Compare.Enabled := ed_MSAccessFileName.Text <> '';
      act_Modify.Enabled  := act_Compare.Enabled;
      act_Ok.Enabled      := false;
    end;
    1: begin
      act_Compare.Enabled := false;
      act_Modify.Enabled  := false;
      act_Ok.Enabled      := ed_NewDatabase.Text <> '';
    end;
    2: begin
      act_Compare.Enabled := false;
      act_Modify.Enabled  := false;
      act_Ok.Enabled      := false;
    end;
  end;

  act_Create_Object.Enabled := false;

  if GSPages1.ActivePageIndex = 0 then
    if Assigned (dx_Compare_Results.FocusedNode) then
    begin
      sType:= dx_Compare_Results.FocusedNode.Values[col_type.Index];
      act_Create_Object.Enabled := (sType <> '');
    end;
end;


//----------------------------------------------------------------------
procedure Tdlg_Install_.ExecuteNewDB();
//----------------------------------------------------------------------
begin
  with Fframe_DB_Connect_Params do
    if not dmMain.TestConnect(Server, User, Password) then
      exit;

  with Fframe_DB_Connect_Params do
  begin
    dmMain.SaveConnectRecToReg(Server, ed_NewDatabase.Text, User, Password);
    dmMain.Add_ODBC_Source(Server, ed_NewDatabase.Text, User);
  end;

  dmMain.Open;

  if dmMain.ADOConnection.Connected then
  begin
    if (MessageDlg ('��������! ���� ������ � ����� ������ ��� ����������. '+//#10#13+
                    '��� ������ ����� ����������. ����������?',
                    mtWarning, [mbYes,mbNo], 0) = mrNo) then
      Exit
    else
      dmInstall.Params.CreateNew:= False;
  end
  else
    dmInstall.Params.CreateNew:= True;

  with Fframe_DB_Connect_Params do
    dmMain.TestConnect(Server, User, Password);

  dmInstall.Params.DB_Name := ed_NewDatabase.Text;

  dmInstall.ExecuteDlg('��������� ���� ������...');
end;


//----------------------------------------------------------------------
procedure Tdlg_Install_.act_OkExecute(Sender: TObject);
//----------------------------------------------------------------------
begin
  //----------------------------
  if Sender = act_Ok then
  begin
    if GSPages1.ActivePageIndex=Page_NewBD1.PageIndex then
      ExecuteNewDB() else
  end else

  //----------------------------
  if Sender = act_Compare then
    ExecuteUpdateDB (false)
  else

  //----------------------------
  if Sender = act_Modify then
    ExecuteUpdateDB (true);

end;


//----------------------------------------------------------------------
procedure Tdlg_Install_.ExecuteUpdateDB (aIsModifyFields: boolean);
//----------------------------------------------------------------------
var
  I: Integer;
begin
  dmCompareAndModify.IsModifyFields:=       aIsModifyFields;

(*  if FIsShowUpdateObjectsCheckbox then
    dmCompareAndModify.IsModifyObjectFields:= cb_RenewObjectFields.Checked
  else
    dmCompareAndModify.IsModifyObjectFields:= true;*)

  dmMain.Open;

  if not dmMain.AdoConnection.Connected then
      exit;

  with dmMDBConnect.Params do  begin
    MDBFileName  := ed_MSAccessFileName.Text;
  end;

  dmCustomerSQLServToMem.ConnectClienSQLServ;

  if not dmMDBConnect.Connect then
  begin
    ErrorDlg('������ �������� ����� MS Access');
    exit;
  end;

  Screen.Cursor:= crHourGlass;

  dmCustomerSQLServToMem.ClientSQLServ_to_Mem;  // ���������� �����

  dmCompareAndModify.ExecuteDlg('');

  Screen.Cursor:= crDefault;

  // ---------------------------
  dx_Compare_Results.FullCollapse;

  dx_Compare_Results.DataSource.DataSet.DisableControls;

  for i:=0 to dx_Compare_Results.Count-1 do
    if (dx_Compare_Results.DataSource.DataSet[FLD_DXTREE_ID] = -1) or
       (dx_Compare_Results.DataSource.DataSet[FLD_DXTREE_ID] = 1)  or
       (dx_Compare_Results.DataSource.DataSet[FLD_DXTREE_ID] = 2)  or
       (dx_Compare_Results.DataSource.DataSet[FLD_DXTREE_ID] = 3)  or
       (dx_Compare_Results.DataSource.DataSet[FLD_DXTREE_ID] = 4)
    then
      dx_Compare_Results.Items[i].Expand(false);

  dx_Compare_Results.DataSource.DataSet.EnableControls;
  // ---------------------------
end;

//----------------------------------------------------------------------
procedure Tdlg_Install_.act_Run_Dict_ImportExecute(Sender: TObject);
//----------------------------------------------------------------------
begin
  if ConfirmDlg('��������� ������ ������������?') then
    RunApp (GetParentFileDir(GetApplicationDir) + 'bin\GuidesImport.exe', AnsiQuotedStr (GetApplicationDir+ 'Guides.mdb', '"'));
end;

//----------------------------------------------------------------------
procedure Tdlg_Install_._Action(Sender: TObject);
//----------------------------------------------------------------------
begin
  if Sender = act_Create_Object then
  begin
///////    db_View(mem_Data);
    dmCompareAndModify.CreateObject; //(dx_Compare_Results.FocusedNode.Values[col_tree_id.Index]);
  end else
end;


end.
