inherited dlg_UpdateDB_FromSQL: Tdlg_UpdateDB_FromSQL
  Left = 776
  Top = 361
  Caption = 'dlg_UpdateDB_FromSQL'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    ExplicitTop = 401
    ExplicitWidth = 519
    inherited Bevel1: TBevel
      Width = 519
      ExplicitWidth = 519
    end
    inherited Panel3: TPanel
      Left = 340
      ExplicitLeft = 340
      inherited btn_Ok: TButton
        Left = 356
        ExplicitLeft = 356
      end
      inherited btn_Cancel: TButton
        Left = 440
        ExplicitLeft = 440
      end
    end
    object btn_Hand: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 23
      Caption = #1042#1088#1091#1095#1085#1091#1102
      TabOrder = 1
      OnClick = btn_HandClick
    end
  end
  inherited pn_Top_: TPanel
    ExplicitWidth = 519
    inherited Bevel2: TBevel
      Width = 519
      ExplicitWidth = 519
    end
    inherited pn_Header: TPanel
      Width = 519
      ExplicitWidth = 519
    end
  end
  object pn_Connect: TPanel [2]
    Left = 0
    Top = 60
    Width = 500
    Height = 285
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 2
    ExplicitWidth = 519
    object gb_ConnectToSQL: TGroupBox
      Left = 5
      Top = 5
      Width = 490
      Height = 194
      Align = alTop
      Caption = #1055#1086#1076#1082#1083#1102#1095#1077#1085#1080#1077' '#1082' '#1041#1044' RPLS'
      TabOrder = 0
      ExplicitWidth = 509
      object pn_Connect_Params: TPanel
        Left = 2
        Top = 15
        Width = 505
        Height = 177
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 5
        TabOrder = 0
      end
    end
    object gb_Script: TGroupBox
      Left = 5
      Top = 199
      Width = 490
      Height = 41
      Align = alTop
      Caption = #1056#1072#1089#1087#1086#1083#1086#1078#1077#1085#1080#1077' SQL-'#1089#1082#1088#1080#1087#1090#1072' (.sql)'
      TabOrder = 1
      ExplicitWidth = 509
      object FilenameEdit1: TFilenameEdit
        Left = 2
        Top = 15
        Width = 486
        Height = 21
        Align = alTop
        DefaultExt = '*.mdb'
        Filter = 'MDB files (*.mdb)|*.mdb'
        NumGlyphs = 1
        TabOrder = 0
        Text = ''
        ExplicitWidth = 505
      end
    end
  end
  inherited ActionList1: TActionList
    OnUpdate = ActionList1Update
  end
end
