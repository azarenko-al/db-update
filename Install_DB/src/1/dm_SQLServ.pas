unit dm_SQLServ;

interface

uses
  Classes, Forms, 
  ADODB, 

  dm_MDB,


  
  

  

  dm_ExportStructure,
  

  
  

  
  
  u_DB, DB

  ;

type
  TdmSQLServ1111 = class(TDataModule)
    qry_Object_Fields: TADOQuery;
    t_Tables: TADOTable;
    t_Views: TADOTable;
    t_FK: TADOTable;
    t_CK: TADOTable;
    t_TableFields: TADOTable;
    t_Triggers: TADOTable;
    t_Indexes: TADOTable;
    t_StoredProcs: TADOTable;

    procedure DataModuleCreate(Sender: TObject);
  private
  //  procedure FillIndexes;
  //  procedure FillFK;

  public
    procedure ExportStructure;
  end;

//const
//  SQL_SEL_OBJECT_FIELDS1 =
//    'SELECT o.name AS obj_name, f.*                                     '+
//    ' FROM  _objects o INNER JOIN _object_fields f                      '+
//    '       ON o.id = f.object_id                                       '+
//    ' WHERE o.name not in (''linkend'', ''pmp_sector'', ''pmpterminal'')'+
////    '       AND f.type <> ''userlist''                                  '+
//    ' ORDER BY obj_name, f.id                                           ';

var
  FdmSQLServ1: TdmSQLServ1111;

function dmSQLServ1111: TdmSQLServ1111;

//=====================================================================
implementation   {$R *.DFM}
//=====================================================================

//--------------------------------------------------------------------
function dmSQLServ1111: TdmSQLServ1111;
//--------------------------------------------------------------------
begin
  if not Assigned(FdmSQLServ1) then
    FdmSQLServ1:= TdmSQLServ1111.Create(Application);

  Result:= FdmSQLServ1;
end;

//------------------------------------------------------------------------------
procedure TdmSQLServ1111.DataModuleCreate(Sender: TObject);
//------------------------------------------------------------------------------
begin
  db_SetComponentADOConnection(Self, dmMDB.ADOConnection1_);

end;

//------------------------------------------------------------------------------
procedure TdmSQLServ1111.ExportStructure;
//------------------------------------------------------------------------------
begin
  dmMDB.EmptyTable('sql_Tables');
  dmMDB.EmptyTable('sql_Table_Fields');
  dmMDB.EmptyTable('sql_Views');
  dmMDB.EmptyTable('sql_Indexes');
  dmMDB.EmptyTable('sql_Triggers');
  dmMDB.EmptyTable('sql_StoredProcs');
  dmMDB.EmptyTable('sql_FK');
  dmMDB.EmptyTable('sql_CK');


  with dmExportStructure do
  begin
    db_TableOpen1(t_Tables, 'sql_Tables');
    ExportTables(t_Tables);

    db_TableReOpen(t_TableFields, 'sql_Table_Fields');
    ExportTableFields(t_TableFields);

    db_TableReOpen(t_Views, 'sql_Views');
    ExportViewsToDataSet(t_Views) ;

    db_TableReOpen(t_Indexes, 'sql_Indexes');
    ExportIndexesToDataSet(t_Indexes) ;

    db_TableReOpen(t_StoredProcs, 'sql_StoredProcs');
    ExportStoredProcsToDataSet(t_StoredProcs);

    db_TableReOpen(t_Triggers, 'sql_Triggers');
    ExportTriggersToDataSet(t_Triggers);

    db_TableReOpen(t_CK, 'sql_CK');
    ExportCKToDataSet(t_CK);

    db_TableReOpen(t_FK, 'sql_FK');
    ExportFKToDataSet(t_FK);
  end;

end;


end.



//
////------------------------------------------------------------------------------
//procedure TdmSQLServ.FillFK;
////------------------------------------------------------------------------------
//begin
//  dmExportStructure.ExportFKToDataSet(t_FK);
//end;
//
////------------------------------------------------------------------------------
//procedure TdmSQLServ.FillIndexes;
////------------------------------------------------------------------------------
//begin
//  with dmExportStructure do
//    ExportIndexesToDataSet(t_indexes);
//end;


