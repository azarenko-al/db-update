unit dm_Install_;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs,
  Db, ADODB,

  dm_Progress,
//  dm_Main_SQL,

  dm_Connection,

//  I_db_login,

u_sql,

  u_Log,

 // u_func_,
  u_func,
  u_db

  ;


type
  TdmInstall_ = class(TDataModule)
    ADOConnection1: TADOConnection;
    qry_Active_Users: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  private
    FDatabaseExists: boolean;  

    FScript_create: TStringList;

    FScript: TStringList;
    procedure CheckConnection;

  protected

  public
    Params: record
              LoginRec : TdbLoginRec;

              IsShowScript : Boolean;

              New_DB_Name : string;
           //   IsCreateNew: boolean;  // ������� DB ��� ��� ���������� ���

           //   User, Password: string;
              FileName: string;
            end;

    IsCreationCompeted: boolean;

    function ExecCommands(aCommandList: TStringList): Boolean;
//    destructor Destroy; override;
    procedure Execute;
    procedure ExecuteProc(); // override;

  end;

  function dmInstall_: TdmInstall_;


//========================================================
implementation {$R *.DFM}
//========================================================

var
  FdmInstall : TdmInstall_;


function dmInstall_: TdmInstall_;
begin
  if not Assigned(FdmInstall) then
    FdmInstall:=TdmInstall_.Create(Application);

  Result:= FdmInstall;
end;


procedure TdmInstall_.DataModuleCreate(Sender: TObject);
begin
  inherited;

  db_SetComponentADOConnection(Self, ADOConnection1);

  FScript_create := TStringList.Create();
  FScript := TStringList.Create();

 // TdmConnection.Init;
end;


procedure TdmInstall_.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FScript);
  FreeAndNil(FScript_create);
end;


// ---------------------------------------------------------------
function TdmInstall_.ExecCommands(aCommandList: TStringList): Boolean;
// ---------------------------------------------------------------
var I: Integer;
  s: string;
begin
  Result := True;

  for I := 0 to aCommandList.Count - 1 do
  try
   // DoProgress(i, aCommandList.Count);

    s:=aCommandList[i];

    ADOConnection1.Execute(s);
  except
    on e: Exception do
    begin

     // DoLog('Exception: '+ E.Message +CRLF+ s);
      g_Log.AddException('TdmInstall.ExecuteProc', E.Message +CRLF+ s);

      ShowMessage('������: '+ E.Message +CRLF+ ' ����������� ����������');
      Result := False;

      Exit;
    end;
  end;


end;

//----------------------------------------------------------------------
procedure TdmInstall_.CheckConnection;
//----------------------------------------------------------------------
begin

(*  TdmLogin.Init;
  if not dmLogin.OpenMaster_(Params.LoginRec, ADoConnection1) then
    Exit;
*)
end;

//----------------------------------------------------------------------
procedure TdmInstall_.ExecuteProc;
//----------------------------------------------------------------------
const
  SQL_SELECT_PID =
    'select spid from sysprocesses                          '+
    'WHERE hostname = HOST_NAME() and dbid =                '+
    '   (select dbid from sysdatabases where name = ''%s'') ';
var
  i, k: Integer;
  sSqlCommand: string;
  s: string;

begin

  IsCreationCompeted:= false;

  //DlgSetShowDetails(true);


  if FDatabaseExists then
  begin
    s:= Format(SQL_SELECT_PID, [Params.New_DB_Name]);
//    db_OpenQueryF( qry_Active_Users, s);
    db_OpenQuery( qry_Active_Users, s);

    with qry_Active_Users do
      while not EOF do
    begin
      sSqlCommand:= Format('KILL %d', [qry_Active_Users.FieldByName('spid').AsInteger]);
      FScript_create.Add(sSqlCommand);

      Next;
    end;

    sSqlCommand:= 'DROP DATABASE ['+Params.new_DB_Name+']';
    FScript_create.Add(sSqlCommand);

  end;

  sSqlCommand:= 'CREATE DATABASE ['+Params.new_DB_Name+'] COLLATE Cyrillic_General_CI_AS';
  FScript_create.Add(sSqlCommand);

  ExecCommands(FScript_create);

  ADOConnection1.DefaultDatabase := Params.new_DB_Name;

  ExecCommands(FScript);

  ADOConnection1.Close;

  IsCreationCompeted:= true;

end;


// ---------------------------------------------------------------
procedure TdmInstall_.Execute;
// ---------------------------------------------------------------
var
  oStrList: TStringList;
begin
  if not FileExists(Params.FileName) then
    Exit;


//  if not dmConnection.OpenMaster_(Params.LoginRec, ADoConnection1) then
  if not dmConnection.OpenDatabase(Params.LoginRec, ADoConnection1) then
    Exit;


(*  if  then
    MSG_NOT_ENOUGH_RIGHTS   = '������������ ���� ������� ��� ���������� ��������';

*)

  // -------------------------
  oStrList:=TStringList.Create;

/////////////////  oStrList.Text :=Params.LoginRec.Results.DatabaseListText;
  FDatabaseExists :=oStrList.IndexOf(Params.New_DB_Name)>=0;

  oStrList.Free;
  // -------------------------

  if FDatabaseExists then
    if (MessageDlg ('��������! ���� ������ � ����� ������ ��� ����������. '+
                    '��� ������ ����� ����������. ����������?',
                    mtWarning, [mbYes,mbNo], 0) = mrNo)
    then Exit;

(*
  oStrList:=TStringList.Create;
  oStrList.LoadFromFile(Params.FileName);

  db_SQLScriptParse(oStrList.Text, FScript);

  oStrList.Free;
*)

//  Params.DB_Name

///////////  db_LoadScriptFromFileAndParse(Params.FileName, FScript);

//  LoadFromFileAndParse(Params.FileName);

 /////////// ExecuteDlg();

//  if Confirm('�������� ������?') then
   

//  ExecuteProc;

end;



end.


//
//  for i := 0 to oStrFile.Count -1 do
//  begin
//    if Terminated then
//    begin
//      ADOConnection1.Close;
//  //    oStrFile.Free;
//   //   oStrFile1.Free;
//      exit;
//    end;
//
//    DoProgress2(i+1, oStrFile.Count);
//
//    if (oStrFile[i] = '') or (Pos('--', oStrFile[i]) = 1) then
//      k:=k;
//
//{      if (Pos('SET', oStrFile[i]) = 1) then
//    begin
//      if k = 0 then begin
//        dmMain.ADOConnection.Connected := False;
//        dmMain.ADOConnection.Connected := True;
//      end;
//      k:= 1;
//    end;
//}
//
//    if (Pos('GO', oStrFile[i]) = 1) then
//    begin
//      try
//        ADOConnection1.Execute(oStrFile1.Text);
//      except
//        on E: Exception do
//        begin
//          DoLog('Exception: '+ E.Message+CRLF+oStrFile1.Text);
//          g_Log.AddException('TdmInstall.ExecuteProc', E.Message+CRLF+oStrFile1.Text);
//          CreationCompeted:= false;
//        end;
//      end;
//      oStrFile1.Clear;
//      Continue;
//    end;
//
//    oStrFile1.Add(oStrFile[i]);
//  end;

