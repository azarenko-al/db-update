unit u_Sql_Parser;

interface
uses
  Classes, SysUtils, StrUtils,

  u_files,
  u_func

  ;


type
  TSqlParser = class(TStringList)
  public
    procedure LoadFromFile(aFileName: string);// override;

//    procedure LoadFromFile(aSQL: string);
    procedure LoadFromStr(aSQL: string);
    procedure SaveToTestFile(aFileName: string);
  end;


//function db_SQLScriptParse1(aSQL: string): TStringList;

implementation


// ---------------------------------------------------------------
procedure TSqlParser.LoadFromStr(aSQL: string);
// ---------------------------------------------------------------
var
  bHasComment: Boolean;
  oStrList: TStringList;
  oStrFile_temp: TStringList;

  s: string;
  i: Integer;
  iPos1: Integer;
  iPos2: Integer;
  s1: string;

  state: (stText,stComment);

begin
 // aSQL:='    /* sdfsdfsdf */    /* sdfsdfsdf   ';

(*
aSQL:=

'  /*'+ CRLF+
'ALTER TABLE [dbo].[AntennaType]'+CRLF+
'ADD CONSTRAINT [FK_AntennaType_Folder] FOREIGN KEY ([folder_id])'+CRLF+
'  REFERENCES [dbo].[Folder] ([id])'+CRLF+
'  ON UPDATE NO ACTION'+CRLF+
'  ON DELETE NO ACTION'+     CRLF+
'GO  */';


*)
  Clear;

//  Result:=TStringList.Create;

  state:=stText;

  oStrList:=TStringList.Create;
  oStrFile_temp:= TStringList.Create;

  oStrList.Text := aSQL;

 // sSQL :='';

  for i := 0 to oStrList.Count -1 do
  begin
    s :=oStrList[i];

    if (LeftStr(s,2)='--') then
    begin
      if (Pos('*/', s) > 1) then
        state:=stText;


      oStrFile_temp.Add(s);
      Continue;
    end;


   iPos2 :=0;
   s1:=s;

   bHasComment:=  (Pos('/*', s)>0) or (Pos('*/', s)>0) ;


   if Trim(s)<>'' then
     if (Pos('/*', s)>0) or (Pos('*/', s)>0) then
     repeat
       iPos1:=Pos  ('/*', s1);
       iPos2:=PosEx('*/', s1,iPos1);

//       if (iPos2>0) and (iPos1=0) then
 //        Break;


       //  ......*/........
       if (iPos1=0) and (iPos2>0) then
       begin
         state:=stText;
         s1:=Copy(s1, iPos2+1, Length(s1));
         Continue;
       end else

       //  ....../*........
       if (iPos1>0) and (iPos2=0) then
       begin
         state:=stComment;
         Break;
       end else

       //  .../*...*/........
       if (iPos1>0) and (iPos2>0) then
       begin
         state:=stText;
         s1:=Copy(s1, iPos2+1, Length(s1));
         Continue;
       end;

//
//       if (iPos2>0) then
//         state:=stText;
//
//       if (iPos2=0) then
//         Break;

     until
    //    Length(s1)>0;
         (iPos1=0) and (iPos2=0) ;

  //  if (Pos('/*', s) > 1) then
   //   state:=stComment;

 //   if (Pos('*/', s) > 1) then
  //    state:=stText;
          

    if (state = stText) and (not bHasComment) then
      if (UpperCase(Trim(s))='GO') then
      begin
        if Trim(oStrFile_temp.Text)<>'' then
          Add(oStrFile_temp.Text);

  //      aStrings.Add(sSQL); //oStrFile_temp.Text);

    //    sSQL := '';
        oStrFile_temp.Clear;
        Continue;
      end;// else
  //      sSQL :=sSQL+s;

    oStrFile_temp.Add(s);

  end;

  if oStrFile_temp.Count>0 then
    Add(oStrFile_temp.Text);



(*
  for I := 0 to Result.Count - 1 do
  begin
    Result[i]:='---aaaaaaaaaaaaaaa--------------------' + Result[i];

  end;


  ShellExec_Notepad_temp(Result.Text);
*)


  FreeAndNil(oStrList);
  FreeAndNil(oStrFile_temp);
end;



// ---------------------------------------------------------------
procedure TSqlParser.LoadFromFile(aFileName: string);
// ---------------------------------------------------------------
var
  oSList: TStringList;
begin
  oSList:=TStringList.Create;
  oSList.LoadFromFile(aFileName);

  LoadFromStr (oSList.Text);

  FreeAndNil(oSList);

end;


procedure TSqlParser.SaveToTestFile(aFileName: string);
var
  I: Integer;
  oSList: TStringList;
begin
  oSList:=TStringList.Create;



  for I := 0 to Count - 1 do
  begin
    oSList.Add(Self[i]);
    oSList.Add('-------------------------------------------------');
    oSList.Add('-------------------------------------------------');

  end;

  oSList.SaveToFile(aFileName);

  FreeAndNil(oSList);

end;


//var
//
//  oSqlParser: TSqlParser;
//
//  oSList: TStringList;
//begin


//
//  oSList:=TStringList.Create;
//
//  oSList.Add('exec aaaaaa');
//  oSList.Add('exec bbbb');
//
//  oSqlParser:=TSqlParser.Create;
//
//  oSqlParser.LoadFromStr(oSList.Text);
//
//  FreeAndNil(oSqlParser);
//
//
//  FreeAndNil(oSList);
//


(*  obj:=TSqlParser.Create;

  obj.LoadFromFile  ('d:\000.sql');
  obj.SaveToTestFile('d:\000_.sql');

  obj.LoadFromFile  ('d:\0001.sql');
  obj.SaveToTestFile('d:\0001_.sql');


//  obj.LoadFromFile ('d:\000.sql');
  obj.SaveToTestFile('d:\000_.sql');

  FreeAndNil(obj);
*)

 // db_SQLScriptParse('');


end.
