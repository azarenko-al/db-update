inherited dlg_Start: Tdlg_Start
  Left = 735
  Top = 340
  Caption = 'dlg_Start'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pn_Buttons: TPanel
    inherited Bevel1: TBevel
      ExplicitWidth = 461
    end
    inherited Panel3: TPanel
      inherited btn_Ok: TButton
        Left = 298
        Caption = #1044#1072#1083#1077#1077
        ExplicitLeft = 298
      end
      inherited btn_Cancel: TButton
        Left = 382
        Caption = #1047#1072#1082#1088#1099#1090#1100
        ExplicitLeft = 382
      end
    end
  end
  inherited pn_Top_: TPanel
    inherited Bevel2: TBevel
      ExplicitWidth = 461
    end
  end
  object rg_NextAction: TRadioGroup [2]
    Left = 8
    Top = 64
    Width = 445
    Height = 81
    Anchors = [akLeft, akTop, akRight]
    ItemIndex = 0
    Items.Strings = (
      #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1085#1086#1074#1091#1102' '#1041#1044
      #1054#1073#1085#1086#1074#1080#1090#1100' '#1080#1084#1077#1102#1097#1091#1102#1089#1103' '#1041#1044' ('#1060#1072#1081#1083' MS Access (.mdb))'
      #1054#1073#1085#1086#1074#1080#1090#1100' '#1080#1084#1077#1102#1097#1091#1102#1089#1103' '#1041#1044' (SQL '#1089#1082#1088#1080#1087#1090' (.sql))')
    TabOrder = 2
  end
  inherited ActionList1: TActionList
    Left = 264
  end
  inherited FormStorage1: TFormStorage
    Left = 412
  end
  inherited cxPropertiesStore: TcxPropertiesStore
    Left = 335
  end
end
