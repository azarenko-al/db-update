object dlg_CompareSQL: Tdlg_CompareSQL
  Left = 1065
  Top = 239
  ActiveControl = Button1
  BorderWidth = 5
  Caption = #1057#1088#1072#1074#1085#1077#1085#1080#1077
  ClientHeight = 542
  ClientWidth = 800
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 193
    Width = 800
    Height = 3
    Cursor = crVSplit
    Align = alTop
    ExplicitWidth = 808
  end
  object Panel1: TPanel
    Left = 0
    Top = 511
    Width = 800
    Height = 31
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      800
      31)
    object Button1: TButton
      Left = 716
      Top = 5
      Width = 75
      Height = 23
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ModalResult = 2
      TabOrder = 0
    end
    object Button2: TButton
      Left = 0
      Top = 6
      Width = 75
      Height = 23
      Action = act_Exec
      TabOrder = 1
    end
  end
  object pn_Bottom: TPanel
    Left = 0
    Top = 325
    Width = 800
    Height = 186
    Align = alBottom
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 1
    object GroupBox2_update_SQL: TGroupBox
      Left = 3
      Top = 3
      Width = 794
      Height = 108
      Align = alClient
      Caption = 'SQL'
      TabOrder = 0
      object DBMemo2: TDBMemo
        Left = 2
        Top = 15
        Width = 790
        Height = 91
        Align = alClient
        DataSource = DataSource1
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
        OnKeyPress = box_MDBKeyPress
      end
    end
    object GroupBox1: TGroupBox
      Left = 3
      Top = 111
      Width = 794
      Height = 72
      Align = alBottom
      Caption = 'Errors'
      TabOrder = 1
      object DBMemo1: TDBMemo
        Left = 2
        Top = 15
        Width = 790
        Height = 55
        Align = alClient
        DataSource = DataSource1
        ReadOnly = True
        TabOrder = 0
        WordWrap = False
        OnKeyPress = box_MDBKeyPress
      end
    end
  end
  object pn_Top: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 193
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object gb_Mdb: TGroupBox
      Left = 0
      Top = 0
      Width = 297
      Height = 193
      Align = alLeft
      Caption = 'MDB'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object DBRichEdit_MDB: TDBRichEdit
        Left = 2
        Top = 15
        Width = 293
        Height = 176
        Align = alClient
        DataField = 'Definition_local'
        DataSource = DataSource1
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyPress = box_MDBKeyPress
      end
    end
    object gb_Sql: TGroupBox
      Left = 521
      Top = 0
      Width = 279
      Height = 193
      Align = alRight
      Caption = 'SQL'
      TabOrder = 1
      object DBRichEdit_SQL: TDBRichEdit
        Left = 2
        Top = 15
        Width = 275
        Height = 176
        Align = alClient
        DataField = 'Definition_remote'
        DataSource = DataSource1
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        OnKeyPress = box_MDBKeyPress
      end
    end
  end
  object FormStorage1: TFormStorage
    IniFileName = 'Software\Onega\'
    Options = [fpPosition]
    UseRegistry = True
    StoredProps.Strings = (
      'pn_Bottom.Height')
    StoredValues = <>
    Left = 336
    Top = 240
  end
  object DataSource1: TDataSource
    Left = 208
    Top = 240
  end
  object ActionList1: TActionList
    Left = 48
    Top = 208
    object act_Exec: TAction
      Caption = 'Exec'
      OnExecute = act_ExecExecute
    end
  end
end
