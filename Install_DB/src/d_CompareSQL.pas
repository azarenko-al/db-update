unit d_CompareSQL;

interface

uses

  dm_CompareAndModify_v1,

  u_func_,

  Windows, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls,
  rxPlacemnt, ExtCtrls, DB, DBCtrls, ActnList, System.Actions;

type
  Tdlg_CompareSQL = class(TForm)
    FormStorage1: TFormStorage;
    Panel1: TPanel;
    Button1: TButton;
    Splitter1: TSplitter;
    DataSource1: TDataSource;
    Button2: TButton;
    ActionList1: TActionList;
    act_Exec: TAction;
    pn_Bottom: TPanel;
    GroupBox2_update_SQL: TGroupBox;
    DBMemo2: TDBMemo;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    pn_Top: TPanel;
    gb_Mdb: TGroupBox;
    DBRichEdit_MDB: TDBRichEdit;
    gb_Sql: TGroupBox;
    DBRichEdit_SQL: TDBRichEdit;
    procedure act_ExecExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure box_MDBKeyPress(Sender: TObject; var Key: Char);
  public
    class procedure ExecDlg(aDataset: TDataset);
  end;


//===================================================================
implementation


{$R *.DFM}


procedure Tdlg_CompareSQL.act_ExecExecute(Sender: TObject);
begin
  dmCompareAndModify_v1.ExecSingleRecord();
end;


procedure Tdlg_CompareSQL.FormCreate(Sender: TObject);
begin
  pn_Bottom.Align:=alClient;
end;

//--------------------------------------------------------------------
class procedure Tdlg_CompareSQL.ExecDlg(aDataset: TDataset);
//--------------------------------------------------------------------
var
  I, iIndex: Integer;
 // sMDB, sSQL: string;
begin
  with Tdlg_CompareSQL.Create(Application) do
  begin
    datasource1.dataSet := aDataset;

    try
   //   aDataSet.Edit;

   //   s1:=aDataset.FieldByName('text_MDB').AsString;
   //   s2:=aDataset.FieldByName('text_SQL').AsString;


      if not SqlCompare(DBRichEdit_MDB.Lines.Text, DBRichEdit_SQL.Lines.Text, iIndex) then
      begin
     //   sMDB:= DBRichEdit_MDB.Lines[i];
      //  sSQL:= DBRichEdit_SQL.Lines[i];

        DBRichEdit_SQL.SelStart := iIndex;
        DBRichEdit_SQL.SelLength:= 100000;//Length(sSQL);
        DBRichEdit_SQL.SelAttributes.Color:= clRed;
        DBRichEdit_SQL.SelAttributes.Style:= [fsBold];

     //   break;
      end;

    finally
    end;

    ShowModal;

//    aDataset.Cancel;

    Free;
  end;
end;

//--------------------------------------------------------------------
procedure Tdlg_CompareSQL.FormResize(Sender: TObject);
//--------------------------------------------------------------------
begin
  gb_MDB.Width:= Trunc(Width/2)-10;
  gb_SQL.Width:= Trunc(Width/2)-10;
end;

//--------------------------------------------------------------------
procedure Tdlg_CompareSQL.box_MDBKeyPress(Sender: TObject; var Key: Char);
begin
  if Ord(Key) = VK_ESCAPE then
    Close;
end;

end.

