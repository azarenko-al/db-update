unit d_NewDB;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, rxPlacemnt,
  ActnList, IniFiles,


  fra_DB_Login,

  dm_Install_,

  //fr_DB_Connect_Params,

  u_func,
  u_dlg,

  rxToolEdit, Mask, System.Actions

  ;

type
  Tdlg_NewDB = class(TForm)
    pn_Header: TPanel;
    lb_Action: TLabel;
    ActionList1: TActionList;
    act_Ok: TAction;
    act_Cancel: TAction;
    FormStorage1: TFormStorage;
    pn_Main: TPanel;
    gb_ConnectToSQL: TGroupBox;
    pn_Buttons: TPanel;
    Button2: TButton;
    Button1: TButton;
    Bevel2: TBevel;
    Panel1: TPanel;
    FilenameEdit1: TFilenameEdit;
    Label1: TLabel;
    ed_New_DB: TEdit;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    FilenameEdit2: TFilenameEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    FilenameEdit3: TFilenameEdit;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btn_CancelClick(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure act_OkExecute(Sender: TObject);
  private
    Fframe_DB_Login: Tframe_DB_Login;
    FIniFileName: string;


   // Fframe_DB_Connect_Params: Tframe_DB_Connect_Params;

    procedure ExecuteNewDB();

  public
    class function CreateForm: boolean;
  end;

var
  dlg_NewDB: Tdlg_NewDB;

//=====================================================================
implementation {$R *.DFM}




//-------------------------------------------------------------------
class function Tdlg_NewDB.CreateForm: boolean;
//-------------------------------------------------------------------
begin
  with Tdlg_NewDB.Create(Application) do
  begin
    Result:= (ShowModal=mrOk);
  end;
end;

//--------------------------------------------------------------
procedure Tdlg_NewDB.FormCreate(Sender: TObject);
//--------------------------------------------------------------
var
//  sDbName, sCurAppPath: string;
  oIniFile: TIniFile;
 // oList: TStringList;
  sDbName: string;
  sNew_Database: string;
  sScript_filename: string;
  sScript_data_filename: string;
begin
  Caption:= '������ ��������� �� RPLS';
  Caption :=Caption+ GetAppVersionStr();

  lb_Action.Caption:= '�������� ����� ��';

  pn_Main.Align:= alClient;

  // ---------------------------------------------------------------

  CreateChildForm_ (Tframe_DB_Login, Fframe_DB_Login, gb_ConnectToSQL);

  FIniFileName :=ChangeFileExt(Application.ExeName, '.ini');

  Fframe_DB_Login.IniFileName := FIniFileName;
  Fframe_DB_Login.LoadFromIni();


  gb_ConnectToSQL.Height:= Fframe_DB_Login.MaxHeight;

 // pn_Connect.Align:= alClient;

(*  Fframe_DB_Connect_Params:=
        Tframe_DB_Connect_Params.CreateChildForm (Self, pn_Connect);

  Fframe_DB_Connect_Params.IsShowImage:= false;

  Fframe_DB_Connect_Params.IsNewDB := true;
*)


(*
  FormStorage1.IniFileName:=REGISTRY_FORMS + ClassName +'\' + FormStorage1.name;
  FormStorage1.IniSection:='Window';
  FormStorage1.Active:=True;
*)

//  sCurAppPath:= IncludeTrailingBackslash(ExtractFileDir(Application.ExeName)) +
 //       'install_db.ini';

  with TIniFile.Create(FIniFileName) do
  begin
    sNew_Database   := ReadString('main', 'new_database', '');
    sScript_filename:= ReadString('main', 'script_filename', '');
    sScript_data_filename:= ReadString('main', 'script_data_filename', '');

    Free;
  end;

(*
  oIniFile:= TIniFile.Create(FIniFileName);
  sDbName:= oIniFile.ReadString('main', 'default_data', 'onega');
  oIniFile.Free;
*)

  if not FileExists(FilenameEdit1.FileName) then
    FilenameEdit1.FileName:= GetApplicationDir()+ sScript_filename;//'onega.sql';

  if ed_New_DB.Text='' then
    ed_New_DB.Text :=sNew_Database;



 // Fframe_DB_Connect_Params.ed_Database_New.Text:= sDbName;


//  gb_ConnectToSQL.Height:= Fframe_DB_Connect_Params.CurHeight-10;

(*
  sCurAppPath:= ExtractFilePath(Application.ExeName);

  ed_FileName.Text:= '';
  if FileExists (sCurAppPath + 'onega.sql') then
    ed_FileName.Text:= sCurAppPath + 'onega.sql'
  else begin
    oList:= TStringList.Create;
    FindFilesByMaskNotRecurse(sCurAppPath, '*.sql', oList, true);

    if oList.Count>0 then
      ed_FileName.Text:= oList[0];

    oList.Free;
  end;
*)
end;

procedure Tdlg_NewDB.FormDestroy(Sender: TObject);
begin
  if Fframe_DB_Login.IsModified then
    Fframe_DB_Login.SaveToIni();

end;


//----------------------------------------------------------------------
procedure Tdlg_NewDB.FormClose(Sender: TObject; var Action: TCloseAction);
//----------------------------------------------------------------------
begin
  Action:= caFree;
end;

//----------------------------------------------------------------------
procedure Tdlg_NewDB.btn_CancelClick(Sender: TObject);
//----------------------------------------------------------------------
begin
  Close;
end;

//----------------------------------------------------------------------
procedure Tdlg_NewDB.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
//----------------------------------------------------------------------
begin

 // act_Ok.Enabled:= Fframe_DB_Connect_Params.DefaultDB<>'';

end;

//----------------------------------------------------------------------
procedure Tdlg_NewDB.ExecuteNewDB();
//----------------------------------------------------------------------
begin
  if not FileExists(FilenameEdit1.FileName) then
  begin
    ErrorDlg('SQL ������ �� ���������� ��� �� ������');
    exit;
  end;

  dmInstall_.Params.LoginRec := Fframe_DB_Login.GetLoginRec;
  dmInstall_.Params.new_DB_Name := ed_New_DB.Text;
  
 // dmInstall_.Params.LoginRec.DataBase :=FileName1.FileName;

  dmInstall_.Params.FileName:= FilenameEdit1.FileName;

  dmInstall_.Execute;

//      aLoginRec.ConnectionStatusStr := db_GetConnectionStatusStr(aLoginRec);



 (*
  ModalResult:= mrNone;

  if not FileExists(ed_FileName.Text) then begin
    ErrorDlg('SQL ������ �� ���������� ��� �� ������');
    exit;
  end;

  with Fframe_DB_Connect_Params do begin
    if not dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then begin
      ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
      exit;
    end;

    dmMain.SaveConnectRecToReg(Server, DefaultDB, User, Password, UseWinAuth);

    if dmMain.Open then begin
      if (MessageDlg ('��������! ���� ������ � ����� ������ ��� ����������. '+
                      '��� ������ ����� ����������. ����������?',
                      mtWarning, [mbYes,mbNo], 0) = mrNo)
      then
        Exit
      else
        dmInstall.Params.CreateNew:= False;
    end
    else
      dmInstall.Params.CreateNew:= True;

    dmMain.ADOConnection1.Close;
    dmMain.OpenByParams(Server, User, Password, 'master', NetworkLibrary, NetworkAddress, UseWinAuth);

    if not dmMain.TestForAdminWrites then begin
      ErrorDlg(MSG_NOT_ENOUGH_RIGHTS);
      exit;
    end;

    dmInstall.Params.FileName:= ed_FileName.Text;
    dmInstall.Params.DB_Name := DefaultDB;
    dmInstall.Params.User    := User;
    dmInstall.Params.Password:= Password;
  end;

  dmInstall.ExecuteDlg('��������� ���� ������...');

  if dmInstall.CreationCompeted then
  begin
    MsgDlg('��������� ���������');
    ModalResult:= mrOk;
    Close;
  end else
  begin
    MsgDlg('��������� ��������');
  end;
*)

end;


//----------------------------------------------------------------------
procedure Tdlg_NewDB.act_OkExecute(Sender: TObject);
//----------------------------------------------------------------------
begin
  //----------------------------
  if Sender = act_Ok then
  begin
    ExecuteNewDB();
  end else
end;


end.
