unit d_CustomLogin;

interface

uses Classes, Controls, Forms, 
     ExtCtrls, 

  //ODBCList,

  //fr_DB_Connect_Params,  

  d_Wizard, cxPropertiesStore, rxPlacemnt, ActnList, StdCtrls,
  cxLookAndFeels, cxClasses, System.Actions;

type

  Tdlg_CustomLogin = class(Tdlg_Wizard)
    pn_Connect_Params: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure act_OkExecute(Sender: TObject);
  private

    function  Login_To_DB(): boolean;
  public
    class function ShowModalForm: boolean;
  end;


//=================================================================
implementation

 {$R *.DFM}
//=================================================================


//-----------------------------------------------------------------
class function Tdlg_CustomLogin.ShowModalForm: boolean;
//-----------------------------------------------------------------
begin
  with Tdlg_CustomLogin.Create(Application) do
  begin
    Result:= (ShowModal=mrOk);
  end;
end;

//-----------------------------------------------------------------
procedure Tdlg_CustomLogin.FormCreate(Sender: TObject);
//-----------------------------------------------------------------
var
  sPath: string;
begin
  inherited;

  Caption:= '������ ��������� �� RPLS';
  SetActionName('��������� ����������� � ��');

  btn_Cancel.Caption:= '�������';

(*  Height:= 385;
  Width:= 505;
  Left:= 364;
  Top:= 347;*)

  pn_Connect_Params.Align:= alClient;

//  Ffr_DB_Connect_Params:=
  //   Tframe_DB_Connect_Params.CreateChildForm (Self, pn_Connect_Params);

//  Width := Ffr_DB_Connect_Params.CurWidth +2;
//  Height:= Ffr_DB_Connect_Params.CurHeight+pn_Buttons.Height+pn_header.Height+2;


//  Ffr_DB_Connect_Params.pn_Connect.Visible:= false;
end;

//-----------------------------------------------------------------
procedure Tdlg_CustomLogin.act_OkExecute(Sender: TObject);
//-----------------------------------------------------------------
begin

(*
  if Login_To_DB() then
  begin
//    with Ffr_DB_Connect_Params do
//      dmMain.Add_ODBC_Source (ed_Server.Text, ed_DataBase.Text, ed_User.Text);

  //  MsgDlg('��������� ����������� ������� ��������');
    ModalResult:= mrOk;
  end  else begin
    ErrorDlg('������ �����������: '+ dmMain.ErrorMsg);
    ModalResult:= mrNone;
  end;

*)
end;

//-------------------------------------------------------------------
function Tdlg_CustomLogin.Login_To_DB(): boolean;
//-------------------------------------------------------------------
(*
var
  oConnectRec: TLoginRec;
  sNetworkLibrary, sNetworkAddress: string;
*)
begin
  (*Result:= true;

  with Ffr_DB_Connect_Params, oConnectRec do
  begin
    Server:=      ed_Server.Text;
    DefaultDB:=   ed_DataBase.Text;
    User:=        ed_User.Text;
    Password:=    ed_Password.Text;
    IsUseWinAuth:=UseWinAuth;

    if dmMain.TestConnect(Server, User, Password, NetworkLibrary, NetworkAddress, UseWinAuth) then
    begin
      dmMain.SaveConnectRecToReg(oConnectRec);
      dmMain.Open;
    end;
  end;

  if not dmMain.ADOConnection.Connected then
    Result:= false;
    *)
end;


//-----------------------------------------------------------------
procedure Tdlg_CustomLogin.FormClose(Sender: TObject;  var Action: TCloseAction);
//-----------------------------------------------------------------
begin
  Action:= caFree;
end;


end.
