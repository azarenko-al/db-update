unit dm_ObjectFields;

interface

uses
  SysUtils, Classes, Forms, Db, AdoDb, Dialogs, Variants,

  d_progress,

  u_DB,
  u_func,

  u_Dlg,

  u_files,

  u_log,

  dm_Main_SQL,
  dm_MDB     
  ;

type
  TdmObjectFields = class(TDatamodule)
    t_MDB_Objects: TADOTable;
    t_MDB_Object_fields: TADOTable;
    qry_SQL_object_fields: TADOQuery;
    qry_MDB_Object_fields: TADOQuery;
    qry_SQL_objects: TADOQuery;
    ADOQuery1: TADOQuery;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
  private

//    function Check_ID_is_Identity: Boolean;
    procedure CopyObjectRecords1_new(aObject_ID, aObject_ID_SQL: Integer);
    procedure CopyObjects;

    procedure Log(aMsg: string);

  public

    procedure Exec_Progress;
    procedure Exec;

    class procedure Init;
  end;

var
  dmObjectFields: TdmObjectFields;

//===================================================================
implementation {$R *.DFM}

//===================================================================

const
  TBL_Objects       = '_Objects';
  TBL_Object_fields = '_Object_fields';
  FLD_Object_ID     = 'Object_ID';



//------------------------------------------------------------------
class procedure TdmObjectFields.Init;
//------------------------------------------------------------------
begin
  if not Assigned(dmObjectFields) then
    dmObjectFields:= TdmObjectFields.Create(Application);
end;

// ---------------------------------------------------------------
procedure TdmObjectFields.DataModuleCreate(Sender: TObject);
// ---------------------------------------------------------------
begin
  inherited;
//  db_SetComponentADOConnection(Self, dmMain_SQL.ADOConnection1);


  db_SetComponentsADOConn([qry_SQL_objects, qry_SQL_object_fields, ADOQuery1],
      dmMain_SQL.ADOConnection1);


(*  db_SetComponentsADOConn([t_SQL_Objects, t_SQL_Object_fields,
                           ADOQuery1],
      dmMain_SQL.ADOConnection1);
*)
  db_SetComponentsADOConn([t_MDB_Objects, t_MDB_Object_fields, qry_MDB_Object_fields],
      dmMDB.ADOConnection_MDB);

end;


procedure TdmObjectFields.DataModuleDestroy(Sender: TObject);
begin
  
end;


// ---------------------------------------------------------------
procedure TdmObjectFields.CopyObjectRecords1_new(aObject_ID, aObject_ID_SQL:
    Integer);
// ---------------------------------------------------------------
const
  DEF_SQL =
    'insert into :table (object_id) values (:object_id); '+
    'select * from :table where id=scope_identity()'; //@@IDENTITY' ;

var
  bTerminated: Boolean;
  iSQL_ID: Integer;
  iID_mdb: Integer;
  iPARENT_ID_mdb: Integer;
  oSrcQuery1: TADOQuery;
  s: string;
  sPARENT_SQL_ID: string;
  sSQL: string;
  sSQL_ID: string;

  oIDList: TStringList;


begin
  oIDList := TStringList.Create();

//  oIDList.Clear;

  s:=Format('DELETE FROM %s WHERE object_id=%d', [TBL_Object_fields, aObject_ID_SQL]);
  dmMain_SQL.ADOConnection1.Execute(s);


 // s:=Format('ALTER TABLE %s NOCHECK CONSTRAINT All', [TBL_Object_fields]);
 // dmMain_SQL.ADOConnection1.Execute(s);


   s:=Format('SELECT * FROM %s WHERE (object_id=%d)', [TBL_Object_fields, aObject_ID]);
   db_OpenQuery(qry_MDB_Object_fields, s);


  ///////  db_View(qry_MDB_Object_fields);


    oSrcQuery1:=qry_MDB_Object_fields;

   // i:=oSrcQuery1.RecordCount;

    with oSrcQuery1 do
      while not EOF do
    begin
      Progress_SetProgress2(RecNo, RecordCount, bTerminated);

(*
  DEF_SQL =
    'insert into :table (object_id) values (:object_id) '+
    'select * from :table where id=scope_identity()'; //@@IDENTITY' ;
*)

      s:=Format('insert into %s (object_id) values (%d)', [TBL_Object_fields, aObject_ID_SQL]);
      dmMain_SQL.ADOConnection1.Execute(s);

//      db_OpenQuery(ADOQuery11, Format('insert into %s (object_id) values (%d)', [TBL_Object_fields, aObject_ID_SQL]));
      s:=Format('select * from %s where id=scope_identity()', [TBL_Object_fields]);
      db_OpenQuery(qry_SQL_object_fields, s);


 //     s:=ReplaceStr(DEF_SQL, ':table',     TBL_Object_fields);
  //    s:=ReplaceStr(s,       ':object_id', IntToStr(aObject_ID_SQL));

   //   db_OpenQuery(ADOQuery11, s);

      Assert(qry_SQL_object_fields.recordCount>0, 'Value <=0');

//      db_View(ADOQuery1);


      iSQL_ID :=qry_SQL_object_fields.FieldByName(FLD_id).AsInteger;


      qry_SQL_object_fields.Edit;
      db_CopyRecord_ExceptFields(oSrcQuery1, qry_SQL_object_fields, [FLD_ID,FLD_Object_ID]);

      qry_SQL_object_fields.FieldByName(FLD_Parent_id).Value := null;
      qry_SQL_object_fields.Post;

    //  db_View(ADOQuery1);


    //////  db_OpenQuery(ADOQuery1, 'SELECT @@IDENTITY');
      iSQL_ID :=qry_SQL_object_fields.FieldByName(FLD_id).AsInteger;
      Assert(iSQL_ID>0, 'Value <=0');

      iID_mdb:=FieldByName(FLD_ID).AsInteger;
      oIDList.Values[IntToStr(iID_mdb)]:= IntToStr(iSQL_ID) ;

      Next;
    end;


 ////   ShellExec_Notepad_temp (oIDList.Text);

    // -------------------------
    s:=Format('SELECT * FROM %s WHERE (object_id=%d) and (parent_id is not null)',
       [TBL_Object_fields, aObject_ID]);
    db_OpenQuery(qry_MDB_Object_fields, s);

    // ---------------------------------------------------------------
    with oSrcQuery1 do
      while   (not EOF) do //False and
    begin
      Progress_SetProgress2(RecNo, RecordCount, bTerminated);

      iID_mdb       :=FieldByName(FLD_ID).AsInteger;
      iPARENT_ID_mdb:=FieldByName(FLD_PARENT_ID).AsInteger;

      sPARENT_SQL_ID := oIDList.Values[IntToStr(iPARENT_ID_mdb)];// := IntToStr(iSQL_ID);
      sSQL_ID        := oIDList.Values[IntToStr(iID_mdb)];// := IntToStr(iSQL_ID);

     Assert(sPARENT_SQL_ID<>'', IntToStr(iPARENT_ID_mdb));
     Assert(sSQL_ID<>'',        IntToStr(iID_mdb));

//      if sPARENT_SQL_ID then


      s:=Format('UPDATE %s SET parent_id=%s WHERE id=%s',
          [TBL_Object_fields, sPARENT_SQL_ID, sSQL_ID]);

      dmMain_SQL.ADOConnection1.Execute(s);


      Next;
    end;

 // end;
  // ---------------------------------------------------------------


  FreeAndNil(oIDList);
end;


procedure TdmObjectFields.Exec;
begin
   CopyObjects();
end;

(*
// ---------------------------------------------------------------
function TdmObjectFields.Check_ID_is_Identity: Boolean;
// ---------------------------------------------------------------
const
  DEF_SQL =
    ' SELECT o.name AS table_name, c.name, '+
    '    ColumnProperty(o.id, c.name, ''IsIdentity'') AS IsIdentity '+
    ' FROM  sysobjects o INNER JOIN  syscolumns c ON o.id = c.id '+
    ' WHERE (o.xtype = ''U'') AND (o.name = ''_objects'')  and (c.name = ''id'') ';

begin
  db_OpenQuery(ADOQuery1, DEF_SQL);
  Result := ADOQuery1.FieldByName('IsIdentity').AsInteger = 1;

end;

*)

// ---------------------------------------------------------------
procedure TdmObjectFields.CopyObjects;
// ---------------------------------------------------------------
//
// SELECT *
// FROM  sysobjects o INNER JOIN  syscolumns c ON o.id = c.id
//
// WHERE (o.xtype = 'U') AND
//       (o.name = '_objects') and (c.name = 'id') and
//       (ColumnProperty(o.id, c.name, 'IsIdentity')=1)

const
{  DEF_SQL_Check_ID_is_Identity =
    ' SELECT o.name AS table_name, c.name, '+
    '    ColumnProperty(o.id, c.name, ''IsIdentity'') AS IsIdentity '+
    ' FROM  sysobjects o INNER JOIN  syscolumns c ON o.id = c.id '+
    ' WHERE (o.xtype = ''U'') AND (o.name = ''_objects'')  and (c.name = ''id'') ';

}

  DEF_SQL_Check_ID_is_Identity_ =
    ' SELECT o.name AS table_name, c.name, '+
    '    ColumnProperty(o.id, c.name, ''IsIdentity'') AS IsIdentity '+
    ' FROM  sysobjects o INNER JOIN  syscolumns c ON o.id = c.id '+
    ' WHERE (o.xtype = ''U'') AND (o.name = ''%s'')  and (c.name = ''id'') ';




const
  DEF_SQL_INIT =
    'if not exists(select * from _objects where name='':name'') '+
    'begin                                                      '+
    '  declare @id int  '+

    '  select @id=MAX(id)+1 from _objects;                      '+
    '  set @id=IsNull(@id,1);                                   '+
    '                                                           '+
    '  insert into _objects (id,name,table_name) values (@id,  '':name'', '':table_name'') '+
    'end ';

  DEF_SQL_INIT_IDENTITY =
    'if not exists(select * from _objects where name='':name'') '+
    'begin                                                      '+
    '  insert into _objects (name,table_name) values ( '':name'', '':table_name'') '+
    'end ';


  DEF_SQL_SELECT =
    'select * from _objects where name='':name''' ;

var
  bIsIdentity: Boolean;
  bIsIdentity_: Boolean;
  bTerminated: Boolean;
 // iID: Integer;

  iID_mdb: Integer;
  iID_new: Integer;
  iID_sql: Integer;
  s: string;
  sName: string;
  sTableName: string;


begin
  db_OpenQuery(ADOQuery1,  Format( DEF_SQL_Check_ID_is_Identity_,  [TBL_Objects] ) );
  bIsIdentity := ADOQuery1.FieldByName('IsIdentity').AsInteger=1;


//  db_OpenQuery(ADOQuery1, Format( DEF_SQL_Check_ID_is_Identity,  [TBL_Objects] ) );
//  bIsIdentity1 := ADOQuery1.FieldByName('IsIdentity').AsInteger=1;

  db_OpenQuery(ADOQuery1, Format( DEF_SQL_Check_ID_is_Identity_,  [TBL_Object_fields] ) );
  bIsIdentity_ := ADOQuery1.FieldByName('IsIdentity').AsInteger=1;

  if not bIsIdentity_ then
  begin
    ErrorDlg('���� ID � ������� Object_fields �� Identity');
    exit;
  end;
    



  t_MDB_Objects.tableName :='view_Objects_to_export';
  t_MDB_Objects.Open;



  with t_MDB_Objects do
    while not EOF do
    begin
      Progress_SetProgress1(RecNo, RecordCount, bTerminated);

      sName      := FieldByName(FLD_NAME).AsString;
      sTableName := FieldByName(FLD_Table_Name).AsString;
      iID_mdb    := FieldByName(FLD_ID).AsInteger;


      Assert(sTableName<>'');

      try
        if bIsIdentity then
          s:=DEF_SQL_INIT_IDENTITY
        else
          s:=DEF_SQL_INIT;


        // -------------------------------------------
        s:=ReplaceStr(s, ':name', sName);
        s:=ReplaceStr(s, ':Table_name', sTableName);

        dmMain_SQL.ADOConnection1.Execute(s);

//        db_ExecCommand(qry_SQL_objects, s, []);

        // -------------------------------------------
        s:=ReplaceStr(DEF_SQL_SELECT, ':name', sName);
//        db_OpenQuery(qry_SQL_objects, s);

        if db_OpenQuery(qry_SQL_objects, s) then
        begin
          qry_SQL_objects.Edit;

          db_CopyRecord_ExceptFields(t_MDB_Objects, qry_SQL_objects, [FLD_ID,FLD_NAME]);

        //  db_View(ADOQuery1);

          qry_SQL_objects.Post;

          iID_sql := qry_SQL_objects.FieldByName(FLD_ID).AsInteger;

          Assert(iID_sql>0, 'Value <=0');

          CopyObjectRecords1_new (iID_mdb, iID_sql);
        end;
      except
        on E: Exception do
          ShowMessage(sTableName);
      end;
          
      Next;
    end;


  Log('������ �������� ��������: '+ IntToStr(t_MDB_Objects.RecordCount));

end;

procedure TdmObjectFields.Exec_Progress;
begin
  Progress_ExecDlg_proc(Exec);
end;


procedure TdmObjectFields.Log(aMsg: string);
begin
  g_Log.Add(aMsg);
end;



begin

end.



    {

 SELECT

 o.name AS table_name,
     c.name,

    ColumnProperty(o.id, c.name, 'IsIdentity') AS IsIdentity

 FROM  sysobjects o INNER JOIN
       syscolumns c ON o.id = c.id --INNER JOIN
     --  systypes t ON c.xusertype = t.xusertype
 WHERE (o.xtype = 'U') AND
       (o.name = '_objects')  and

       (c.name = 'id')

