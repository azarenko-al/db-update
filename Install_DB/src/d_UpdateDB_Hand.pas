unit d_UpdateDB_Hand;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ImgList,  cxControls, cxPropertiesStore,
  ComCtrls, cxPC,
  StdCtrls, ExtCtrls, DB, ADODB,
  //TB2Item, TB2Dock, TB2Toolbar,

  d_Wizard,

  dm_Main_SQL,

  u_func,

  rxPlacemnt, ActnList, dxBarBuiltInMenu, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, cxClasses, System.Actions, dxSkinsCore,
  dxSkinsDefaultPainters;

type

  Tdlg_UpdateDB_Hand = class(Tdlg_Wizard)
    Memo1: TMemo;
    DataSource: TDataSource;
    Splitter1: TSplitter;
    ImageList1: TImageList;
    StatusBar1: TStatusBar;
    pages: TcxPageControl;
    tab_data: TcxTabSheet;
    tab_msg: TcxTabSheet;
    memo_msg: TMemo;
    ADOQuery1: TADOQuery;
    ADODataSet1: TADODataSet;
    procedure FormCreate(Sender: TObject);
    procedure TBItem1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Memo1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
//    qry_Temp: TMyAdoQuery;
    procedure OpenData;
  public
    class procedure CreateForm;

  end;

(*
var
  dlg_UpdateDB_Hand: Tdlg_UpdateDB_Hand;
*)

//==============================================================
implementation {$R *.dfm}
//==============================================================


//--------------------------------------------------------------
class procedure Tdlg_UpdateDB_Hand.CreateForm;
//--------------------------------------------------------------
begin
  with Tdlg_UpdateDB_Hand.Create(Application) do
  begin
    ShowModal;
//    Result:= (ShowModal=mrOk);
  end;
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_Hand.FormCreate(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;
  Caption:= '������ ���������� ��';
  btn_Cancel.Caption:= '�������';


  ADOQuery1.Connection:= dmMain_SQL.ADOConnection1;
  ADOQuery1.CommandTimeout:= 600;


  ADODataSet1.Connection:= dmMain_SQL.ADOConnection1;


(*
  qry_Temp:= TMyAdoQuery.Create(Application);
  qry_Temp.Connection:= dmMain_SQL.ADOConnection1;
  qry_Temp.CommandTimeout:= 600;

  ds.DataSet:= qry_Temp;
*)

 // grid.Align:= alClient;
  pages.Align:= alClient;
  memo_Msg.Align:= alClient;
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_Hand.FormDestroy(Sender: TObject);
//--------------------------------------------------------------
begin
  inherited;
 // qry_Temp.Free;
end;


procedure Tdlg_UpdateDB_Hand.TBItem1Click(Sender: TObject);
begin
  OpenData;
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_Hand.OpenData;
//--------------------------------------------------------------
var
  sSql: string;
  iRowsAffected: Integer;
  bOpen: boolean;
begin
  inherited;

  if Memo1.SelText<>'' then
    sSql:= Memo1.SelText
  else
    sSql:= Memo1.Lines.Text;

  if sSql='' then
    exit;

  ADOQuery1.DisableControls;
  CursorSql;

  memo_msg.Lines.Clear;


(*  ADODataSet1.CommandText := sSql;
  ADODataSet1.Open;

*)


  with ADOQuery1 do
  begin
    try
     // Close;



      SQL.Text:= sSql;
      bOpen:= (Pos(LowerCase('select'), LowerCase(sSql)) > 0);

      if bOpen then
        Open
      else
        iRowsAffected:= ExecSQL;

      pages.ActivePageIndex:= 0;
      CursorDefault;
    //  qry_Temp.EnableControls;

    except
      on E: Exception do
      begin
        pages.ActivePageIndex:= 1;
        CursorDefault;
      //  ADOQuery1.EnableControls;
        memo_msg.Lines.Text:= E.Message;
      end;
    end;
  end;

  CursorDefault;
  ADOQuery1.EnableControls;

  if bOpen then begin
    if ADOQuery1.Active then
      StatusBar1.Panels[0].Text:= Format('%d �������', [ADOQuery1.RecordCount]);
  end else
    StatusBar1.Panels[0].Text:= Format('%d ������� ���������', [iRowsAffected]);

 // grid.ApplyBestFit(nil);
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_Hand.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//--------------------------------------------------------------
begin
  inherited;

  if Key=Windows.VK_F5 then
    TBItem1Click(nil);
end;

//--------------------------------------------------------------
procedure Tdlg_UpdateDB_Hand.Memo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//--------------------------------------------------------------
begin
  inherited;

  if Key=Windows.VK_F5 then
    TBItem1Click(nil);
end;

end.


