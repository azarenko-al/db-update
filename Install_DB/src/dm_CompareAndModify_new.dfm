object dmCompareAndModify_new: TdmCompareAndModify_new
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 1424
  Top = 624
  Height = 217
  Width = 571
  object ds_Data_Compare: TDataSource
    DataSet = t_Data_Compare
    Left = 64
    Top = 80
  end
  object t_Data_Compare: TADOTable
    CursorType = ctStatic
    TableName = '_Compare'
    Left = 64
    Top = 24
  end
  object ADOTable1: TADOTable
    TableName = '_Compare'
    Left = 184
    Top = 24
  end
  object ActionList1: TActionList
    Left = 400
    Top = 24
    object act_Script_ToNotepad: TAction
      Caption = 'Script To Notepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_ScriptNoDrop_ToNotepad: TAction
      Caption = 'ScriptNoDrop_ToNotepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_ScriptChecked_ToNotepad: TAction
      Caption = 'ScriptCheckedToNotepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_Check_All: TAction
      Caption = 'Check_All'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_ExecutedScript: TAction
      Caption = 'Executed Script'
      OnExecute = act_Script_ToNotepadExecute
    end
    object Action1: TAction
      Caption = 'Action1'
    end
    object act_Script_new_ToNotepad: TAction
      Caption = 'Script_new_ToNotepad'
      OnExecute = act_Script_ToNotepadExecute
    end
    object act_Script_new_exec: TAction
      Caption = 'Script_new_exec'
      OnExecute = act_Script_ToNotepadExecute
    end
  end
  object t_Owner: TADOTable
    CursorType = ctStatic
    IndexName = 'id'
    TableName = 'Owner'
    Left = 288
    Top = 24
  end
  object ds_Owner: TDataSource
    DataSet = t_Owner
    Left = 288
    Top = 80
  end
end
