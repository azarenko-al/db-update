inherited dmCompareAndModify_: TdmCompareAndModify_
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 742
  Top = 444
  Height = 312
  Width = 494
  object ds_Data_Compare: TDataSource
    DataSet = t_Data_Compare
    Left = 40
    Top = 56
  end
  object t_Data_Compare: TADOTable
    TableName = '_Compare'
    Left = 40
    Top = 8
  end
  object qry_Object_Fields: TADOQuery
    Parameters = <>
    Left = 40
    Top = 192
  end
  object t_Tables_sql: TADOTable
    Left = 40
    Top = 136
  end
  object t_Views_sql: TADOTable
    Left = 344
    Top = 136
  end
  object t_FK: TADOTable
    Left = 280
    Top = 24
  end
  object t_CK_sql: TADOTable
    Left = 192
    Top = 136
  end
  object t_TableFields: TADOTable
    Left = 120
    Top = 136
  end
  object t_Triggers: TADOTable
    Left = 408
    Top = 136
  end
  object t_Indexes: TADOTable
    Left = 352
    Top = 24
  end
  object t_StoredProcs_sql: TADOTable
    Left = 264
    Top = 136
  end
  object ds_Data_Compare_: TDataSource
    DataSet = t_Data_Compare_
    Left = 152
    Top = 56
  end
  object t_Data_Compare_: TADOTable
    TableName = '_Compare'
    Left = 152
    Top = 8
  end
end
