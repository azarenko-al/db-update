unit dm_CompareAndModify_1;

interface

uses
  SysUtils, Classes, Forms, Dialogs, Db, AdoDB, Clipbrd, variants,

  dm_ExportStructure,

  I_db_login,

  u_DB,

  u_Func_arrays,
  u_func,

  u_dlg,
  u_log,

  u_const_db,
  u_local_const,

  dm_Main_SQL,

  d_CompareSQL,

  dm_MDB,

  dm_Progress

  , u_sync_classes;

type
  TProcessAction = (paNone, paAdd, paDel, paUpdate);

  TErrorType = (etError, etCriticalError, etWarning);


  TInterface_rec = record

    NameMDB, NameSQL : string;
    ObjectType: TDBObjectType;

    Update_SQL: string;

    Action : string;

    TreeID: integer;
    TreeParentID: Integer;


//   (aNameMDB, aNameSQL: variant;
 //   aAction, aType, aUpdate_SQL: string; aTreeID: integer = -1; aTreeParentID:
  //  integer = -1): Integer;
  end;




  TdmCompareAndModify_ = class(TdmProgress)
    ds_Data_Compare: TDataSource;
    t_Data_Compare: TADOTable;
    qry_Object_Fields: TADOQuery;
    t_Tables_sql: TADOTable;
    t_Views_sql: TADOTable;
    t_FK: TADOTable;
    t_CK_sql: TADOTable;
    t_TableFields: TADOTable;
    t_Triggers: TADOTable;
    t_Indexes: TADOTable;
    t_StoredProcs_sql: TADOTable;
    ds_Data_Compare_: TDataSource;
    t_Data_Compare_: TADOTable;
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  public
    DBStructure_SQL: TDBStructure;
    DBStructure_MDB: TDBStructure;

  private
    FScript: TStringList;

    FiTableID_new: integer;

    FiTreeID, FiTableID: integer;

    FDataSet, FMdbDataSet, FSqlDataSet: TDataSet;
    FTypeStr: string;
    FObjName, FModuleName, FMdbObjName, FSqlObjName: string;
    FDiffers, FExists: boolean;

    FTblName: string;

    FProcessAction: TProcessAction;

    FSql: string;

    function  GetErrorType(aObjType: string): TErrorType;

      procedure AddView_1(aObjectType: TDBObjectType);

//    function Local_Eq(aStr1, aStr2: string): boolean;

    procedure UpdateError (aAction: string);

    procedure AddTablesAndFields_();
// TODO: AddView_
//  procedure AddView_;


    procedure UpdateAction_();

    function DoAddToInterface1_new(aRec: TInterface_rec; aNameMDB, aNameSQL:
        variant; aAction: string; aObjectType: TDBObjectType; aUpdate_SQL: string;
        aTreeParentID: Variant): Integer;


    procedure DoSimpleDropObject(aSql: string; aExists: boolean);
    procedure DoSimpleDropObjects();
    procedure DoSimpleAddObjects(aCheckTableName: boolean = true);
    procedure DoSimpleAddObject();
    function  DoMakeDropSql (aType: string; aDataSet: TDataSet;
                             aObjNameToDrop: string = ''): string;

//    procedure UpdatePropertyPos();
    procedure DoApplyDataset (aType: string);

    procedure AddTablesAndFields();
    procedure AddViews();
    procedure AddTriggers();
    procedure AddIndexes();
    procedure AddFK();
    procedure AddCK();

    procedure AddSript(aSQL: string);

    procedure AddStoredProcs();

    function  CreateTable (): boolean;
    function  CreateField (aTblName: string): boolean;
    function  AlterField (aTblName: string): boolean;

    procedure ExecuteProc; override;

    procedure DoAddToInterface1_(vNameMDB, vNameSQL: variant; aAction, aType:
        string; aTreeID: integer = -1; aTreeParentID: integer = -1; aIDMdb: integer
        = -1; aIDSql: integer = -1; aParentName: string = '');

// TODO: Local_Eq_Old
//  function Local_Eq_Old(aStr1, aStr2: string): boolean;

    function Local_Eq_alex(aStr1, aStr2: string): boolean;
//    function Eq_DEFAULT_CONTENT(aSQL,aMDB : string): Boolean;

    procedure OpenData;


  public
    Params: record
              LoginRec : TdbLoginRec;

            //  IsShowScript : Boolean;

          //    New_DB_Name : string;
           //   IsCreateNew: boolean;  // ������� DB ��� ��� ���������� ���

           //   User, Password: string;
              FileName: string;


              UpdateStructure: Boolean;
              UpdateObjectFields: Boolean;
              IsModifyFields1: boolean;

            end;

//    destructor Destroy; override;
//    procedure SetDboOwner ();
    procedure RefreshViews1;

    procedure DlgCompareSQL ();
    procedure CreateObject ();
    procedure ExportStructure;

    class procedure Init;

  end;

const
  STR_UPDATE      = '��������';
  STR_UPDATE_DONE = '��������� ����������';
  STR_ERR_UPDATE  = '������ ����������';
  STR_UPDATED     = STR_UPDATE_DONE;
  STR_ADD         = '��������';
  STR_ADDED       = '��������� ����������';

  STR_CREATE      = '�������';
  STR_CREATE_     = 'CREATE';

  STR_ALTER_      = 'ALTER';


  STR_CREATE_DONE = '��������� ��������';
  STR_CREATED     = STR_CREATE_DONE;
  STR_ERR_CREATE  = '������ ��������';
  STR_DEL         = '�������';
  STR_DELETED     = '��������� ��������';
  STR_ERR_DELETE  = '������ ��������';
  STR_ERR_DEL_WHILE_UPD = '������ ��������  (��� ����������)';
  STR_ERR_ADD_WHILE_UPD = '������ ���������� (��� ����������)';

  STR_AUTO_Del_NOT_ALLOWED = '�������������� �������� �� ��������������';
  STR_AUTO_ADD_NOT_ALLOWED = '�������������� �������� �� ��������������';

  ERROR_MSG_LOCAL = '������. '#13#10' ������: %s (%s). '#13#10' ����� ������: %s. '#13#10' SQL: %s';

const

  SQL_ALTER_TABLE_DROP_CONSTRAINT   = 'ALTER TABLE [%s] DROP CONSTRAINT [%s]';

  SQL_DROP_INDEX        = 'DROP INDEX [%s].[%s]';
  SQL_DROP_TRIGGER      = 'DROP TRIGGER [%s]';
  SQL_DROP_VIEW         = 'DROP VIEW [%s]';


var
  dmCompareAndModify_: TdmCompareAndModify_;

//function dmCompareAndModify: TdmCompareAndModify;

//===================================================================
implementation {$R *.DFM}
//===================================================================


class procedure TdmCompareAndModify_.Init;
begin
  if not Assigned(dmCompareAndModify) then
    dmCompareAndModify:=TdmCompareAndModify.Create(Application);

end;



procedure TdmCompareAndModify_.OpenData;
begin
  db_TableReOpen(t_Data_Compare, '_Compare');
  db_TableReOpen(t_Data_Compare_, '_Compare_');

end;


//---------------------------------------------------------
procedure TdmCompareAndModify_.DataModuleCreate(Sender: TObject);
//---------------------------------------------------------
begin
  Params.IsModifyFields1 := false;

//  db_SetComponentsADOConn([t_Data], dmMDB.ADOConnection1_);

  TdmMDB.Init;
  TdmExportStructure.Init;

  db_SetComponentADOConnection(Self, dmMDB.ADOConnection1);


  FScript := TStringList.Create();

  DBStructure_SQL := TDBStructure.Create();
  DBStructure_MDB := TDBStructure.Create();
end;


procedure TdmCompareAndModify_.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(DBStructure_MDB);
  FreeAndNil(DBStructure_SQL);
  FreeAndNil(FScript);

  inherited;
end;


//---------------------------------------------------------
function TdmCompareAndModify_.Local_Eq_alex(aStr1, aStr2: string): boolean;
//---------------------------------------------------------
    function DoCutString(aStr: string): string;
    begin
     // aStr := ReplaceStr(aStr, '[', '');
     // aStr := ReplaceStr(aStr, ']', '');
      aStr := Trim(aStr);
      aStr := ReplaceStr(aStr, '(', '');
      aStr := ReplaceStr(aStr, ')', '');
      Result := ReplaceStr(aStr, ' ', '');
    end;
var
  i: Integer;
begin
  aStr1 := Trim(aStr1);
  aStr2 := Trim(aStr2);

  if Pos('trigger', LowerCase(aStr1))>0 then
    i:=0;


  aStr1 := DoCutString(aStr1);
  aStr2 := DoCutString(aStr2);
  Result := CompareText(aStr1,aStr2)=0;

end;



//------------------------------------------------------------------------------------
procedure TdmCompareAndModify_.DoAddToInterface1_(vNameMDB, vNameSQL: variant;
    aAction, aType: string; aTreeID: integer = -1; aTreeParentID: integer = -1;
    aIDMdb: integer = -1; aIDSql: integer = -1; aParentName: string = '');
//------------------------------------------------------------------------------------
var
  iTreeID, iParentID: Integer;
begin
  if aTreeParentID=-1 then
  begin
    if Eq(aType, TYPE_TABLE) then        aTreeParentID:= 1 else
    if Eq(aType, TYPE_VIEW) then         aTreeParentID:= 2 else
    if Eq(aType, TYPE_TRIGGER) then      aTreeParentID:= 3 else
    if Eq(aType, TYPE_PROC) then         aTreeParentID:= 4 else
    if Eq(aType, TYPE_OBJECT_FIELD) then aTreeParentID:= 5 else
    if Eq(aType, TYPE_INDEX) then        aTreeParentID:= 6 else
    if Eq(aType, TYPE_FK) then           aTreeParentID:= 7 else
    if Eq(aType, TYPE_CK) then           aTreeParentID:= 8 else
    Raise Exception.Create('not recognizable type '+aType);
  end;

  Inc(FiTreeID);

  iTreeID  := IIF(aTreeID=-1, FiTreeID, aTreeID);
  iParentID:= aTreeParentID;

 // Result := FiTreeID;

  db_AddRecord (t_Data_Compare,
          [
           db_Par (FLD_NAME_MDB,         vNameMDB),
           db_Par (FLD_NAME_SQL,         vNameSQL),

           db_Par (FLD_ID_MDB,           IIF(aIDMdb=-1, NULL, aIDMdb)),
           db_Par (FLD_ID_SQL,           IIF(aIDSql=-1, NULL, aIDSql)),
           db_Par (FLD_TYPE,             aType),
           db_Par (FLD_ACTION,           aAction),

           db_Par (FLD_DXTREE_ID,        iTreeID),
           db_Par (FLD_DXTREE_PARENT_ID, iParentID),

           db_Par (FLD_PARENT_NAME, IIF(aParentName='',NULL, aParentName))

          ]);

  UpdateError(aAction);
end;


//------------------------------------------------------------------------------------
function TdmCompareAndModify_.DoAddToInterface1_new(aRec: TInterface_rec;
    aNameMDB, aNameSQL: variant; aAction: string; aObjectType: TDBObjectType;
    aUpdate_SQL: string; aTreeParentID: Variant): Integer;
//------------------------------------------------------------------------------------
var
  iTreeID, iParentID: Integer;
begin
  if aTreeParentID=null then
  begin
    case aObjectType of
      otTABLE      :  aTreeParentID:= 1;
      otVIEW       :  aTreeParentID:= 2;
      otTRIGGER    :  aTreeParentID:= 3;
      otStoredProc :  aTreeParentID:= 4;
      otStoredFunc :  aTreeParentID:= 5;
      otIndex      :  aTreeParentID:= 6;
      otFK         :  aTreeParentID:= 7;
      otCK         :  aTreeParentID:= 8;

    end;

(*
    if Eq(aType, TYPE_TABLE) then        aTreeParentID:= 1 else
    if Eq(aType, TYPE_VIEW) then         aTreeParentID:= 2 else
    if Eq(aType, TYPE_TRIGGER) then      aTreeParentID:= 3 else
    if Eq(aType, TYPE_PROC) then         aTreeParentID:= 4 else
    if Eq(aType, TYPE_OBJECT_FIELD) then aTreeParentID:= 5 else
    if Eq(aType, TYPE_INDEX) then        aTreeParentID:= 6 else
    if Eq(aType, TYPE_FK) then           aTreeParentID:= 7 else
    if Eq(aType, TYPE_CK) then           aTreeParentID:= 8
    else
      Raise Exception.Create('not recognizable type '+ aType);
*)

  end;

  Inc(FiTableID_new);

 // iTreeID  := IIF(aTreeID=null, FiTreeID, aTreeID);
 // iParentID:= aTreeParentID;

  Result := FiTableID_new;

  db_AddRecord (t_Data_Compare_,
          [
           db_Par (FLD_NAME_MDB,         aNameMDB),
           db_Par (FLD_NAME_SQL,         aNameSQL),

           db_Par (FLD_TYPE,             ObjectTypeToStr(aObjectType)),
           db_Par (FLD_ACTION,           aAction),

           db_Par (FLD_DXTREE_ID,        FiTableID_new),
           db_Par (FLD_DXTREE_PARENT_ID, aTreeParentID),

           db_Par (FLD_Update_SQL,       aUpdate_SQL)

          // db_Par (FLD_PARENT_NAME, IIF(aParentName='',NULL, aParentName))

          ]);

//  UpdateError(aAction);
end;


//---------------------------------------------------------
function  TdmCompareAndModify_.DoMakeDropSql(aType: string; aDataSet: TDataSet;
        aObjNameToDrop: string = ''): string;
//---------------------------------------------------------
var
  s: string;
  sTblName, sObjName: string;
begin
  if aObjNameToDrop<>'' then
    sObjName:= aObjNameToDrop
  else
    sObjName:= aDataSet[FLD_NAME];

  // -------------------------
  if Eq(aType, TYPE_VIEW) then
    Result:= Format(SQL_DROP_VIEW, [sObjName])
  else

  // -------------------------
  if Eq(aType, TYPE_TRIGGER) then
    Result:= Format(SQL_DROP_TRIGGER, [sObjName])
  else

  // -------------------------
  if Eq(aType, TYPE_PROC) then
  begin
    s:=UpperCase(aDataSet[FLD_CONTENT]);

    if Pos(UpperCase('procedure'), s)>0 then
      Result:= Format('DROP PROCEDURE %s', [sObjName])
    else

    if Pos(UpperCase('function'), s)>0 then
      Result:= Format('DROP FUNCTION %s', [sObjName]);

  end else

  // -------------------------
  if Eq(aType, TYPE_INDEX) then
  begin
    sTblName:= aDataSet[FLD_TABLENAME];

    if aDataSet.FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean or
       aDataSet.FieldByName(FLD_IS_UNIQUE_KEY).AsBoolean
    then
      Result:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [sTblName, sObjName])
    else
      Result:=Format(SQL_DROP_INDEX, [sTblName, sObjName]);
  end else

  // -------------------------
  if Eq(aType, TYPE_FK) or Eq(aType, TYPE_CK) then
  begin
    sTblName:= aDataSet[FLD_TABLENAME];
    Result:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [sTblName, sObjName]);
  end else

    Raise Exception.Create('not recognizable type '+ aType);
end;


//---------------------------------------------------------
function  TdmCompareAndModify_.CreateTable(): boolean;
//---------------------------------------------------------
var 
  sDefault, sPKDefinition: string;
  oFieldArray: TStrArray;

 // bClustered,
  bVariable, bIdentity: boolean;
  s: string;
begin
  Result:= false;

  FModuleName:= 'TdmCompareAndModify.CreateTable';


  //------------------------------
  //  Primary Key
  with dmMDB do
  begin
    FTypeStr  := TYPE_TABLE;
    FDataSet  := tbl_table_fields;

  //  bClustered    := false;
    sPKDefinition := '';

    if tbl_indexes.Locate(FLD_TABLENAME+';'+FLD_IS_PRIMARY_KEY,
                  VarArrayOf([FObjName, true]), [loCaseInsensitive]) then
    begin

      sPKDefinition:=
        Format('ALTER TABLE [%s] ADD                               '+
               'CONSTRAINT [%s] PRIMARY KEY (%s)ON [PRIMARY]', // %s
                [FObjName,
                 tbl_indexes.FieldByName(FLD_NAME).AsString,
                 tbl_indexes.FieldByName(FLD_INDEX_KEYS).AsString

                ]);
    end;
  end;
  //------------------------------

  SetLength(oFieldArray, 0);

  with FDataSet do
  begin
    First;

    while not EOF do
    begin
      bVariable:= (FDataSet[FLD_VARIABLE]) ;
      bIdentity:=  FDataSet[FLD_IDENTITY];

      if FieldByName(FLD_DEFAULT_CONTENT).AsString='' then
        sDefault:= ''
      else
        sDefault:= Format('DEFAULT %s', [FDataSet[FLD_DEFAULT_CONTENT]]);


      FSql:= Format('[%s] %s %s %s %s %s ',
               [FDataSet[FLD_NAME],
                FDataSet[FLD_TYPE],
                IIF (bVariable, Format('(%s)', [FDataSet[FLD_SIZE]]), ' '),
                sDefault,
                IIF (bIdentity,'IDENTITY (1, 1)',' '),
                IIF (FieldByName(FLD_NULLS).AsBoolean, 'NULL', 'NOT NULL')
               ]);


      StrArrayAddValue(oFieldArray, FSql);

      Next;
    end;
  end;

  s := StrArrayToString(oFieldArray, ',');
  FSql := Format('CREATE TABLE [dbo].[%s] ( %s ) ON [PRIMARY]', [FObjName, s]);

  AddSript(FSql);

  Result:= gl_DB.ExecCommandSimple(FSql);

  if not Result then
  begin
    g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
    exit;
  end;

  if Result then
    if sPKDefinition<>'' then
      if not gl_DB.ExecCommandSimple (sPKDefinition) then
        g_Log.AddError(FModuleName, gl_DB.ErrorMsg + sPKDefinition);

end;


//---------------------------------------------------------
function TdmCompareAndModify_.CreateField(aTblName: string): boolean;
//---------------------------------------------------------
var
  sDefault: string;
  bIsColumnTypeVariable: boolean;
begin
  Result:= false;
  gl_DB.ErrorMsg:='';
  
  FModuleName:= 'TdmCompareAndModify.CreateField';

  with dmMDB do
  begin
    if tbl_Table_Fields.FieldByName(FLD_DEFAULT_CONTENT).AsString='' then
      sDefault:= ''
    else
      sDefault:= Format('DEFAULT %s', [tbl_Table_Fields[FLD_DEFAULT_CONTENT]]);

    bIsColumnTypeVariable:= (tbl_Table_Fields[FLD_VARIABLE]);
    // AND
     //                       (tbl_Table_Fields[FLD_USERTYPE] < 250);

    FSql:= Format('ALTER TABLE %s ADD [%s] %s%s %s %s ',
              [aTblName,
               tbl_Table_Fields.FieldByName(FLD_NAME).AsString,
               tbl_Table_Fields.FieldByName(FLD_TYPE).AsString,
               IIF(bIsColumnTypeVariable, '('+tbl_Table_Fields[FLD_SIZE]+')', ' '),
               sDefault,
               IIF(tbl_Table_Fields.FieldByName(FLD_IDENTITY).AsBoolean,
                   'IDENTITY', ' ')
               ] );

    AddSript(FSql);
    Result:= gl_DB.ExecCommandSimple(FSql);

    if not Result then
    begin
      g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
      exit;
    end;


    if (not tbl_Table_Fields.FieldByName(FLD_NULLS).AsBoolean) then
    begin
      FSql:= Format('ALTER TABLE %s ALTER COLUMN [%s] %s%s NOT NULL',
                  [aTblName,
                   tbl_Table_Fields[FLD_NAME],
                   tbl_Table_Fields[FLD_TYPE],
                   IIF (bIsColumnTypeVariable, '('+tbl_Table_Fields[FLD_SIZE]+')', ' ')
                   ]);

      AddSript(FSql);

      if not gl_DB.ExecCommandSimple(FSql) then
        g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
    end;
  end;

end;

//---------------------------------------------------------
function TdmCompareAndModify_.AlterField(aTblName: string): boolean;
//---------------------------------------------------------
// ��������� �������� IDENTITY �� ��������������, �.�.
// ��� ������� ��������� � TSQL, ������� �� ������ ��� �������� ��� ���������� ���
// ��� ��������� ����� �������� ����������:
// - ������� ��������� ��������, �� ��������� IDENTITY ����� ��� ����
// - ����������� ���� ������ �� ���������
// - ��������� ��� �����������, ��������, ����� ��������� ��������
// - ������� ��������� ��������
// - ������������� ��������� - ��������� �� ��� ���������
// - ������������ ��� �����������, ��������, �����
//---------------------------------------------------------
var
  sMdbDef: string;
  bNeedAlterColumn,
  
  bNeedAlterIdentity,

  bNeedAlter_Default_CONTENT, bIsColumnTypeVariable: boolean;
  label label_exit;
begin
  Result:= false;
  gl_DB.ErrorMsg:='';
  FModuleName:= 'TdmCompareAndModify.AlterField';

  gl_DB.BeginTrans;

  FMdbDataSet:= dmMdb.tbl_table_fields;
  FSqlDataSet:= t_TableFields;


  if not (Eq(FSqlDataSet[FLD_NAME],      FMdbDataSet[FLD_NAME])     and
          Eq(FSqlDataSet[FLD_TABLENAME], FMdbDataSet[FLD_TABLENAME]))
  then
    Raise Exception.Create('datasets not sincronized');


  //-----------------------------------------
  // RowGuidColumns


  bNeedAlterColumn:=
       (FSqlDataSet[FLD_TYPE] <>FMdbDataSet[FLD_TYPE])      or
       (FSqlDataSet[FLD_SIZE] <>FMdbDataSet[FLD_SIZE])      or
       (FSqlDataSet[FLD_NULLS]<>FMdbDataSet[FLD_NULLS]);


  bNeedAlter_Default_CONTENT:=
      Local_Eq_alex(FSqlDataSet.FieldByName(FLD_DEFAULT_CONTENT).AsString,
                    FMdbDataSet.FieldByName(FLD_DEFAULT_CONTENT).AsString);


  if bNeedAlterColumn then
    bNeedAlter_Default_CONTENT:= true;


  if bNeedAlterColumn or bNeedAlter_Default_CONTENT then
    if FSqlDataSet.FieldByName(FLD_DEFAULT_NAME).AsString<>'' then
    begin

        FSql:= Format(SQL_ALTER_TABLE_DROP_CONSTRAINT,
                   [FMdbDataSet.FieldByName(FLD_TABLENAME).AsString,
                    FSqlDataSet.FieldByName(FLD_DEFAULT_NAME).AsString
                   ]);

      AddSript(FSql);

      if not gl_DB.ExecCommandSimple(FSql) then
      begin
        g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);
        goto label_exit;
      end;
    end;


  if bNeedAlterColumn then
  begin
    bIsColumnTypeVariable:= (FMdbDataSet[FLD_VARIABLE]);
    // AND
     //                       (FMdbDataSet[FLD_USERTYPE] < 250);

    FSql:= Format('ALTER TABLE [%s] ALTER COLUMN [%s] %s%s %s',
                [aTblName,
                 FMdbDataSet[FLD_NAME],
                 FMdbDataSet[FLD_TYPE],
                 IIF (bIsColumnTypeVariable, '('+FMdbDataSet[FLD_SIZE]+')', ' '),
                 IIF (FMdbDataSet.FieldByName(FLD_NULLS).AsBoolean, 'NULL', 'NOT NULL')
                 ]);

    AddSript(FSql);

    if not gl_DB.ExecCommandSimple(FSql) then
    begin
      g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
      goto label_exit;
    end;


  end;


  if bNeedAlter_Default_CONTENT then
  begin
    sMdbDef:= FMdbDataSet.FieldByName(FLD_DEFAULT_CONTENT).AsString;

    if sMdbDef<>'' then
    begin
        FSql:= Format('ALTER TABLE [%s] ADD DEFAULT %s FOR [%s]',  // CONSTRAINT [%s]
                 [FMdbDataSet[FLD_TABLENAME], // FMdbDataSet[FLD_DEFAULT_NAME],
                  sMdbDef,
                  FMdbDataSet[FLD_NAME] ]);

      AddSript(FSql);

      if not gl_DB.ExecCommandSimple(FSql) then
      begin
        g_Log.AddError(FModuleName, gl_Db.ErrorMsg + FSql);
        goto label_exit;
      end;
    end;
  end;

  Result:= (gl_DB.ErrorMsg='');

label_exit:

  if Result then
    gl_DB.CommitTrans
  else
    gl_DB.RollbackTrans;


  bNeedAlterIdentity:=
       (FSqlDataSet[FLD_IDENTITY] <> FMdbDataSet[FLD_IDENTITY]);

  if bNeedAlterIdentity then
  begin
    db_UpdateRecord(t_Data_Compare, FLD_ERROR,
        '���������� �������� IDENTITY �� ��������������. ���������� � �������������');
    Result:= false;
  end;

end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.UpdateError (aAction: string);
//---------------------------------------------------------
begin
  if (aAction = STR_ERR_DELETE) or
     (aAction = STR_ERR_DEL_WHILE_UPD) or
     (aAction = STR_ERR_ADD_WHILE_UPD) or
     (aAction = STR_ERR_UPDATE) or
     (aAction = STR_ERR_CREATE)
  then
    db_UpdateRecord(t_Data_Compare, FLD_ERROR, gl_DB.ErrorMsg);
end;


//---------------------------------------------------------
procedure TdmCompareAndModify_.AddTablesAndFields_();
//---------------------------------------------------------
var
  oTable_SQL: TDBObject;
  oTable_MDB: TDBObject;


    //---------------------------------------------------------
    procedure DoInsertIndexes(aTreeID: Integer);
    //---------------------------------------------------------
    var
      j: Integer;
      oIndex1: TDBObject;
      oIndex2: TDBObject;

    begin
      for j := 0 to oTable_MDB.Indexes1.Count - 1 do
      begin
        oIndex1:=oTable_MDB.Indexes1[j];
        oIndex2:=oTable_SQL.Indexes1.FindByName(oIndex1.Name);

        if not Assigned(oIndex2) then
        begin
          s:=oIndex1.MakeAddSQL;

          DoAddToInterface1_new(rec, oIndex1.Name, '', STR_CREATE_,  otIndex, s,  aTreeID );
        end else

        if not oIndex1.EqualTo(oIndex2) then
        begin
          s:=oIndex1.MakeAlterSQL;
          DoAddToInterface1_new(rec, oIndex1.Name, oIndex2, STR_ALTER_, otIndex, s,  aTreeID );
        end;

      end;
    end;
    // ---------------------------------------------------------------

    //---------------------------------------------------------
    procedure DoInsertFields(aTreeID: Integer);
    //---------------------------------------------------------
    var
      j: Integer;
      oTableField1: TDBTableField;
      oTableField2: TDBTableField;
      rec: TInterface_rec;

    begin
      for j := 0 to oTable_MDB.TableFields.Count - 1 do
      begin
        oTableField1:=oTable_MDB.TableFields[j];
        oTableField2:=oTable_SQL.TableFields.FindByName(oTableField1.Name);

        if not Assigned(oTableField2) then
        begin
          s:=oTableField1.MakeAddSQL;

          rec.NameMDB := oTableField1.Name;
          rec.NameSQL := '';
          rec.ObjectType :=otTABLEField;
          rec.Action := STR_CREATE_;
          rec.Update_SQL :=s;

//          aTreeID, FiTableID

          DoAddToInterface1_new(rec, oTableField1.Name, '', STR_CREATE_,  otTABLEField, s,  aTreeID );
        end else

        if not oTableField1.EqualTo(oTableField2) then
        begin
          rec.NameMDB := oTableField1.Name;
          rec.NameSQL := oTableField1.Name;
          rec.Action := STR_ALTER_;


          s:=oTableField1.MakeAlterSQL;
          DoAddToInterface1_new(rec, oTableField1.Name, '', STR_ALTER_, otTABLEField, s,  aTreeID );

        end;

      end;
    end;
    // ---------------------------------------------------------------

var
  i: Integer;
  iTreeID: Integer;
  j: Integer;
 // oTable_SQL: TDBObject;
 // oTable_MDB: TDBObject;


  s: string;

  rec: TInterface_rec;

begin
  FiTableID_new:= 10;


  i:=DBStructure_SQL.Tables.Count;
  i:=DBStructure_MDB.Tables.Count;

  for I := 0 to DBStructure_MDB.Tables.Count - 1 do
  begin
    oTable_MDB := DBStructure_MDB.Tables[i];
    oTable_SQL := DBStructure_SQL.Tables.FindByName(oTable_MDB.Name);

    if not Assigned(oTable_SQL) then
    begin
      s:=oTable_MDB.MakeCreateSQL;

      oTable_MDB.Runtime.IsPrimaryKeyUsed := True;

      rec.NameMDB := oTable_MDB.Name;
      rec.NameSQL := '';
      rec.Update_SQL :=s;
      rec.ObjectType :=otTABLE;
      rec.Action := STR_CREATE_;
      rec.TreeID := iTreeID;
      rec.TreeID := FiTableID;

      iTreeID:=DoAddToInterface1_new(rec, oTable_MDB.Name, '', STR_CREATE_,  otTABLE, s,  null);
    end else
    begin

      iTreeID:=DoAddToInterface1_new(rec, oTable_MDB.Name, oTable_MDB.Name, '',  otTABLE, s,  null);

//      if not oTable_MDB.EqualTo(oTable_SQL) then
//      begin
//      end;

      DoInsertFields(iTreeID);
      DoInsertIndexes(iTreeID);


    end;
  end;

end;

// ---------------------------------------------------------------
procedure TdmCompareAndModify_.AddView_1(aObjectType: TDBObjectType);
// ---------------------------------------------------------------
var
  oList_MDB : TDBObjectList;
  oList_SQL : TDBObjectList;

  i: Integer;
  oView_SQL: TDBObject;
  oView_MDB: TDBObject;

  s: string;
  rec: TInterface_rec;
begin
  oList_MDB:=DBStructure_MDB.GetListByObject(aObjectType);
  oList_SQL:=DBStructure_SQL.GetListByObject(aObjectType);



  for I := 0 to oList_MDB.Count - 1 do
  begin
    oView_MDB := oList_MDB[i];
    oView_SQL := oList_SQL.FindByName(oView_MDB.Name);

    if not Assigned(oView_SQL) then
    begin
      s:=oView_MDB.MakeCreateSQL;

      rec.NameMDB := oView_MDB.Name;
      rec.NameSQL := '';

   //   rec.NameMDB := oTableField1.Name;
   //   rec.NameSQL := '';
      rec.ObjectType :=aObjectType;
      rec.Action := STR_CREATE_;
      rec.Update_SQL :=s;


      DoAddToInterface1_new(rec, oView_MDB.Name, '', STR_CREATE_,  aObjectType, s, null );
    end else

    if not oView_MDB.EqualTo(oView_SQL) then
    begin
      s:=oView_MDB.MakeDropSQL + CRLF + oView_MDB.MakeCreateSQL;

      rec.NameMDB := oView_MDB.Name;
      rec.NameSQL := oView_SQL.Name;

      rec.ObjectType :=aObjectType;
      rec.Action := STR_ALTER_;
      rec.Update_SQL :=s;


      DoAddToInterface1_new(rec, oView_MDB.Name, oView_SQL.Name, STR_ALTER_,  aObjectType, s, null );

    end;
  end;
end;



//---------------------------------------------------------
procedure TdmCompareAndModify_.AddTablesAndFields();
//---------------------------------------------------------

  // -------------------------
  function TableFieldDiffers(): boolean;
  // -------------------------
  begin
    with dmMDB do
    begin
      Result:= NOT (
         (tbl_Table_Fields[FLD_TYPE]              = t_TableFields[FLD_TYPE]) AND
         (tbl_Table_Fields[FLD_SIZE]              = t_TableFields[FLD_SIZE]) AND

         Local_Eq_alex(tbl_Table_Fields.FieldByName(FLD_DEFAULT_CONTENT).AsString,
                        t_TableFields.FieldByName(FLD_DEFAULT_CONTENT).AsString) AND

         (tbl_Table_Fields[FLD_IDENTITY]          = t_TableFields[FLD_IDENTITY]) AND

         (tbl_Table_Fields[FLD_NULLS]             = t_TableFields[FLD_NULLS])
         );
    end;
  end;

  // -------------------------

  procedure DoAddTableInfoToInterface(aAction: string; vNameSql: variant);
  begin
    Inc(FiTableID);
    DoAddToInterface1_(FObjName, vNameSql, aAction, TYPE_TABLE, FiTableID);
  end;

  // -------------------------

  procedure DoAddFieldInfoToInterface(aAction: string; vNameSql: variant);
  var
    s: string;
    sTableName: string;
    N: string;
  begin
    if not t_Data_Compare.Locate(FLD_NAME_MDB+';'+FLD_TYPE,
                VarArrayOf([dmMDB.tbl_Tables1[FLD_NAME], TYPE_TABLE]), [loCaseInsensitive] )
    then begin
      Inc(FiTableID);
      DoAddToInterface1_(FObjName, FObjName, '', TYPE_TABLE, FiTableID);
    end;

    sTableName := dmMDB.tbl_Tables1[FLD_NAME];

    DoAddToInterface1_(dmMDB.tbl_Table_Fields[FLD_NAME], vNameSql,
                      aAction, TYPE_FIELD, FiTreeID, FiTableID, -1, -1,
                      sTableName);

//    db_UpdateRecord(t_Data, FLD_PARENT_NAME, dmMDB.tbl_Tables[FLD_NAME]);
  end;

  // -------------------------

begin


  with dmMDB do
  begin
    tbl_Tables1.First;
    FiTableID:= 10;

    while not tbl_Tables1.EOF do
    begin
      DoProgress2(tbl_Tables1.RecNo, tbl_Tables1.RecordCount);

      FiTreeID:= FiTableID + 1;
      FObjName:= tbl_tables1[FLD_NAME];
      FDataSet:= tbl_table_fields;

      tbl_table_fields.Filter:= 'TableName = '+ QuotedStr(tbl_Tables1[FLD_NAME]);
      tbl_table_fields.Filtered:= true;
      tbl_table_fields.First;


      if not t_Tables_sql.Locate(FLD_NAME, tbl_Tables1[FLD_NAME], [loCaseInsensitive]) then
      begin
        if Params.IsModifyFields1 then
        begin
          if CreateTable() then
            DoAddTableInfoToInterface(STR_CREATE_DONE, FObjName)
          else
            DoAddTableInfoToInterface(STR_ERR_CREATE, NULL);
        end else
          DoAddTableInfoToInterface(STR_CREATE, NULL);

      end else
      begin
        while not tbl_table_fields.EOF do
        begin
          if not (t_TableFields.Locate(FLD_TABLENAME+';'+FLD_NAME,
              VarArrayOf([ dmMDB.tbl_Tables1[FLD_NAME], dmMDB.tbl_Table_Fields[FLD_NAME] ]),
                      [loCaseInsensitive]))
          then
          begin
            if Params.IsModifyFields1 then
            begin
              if CreateField(tbl_Tables1[FLD_NAME]) then
                DoAddFieldInfoToInterface(STR_CREATE_DONE, tbl_Table_Fields[FLD_NAME])
              else
                DoAddFieldInfoToInterface(STR_ERR_CREATE, NULL);
            end else
              DoAddFieldInfoToInterface(STR_CREATE, NULL);

          end else

          begin
            if TableFieldDiffers() then
              if Params.IsModifyFields1 then
              begin
                if AlterField(FObjName) then
                  DoAddFieldInfoToInterface(STR_UPDATE_DONE, tbl_Table_Fields[FLD_NAME])
                else
                  DoAddFieldInfoToInterface(STR_ERR_UPDATE, tbl_Table_Fields[FLD_NAME]);
              end else
                DoAddFieldInfoToInterface(STR_UPDATE, tbl_Table_Fields[FLD_NAME]);
          end;

          tbl_table_fields.Next;
        end;

      end;

      Inc(FiTableID);
      FiTableID:= FiTreeID;
      tbl_Tables1.Next;
    end;
//////////  end;

    if Assigned(FDataSet) then
      FDataSet.Filtered:= false;

  end;    // with
end;

//---------------------------------------------------------
function TdmCompareAndModify_.GetErrorType(aObjType: string): TErrorType;
//---------------------------------------------------------
begin
  if Eq_Any(aObjType, [TYPE_TABLE
                      ,TYPE_FIELD
                      ,TYPE_PROC
                      ,TYPE_TRIGGER
                      ,TYPE_VIEW
                      ])
  then
    Result:= etCriticalError
  else
    Result:= etWarning;

(*  if Eq_Any(aObjType, [TYPE_INDEX
                      ,TYPE_FK
                      ,TYPE_CK
                      ])
  then
    Result:= etWarning;
*)

end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.DoSimpleAddObjects(aCheckTableName: boolean);
//---------------------------------------------------------
begin
  with FMdbDataSet do
  begin
    First;
    while not EOF do
    begin
      if Eq_Any(FTypeStr, [TYPE_VIEW, TYPE_PROC, TYPE_TABLE]) then
      begin
        FTblName:= FMdbDataSet.FieldByName(FLD_NAME).AsString;


      end else
      begin
        FTblName:= FMdbDataSet.FieldByName(FLD_TABLENAME).AsString;


      end;

      if Eq_Any(FTypeStr, [TYPE_VIEW, TYPE_PROC, TYPE_TABLE]) then
        FExists:= FSqlDataSet.Locate(FLD_NAME, FMdbDataSet[FLD_NAME], [loCaseInsensitive])
      else
        FExists:= FSqlDataSet.Locate(FLD_NAME+';'+FLD_TABLENAME,
                    VarArrayOf([FMdbDataSet[FLD_NAME], FTblName]), [loCaseInsensitive]);

      FDiffers:= not Local_Eq_alex(AsString(FSqlDataSet[FLD_CONTENT]), AsString(FMdbDataSet[FLD_CONTENT]));

      if FExists and (not FDiffers) then
      begin
        Next;
        continue;
      end;

      DoSimpleAddObject();

      Next;
    end;
  end;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.DoSimpleAddObject();
//---------------------------------------------------------
var
  sTblName: string;
  label exit_lb;
begin
  FMdbObjName:= FMdbDataSet.FieldByName(FLD_NAME).AsString;
  FSqlObjName:= FSqlDataSet.FieldByName(FLD_NAME).AsString;


  if Params.IsModifyFields1 then
  begin

    gl_DB.ErrorMsg:= '';

    if FExists then
    begin
      if (FTypeStr=TYPE_INDEX) and
         (FMdbDataSet.FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean or
          FMdbDataSet.FieldByName(FLD_IS_UNIQUE_KEY).AsBoolean)
      then begin
        sTblName:= FMdbDataSet.FieldByName(FLD_TABLENAME).AsString;

        FSql:= DoMakeDropSql(FTypeStr, FMdbDataSet, FSqlObjName); //sTblName);
      end else
        FSql:= DoMakeDropSql(FTypeStr, FMdbDataSet);

      AddSript(FSql);

      if not gl_DB.ExecCommandSimple(FSql) then
      begin
        DoAddToInterface1_(NULL, FSqlObjName, STR_ERR_DEL_WHILE_UPD, FTypeStr);
        g_Log.AddError(FModuleName, gl_DB.ErrorMsg+ FSql);//, GetErrorType(FTypeStr));

        goto exit_lb;
      end;
    end;

    FSql:= FMdbDataSet[FLD_CONTENT];

    AddSript(FSql);

    if not gl_DB.ExecCommandSimple(FSql) then
    begin
      DoAddToInterface1_(NULL, FSqlObjName, STR_ERR_ADD_WHILE_UPD, FTypeStr);
      g_Log.AddError(FModuleName, gl_DB.ErrorMsg+ FSql);//, GetErrorType(FTypeStr));
      goto exit_lb;
    end else
      DoAddToInterface1_(FMdbObjName, FMdbObjName,
          IIF(FExists, STR_UPDATED, STR_CREATE_DONE), FTypeStr);

  end else
    DoAddToInterface1_(FMdbObjName, IIF(FExists, FSqlObjName, NULL),
                     IIF(FExists, STR_UPDATE, STR_CREATE), FTypeStr);

exit_lb:

  if Params.IsModifyFields1 then
  begin
    if gl_DB.ErrorMsg='' then
      gl_DB.CommitTrans
    else
      gl_DB.RollbackTrans;
  end;

end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.AddViews();
//---------------------------------------------------------
begin
  FMdbDataSet:= dmMDB.tbl_Views;
  FSqlDataSet:= t_Views_sql;
  FModuleName:= 'TdmCompareAndModify.AddViews';
  FTypeStr   := TYPE_VIEW;

  DoSimpleAddObjects;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.AddStoredProcs();
//---------------------------------------------------------
begin
  FMdbDataSet:= dmMDB.tbl_StoredProcs;
  FSqlDataSet:= t_StoredProcs_sql;
  FTypeStr      := TYPE_PROC;
  FModuleName:= 'TdmCompareAndModify.AddStoredProcs';

  Assert(FMdbDataSet.Active);
  Assert(FSqlDataSet.Active);


  DoSimpleAddObjects; //(false)
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.DoSimpleDropObjects();
//---------------------------------------------------------
begin
  FSqlDataSet.First;

  while not FSqlDataSet.EOF do
  begin
    FSqlObjName:= FSqlDataSet[FLD_NAME];

    FTblName:= FSqlDataSet.FieldByName(FLD_TABLENAME).AsString;

    // ���� ��������� ���� ��� ���������� ������ - �� �������
    if FTypeStr=TYPE_INDEX then
      if (FSqlDataSet.FieldByName(FLD_IS_PRIMARY_KEY).AsBoolean) or
         (FSqlDataSet.FieldByName(FLD_IS_UNIQUE_KEY).AsBoolean) then
      begin
        FSqlDataSet.Next;
        continue;
      end;

    FExists:= FMdbDataSet.Locate(FLD_NAME+';'+FLD_TABLENAME,
                VarArrayOf([FSqlObjName, FTblName]), [loCaseInsensitive]);

    if FExists then
    begin
      FSqlDataSet.Next;
      continue;
    end;

    if FTypeStr=TYPE_INDEX then
    begin
      if FSqlDataSet.FieldByName(FLD_IS_UNIQUE_KEY).AsBoolean then
        FSql:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [FSqlDataSet[FLD_TABLENAME], FSqlObjName])
      else
        FSql:=Format(SQL_DROP_INDEX, [FSqlDataSet[FLD_TABLENAME], FSqlObjName]);
    end else


    if FTypeStr=TYPE_CK then
      FSql:=Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [FSqlDataSet[FLD_TABLENAME], FSqlObjName])
    else

    if FTypeStr=TYPE_TRIGGER then
      FSql:=Format(SQL_DROP_TRIGGER, [FSqlObjName]);

    DoSimpleDropObject (FSql, FExists);

    FSqlDataSet.Next;
  end;
end;

//----------------------------------------------------------------------------------
procedure TdmCompareAndModify_.DoSimpleDropObject(aSql: string; aExists:
    boolean);
//----------------------------------------------------------------------------------
begin
  if aExists then
    exit;

  if Params.IsModifyFields1 then
  begin
    if gl_DB.ExecCommandSimple(aSql) then
      DoAddToInterface1_(NULL, FSqlObjName, STR_DELETED, FTypeStr)
    else begin
      DoAddToInterface1_(NULL, FSqlObjName, STR_ERR_DELETE, FTypeStr);
//      cus_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, aSql);
      g_Log.AddError(FModuleName, gl_DB.ErrorMsg+ aSql);//, GetErrorType(FTypeStr));
    end;

  end else
    DoAddToInterface1_(NULL, FSqlObjName, STR_DEL, FTypeStr);
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.AddTriggers();
//---------------------------------------------------------
begin
  FMdbDataSet:= dmMDB.tbl_triggers;
  FSqlDataSet:= t_triggers;
  FTypeStr      := TYPE_TRIGGER;
  FModuleName:= 'TdmCompareAndModify.AddTriggers';

  DoSimpleDropObjects;
  DoSimpleAddObjects;
end;

//----------------------------------------------------------------------------------
procedure TdmCompareAndModify_.AddIndexes;
//----------------------------------------------------------------------------------
begin
  FMdbDataSet:= dmMDB.tbl_indexes;
  FSqlDataSet:= t_indexes;
  FTypeStr      := TYPE_INDEX;
  FModuleName:= 'TdmCompareAndModify.AddIndexes';

  FMdbDataSet.Filtered:= true;

  Assert(FMdbDataSet.Active);
  Assert(FSqlDataSet.Active);

  //---------------------------------------------
  // primary keys
  FMdbDataSet.Filter:= 'is_primary_key = true';

  with FMdbDataSet do
  begin
    First;

    while not EOF do
    begin
      FTblName:= FMdbDataSet.FieldByName(FLD_TABLENAME).AsString;

      FExists:= FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_IS_PRIMARY_KEY,
                  VarArrayOf([FMdbDataSet[FLD_TABLENAME], true]),
                                   [loCaseInsensitive]);
      FDiffers:= not (
       //  (FSqlDataSet[FLD_IS_CLUSTERED] = FMdbDataSet[FLD_IS_CLUSTERED]) AND
         (FSqlDataSet[FLD_INDEX_KEYS]   = FMdbDataSet[FLD_INDEX_KEYS]) ); // AND
        // (FSqlDataSet[FLD_FILL_FACTOR]  = FMdbDataSet[FLD_FILL_FACTOR])  );

      if FExists and (not FDiffers) then
      begin
        Next;
        continue;
      end;

      DoSimpleAddObject;

      Next;
    end;
  end;
  //---------------------------------------------



  //---------------------------------------------
  // indexes
  FMdbDataSet.Filter:= 'is_unique_key = true';

  FSqlDataSet.First;
  while not FSqlDataSet.EOF do
  begin
    FSqlObjName:= FSqlDataSet[FLD_NAME];

    FTblName:= FMdbDataSet.FieldByName(FLD_TABLENAME).AsString;

    if not FSqlDataSet[FLD_IS_UNIQUE_KEY] then
    begin
      FSqlDataSet.Next;
      continue;
    end;

    FExists:= FMdbDataSet.Locate(FLD_TABLENAME+';'+FLD_IS_UNIQUE_KEY+';'+FLD_INDEX_KEYS,
                 VarArrayOf([FSqlDataSet[FLD_TABLENAME], true, FSqlDataSet[FLD_INDEX_KEYS]]),
                                   [loCaseInsensitive]);

    if FExists then
    begin
      FSqlDataSet.Next;
      continue;
    end;

    FSql:= Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [FSqlDataSet[FLD_TABLENAME], FSqlObjName]);

    DoSimpleDropObject(FSql, FExists);

    FSqlDataSet.Next;
  end;


  FMdbDataSet.First;
  while not FMdbDataSet.EOF do
  begin
    FTblName:= FMdbDataSet.FieldByName(FLD_TABLENAME).AsString;

    FExists:= FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_IS_UNIQUE_KEY+';'+FLD_INDEX_KEYS,
               VarArrayOf([FMdbDataSet[FLD_TABLENAME], true, FMdbDataSet[FLD_INDEX_KEYS]]),
                                 [loCaseInsensitive]);
  //  FDiffers:= not (
    //   (FSqlDataSet[FLD_IS_CLUSTERED] = FMdbDataSet[FLD_IS_CLUSTERED])
    //    );

    //   AND
     //  (FSqlDataSet[FLD_FILL_FACTOR]  = FMdbDataSet[FLD_FILL_FACTOR])  );

    if FExists  then //and (not FDiffers)
    begin
      FMdbDataSet.Next;
      continue;
    end;

    DoSimpleAddObject;

    FMdbDataSet.Next;
  end;
  //---------------------------------------------


  //---------------------------------------------
  // indexes
  FMdbDataSet.Filter:= '(is_primary_key = false) and (is_unique_key = false)';
  DoSimpleDropObjects();
  DoSimpleAddObjects;
  //---------------------------------------------


  FMdbDataSet.Filtered:= false;
end;


//---Check constraints--------------------------------------------------------------------
procedure TdmCompareAndModify_.AddCK;
//------------------------------------------------------------------------------------
begin
  FMdbDataSet:= dmMDB.tbl_CK;
  FSqlDataSet:= t_CK_sql;
  FTypeStr      := TYPE_CK;
  FModuleName:= 'TdmCompareAndModify.AddCK';

  Assert(FMdbDataSet.Active);
  Assert(FSqlDataSet.Active);

  DoSimpleDropObjects();

  DoSimpleAddObjects();
end;


//---Foreign Keys--------------------------------------------------------------------
procedure TdmCompareAndModify_.AddFK;
//------------------------------------------------------------------------------------
var
//  bCommit: boolean;
  sTableName: string;
  oTablesDataset: TDataSet;

    //---------------------------------------------
    procedure DoCompare ();
    //---------------------------------------------
    begin
      // ����� ��������� FK
      FSqlDataSet.First;
      while not FSqlDataSet.EOF do
      begin
        FTblName:= FSqlDataSet.FieldByName(FLD_TABLENAME).AsString;

        if (not FMdbDataSet.Locate(FLD_NAME, FSqlDataSet[FLD_NAME], [loCaseInsensitive])) then
          DoAddToInterface1_(NULL, FSqlDataSet[FLD_NAME], STR_DEL, TYPE_FK);

        FSqlDataSet.Next;
      end;

      //����� ����������� � ����������� FK
      FMdbDataSet.First;
      while not FMdbDataSet.EOF do
      begin
        FTblName:= FMdbDataSet.FieldByName(FLD_TABLENAME).AsString;


        FExists:= FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_NAME,
                      VarArrayOf([FMdbDataSet[FLD_TABLENAME],
                                  FMdbDataSet[FLD_NAME]]),
                                 [loCaseInsensitive]);

        FDiffers:= not Eq(AsString(FSqlDataSet[FLD_CONTENT]),
                          AsString(FMdbDataSet[FLD_CONTENT]));

        if FExists and (not FDiffers) then
        begin
          FMdbDataSet.Next;
          continue;
        end;

        DoAddToInterface1_(FMdbDataSet[FLD_NAME], IIF(FExists, FSqlDataSet[FLD_NAME], NULL),
              IIF(FExists, STR_UPDATE, STR_CREATE), TYPE_FK);

        FMdbDataSet.Next;
      end;
    end;


begin
  FMdbDataSet   := dmMDB.tbl_FK;
  FSqlDataSet   := t_FK;
  FTypeStr         := TYPE_FK;
  FModuleName   := 'TdmCompareAndModify.AddFK';

  Assert(FMdbDataSet.Active);
  Assert(FSqlDataSet.Active);


  oTablesDataset:= t_Tables_sql;

  FMdbDataSet.Filtered:=false;


  if not Params.IsModifyFields1 then
    DoCompare
  else

  // �������� ��� ������� ������ �������������� ------------------------------
  begin
    oTablesDataset.First;

    while not oTablesDataset.Eof do
    begin
      FTblName:= oTablesDataset.FieldByName(FLD_NAME).AsString;


      FMdbDataSet.Filter:=FLD_TABLENAME+' = '+QuotedStr(oTablesDataset[FLD_NAME]);
      FMdbDataSet.Filtered:=true;

      if FMdbDataSet.RecordCount=0 then
      begin
        oTablesDataset.Next;
        Continue;
      end;

//      gl_DB.BeginTrans;
//      bCommit:=true;


      //���������� � ����������� ������� ������
      FMdbDataSet.First;

      while not FMdbDataSet.EOF do
      begin
        FExists:= FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_NAME,
                       VarArrayOf([FMdbDataSet[FLD_TABLENAME],
                                   FMdbDataSet[FLD_NAME]]),
                                   [loCaseInsensitive]);

        FDiffers:= not Eq(AsString(FSqlDataSet[FLD_CONTENT]),
                          AsString(FMdbDataSet[FLD_CONTENT]));

        if FExists and (not FDiffers) then
        begin
          FMdbDataSet.Next;
          continue;
        end;

        FMdbObjName:= FMdbDataSet[FLD_NAME];
        FSqlObjName:= FSqlDataSet[FLD_NAME];

//        bCommit:=true;
        if FExists then
        begin
          FSql:= Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [FMdbDataSet[FLD_TABLENAME], FMdbObjName]);

          AddSript(FSql);

          if not gl_DB.ExecCommandSimple(FSql) then
          begin
            DoAddToInterface1_(NULL, FSqlObjName, STR_ERR_DELETE, FTypeStr);

            g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);// GetErrorType(FTypeStr));
//              cus_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
//              bCommit:=false;
            break;
          end;
        end;

        if not gl_DB.ObjectExists(FMdbObjName) then
        begin
          FSql:=FMdbDataSet[FLD_CONTENT];

          AddSript(FSql);

          if gl_DB.ExecCommandSimple(FSql) then
          begin
            DoAddToInterface1_(FMdbObjName, FMdbObjName, IIF(FExists, STR_UPDATED, STR_CREATED), FTypeStr);
          end else
          begin
            DoAddToInterface1_(NULL, FSqlObjName, STR_ERR_ADD_WHILE_UPD, FTypeStr);
//            cus_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
            g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);//, GetErrorType(FTypeStr));
//            bCommit:=false;
            break;
          end;
        end;

        FMdbDataSet.Next;
      end;


(*
      if not bCommit then
      begin
        gl_DB.RollbackTrans;
        oTablesDataset.Next;
        continue;
      end;*)

      //�������� ������� ������
      FSqlDataSet.First;
      while not FSqlDataSet.EOF do
      begin
        if FSqlDataSet[FLD_TABLENAME]<>FMdbDataSet[FLD_TABLENAME] then
        begin
          FSqlDataSet.Next;
          continue;
        end;

//        bCommit:= true;
        sTableName:= FSqlDataSet[FLD_TABLENAME];
        FSqlObjName:= FSqlDataSet[FLD_NAME];

        if not FMdbDataSet.Locate(FLD_NAME, FSqlObjName, [loCaseInsensitive]) then
        begin
          FSql:= Format(SQL_ALTER_TABLE_DROP_CONSTRAINT, [sTableName, FSqlObjName]);

          AddSript(FSql);

          if gl_DB.ExecCommandSimple(FSql) then
            DoAddToInterface1_(NULL, FSqlObjName, STR_DELETED, FTypeStr)
          else
          begin
            DoAddToInterface1_(NULL, FSqlObjName, STR_ERR_DELETE, FTypeStr);
             g_Log.AddError(FModuleName, gl_DB.ErrorMsg + FSql);//, GetErrorType(FTypeStr));
//            cus_ErrorLog.AddExceptionWithSQL(FModuleName, gl_DB.ErrorMsg, FSql);
//            bCommit:=false;
            break;
          end;
        end;

        FSqlDataSet.Next;
      end;

(*
      if not bCommit then
      begin
        gl_DB.RollbackTrans;
        FSqlDataSet.Next;
        continue;
      end;
*)

//      gl_DB.CommitTrans;

      oTablesDataset.Next;
    end;

  end;
end;


procedure TdmCompareAndModify_.AddSript(aSQL: string);
begin
  FScript.Add(aSQL);
  g_Log.Add(aSql);
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.DoApplyDataset(aType: string);
//---------------------------------------------------------
begin
  with  dmMDB do
  begin
    if (aType = TYPE_VIEW) then
    begin
      FMdbDataSet:= tbl_Views;
      FSqlDataSet:= t_Views_sql;
    end else

    if (aType = TYPE_PROC) then
    begin
      FMdbDataSet:= tbl_StoredProcs;
      FSqlDataSet:= t_StoredProcs_sql;
    end else

    if (aType = TYPE_TRIGGER) then
    begin
      FMdbDataSet:= tbl_Triggers;
      FSqlDataSet:= t_Triggers;
    end else

    if (aType = TYPE_FK) then
    begin
      FMdbDataSet:= tbl_Fk;
      FSqlDataSet:= t_FK;
    end else

    if (aType = TYPE_CK) then begin
      FMdbDataSet:= tbl_Ck;
      FSqlDataSet:= t_CK_sql;
    end else

    if (aType = TYPE_FIELD) then begin
      FMdbDataSet:= tbl_Table_Fields;
      FSqlDataSet:= t_TableFields;
    end else

    if (aType = TYPE_INDEX) then begin
      FMdbDataSet:= tbl_indexes;
      FSqlDataSet:= t_Indexes;
    end;
  end;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.DlgCompareSQL();
//---------------------------------------------------------
var
  s: string;
  sDEFAULT_CONTENT: string;
  sServSql, sMdbSql, sObjNameMdb, sObjNameSql: string;
begin
  FTypeStr:= t_Data_Compare.FieldByName(FLD_TYPE).AsString;

  with  dmMDB do
  begin
    DoApplyDataset(t_Data_Compare.FieldByName(FLD_TYPE).AsString);

    //-----------------------------------------------
    if (FTypeStr = TYPE_VIEW) or
       (FTypeStr = TYPE_PROC) or
       (FTypeStr = TYPE_TRIGGER) or
       (FTypeStr = TYPE_INDEX) or
       (FTypeStr = TYPE_FK) or
       (FTypeStr = TYPE_CK)
    //-----------------------------------------------
    then begin
      FMdbDataSet.Filtered:= false;

      sObjNameMdb:= t_Data_Compare.FieldByName(FLD_NAME_MDB).AsString;
      sObjNameSql:= t_Data_Compare.FieldByName(FLD_NAME_SQL).AsString;

      sMdbSql := '';
      sServSql:= '';

      if FMdbDataSet.Locate(FLD_NAME, sObjNameMdb, [loCaseInsensitive]) then
        sMdbSql := FMdbDataSet.FieldByName(FLD_CONTENT).AsString;

      if FSqlDataSet.Locate(FLD_NAME, sObjNameSql, [loCaseInsensitive]) then
        sServSql:= FSqlDataSet.FieldByName(FLD_CONTENT).AsString;


      Tdlg_CompareSQL.ExecDlg(Trim(sMdbSql), Trim(sServSql));

    end else

    //-----------------------------------------------
    if (FTypeStr = TYPE_FIELD) then begin
    //-----------------------------------------------
      FMdbDataSet.Filtered:= false;

      s:=t_Data_Compare[FLD_PARENT_NAME];

      if FMdbDataSet.Locate(FLD_TABLENAME+';'+FLD_NAME,
            VarArrayOf([t_Data_Compare[FLD_PARENT_NAME], t_Data_Compare[FLD_NAME_MDB]]), [loCaseInsensitive]) then

       if FSqlDataSet.Locate(FLD_TABLENAME+';'+FLD_NAME,
         VarArrayOf([t_Data_Compare[FLD_PARENT_NAME], t_Data_Compare[FLD_NAME_MDB]]), [loCaseInsensitive]) then
      begin
     //   sDEFAULT_CONTENT:=FSqlDataSet[FLD_DEFAULT_CONTENT];

        sServSql:=
          Format('���: %s' +#13#10+
                 '������: %s' +#13#10+
                 'Default_content: %s' +#13#10 +
                 'Nulls: %s' +#13#10+
                 'IDENTITY: %s',
                  [FSqlDataSet[FLD_TYPE],
                   FSqlDataSet[FLD_SIZE],
                   FSqlDataSet[FLD_DEFAULT_CONTENT],
                   AsString(FSqlDataSet[FLD_NULLS]),

                   AsString(FSqlDataSet[FLD_IDENTITY])
                  ] );

        sMdbSql:=
          Format('���: %s' +#13#10+
                 '������: %s'+ #13#10+
                 'Default_content: %s'+ #13#10+
                 'Nulls: %s'+ #13#10+
                 'IDENTITY: %s',
                  [FMdbDataSet[FLD_TYPE],
                   FMdbDataSet[FLD_SIZE],
                   FMdbDataSet[FLD_DEFAULT_CONTENT],
                   AsString(FMdbDataSet[FLD_NULLS]),

                   AsString(FMdbDataSet[FLD_IDENTITY])
                  ] );

        Tdlg_CompareSQL.ExecDlg(sMdbSql, sServSql);
      end;
    end else

    //-----------------------------------------------
    if FTypeStr = TYPE_OBJECT_FIELD then begin
    //-----------------------------------------------
(*      if FMdbDataSet.Locate(FLD_ID, mem_Data.FieldByName(FLD_ID_MDB).AsInteger, []) AND
         FSqlDataSet.Locate(FLD_ID, mem_Data.FieldByName(FLD_ID_SQL).AsInteger, [])
      then
        db_View(qry_object_fields_Mdb, qry_object_fields);*)

      qry_object_fields_Mdb.Filtered:= true;
      qry_object_fields_Mdb.Filter:= 'id = '+ t_Data_Compare.FieldByName(FLD_ID_MDB).AsString;

      qry_object_fields.Filtered:= true;
      qry_object_fields.Filter:= 'id = '+ t_Data_Compare.FieldByName(FLD_ID_SQL).AsString;

   //   db_View(qry_object_fields_Mdb, qry_object_fields);

      qry_object_fields_Mdb.Filtered:= false;
      qry_object_fields.Filtered:= false;
    end;

  end;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.UpdateAction_();
//---------------------------------------------------------
var
  sAction: string;
begin
  FTypeStr:= t_Data_Compare.FieldByName(FLD_TYPE).AsString;
  sAction := t_Data_Compare.FieldByName(FLD_ACTION).AsString;

  if (sAction=STR_CREATE) or
     (sAction=STR_ADD)
  then
    FProcessAction:= paAdd
  else

  if (sAction=STR_UPDATE) then
    FProcessAction:= paUpdate
  else

  if (sAction=STR_DEL) then
    FProcessAction:= paDel
  else

  if (sAction='') then
    FProcessAction:= paNone
  else
    Raise Exception.Create('not recognizible action: '+sAction);

  case FProcessAction of
    paAdd,
    paUpdate: FObjName:= t_Data_Compare.FieldByName(FLD_NAME_MDB).AsString;

    paDel:    FObjName:= t_Data_Compare.FieldByName(FLD_NAME_SQL).AsString;
  end;

  FMdbObjName:= t_Data_Compare.FieldByName(FLD_NAME_MDB).AsString;
  FSqlObjName:= t_Data_Compare.FieldByName(FLD_NAME_SQL).AsString;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.CreateObject();
//---------------------------------------------------------

  function DoDelObject(): boolean;
  begin
    FSql:= DoMakeDropSql(FTypeStr, FSqlDataSet);
    AddSript(FSql);
    Result:= gl_DB.ExecCommandSimple(FSql);

//    AddSc
  end;

  function DoAddObject(): boolean;
  begin
    FSql:= FMdbDataSet[FLD_CONTENT];
    AddSript(FSql);

    Result:= gl_DB.ExecCommandSimple(FSql);
  end;

var
  sParentName: string;
begin
  UpdateAction_();

  FModuleName:= 'TdmCompareAndModify.CreateObject.'+ FTypeStr;

  //-----------------------------------------------
  if FTypeStr = TYPE_TABLE then
  //-----------------------------------------------
  begin
    if FProcessAction = paNone then
      ShowMessage('�������� �� ����������');

    if FProcessAction in [paUpdate, paDel] then
      ShowMessage('�������� �� ��������������');

    if not (FProcessAction in [paAdd, paUpdate, paDel]) then
      Raise Exception.Create('��� �������� �� �������: ' + AsString(FProcessAction));


    if FProcessAction=paAdd then
    begin
      FDataSet:= dmMDB.tbl_table_fields;

      FDataSet.Filtered:= false;
      FDataSet.Filter:= 'TableName = '+ QuotedStr(FObjName);
      FDataSet.Filtered:= true;
      FDataSet.First;

      if CreateTable() then
        t_Data_Compare.Delete
      else
        ErrDlg(Format(ERROR_MSG_LOCAL, [FObjName, FTypeStr, gl_DB.ErrorMsg, FSql]));

      FDataSet.Filtered:= false;
    end;

  end else

  //-----------------------------------------------
  if (FTypeStr = TYPE_PROC) or
     (FTypeStr = TYPE_TRIGGER) or
     (FTypeStr = TYPE_VIEW) or
     (FTypeStr = TYPE_FK) or
     (FTypeStr = TYPE_CK) or
     (FTypeStr = TYPE_INDEX)
  //-----------------------------------------------
  then begin
    if not (FProcessAction in [paAdd, paUpdate, paDel]) then
      Raise Exception.Create('��� �������� �� �������: ' +AsString(FProcessAction));

    DoApplyDataSet(FTypeStr);

    case FProcessAction of
      paDel: if not FSqlDataSet.Locate(FLD_NAME, FSqlObjName, [loCaseInsensitive]) then
               Raise Exception.Create(FModuleName+' must locate FSqlDataSet.Locate');

      paAdd: if not FMdbDataSet.Locate(FLD_NAME, FMdbObjName, [loCaseInsensitive]) then
               Raise Exception.Create(FModuleName+' must locate FMdbDataSet.Locate');

      paUpdate: begin
                  if not FSqlDataSet.Locate(FLD_NAME, FSqlObjName, [loCaseInsensitive]) then
                    Raise Exception.Create(FModuleName+' must locate FSqlDataSet.Locate');

                  if not FMdbDataSet.Locate(FLD_NAME, FMdbObjName, [loCaseInsensitive]) then
                    Raise Exception.Create(FModuleName+' must locate FMdbDataSet.Locate');
                end;
    end;

    gl_DB.ErrorMsg:= '';

    case FProcessAction of
      paDel: if DoDelObject() then
               t_Data_Compare.Delete;

      paAdd: if DoAddObject() then
               t_Data_Compare.Delete;

      paUpdate: begin
                  gl_DB.BeginTrans;
                  if DoDelObject() then
                    if DoAddObject() then
                    begin
                      t_Data_Compare.Delete;
                      gl_DB.CommitTrans;
                    end;
                  gl_DB.RollbackTrans;
                end;
    end;

    if gl_DB.ErrorMsg<>'' then
    begin
      ErrDlg(Format(ERROR_MSG_LOCAL, [FObjName, FTypeStr, gl_DB.ErrorMsg, FSql]));

      if (FProcessAction in [paAdd, paUpdate]) and (FTypeStr=TYPE_FK) then
      begin
        ShowMessage(FMdbDataSet[FLD_INTEGRITY_SQL]);

        Clipboard.Clear;
        Clipboard.SetTextBuf(PChar(FMdbDataSet.FieldByName(FLD_INTEGRITY_SQL).AsString));
      end;
//      cus_ErrorLog.AddError(FModuleName, gl_DB.ErrorMsg);
    end;
  end else

  //-----------------------------------------------
  if FTypeStr = TYPE_FIELD then
  //-----------------------------------------------
  begin
    sParentName:= t_Data_Compare.FieldByName(FLD_PARENT_NAME).AsString;
    
    if sParentName='' then
      Raise Exception.Create('sParentName=''''');

    if dmMDB.tbl_table_fields.Locate(FLD_TABLENAME+';'+FLD_NAME,
                       VarArrayOf([sParentName, FObjName]), [loCaseInsensitive]) then
    begin
      // -------------------------
      case FProcessAction of

        paAdd:
          if not CreateField(sParentName) then
            ErrDlg(Format(ERROR_MSG_LOCAL, [FObjName, FTypeStr, gl_DB.ErrorMsg, FSql]))
          else
            t_Data_Compare.Delete;

        paUpdate:
          if t_tablefields.Locate(FLD_TABLENAME+';'+FLD_NAME,
                       VarArrayOf([sParentName, FObjName]), [loCaseInsensitive]) then
            if not AlterField(sParentName) then
            begin
              if gl_DB.ErrorMsg<>'' then
                ErrDlg(Format(ERROR_MSG_LOCAL, [FObjName, FTypeStr, gl_DB.ErrorMsg, FSql]))
            end else
              t_Data_Compare.Delete;
      end;

    end;
  end

end;


//---------------------------------------------------------
procedure TdmCompareAndModify_.RefreshViews1;
//---------------------------------------------------------
begin
  CursorHourGlass;
 // gl_DB.UpdateViews(cus_ErrorLog);
  CursorDefault;
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_.ExecuteProc ();
//---------------------------------------------------------

  procedure DoAddRoot (sName: string; iInd: Integer);
  begin
    db_AddRecord (t_Data_Compare,
     [db_Par (FLD_NAME_MDB,             sName),
      db_Par (FLD_DXTREE_ID,            iInd),
      db_Par (FLD_DXTREE_PARENT_ID,     -1)   ]);
  end;

  procedure DoAddRoot1 (sName: string; iInd: Integer);
  begin
    db_AddRecord (t_Data_Compare_,
     [db_Par (FLD_NAME_MDB,             sName),
      db_Par (FLD_DXTREE_ID,            iInd),
      db_Par (FLD_DXTREE_PARENT_ID,     Null)   ]);
  end;



begin
  FScript.Clear;


(*  TdmLogin.Init;
  if not dmLogin.OpenMaster_(Params.LoginRec, dmMain_SQL.ADOConnection1) then
    Exit;

*)

  t_Data_Compare.DisableControls;

  dmMdb.EmptyTable('_Compare');
  dmMdb.EmptyTable('_Compare_');

  db_TableReOpen(t_Data_Compare,  '_Compare');
  db_TableReOpen(t_Data_Compare_, '_Compare_');



//  db_Clear(t_Data);

  Params.UpdateStructure:=True;

 // UpdateStructure:=
  //  AsBoolean(dmMdb.MDB.GetFieldValue('Options', FLD_VALUE, [db_Par(FLD_NAME, 'UpdateStructure')]));

  if Params.UpdateStructure then
  begin
    DoAddRoot('�������',                  1);
    DoAddRoot('�������',                  2);
    DoAddRoot('��������',                 3);
    DoAddRoot('�������� ���������',       4);
    DoAddRoot('�������',                  6);
    DoAddRoot('������� �����',            7);
    DoAddRoot('����������� �����������',  8);

    DoAddRoot1('�������',                  1);
    DoAddRoot1('�������',                  2);
    DoAddRoot1('��������',                 3);
    DoAddRoot1('�������� ���������',       4);
    DoAddRoot1('�������',                  6);
    DoAddRoot1('������� �����',            7);
    DoAddRoot1('����������� �����������',  8);

  end;

//  if dmMain.DEBUG then
//    DoAdd('������������ ����',        5);

  FiTreeID:=10;

  if Params.UpdateStructure then
  begin
    DoProgress(0,6);
    AddTablesAndFields ();
    AddTablesAndFields_ ();
    DoProgress(1,6);

    Inc(FiTreeID);

    AddViews ();
    AddView_1(otView);
//    AddView_ ();
    DoProgress(2,6);

    AddTriggers ();
    AddView_1(otTriggers);

    DoProgress(3,6);

    AddStoredProcs ();
    AddView_1(otStoredProc);

    DoProgress(4,6);

   // dmSQLServ.FillIndexes;
 //   AddIndexes ();
  //  AddView_1(otIndex);

    DoProgress(5,6);

   // dmSQLServ.FillFk;
    AddFK ();
    AddView_1(otFK);

    DoProgress(6,6);

  //  AddCK ();
  //  AddView_1(otCK);

  end;

  //------------------------------------------------------------------
  if Params.IsModifyFields1 and Params.UpdateObjectFields then
  begin
   // dmObjectFields.FillTableObjects(DoProgress2);
 //   dmObjectFields.FillTableObjectFields(DoProgress2);
  end;
  //------------------------------------------------------------------

(*
  dmMDB.MDB.ExecCommandSimple('DELETE FROM mem_Data_mdb');
  db_CopyRecords(mem_Data, dmMdb.tbl_mem_Data);
*)


  db_TableReOpen(t_Data_Compare, '_Compare');
  db_TableReOpen(t_Data_Compare_, '_Compare_');


  t_Data_Compare.EnableControls;
end;

//------------------------------------------------------------------------------
procedure TdmCompareAndModify_.ExportStructure;
//------------------------------------------------------------------------------
begin
  dmMDB.ADOConnection1.BeginTrans;

  dmMDB.EmptyTable('sql_Tables');
  dmMDB.EmptyTable('sql_Table_Fields');
  dmMDB.EmptyTable('sql_Views');
  dmMDB.EmptyTable('sql_Indexes');
  dmMDB.EmptyTable('sql_Triggers');
  dmMDB.EmptyTable('sql_StoredProcs');
  dmMDB.EmptyTable('sql_FK');
  dmMDB.EmptyTable('sql_CK');


  DBStructure_SQL.Clear;

  dmExportStructure.DBStructure_ref := DBStructure_SQL;


  with dmExportStructure do
  begin
    db_TableOpen1(t_Tables_sql, 'sql_Tables');
    ExportTables(t_Tables_sql);

    db_TableReOpen(t_TableFields, 'sql_Table_Fields');
    ExportTableFields(t_TableFields);

    db_TableReOpen(t_Views_sql, 'sql_Views');
    ExportViewsToDataSet(t_Views_sql) ;

    db_TableReOpen(t_Indexes, 'sql_Indexes');
    ExportIndexesToDataSet(t_Indexes) ;

    db_TableReOpen(t_StoredProcs_sql, 'sql_StoredProcs');
    ExportStoredProcsToDataSet(t_StoredProcs_sql);

    db_TableReOpen(t_Triggers, 'sql_Triggers');
    ExportTriggersToDataSet(t_Triggers);

    db_TableReOpen(t_CK_sql, 'sql_CK');
    ExportCKToDataSet(t_CK_sql);

    db_TableReOpen(t_FK, 'sql_FK');
    ExportFKToDataSet(t_FK);
  end;


  dmMDB.ADOConnection1.CommitTrans;

end;




end.

             

// TODO: AddView_
////---------------------------------------------------------
//procedure TdmCompareAndModify.AddView_;
////---------------------------------------------------------
//var
//i: Integer;
//oView_SQL: TDBObject;
//oView_MDB: TDBObject;
//
//s: string;
//rec: TInterface_rec;
//
//begin
//for I := 0 to DBStructure_MDB.Views.Count - 1 do
//begin
//  oView_MDB := DBStructure_MDB.Views[i];
//  oView_SQL := DBStructure_SQL.Views.FindByName(oView_MDB.Name);
//
//  if not Assigned(oView_SQL) then
//  begin
//    s:=oView_MDB.MakeCreateSQL;
//
//    rec.NameMDB := oView_MDB.Name;
//    rec.NameSQL := '';
//
//    DoAddToInterface1_new(rec, oView_MDB.Name, '', STR_CREATE_, TYPE_View, otView, s, -1, FiTableID);
//  end else
//
//  if not oView_MDB.EqualTo(oView_SQL) then
//  begin
//    s:=oView_MDB.MakeDropSQL + CRLF + oView_MDB.MakeCreateSQL;
//
//    rec.NameMDB := oView_MDB.Name;
//    rec.NameSQL := oView_SQL.Name;
//
//    DoAddToInterface1_new(rec, oView_MDB.Name, oView_SQL.Name, STR_ALTER_, TYPE_View, otView, s, -1, FiTableID );
//
//  end;
//end;
//end;

