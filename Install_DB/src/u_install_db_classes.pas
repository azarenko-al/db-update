unit u_install_db_classes;

interface

uses
  Classes, XMLIntf;

type
  //-------------------------------------------------------------------
  TInterfaceItem = class(TCollectionItem)
  //-------------------------------------------------------------------
  public
   //  TextMDB,
   //  TextSQL,

     NAME_MDB,
     NAME_SQL,

     ObjectType,
     ACTION,

     Update_SQL: string;

     ID,
     PARENT_ID : Integer;

  end;


  //-------------------------------------------------------------------
  TInterfaceItemList = class(TCollection)
  //-------------------------------------------------------------------
  private
    function GetItems(Index: Integer): TInterfaceItem;
  public
    constructor Create;
    function AddItem: TInterfaceItem;

    property Items[Index: Integer]: TInterfaceItem read GetItems; default;

  end;


implementation

constructor TInterfaceItemList.Create;
begin
  inherited Create(TInterfaceItem);
end;

function TInterfaceItemList.AddItem: TInterfaceItem;
begin
  Result := TInterfaceItem (inherited Add);
end;

function TInterfaceItemList.GetItems(Index: Integer): TInterfaceItem;
begin
  Result := TInterfaceItem(inherited Items[Index]);
end;

end.
