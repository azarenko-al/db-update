unit dm_CompareAndModify_v1;

interface

uses
  u_install_db_classes,

  u_Sql_Parser,

  d_Progress,

  dm_MDB,
  dm_Main_SQL,
  dm_Progress,

  u_DB,
  u_func,
  u_log,
  u_files,

 // u_const_db,
  u_local_const,


//  u_sync_classes_new,
  u_sync_classes,

  SysUtils, Classes, Forms, Dialogs, Db, AdoDB, variants,  IOUtils,
  ActnList, System.Actions;


type
 // TProcessAction = (paNone, paAdd, paDel, paUpdate);

//  TErrorType = (etError, etCriticalError, etWarning);

//  TScriptRecordType = (rtNone,rtRename);


  TdmCompareAndModify_v1 = class(TDataModule)
    ds_Data_Compare: TDataSource;
    t_Data_Compare: TADOTable;
    ADOTable1: TADOTable;
    ActionList1: TActionList;
    act_Script_ToNotepad: TAction;
    act_ScriptChecked_ToNotepad: TAction;
    act_Check_All: TAction;
    act_ScriptNoDrop_ToNotepad: TAction;
    act_ExecutedScript: TAction;
    Action1: TAction;
    act_Script_new_ToNotepad: TAction;
    act_Script_new_exec: TAction;
    ADOQuery1: TADOQuery;
    q_Tables: TADOQuery;
    procedure act_Script_ToNotepadExecute(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);

  public
    DBStructure_SQL: TDBStructure;
    DBStructure_MDB: TDBStructure;


    FInterfaceItemList: TInterfaceItemList;

  private
    FIsFirstTimeExecuted: boolean;

//    FTableDict: TDBTableDict;

//    FIsUseInterface : Boolean;
  private
    FTableID: integer;

    FTempFileName: string;

    procedure AddObject(aObjectType: TDBObjectType; aGroupName: string);

    procedure AddTableFields(aTreeID: Integer; aTable_SQL, aTable_MDB: TDBObject;
        aTableName: string; aAllowDrop: Boolean);

    procedure AddTableObjects(aTreeID: Integer; aObjectType: TDBObjectType;
        aItem_SQL, aItem_MDB: TDBObject; aTableName: string; aAllowDrop: Boolean);

    procedure CompareForRename(aObjectType: TDBObjectType; aList_MDB, aList_SQL :  TDBObjectList; aTreeParentID: Variant);

     procedure DoProgress1(aValue,aMax : Integer);
//    function Exec_Script_new: Boolean;

     function Interface_AddItem(aNameMDB, aNameSQL, aAction: string; aObjectType:
         TDBObjectType; aUpdate_SQL: string; aTreeParentID: Variant): Integer;

    procedure ExecuteProc; //override;
    procedure Execute_objects;
    procedure Execute_Table_columns;
    function GetScriptFileByName(aName: string): string;

//    function GetOwnerRights(aName: string; var aAllowAlter, aAllowDrop: boolean):
//        Boolean;
    procedure Install_service_sql;

    function Interface_AddRoot(sName: string): Integer;
    procedure Interface_Update(aTextMDB, aTextSQL: string);
    procedure Interface_Update_Is_Updated(aIs_Updated: Boolean);
    procedure Interface_Update_Owner(aOwner: string);

//    procedure Interface_Update_Priority(aPriority: Integer);
//    DatabaseName : string;


    procedure Progress2(aValue,aMax : Integer);
    procedure Progress1(aValue,aMax : Integer);
    procedure UpdateAllViews;
  public

    Script: TStringList;
//    Script_rename: TStringList;


  //  ScriptNoDrop: TStringList;

   // FExecutedScript: TStringList;

    Params: record
//                IsHiddenW

//                DatabaseName : string;

      //        LoginRec : TdbLoginRec;

            //  IsShowScript : Boolean;

          //    New_DB_Name : string;
           //   IsCreateNew: boolean;  // ������� DB ��� ��� ���������� ���

           //   User, Password: string;
            //  FileName: string;


            //  UpdateStructure: Boolean;

            //  UpdateObjectFields: Boolean;
            //  IsModifyFields1: boolean;

          //    IsAllowDrop : Boolean;

           //   IsCreateOnly : Boolean;

            //  ExecuteOptions: set of (eoCreateOnly);

            //  IsOnlyCreate : Boolean;

            //  ADOConnection_SQL: TADOConnection

            end;

    procedure Exec_Dlg_Compare;
    procedure ExecSingleRecord;
    procedure OpenCompareData;
    procedure Check_All;
    procedure Depends(t_Data_Compare: TADOTable; Value: Integer);
    procedure ExecDlg;
    procedure Execute;

    class function Init: TdmCompareAndModify_v1;

    procedure ShowScriptForChecked;
    procedure UnCheck_All;

  end;




const
  FLD_Text_MDB='Text_MDB';
  FLD_Text_SQL='Text_SQL';
  FLD_Is_Updated = 'Is_Updated';

  STR_UPDATE      = '��������';
  STR_UPDATE_DONE = '��������� ����������';
  STR_ERR_UPDATE  = '������ ����������';
  STR_UPDATED     = STR_UPDATE_DONE;
 // STR_ADD         = '��������';
  STR_ADDED       = '��������� ����������';

//  STR_CREATE      = '�������';
  STR_CREATE_     = 'CREATE';
  STR_ALTER_      = 'ALTER';
  STR_DROP      = 'DROP';
  STR_RENAME = 'RENAME';

  STR_CREATE_DONE = '��������� ��������';
  STR_CREATED     = STR_CREATE_DONE;
  STR_ERR_CREATE  = '������ ��������';
  STR_DEL         = '�������';
  STR_DELETED     = '��������� ��������';
  STR_ERR_DELETE  = '������ ��������';
  STR_ERR_DEL_WHILE_UPD = '������ ��������  (��� ����������)';
  STR_ERR_ADD_WHILE_UPD = '������ ���������� (��� ����������)';

//  STR_AUTO_Del_NOT_ALLOWED = '�������������� �������� �� ��������������';
//  STR_AUTO_ADD_NOT_ALLOWED = '�������������� �������� �� ��������������';

  ERROR_MSG_LOCAL = '������. '#13#10' ������: %s (%s). '#13#10' ����� ������: %s. '#13#10' SQL: %s';

//  TBL_Owner = 'Owner';


const
//  TAG_RENAME = 1;
  TAG_DROP   = 2;




var
  dmCompareAndModify_v1: TdmCompareAndModify_v1;

//===================================================================
implementation
{$R *.DFM}

const
  TBL_Compare_v1 = '_Compare_v1';

  TBL_Compare_v1_table_columns = '_Compare_v1_table_columns';


class function TdmCompareAndModify_v1.Init: TdmCompareAndModify_v1;
begin
  if not Assigned(dmCompareAndModify_v1) then
    dmCompareAndModify_v1:=TdmCompareAndModify_v1.Create(Application);

  Result:=dmCompareAndModify_v1;
end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.DataModuleCreate(Sender: TObject);
//---------------------------------------------------------
begin
 // Params.IsModifyFields1 := false;              ceItemList.Create;


  FTempFileName:= ChangeFileExt(Application.ExeName, '.sql');


//  FTableDict:=TDBTableDict.Create;



 // if ADOConnection1.Connected then
  //  ShowMessage('procedure TdmCompareAndModify_new.DataModuleCreate(Sender: TObject);  Connection.Connected');

//  FInterfaceItemList:=TInterfa
  TdmMDB.Init1;
 // dmMDB.OpenDB1;

//  db_SetComponentADOConnection(Self, dmMDB.ADOConnection_MDB);

  Script := TStringList.Create();


  DBStructure_SQL := TDBStructure.Create();
  DBStructure_MDB := TDBStructure.Create();


//  FIsUseInterface :=True;


 // Params.ExecuteOptions:=[eoCreateOnly];


 // Params.IsCreateOnly := True;


//  OpenCompareData();

 // Params.IsAllowDrop := True;

//  Script_rename := TStringList.Create();
end;


procedure TdmCompareAndModify_v1.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(FInterfaceItemList);

//  FreeAndNil(FTableDict);

//  FreeAndNil(Script_rename);

  FreeAndNil(DBStructure_MDB);
  FreeAndNil(DBStructure_SQL);

  FreeAndNil(Script);

  inherited;
end;


//------------------------------------------------------------------------------------
function TdmCompareAndModify_v1.Interface_AddItem(aNameMDB, aNameSQL, aAction:
    string; aObjectType: TDBObjectType; aUpdate_SQL: string; aTreeParentID:
    Variant): Integer;
//------------------------------------------------------------------------------------
var
  i: Integer;
begin
  with FInterfaceItemList.AddItem do
  begin
    NAME_MDB   := aNameMDB;
    NAME_SQL   := aNameSQL;

    ObjectType := ObjectTypeToStr(aObjectType);
    Action     := aAction;
    Update_SQL := aUpdate_SQL;

    ID         := FTableID;
    PARENT_ID  := aTreeParentID;

  end;


  Inc(FTableID);
  Result := FTableID;

  db_AddRecord_ (t_Data_Compare,
          [
           FLD_NAME_MDB,         aNameMDB,
           FLD_NAME_SQL,         aNameSQL,

           FLD_TYPE,             ObjectTypeToStr(aObjectType),
           FLD_ACTION,           aAction,

           FLD_Update_SQL,       aUpdate_SQL,

           FLD_ID,               FTableID,
           FLD_PARENT_ID,        aTreeParentID

          ]);
end;

//------------------------------------------------------------------------------------
procedure TdmCompareAndModify_v1.Interface_Update(aTextMDB, aTextSQL: string);
//------------------------------------------------------------------------------------
begin

  db_UpdateRecord__ (t_Data_Compare,
        [
         FLD_Text_MDB, aTextMDB,
         FLD_Text_SQL, aTextSQL
        ]);
end;

//
////------------------------------------------------------------------------------------
//procedure TdmCompareAndModify_v1.Interface_Update_Priority(aPriority: Integer);
////------------------------------------------------------------------------------------
//begin
//  db_UpdateRecord__ (t_Data_Compare,
//    [FLD_Priority, IIF(aPriority>0,aPriority,null)]);
//end;

//------------------------------------------------------------------------------------
procedure TdmCompareAndModify_v1.Interface_Update_Is_Updated(aIs_Updated:
    Boolean);
//------------------------------------------------------------------------------------
begin
  db_UpdateRecord__ (t_Data_Compare, [FLD_Is_Updated, aIs_Updated]);
end;

//------------------------------------------------------------------------------------
procedure TdmCompareAndModify_v1.Interface_Update_Owner(aOwner: string);
//------------------------------------------------------------------------------------
begin
  db_UpdateRecord__ (t_Data_Compare, [FLD_Owner, aOwner]);
end;

// ---------------------------------------------------------------
function TdmCompareAndModify_v1.Interface_AddRoot(sName: string): Integer;
// ---------------------------------------------------------------
//var
 // i: Integer;
begin
  Inc(FTableID);
  Result := FTableID;

  db_AddRecord_ (t_Data_Compare,
     [FLD_NAME_MDB,   sName,
      FLD_ID,         FTableID
      ]);
end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.AddTableObjects(aTreeID: Integer;
    aObjectType: TDBObjectType; aItem_SQL, aItem_MDB: TDBObject; aTableName:   string; aAllowDrop: Boolean);
//---------------------------------------------------------
// index,...
//---------------------------------------------------------
var
  s: string;
  i: Integer;
  k: Integer;

  oItem_MDB: TDBObject;
  oItem_SQL: TDBObject;

  oList_MDB : TDBObjectList;
  oList_SQL : TDBObjectList;
  oScript: TStringList;

begin
  oList_MDB:=aItem_MDB.GetListByObject(aObjectType);
  oList_SQL:=aItem_SQL.GetListByObject(aObjectType);


//  if Eq(aItem_MDB.Name,'LinkNet') then
//    s:=s;
//   if (aObjectType in [otPrimaryKey,otIndex]) then

  if (aObjectType in [otIndex]) then
    CompareForRename (aObjectType, oList_MDB, oList_SQL, aTreeID);


  oScript:=TStringList.Create;


  // ---------------------------------------------------------------
  // MDB -> SQL
  // ---------------------------------------------------------------

  if aAllowDrop then
//  if not Params.IsCreateOnly then

    for i := 0 to oList_SQL.Count - 1 do
      if not oList_SQL[i].Runtime.IsProcessed then
    begin
      oItem_SQL:=oList_SQL[i];

      if oItem_SQL.Runtime.IsProcessed then
       Continue;


      oItem_MDB:=oList_MDB.FindByName(oItem_SQL.Name);

      // DROP ---------------------
    //  if not IsCreateOnly then

      if (not Assigned(oItem_MDB)) then
//      and params.IsAllowDrop then //oItem_SQL
      begin

//        if not (eoCreateOnly in Params.ExecuteOptions) then
//        begin
//
//        end;
//
//        if Params.IsAllowDrop then


        s:=oItem_SQL.MakeDropSQL();

        Script.AddObject('---------------', Pointer(TAG_DROP));
        Script.AddObject('--drop '+ ObjectTypeToStr(oItem_SQL.ObjectType), Pointer(TAG_DROP));
        Script.AddObject('---------------', Pointer(TAG_DROP));
        Script.AddObject(s, Pointer(TAG_DROP));

//        if FIsUseInterface then
      //  begin
        Interface_AddItem( '', oItem_SQL.Name, STR_DROP,  aObjectType, s,  aTreeID );
      //  end;   

      end;
    end;


  // ---------------------------------------------------------------
  // MDB -> SQL
  // ---------------------------------------------------------------
  for i := 0 to oList_MDB.Count - 1 do
    if not oList_MDB[i].Runtime.IsProcessed then
  begin
    oItem_MDB:=oList_MDB[i];
    if oItem_MDB.Runtime.IsProcessed then
      Continue;


    oItem_SQL:=oList_SQL.FindByName(oItem_MDB.Name);

    // ---------------------------------------------------------------
    if not Assigned(oItem_SQL) then
    // ---------------------------------------------------------------
    begin
      s:=oItem_MDB.MakeCreateSQL();

//      oScript.Add('---------------');
//      oScript.Add('--create ');
//      oScript.Add('---------------');
      oScript.Add(s);
    //  ScriptNoDrop.Add(s);

//      if FIsUseInterface then
      begin
        Interface_AddItem(oItem_MDB.Name, '', STR_CREATE_,  aObjectType, s,  aTreeID );
        Interface_Update (oItem_MDB.GetInfoText(), '');
//        Interface_Update_Owner(oItem_MDB.Owner);
       // Interface_Update_Priority(oItem_MDB.Priority);

      end;
//
//  k:=PosCount('CREATE VIEW dbo.view_MapFile', Script.Text);
//  if k>1 then
//    ShowMessage('CREATE VIEW dbo.view_MapFile');
//


    end else

    // ---------------------------------------------------------------
    if not oItem_MDB.EqualTo(oItem_SQL) then
    // ---------------------------------------------------------------
    /////    if not Params.IsCreateOnly then
    begin
      s:=oItem_SQL.MakeDropSQL();
//      s:=oItem_MDB.MakeDropSQL();
     // s:='';
      s:=s+ oItem_MDB.MakeCreateSQL();

      oScript.AddObject('---------------', Pointer(0));
      oScript.AddObject('--Drop ', Pointer(0));
      oScript.AddObject('---------------', Pointer(0));
      oScript.AddObject(s, Pointer(0));

//      if FIsUseInterface then
   //   begin
        Interface_AddItem(oItem_MDB.Name, oItem_SQL.Name, STR_ALTER_, aObjectType, s,  aTreeID );
        Interface_Update (oItem_MDB.GetInfoText(), oItem_SQL.GetInfoText());
    //  end;

    end;
  end;

  if oScript.Count>0 then
  begin
    Script.Add('--------------------------------');
    Script.Add('-- table: '+ aTableName);
    Script.Add('--------------------------------');

    Script.AddStrings(oScript);
  end;
//
//
//  k:=PosCount('CREATE VIEW dbo.view_MapFile', Script.Text);
//  if k>1 then
//    ShowMessage('CREATE VIEW dbo.view_MapFile');


  FreeAndNil(oScript);

end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.AddTableFields(aTreeID: Integer; aTable_SQL,
    aTable_MDB: TDBObject; aTableName: string; aAllowDrop: Boolean);
//---------------------------------------------------------
var

  s: string;
  i: Integer;
  j: Integer;
  oTableField_MDB: TDBTableField;
  oTableField_SQL: TDBTableField;

  sError: string;
  sFieldAlterSQL: string;

var
  oScript: TStringList;

begin
  s:=aTable_MDB.Name;

//  if Eq(s,'link') then
//    s:=s;
                     

  oScript:=TStringList.Create;


  // ---------------------------------------------------------------
  // MDB -> SQL
  // ---------------------------------------------------------------

  for i := 0 to aTable_MDB.TableFields.Count - 1 do
  begin
    oTableField_MDB:=aTable_MDB.TableFields[i];
    oTableField_SQL:=aTable_SQL.TableFields.FindByName(oTableField_MDB.Name);

    // -------------------------
    if not Assigned(oTableField_SQL) then
    // -------------------------
    begin
      s:=oTableField_MDB.sql_create;// MakeAddSQL();

      oScript.Add('');
      oScript.Add('-- create column --');
      oScript.Add('');
      oScript.Add(s);
    //  ScriptNoDrop.Add(s);

//      if FIsUseInterface then
      begin
        Interface_AddItem(oTableField_MDB.Name, '', STR_CREATE_,  otTABLEField, s,  aTreeID );
      end;
    end else

    // -------------------------

    if aAllowDrop then
//    if not Params.IsCreateOnly then
      if not oTableField_MDB.EqualTo(oTableField_SQL) then
    // -------------------------
    begin
//      sFieldAlterSQL := oTableField_MDB.MakeAlterSQL(oTableField_SQL, DBStructure_SQL.FK);
      s:=sFieldAlterSQL;

     // if sError='' then
    //  begin
      //  s:='';

     //   s:=s+ oTableField_MDB.MakeAlterSQL(oTableField_SQL, DBStructure_SQL.FK, sError);
        s:=sFieldAlterSQL;
      //  s:=s+ sFieldAlterSQL;

(*
        if not (eoCreateOnly in Params.ExecuteOptions) then
        begin

        end;
*)

        oScript.Add('');
        oScript.Add('-- column--');
        oScript.Add('');

        oScript.Add(s);
        //ScriptNoDrop.Add(s);
     // end;

   //   if FIsUseInterface then
   //   begin
        Interface_AddItem(oTableField_MDB.Name, oTableField_SQL.Name, STR_ALTER_, otTABLEField, s,  aTreeID );
        Interface_Update(oTableField_MDB.GetInfoText(), oTableField_SQL.GetInfoText());
     // end;

    end;
  end;

  // ---------------------------------------------------------------
  // SQL -> MDB
  // ---------------------------------------------------------------
  if aAllowDrop then
    for i := 0 to aTable_SQL.TableFields.Count - 1 do
    begin
      oTableField_SQL:=aTable_SQL.TableFields[i];
      oTableField_MDB:=aTable_MDB.TableFields.FindByName(oTableField_SQL.Name);

      if (not Assigned(oTableField_MDB)) //and params.IsAllowDrop
        then
      begin
//        s:=oTableField_SQL.MakeDropSQL();

        oScript.AddObject('', Pointer(0));
        oScript.AddObject('--drop column--', Pointer(0)) ;
        oScript.AddObject('', Pointer(0)) ;
        oScript.AddObject(s,  Pointer(0));

      //  if FIsUseInterface then
        begin
          Interface_AddItem('', oTableField_SQL.Name, STR_DROP,  otTABLEField, s,  aTreeID );
        end;
      end
    end;


  if oScript.Count>0 then
  begin
    Script.Add('--------------------------------');
    Script.Add('-- table: '+ aTableName);
    Script.Add('--------------------------------');

    Script.AddStrings(oScript);
  end;

  FreeAndNil(oScript);


end;


(*
    sOwner:=oItem_MDB.Owner;

*)



//---------------------------------------------------------
procedure TdmCompareAndModify_v1.AddObject(aObjectType: TDBObjectType;
    aGroupName: string);
//---------------------------------------------------------
var
 // b: Boolean;
//  bAllow: Boolean;
  i: Integer;
  iRootTreeID: Integer;
  iTreeID: Integer;
  j: Integer;
  s: string;

  oItem_SQL: TDBObject;
  oItem_MDB: TDBObject;

  oList_MDB : TDBObjectList;
  oList_SQL : TDBObjectList;

//  oSelection: TDBObjectList;
 // sOwner: string;

  bAllowAlter, bAllowDrop: Boolean;
  k: Integer;
  sTableName: string;

begin
  iRootTreeID := Interface_AddRoot(aGroupName);

//      if oTableField_SQL.Name='guid_modify' then
  //      s:='';


  oList_MDB:=DBStructure_MDB.GetListByObject(aObjectType);
  oList_SQL:=DBStructure_SQL.GetListByObject(aObjectType);





//  if aObjectType in [otFK] then
//    CompareForRename (aObjectType, oList_MDB, oList_SQL, iRootTreeID);

 // ---------------------------------------------------------------
  // MDB -> SQL   DROP
  // ---------------------------------------------------------------
////////  if not Params.IsCreateOnly then
    for i := 0 to oList_SQL.Count - 1 do
    begin

      oItem_SQL:=oList_SQL[i];


//      assert (oItem_SQL.SCHEMA<>'');




  /////////    if oItem_SQL.Runtime.IsProcessed then
  //////      Continue;

      oItem_MDB:=oList_MDB.FindByName(oItem_SQL.Name);

      if (not Assigned(oItem_MDB)) //and params.IsAllowDrop
      then
      begin

{
        case aObjectType of
          otFK:      sOwner:=DBStructure_MDB.Tables.GetOwnerByName(oItem_SQL.FK.FK_TABLE_NAME);
          otTrIGGER: sOwner:=DBStructure_MDB.Tables.GetOwnerByName(oItem_SQL.TableName);

          otTable: Continue;
        else
          sOwner:='';
        end;
}
//        bAllow := GetOwnerRights(sOwner, bAllowAlter, bAllowDrop) ;

        bAllowAlter:=True;
        bAllowDrop :=True;

    //    s:='';

        if bAllowDrop then
        begin
//          s:=s+ oItem_SQL.MakeDropSQL();
          s:= oItem_SQL.MakeDropSQL();

          Script.Add('---------------');
          Script.Add('--drop');
          Script.Add('---------------');
          Script.Add(s);
        end;


  //      if not bAllow then
   //       Continue;


        if bAllowDrop then
       //   if FIsUseInterface then
        begin
          Interface_AddItem('', oItem_SQL.Name, STR_DROP,  aObjectType, s,  iRootTreeID);
        //  Interface_Update(oItem_MDB.GetInfoText(), '', oItem_MDB.Owner);
//          Interface_Update_Owner(sOwner);
        end;

      end;
    end;


  // ---------------------------------------------------------------
  for I := 0 to oList_MDB.Count - 1 do
  // ---------------------------------------------------------------
  begin
    oItem_MDB := oList_MDB[i];
    if oItem_MDB.Runtime.IsProcessed then
      Continue;

    oItem_SQL := oList_SQL.FindByName(oItem_MDB.Name);


//    sOwner:=oItem_MDB.Owner;


//    bAllow := GetOwnerRights(oItem_MDB.Owner, bAllowAlter, bAllowDrop) ;

      bAllowAlter:=True;
      bAllowDrop :=True;


   //// if not bAllow then
  ////    Continue;
    {

      if Eq(oItem_MDB.Name, 'view_MapFile') then
      begin
        ShowMessage(' if Eq(oItem_MDB.Name,   111111111111111111');

        Script.SaveToFile('d:\script1.sql');
      end;
     }

    // -------------------------
    // CreateSQL
    // -------------------------
    if not Assigned(oItem_SQL) then
    begin
      s:=oItem_MDB.MakeCreateSQL();

//      Script.Add('---------------');
//      Script.Add('--create');
//      Script.Add('---------------');
      Script.Add(s);


      {
k:=PosCount('CREATE VIEW dbo.view_MapFile', Script.Text);
if k=1 then
  ShowMessage('CREATE VIEW dbo.view_MapFile');



k:=PosCount('CREATE VIEW dbo.view_MapFile', Script.Text);
if k>1 then
  S howMessage('CREATE VIEW dbo.view_MapFile');

  }



    //  ScriptNoDrop.Add(s);

    //  if FIsUseInterface then
      begin
        iTreeID:=Interface_AddItem(oItem_MDB.Name, '', STR_CREATE_,  aObjectType, s,  iRootTreeID);

        Interface_Update(oItem_MDB.GetInfoText(), '');
//        Interface_Update_Owner(oItem_MDB.Owner);
//        Interface_Update_Priority(oItem_MDB.Priority);

    //    Interface_Update_Is_Updated(True);
      end;

    end else
    begin
    // -------------------------
    // update
    // -------------------------
//      if aObjectType=<>otTaBLE then
 //     sOwner:=DBStructure_MDB.Tables.GetOwnerByName(oItem_MDB.FK.FK_TABLE_NAME);


      if not oItem_MDB.EqualTo(oItem_SQL) then
      begin
        // -------------------------
        if aObjectType<>otTaBLE then
        // -------------------------
        begin
       //   s:=oItem_MDB.MakeDropSQL();
//          s:=s+ oItem_MDB.MakeCreateSQL();
         // s:= oItem_MDB.MakeCreateSQL();

          s:= oItem_SQL.MakeDropSQL + CRLF +'GO'+ CRLF +  oItem_MDB.MakeCreateSQL();
//          s:= oItem_MDB.MakeDropSQL + CRLF +'GO'+ CRLF +  oItem_MDB.MakeCreateSQL();


        //        s:=oItem_SQL.SQL_drop + CRLF +'GO'+ CRLF + oItem_MDB.SQL_create;


// --         Script.Add('---------------');
//   --       Script.Add('--create');
//     --     Script.Add('---------------');

          Script.Add(s);

        //  if FIsUseInterface then
          begin
            iTreeID:=Interface_AddItem(oItem_MDB.Name, oItem_SQL.Name, STR_ALTER_, aObjectType, s,  iRootTreeID );

            Interface_Update(oItem_MDB.GetInfoText(), oItem_SQL.GetInfoText());
//            Interface_Update_Owner(oItem_MDB.Owner);
//            Interface_Update_Priority(oItem_MDB.Priority);
          end;


        end else

        // -------------------------
        if aObjectType=otTaBLE then
        // -------------------------
        begin
         // if FIsUseInterface then
//          begin
            iTreeID:=Interface_AddItem(oItem_MDB.Name, oItem_SQL.Name, '', aObjectType, '',  iRootTreeID );

            Interface_Update('', '');
//            Interface_Update_Owner(oItem_MDB.Owner);
   //       end;




         // b:=
//          GetOwnerRights(oItem_MDB.Owner, bAllowAlter, bAllowDrop);


          sTableName:=oItem_MDB.Name;

 //         if Eq(sTableName, 'dbo.link') then
  //          s:='';

//          sTableName:=oItem_MDB.Owner+ '-' +oItem_MDB.Name;

          AddTableFields (iTreeID, oItem_SQL, oItem_MDB, sTableName, bAllowDrop);
          AddTableObjects(iTreeID, otIndex, oItem_SQL, oItem_MDB, sTableName, bAllowDrop);
          AddTableObjects(iTreeID, otCheckConstraint,  oItem_SQL, oItem_MDB, sTableName, bAllowDrop);

          if FTableID=iTreeID then
            t_Data_Compare.Delete;

        end;
      end;
    end;
  end;

end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.UpdateAllViews;
//---------------------------------------------------------
const
  DEF_TEMPLATE1 = 'if object_id(''%s'') is not null  '+ CRLF +
//                 ' print ''refresh %s''  '+ CRLF +
                 ' exec sp_refreshview  ''%s'' ' ;
 //                ' select top 1 * from  %s  '+ CRLF +

        //       [sName, sName, sName]);

  DEF_TEMPLATE211 = 'if object_id(''%s'') is not null   '+ CRLF +
              //   ' print ''refresh %s''  '+ CRLF +
              //   ' exec sp_refreshview  %s'+ CRLF +
                 ' select top 1 * from  %s  ';
              //   'end';
        //       [sName, sName, sName]);



var
  i: Integer;
  s1: string;
 // s2: string;
  sName: string;
//  sSCHEMA: string;

//  oStrList: TStringList;
begin
   Script.Add('--------------- ');
   Script.Add('--refresh views ');
   Script.Add('--------------- ');


 // oStrList:=TStringList.Create;


  for i := 0 to DBStructure_SQL.Views.Count-1 do
  begin
//    sSCHEMA:=DBStructure_SQL.Views[i].SCHEMA;
    sName:=DBStructure_SQL.Views[i].Name;

    s1:= Format(DEF_TEMPLATE1, [ sName,  sName]);
//    s2:= Format(DEF_TEMPLATE2, [ sName,  sName]);

   // oStrList.Add();

//    s:= Format('if object_id(''%s'') is not null  begin print  exec sp_refreshview  %s ',
  //  s:= Format('if object_id(''%s'') is not null  begin print ''refresh %s''  exec sp_refreshview  %s  select top 1 from  %s  end',
//       [sName, sName, sName]);

//    s:= 'exec sp_refreshview ' + DBStructure_SQL.Views[i].Name;

//  exec sp_refreshview @name

    Script.Add(s1 +  DEF_GO_CRLF);
//    Script.Add(s2 +  DEF_GO_CRLF);
  end;


 // FreeAndNil(oStrList);

end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.ExecuteProc ();
//---------------------------------------------------------
var
  I: Integer;
  s: string;
begin
  FTableID:= 0;

  Script.Clear;
//  Script_rename.Clear;

  t_Data_Compare.DisableControls;

  dmMdb.EmptyTable(TBL_Compare_v1);

  db_TableReOpen(t_Data_Compare, TBL_Compare_v1);


//    AddObject(otUDT,  '2. UDT');




//  DBStructure_MDB.Clear;
//  DBStructure_SQL.Clear;


//  if dmMain.DEBUG then
//    DoAdd('������������ ����',        5);

//    for I := 4 to 4 do
//    for I := 7 to 7 do
    for I := 0 to 7 do
    begin
      DoProgress1(i,6);

      case i of
        0:  AddObject(ottaBLE,       '1. �������');
        1:  AddObject(otUDT,         '2. UDT');
        2:  AddObject(otFK,          '3. ������� �����');
        3:  AddObject(otStoredFunc,  '4. �������� �������');
        4:  AddObject(otView,        '5. View');
        5:  UpdateAllViews();
        6:  AddObject(otStoredProc,  '6. �������� ���������');
        7:  AddObject(otTRIGGER,     '7. Trigger');

      end;

    end;


  //  Script.SaveToFile('d:\script_.sql');



    if Script.Count>0 then
    begin
    //  Assert(DBStructure_MDB.DB_GUID<>'', 'Value <=0');

//      Script.Add(Format('IF OBJECT_ID(''%s'') IS NOT NULL ', [TBL__db_version_]));

      s:= Format('IF OBJECT_ID(''%s'') IS NOT NULL ', [TBL__db_version_])+CRLF ;
      s:=s+ 'begin '+CRLF ;

      s:=s+ Format('  DELETE FROM %s WHERE product_name=''%s'';',
             [TBL__db_version_,
              DBStructure_MDB.DB_Product_name ])+CRLF ;

      s:=s+ Format('  INSERT INTO %s (product_name, version) VALUES (''%s'',''%s'')',
             [TBL__db_version_,
              DBStructure_MDB.DB_Product_name,
              DBStructure_MDB.DB_Version ]) +CRLF ;

      s:=s+ 'END '+ DEF_GO_CRLF;

      Script.Add(s);

    end;



(*

   for I := FScript_new.Count - 1  downto 0 do
   begin
     FScript_new.Insert(i,'-----------');
   end;

   FScript_new.SaveToFile('d:\111111.txt');
*)


//  Script_rename.Insert(0,'---------------');
//  Script_rename.Insert(0,'--rename');
//  Script_rename.Insert(0,'---------------');

//  Script_rename.AddStrings(Script);


//  Script.Assign(Script_rename);


 // ShellExec_Notepad_temp(Script_rename.Text, '.sql');

////////////////  ShellExec_Notepad_temp(Script.Text, '.sql');



  db_TableReOpen(t_Data_Compare, TBL_Compare_v1);

  t_Data_Compare.EnableControls;

end;


procedure TdmCompareAndModify_v1.DoProgress1(aValue,aMax : Integer);
var bTerminated: boolean ;
begin
  Progress_SetProgress1(aValue,aMax, bTerminated);

end;


procedure TdmCompareAndModify_v1.OpenCompareData;
begin
//  db_TableReOpen(t_Data_Compare, TBL_Compare);
//  db_TableReOpen(t_Owner, TBL_Owner);
end;



//---------------------------------------------------------
procedure TdmCompareAndModify_v1.Install_service_sql;
//---------------------------------------------------------
var
  i: Integer;
  oSQLParser: TSqlParser;
  s: string;
  sSQL: string;
  sFileName: string;
begin
//  Exit;

  //////////////////////////////


  if FIsFirstTimeExecuted then
    Exit;

  FIsFirstTimeExecuted:=true;

  sFileName:=ExtractFilePath (Application.ExeName) + 'install_db.service.sql';

  Assert(FileExists(sFileName));

  sSQL:=TxtFileToStr (sFileName);


//--------------------------------------------------

  oSQLParser:=TSqlParser.Create;
  oSQLParser.LoadFromStr(sSQL);

  for i:=0 to oSQLParser.Count-1 do
  begin
    s:=oSQLParser[i];

    dmMain_SQL.ADOConnection1.Execute(oSQLParser[i]);
  end;

//  Result := ExecScriptList(FSQLParser,  aIsRollbackTrans); //aMsg,

//  Progress_ExecDlg_proc(ExecScriptSQL_Progress, '');


  FreeAndNil(oSQLParser);


end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.ExecSingleRecord;
//---------------------------------------------------------
var
  I: Integer;
  sErr: string;

 // oStrList: TStringList;
  s: string;

 // oADOConnection: TADOConnection;
  b : Boolean;
  sSQL: string;
begin
//  oADOConnection:=dmMain_SQL.ADOConnection1;

  Install_service_sql;


//  Assert(Assigned(oADOConnection), 'Value not assigned');
  ///////////

  sSQL := t_Data_Compare.FieldByName(FLD_UPDATE_SQL).AsString;
  if sSQL='' then
  begin
    ShowMessage('UPDATE_SQL = ''''');
    Exit;
  end;

 // FExecutedScript.Add(sSQL);

 // oStrList:=StringToStringList(sSQL, DEF_GO);
 // i :=oStrList.Count;


//  b:=dmMain_SQL.ExecScript(oStrList, sErr);


//  b:=dmMain_SQL.ExecSQL(sSQL, sErr);
  b:=dmMain_SQL.ExecScriptSQL_with_GO1(sSQL);


  if b then
    t_Data_Compare.Delete
  else
    db_UpdateRecord__(t_Data_Compare, [FLD_ERROR, sErr]);

 // FreeAndNil(oStrList);

end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.Exec_Dlg_Compare;
//---------------------------------------------------------
begin
//  Tdlg_CompareSQL.ExecDlg(t_Data_Compare);
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_v1.Check_All;
//---------------------------------------------------------
begin
//  exec sp_depends 'dbo.fn_Azimuth'




//  db_UpdateAllRecords(t_Data_Compare, FLD_ENABLED, True);
end;

//---------------------------------------------------------
procedure TdmCompareAndModify_v1.UnCheck_All;
//---------------------------------------------------------
begin
//  db_UpdateAllRecords_(t_Data_Compare, FLD_ENABLED, False);
end;


//---------------------------------------------------------
procedure TdmCompareAndModify_v1.ShowScriptForChecked;
//---------------------------------------------------------
const
  DEF_View_Compare_Enabled = 'view_Compare_Enabled';

var
  oStrList: TStringList;
  s: string;

begin
  oStrList:=TStringList.Create;

  db_TableReOpen(ADOTable1, DEF_View_Compare_Enabled);


  with ADOTable1 do
    while not EOF do
  begin
    s:=FieldByName(FLD_Update_SQL).AsString;

    if s<>'' then
      oStrList.Add(s);

    Next;
  end;

  StrToTxtFile(oStrList.Text, FTempFileName);

  oStrList.Free;

  ShellExec_Notepad (FTempFileName);

end;


procedure TdmCompareAndModify_v1.Progress2(aValue,aMax : Integer);
var bTerminated: boolean ;
begin
  Progress_SetProgress2(aValue,aMax, bTerminated);

end;


procedure TdmCompareAndModify_v1.Progress1(aValue,aMax : Integer);
var bTerminated: boolean ;
begin
  Progress_SetProgress1(aValue,aMax, bTerminated);

end;


procedure TdmCompareAndModify_v1.ExecDlg;
begin
  Progress_ExecDlg_proc(ExecuteProc);
end;


// ---------------------------------------------------------------
procedure TdmCompareAndModify_v1.act_Script_ToNotepadExecute(Sender: TObject);
// ---------------------------------------------------------------
var
  I: Integer;
begin
  // ---------------------------------------------------------------
  if Sender=act_Check_All then
  // ---------------------------------------------------------------
  begin
//    db_UpdateAllRecords_(t_Data_Compare, FLD_CHECKED, True);
  end;

  // ---------------------------------------------------------------
  if Sender=act_ExecutedScript then
  // ---------------------------------------------------------------
  begin
//    StrToTxtFile(FExecutedScript.Text, FTempFileName);
    StrToTxtFile(Script.Text, FTempFileName);
    ShellExec_Notepad (FTempFileName);
  end;

  // ---------------------------------------------------------------
  if Sender=act_Script_ToNotepad then
  begin
//    StrToTxtFile(ScriptNoDrop.Text, FTempFileName);
    StrToTxtFile(Script.Text, FTempFileName);
    ShellExec_Notepad (FTempFileName);
  end;

  // ---------------------------------------------------------------
  if Sender=act_Script_new_ToNotepad then
  begin
    for I := Script.Count - 1  downto 0 do
     Script.Insert(i,'-----------');

  //  FScript_new.SaveToFile('d:\111111.txt');


    StrToTxtFile(Script.Text, FTempFileName);
    ShellExec_Notepad (FTempFileName);

    Script.Clear;
  end;

  // ---------------------------------------------------------------
  if Sender=act_ScriptNoDrop_ToNotepad then
  begin
    StrToTxtFile(Script.Text, FTempFileName);
    ShellExec_Notepad (FTempFileName);
  end;

  // ---------------------------------------------------------------
  if Sender=act_ScriptChecked_ToNotepad then
  begin
    ShowScriptForChecked;
//    StrToTxtFile(Script.Text, FTempFileName);
  //  ShellExec_Notepad (FTempFileName);
  end;


end;


// -------------------------
procedure TdmCompareAndModify_v1.CompareForRename(aObjectType: TDBObjectType;   aList_MDB, aList_SQL : TDBObjectList; aTreeParentID: Variant);
// -------------------------
var
  oItem_MDB: TDBObject;
  oItem_SQL: TDBObject;
  i: Integer;
  s: string;
begin
  if not (aObjectType in [otIndex ]) then
    Exit;


  for i := 0 to aList_SQL.Count - 1 do
  begin
    oItem_SQL:=aList_SQL[i];

    Assert (oItem_SQL.TableName <> '');


    case aObjectType of
      otIndex: begin
                 oItem_MDB:=aList_MDB.FindBy_Index_keys1 (oItem_SQL);
               end;

    end;


    if Assigned(oItem_MDB) then
      if not Eq(oItem_MDB.Name, oItem_SQL.Name) then
    begin
      oItem_SQL.Runtime.IsProcessed := True;
      oItem_MDB.Runtime.IsProcessed := True;

      assert ( oItem_MDB.SQL_create <> '');

      Assert (oItem_MDB.TableName <> '');


 //     s:=oItem_SQL.SQL_drop +
  //       CRLF +'GO'+ CRLF +
   //      oItem_MDB.SQL_create;

      s:=oItem_SQL.MakeRenameSQL(oItem_MDB.Name);

{
      Script.Add('---------------');
      Script.Add('--rename');
      Script.Add('---------------');
 }

      Script.Add(s);


//      Script_rename.AddObject(s, Pointer(TAG_RENAME));

//      if FIsUseInterface then
      begin
        Interface_AddItem(oItem_MDB.Name, oItem_SQL.Name, STR_RENAME,  aObjectType, s,  aTreeParentID );
        Interface_Update(oItem_MDB.GetInfoText(), oItem_SQL.GetInfoText());
//        Interface_Update_Owner(oItem_MDB.Owner);
//        Interface_Update_Priority(oItem_MDB.Priority);
      end;

      Continue;
    end;
  end;
end;

procedure TdmCompareAndModify_v1.Depends(t_Data_Compare: TADOTable; Value:
    Integer);
begin
  // TODO -cMM: TdmCompareAndModify_v1.Depends default body inserted
end;

//----------------------------------------------------------
function TdmCompareAndModify_v1.GetScriptFileByName(aName: string): string;
//----------------------------------------------------------
var
  s: string;
begin
  s:=GetApplicationDir + 'data\' + aName + '.sql';
  Assert(FileExists(s), aName);

  Result:=TFile.ReadAllText(s);
end;


procedure TdmCompareAndModify_v1.Execute;
begin
  Execute_objects;
  Execute_Table_columns;

end;


//-----------------------------------------------------------
procedure TdmCompareAndModify_v1.Execute_objects;
//-----------------------------------------------------------
//.const
//  TBL_compare ='_Compare_v1';


var
  i: Integer;
  iID: Integer;
  s: string;
  sLocal: string;
  sRemote: string;
  sTemplate: string;
//  s,sTemplate: string;
  sTemplate_mdb: string;
  sType : string;
const
//  DEF_STR : array [0..0] of string = ('UDT');
  DEF_STR : array [0..4] of string = ('u','UDT','fn','view','sp');

//  DEF_STR : array [0..0] of string = ('view');


//  DEF_STR : array [0..0] of string = ('u');
//  DEF_STR : array [0..0] of string = ('sp');

begin
//  FTableDict.Clear;

  //  db_SetComponentADOConnection(Self, dmMain_SQL.ADOConnection1);

//  db_SetComponentsADOConn([t_Data_Compare],      dmMDB.ADOConnection_MDB);


//..  t_Data_Compare.TableName:=TBL_compare;
//  t_Data_Compare.Open;

  dmMDB.ADOConnection_MDB.Execute('delete from '+ TBL_Compare_v1);

//  t_Data_Compare.Close;
  dmMDB.OpenTable(t_Data_Compare, TBL_Compare_v1);

//  db_OpenQuery(ADOQuery1, 'delete from '+ TBL_compare);

//  t_Data_Compare.Open;

//  sTemplate:=GetScriptFileByName ('_insert_mdb');
  sTemplate_mdb:=GetScriptFileByName ('_mdb_view');

  {
  db_OpenQuery(ADOQuery1, SQL);

  db_View (ADOQuery1);


  with ADOQuery1 do
    while not EOF do
  begin

 //   Progress2(RecNo, RecordCount);
  //  if FTerminated then
   //   Break;

     Next;
   end;
   }


  for i := 0 to High(DEF_STR) do
  begin
    sType:=DEF_STR[i];

    s:=ReplaceStr(sTemplate_mdb, ':group_name', InttoStr(i+1) +'-'+ sType );
    s:=ReplaceStr(s, ':xtype', sType );


    TFile.WriteAllText(format( 'e:\%s.sql',[sType]) , s);

//    dmMDB.ADOConnection_MDB.Execute_objects(s);

    dmMDB.OpenQuery(ADOQuery1, s);

//db_View (ADOQuery1);

    with ADOQuery1 do
      while not EOF do
      begin
//insert into  _compare_v1 (group_name, xtype,  schema, Definition_local,  Definition_remote,  name_local, name_remote, SQL_create, SQL_drop)
//
//select '1-view', xtype, schema,  Definition_L,  Definition_R, name_local, name_remote,  SQL_create, SQL_drop
//from
//        s:=FieldValues['SQL_create_'];
//        s:=FieldValues['SQL_drop_'];

        sLocal :=ReplaceStr(FieldValues['Definition_L'], Chr(13),'');
        sRemote:=ReplaceStr(FieldValues['Definition_R'], Chr(13),'');

        Assert (sLocal<>'');

        if sLocal<>sRemote then
        begin
          db_AddRecord_ (t_Data_Compare,
                  [
                   'group_name',         InttoStr(i+1) +'-'+ sType,
                   FLD_xtype,            FieldValues[FLD_xtype],
                   FLD_schema,           FieldValues[FLD_schema],

                   'name_local',         FieldValues[FLD_schema]+ '.' +FieldValues['name_local'],
                   'name_remote',        FieldValues[FLD_schema]+ '.' +FieldValues['name_remote'],

                   'SQL_create',         FieldValues['SQL_create_'],
                   'SQL_drop',           FieldValues['SQL_drop_'],

                   'Definition_local',   FieldValues['Definition_L'],
                   'Definition_remote',  FieldValues['Definition_R']

                   {


                    }
                  ]);

          iID:=t_Data_Compare['id'];

          s:=FieldValues[FLD_schema] +'.'+  FieldValues['name_local'];

     //     StrToFile_ ('e:\left.sql',  FieldValues['Definition_L'] );
      //    StrToFile_ ('e:\right.sql', FieldValues['Definition_R'] );

        end;



//        procedure StrToFile_(aFileName: string; aStr: string);


//        if sType='u' then
//          FTableDict.Add(s, iID);


        Next;
      end;


//    case i of
//      0:  AddObject(ottaBLE,       '1. �������');
//      1:  AddObject(otUDT,         '2. UDT');
//      2:  AddObject(otFK,          '3. ������� �����');
//      3:  AddObject(otStoredFunc,  '4. �������� �������');
//      4:  AddObject(otView,        '5. View');
////      5:  UpdateAllViews();
////      6:  AddObject(otStoredProc,  '6. �������� ���������');
////      7:  AddObject(otTRIGGER,     '7. Trigger');
////
//    end;

  end;


//  ADOTable1.Connection := dmMDB.ADOConnection_MDB;
//  ADOQuery1.Connection := dmMDB.ADOConnection_MDB;


end;

//-----------------------------------------------------------
procedure TdmCompareAndModify_v1.Execute_Table_columns;
//-----------------------------------------------------------
//.const
//  TBL_compare ='_Compare_v1';


var
  b: Boolean;
  i: Integer;
  iID: Integer;
  iParent: Integer;
  s: string;
  sTable: string;
  sTemplate: string;
//  s,sTemplate: string;
  sTemplate_mdb: string;
//const
//  DEF_STR : array [0..3] of string = ('u','view','sp','fn');
//  DEF_STR : array [0..0] of string = ('u');
//  DEF_STR : array [0..0] of string = ('sp');

begin
  //  db_SetComponentADOConnection(Self, dmMain_SQL.ADOConnection1);

//  db_SetComponentsADOConn([t_Data_Compare],      dmMDB.ADOConnection_MDB);


//..  t_Data_Compare.TableName:=TBL_compare;
//  t_Data_Compare.Open;          t_Compare_v1_table_columns

  dmMDB.ADOConnection_MDB.Execute('delete from '+ TBL_Compare_v1_table_columns);

  dmMDB.OpenTable(ADOTable1, TBL_Compare_v1_table_columns);

//  db_OpenQuery(ADOQuery1, 'delete from '+ TBL_compare);

//  t_Data_Compare.Open;

//  sTemplate:=GetScriptFileByName ('_insert_mdb');
  sTemplate_mdb:=GetScriptFileByName ('_mdb_column');


  dmMDB.OpenQuery(q_Tables, Format('select * from %s where xtype=''u''', [TBL_Compare_v1]));

//  dmMDB.OpenQuery(q_Tables, sTemplate_mdb);

  {
  db_OpenQuery(ADOQuery1, SQL);

  db_View (ADOQuery1);


  with ADOQuery1 do
    while not EOF do
  begin

 //   Progress2(RecNo, RecordCount);
  //  if FTerminated then
   //   Break;

     Next;
   end;
   }
//
//
//  for i := 0 to High(DEF_STR) do
//  begin
//
//    s:=ReplaceStr(sTemplate_mdb, ':group_name', InttoStr(i+1) +'-'+ DEF_STR[i] );
//    s:=ReplaceStr(s, ':xtype', DEF_STR[i]);
//
//
//    TFile.WriteAllText(format( 'e:\%s.sql',[DEF_STR[i]]) , s);

//    dmMDB.ADOConnection_MDB.Execute_objects(s);


    dmMDB.OpenQuery(ADOQuery1, sTemplate_mdb);

//db_View (ADOQuery1);

    with ADOQuery1 do
      while not EOF do
      begin
        sTable:=FieldValues['TableName'];

        if q_Tables.Locate('name_local',sTable,[]) then
        begin
          iParent:=q_Tables['id'];

          db_AddRecord_ (ADOTable1,
                  [
               //    'group_name',         InttoStr(i+1) +'-'+ DEF_STR[i],
                //   FLD_xtype,            FieldValues[FLD_xtype],
                 //  FLD_schema,           FieldValues[FLD_schema],

                   'parent_id',          iParent,
                   'Table_Name',         FieldValues['TableName'],

                   'name_local',         FieldValues['name_local'],
                   'name_remote',        FieldValues['name_remote'],

                   'local_column_id',    FieldValues['local_id'],
                   'remote_column_id',   FieldValues['remote_id'],


                   'SQL_create',         FieldValues['SQL_create_local'],
              //     'SQL_drop',           FieldValues['SQL_drop_'],

                   'Definition_local',   FieldValues['SQL_create_local'],
                   'Definition_remote',  FieldValues['SQL_create_remote']

                   {


                    }
                  ]);
        end;


//insert into  _compare_v1 (group_name, xtype,  schema, Definition_local,  Definition_remote,  name_local, name_remote, SQL_create, SQL_drop)
//
//select '1-view', xtype, schema,  Definition_L,  Definition_R, name_local, name_remote,  SQL_create, SQL_drop
//from
//        s:=FieldValues['SQL_create_'];
//        s:=FieldValues['SQL_drop_'];


     //   iID:=t_Data_Compare['id'];

        Next;
      end;


//    case i of
//      0:  AddObject(ottaBLE,       '1. �������');
//      1:  AddObject(otUDT,         '2. UDT');
//      2:  AddObject(otFK,          '3. ������� �����');
//      3:  AddObject(otStoredFunc,  '4. �������� �������');
//      4:  AddObject(otView,        '5. View');
////      5:  UpdateAllViews();
////      6:  AddObject(otStoredProc,  '6. �������� ���������');
////      7:  AddObject(otTRIGGER,     '7. Trigger');
////
//    end;

//  end;


//  ADOTable1.Connection := dmMDB.ADOConnection_MDB;
//  ADOQuery1.Connection := dmMDB.ADOConnection_MDB;


end;



end.



{

 db_AddRecord_ (t_Data_Compare,
          [
           FLD_NAME_MDB,         aNameMDB,
           FLD_NAME_SQL,         aNameSQL,

           FLD_TYPE,             ObjectTypeToStr(aObjectType),
           FLD_ACTION,           aAction,

           FLD_Update_SQL,       aUpdate_SQL,

           FLD_ID,               FTableID,
           FLD_PARENT_ID,        aTreeParentID

          ]);
end;

  with qry_Temp do
    while not EOF do
  begin
    Progress2(RecNo, RecordCount);
    if FTerminated then
      Break;

